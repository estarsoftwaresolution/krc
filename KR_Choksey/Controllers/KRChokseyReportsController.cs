﻿using KR_Choksey.Areas.Admin.Models;
using KR_Choksey.Global;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KR_Choksey.Controllers
{
    public class KRChokseyReportsController : Controller
    {
        // GET: KRChokseyReports
        public ActionResult LatestReports()
        {
            System.Web.HttpContext.Current.Session["ReportUrl"] = Request.Url.AbsoluteUri.ToString();
            return View();
        }

        public ActionResult LatestPMSReport()
        {
            List<ResearchRptUpload> objRptList = new List<ResearchRptUpload>();
            objRptList = GetAllPMSReport();
            return PartialView(objRptList);
        }

        public ActionResult LatestResearchReport()
        {
            List<ResearchRptUpload> objRptList = new List<ResearchRptUpload>();
            objRptList = GetAllResearchReport();
            return PartialView(objRptList);
        }


        private List<ResearchRptUpload> GetAllPMSReport()
        {
            List<ResearchRptUpload> objRptList = new List<ResearchRptUpload>();
            try
            {
                int i = 0;
                gConnection.Open();
                string sqlstr = "select AutoId, ReportName, ReportPath, ReportType, CreatedOn, OriginalRptName from dbo.ResearchRptUpload where PMS_Research='PMS report' Order By CreatedOn Desc";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    ResearchRptUpload objRpt = new ResearchRptUpload();
                    objRpt.ReportName = sdr["ReportName"].ToString();
                    objRpt.ReportPath = sdr["ReportPath"].ToString();
                    objRpt.OriginalRptName = sdr["OriginalRptName"].ToString();
                    objRpt.AutoId = i + 1;
                    objRpt.ReportType = sdr["ReportType"].ToString();
                    objRpt.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objRptList.Add(objRpt);
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }

            return objRptList;
        }


        private List<ResearchRptUpload> GetAllResearchReport()
        {
            List<ResearchRptUpload> objRptList = new List<ResearchRptUpload>();
            try
            {
                int i=0;
                gConnection.Open();
                string sqlstr = "select AutoId, ReportName, ReportPath, ReportType, CreatedOn, OriginalRptName from dbo.ResearchRptUpload where PMS_Research='Research report' Order By CreatedOn Desc";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    ResearchRptUpload objRpt = new ResearchRptUpload();
                    objRpt.ReportName = sdr["ReportName"].ToString();
                    objRpt.ReportPath = sdr["ReportPath"].ToString();
                    objRpt.OriginalRptName = sdr["OriginalRptName"].ToString();
                    objRpt.AutoId = i+1;
                    objRpt.ReportType = sdr["ReportType"].ToString();
                    objRpt.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objRptList.Add(objRpt);
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }

            return objRptList;
        }



    }
}