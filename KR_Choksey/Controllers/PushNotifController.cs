﻿using KR_Choksey.Global;
using PushNotif_WindowService;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace KR_Choksey.Controllers
{
    public class PushNotifController : Controller
    {
        // GET: PushNotif
        public ActionResult SndNotification()
        {
            SendData();
            return View();
        }

        private void SendData()
        {
            List<UserNotify> objUserLst = new List<UserNotify>();
            objUserLst = GetAllId();
            string dataBSE = GEtBSEOnlinePriceChange();
            foreach (var data in objUserLst)
            {


                var applicationID = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";


                var SENDER_ID = "XXXXXXXXXXXXXXXXXXXXX";
                var value = "Hello";
                WebRequest tRequest;
                tRequest = WebRequest.Create("https://android.googleapis.com/gcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));

                string postData = "collapse_key=score_update&time_to_live=108&delay_while_idle=1&data.message=" + value + dataBSE + "&registration_id=" + data.GoogleId + "";
                Console.WriteLine(postData);
                Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                tRequest.ContentLength = byteArray.Length;

                Stream dataStream = tRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                WebResponse tResponse = tRequest.GetResponse();

                dataStream = tResponse.GetResponseStream();

                StreamReader tReader = new StreamReader(dataStream);

                String sResponseFromServer = tReader.ReadToEnd();

                //Label1.Text = sResponseFromServer;
                tReader.Close();
                dataStream.Close();
                tResponse.Close();
            }
        }

        private List<UserNotify> GetAllId()
        {
            List<UserNotify> objUserLst = new List<UserNotify>();
            try
            {
                string sqlstr = "select GoogleId,created_on from UserNotify";
                gConnection.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    UserNotify objUser = new UserNotify();
                    objUser.GoogleId = sdr["GoogleId"].ToString();
                    objUserLst.Add(objUser);
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return objUserLst;
        }


        private string GEtBSEOnlinePriceChange()
        {
            string data = "";
            try
            {
                gConnection.Open();
                string sqlstr = "select lname,currtime,currprice from [dbo].[BSEOnlinePrice] where AutoId in (select Max(AutoId) from [BSEOnlinePrice])";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    data = data + sdr["lname"].ToString();
                    data = data + " ";
                    data = data + sdr["currprice"].ToString();
                    data = data + " ";
                    data = data + sdr["currtime"].ToString();
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return data;
        }


    }
}