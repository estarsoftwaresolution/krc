﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KR_Choksey.Global;
using KR_Choksey.Models;
using System.Data.SqlClient;
using Global;

namespace KR_Choksey.Controllers
{
    public class CMOTController : Controller
    {
        //[CustomActionFilter]
        public PartialViewResult EQUITY()
        {
            return PartialView();
        }

        
        //[CustomActionFilter]
        public PartialViewResult NSE_GAINERS()
        {
            List<NSENetPriceGainersData> NSE_Gainer_List = new List<NSENetPriceGainersData>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                //sqlstr = "SELECT co_name,close_price,Prevclose,Netchg,Perchg FROM NSENetPriceGainersData where autoid in (select TOP 5 autoid from NSENetPriceGainersData where cast(created_on as date) = cast(GETDATE() as date) order by perchg DESC) ORDER BY perchg DESC, created_on DESC";
                sqlstr = "SELECT top 5 co_name,close_price,Prevclose,Netchg,Perchg FROM NSENetPriceGainersData ";
                sqlstr = sqlstr + " where autoid in (select TOP 10 autoid from NSENetPriceGainersData order by created_on DESC) order by perchg DESC ";
                //sqlstr = sqlstr + " ORDER BY perchg DESC";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    NSENetPriceGainersData NSE_Gainer = new NSENetPriceGainersData();
                    NSE_Gainer.co_name = sdr["co_name"].ToString();
                    NSE_Gainer.close_price = sdr["close_price"].ToString();
                    NSE_Gainer.Prevclose = sdr["Prevclose"].ToString();
                    NSE_Gainer.Netchg = sdr["Netchg"].ToString();
                    NSE_Gainer.Perchg = sdr["Perchg"].ToString();
                    NSE_Gainer_List.Add(NSE_Gainer);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {                
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(NSE_Gainer_List);
        }

        
        //[CustomActionFilter]
        public PartialViewResult BSE_GAINERS()
        {
            List<BSENetPriceGainersData> BSE_Gainer_List = new List<BSENetPriceGainersData>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                //sqlstr = "SELECT co_name,close_price,Prevclose,Netchg,Perchg FROM BSENetPriceGainersData where autoid in (select TOP 5 autoid from BSENetPriceGainersData where cast(created_on as date) = cast(GETDATE() as date) order by perchg DESC) ORDER BY perchg DESC, created_on DESC";
                sqlstr = "SELECT co_name,close_price,Prevclose,Netchg,Perchg FROM BSENetPriceGainersData where autoid in (select TOP 5 autoid from BSENetPriceGainersData order by created_on DESC, perchg DESC) ORDER BY perchg DESC ";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    BSENetPriceGainersData BSE_Gainer = new BSENetPriceGainersData();
                    BSE_Gainer.co_name = sdr["co_name"].ToString();
                    BSE_Gainer.close_price = sdr["close_price"].ToString();
                    BSE_Gainer.Prevclose = sdr["Prevclose"].ToString();
                    BSE_Gainer.Netchg = sdr["Netchg"].ToString();
                    BSE_Gainer.Perchg = sdr["Perchg"].ToString();
                    BSE_Gainer_List.Add(BSE_Gainer);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(BSE_Gainer_List);
        }

        //[CustomActionFilter]
        public PartialViewResult NSE_LOSERS()
        {
            List<NSENetPriceLosersData> NSE_Loser_List = new List<NSENetPriceLosersData>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                //sqlstr = "SELECT co_name,close_price,Prevclose,Netchg,Perchg FROM NSENetPriceLosersData where autoid in (select TOP 5 autoid from NSENetPriceLosersData where cast(created_on as date) = cast(GETDATE() as date) order by perchg DESC) ORDER BY perchg DESC, created_on DESC";
                sqlstr = "SELECT top 5 co_name,close_price,Prevclose,Netchg,Perchg FROM NSENetPriceLosersData where ";
                sqlstr = sqlstr + " autoid in (select TOP 10 autoid from NSENetPriceLosersData order by created_on Desc) ";
                sqlstr = sqlstr + " ORDER BY perchg Desc ";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    NSENetPriceLosersData NSE_Loser = new NSENetPriceLosersData();
                    NSE_Loser.co_name = sdr["co_name"].ToString();
                    NSE_Loser.close_price = sdr["close_price"].ToString();
                    NSE_Loser.Prevclose = sdr["Prevclose"].ToString();
                    NSE_Loser.Netchg = sdr["Netchg"].ToString();
                    NSE_Loser.Perchg = sdr["Perchg"].ToString();
                    NSE_Loser_List.Add(NSE_Loser);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(NSE_Loser_List);
        }

        //[CustomActionFilter]
        public PartialViewResult BSE_LOSERS()
        {
            List<BSENetPriceLosersData> BSE_Loser_List = new List<BSENetPriceLosersData>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                //sqlstr = "SELECT co_name,close_price,Prevclose,Netchg,Perchg FROM BSENetPriceLosersData where autoid in (select TOP 5 autoid from BSENetPriceLosersData where cast(created_on as date) = cast(GETDATE() as date) order by perchg DESC) ORDER BY perchg DESC, created_on DESC";
                sqlstr = "SELECT top 5 co_name,close_price,Prevclose,Netchg,Perchg FROM BSENetPriceLosersData where ";
                sqlstr = sqlstr + " autoid in (select TOP 10 autoid from BSENetPriceLosersData order by created_on desc) ";
                sqlstr = sqlstr + " ORDER BY perchg DESC ";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    BSENetPriceLosersData BSE_Loser = new BSENetPriceLosersData();
                    BSE_Loser.co_name = sdr["co_name"].ToString();
                    BSE_Loser.close_price = sdr["close_price"].ToString();
                    BSE_Loser.Prevclose = sdr["Prevclose"].ToString();
                    BSE_Loser.Netchg = sdr["Netchg"].ToString();
                    BSE_Loser.Perchg = sdr["Perchg"].ToString();
                    BSE_Loser_List.Add(BSE_Loser);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(BSE_Loser_List);
        }

        //[CustomActionFilter]
        public PartialViewResult NSE_Movers()
        {
            List<NSEIndexMovers> NSE_Movers_List = new List<NSEIndexMovers>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                //sqlstr = "SELECT comp_name,Contribution FROM NSEIndexMovers where autoid in (select TOP 5 autoid from NSEIndexMovers where cast(created_on as date) = cast(GETDATE() as date) order by Contribution DESC) ORDER BY Contribution DESC, created_on DESC";
                sqlstr = "SELECT top 5 comp_name,Contribution FROM NSEIndexMovers where autoid in (select TOP 10 autoid from ";
                sqlstr = sqlstr + " NSEIndexMovers  order by created_on desc ) ORDER BY Contribution DESC, created_on DESC";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    NSEIndexMovers NSE_Movers = new NSEIndexMovers();
                    NSE_Movers.comp_name = sdr["comp_name"].ToString();
                    NSE_Movers.Contribution = Math.Round(Convert.ToDecimal(sdr["Contribution"].ToString()), 2).ToString();
                    NSE_Movers_List.Add(NSE_Movers);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(NSE_Movers_List);
        }

        //[CustomActionFilter]
        public PartialViewResult BSE_Movers()
        {
            List<BSEIndexMovers> BSE_Movers_List = new List<BSEIndexMovers>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                //sqlstr = "SELECT comp_name,Contribution FROM BSEIndexMovers where autoid in (select TOP 5 autoid from BSEIndexMovers where cast(created_on as date) = cast(GETDATE() as date) order by Contribution DESC) ORDER BY Contribution DESC, created_on DESC";
                sqlstr = "SELECT top 5 comp_name,Contribution FROM BSEIndexMovers where autoid in (select TOP 10 autoid ";
                sqlstr = sqlstr + " from BSEIndexMovers order by created_on DESC) ORDER BY Contribution DESC ";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    BSEIndexMovers BSE_Movers = new BSEIndexMovers();
                    BSE_Movers.comp_name = sdr["comp_name"].ToString();
                    BSE_Movers.Contribution = Math.Round(Convert.ToDecimal(sdr["Contribution"].ToString()), 2).ToString();
                    BSE_Movers_List.Add(BSE_Movers);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(BSE_Movers_List);
        }

        //[CustomActionFilter]
        public PartialViewResult Derivatives()
        {
            return PartialView();
        }

        //[CustomActionFilter]
        public PartialViewResult FUTURES_GAINERS()
        {
            List<CmotsDerivativeFuturesTopGainers_Nifty> Future_Gainer_List = new List<CmotsDerivativeFuturesTopGainers_Nifty>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                //sqlstr = "SELECT TOP 5 instname,Symbol,expdate,Ltp,Faodiff FROM CmotsDerivativeFuturesTopGainers_Nifty where cast(created_on as date) = cast(GETDATE() as date) order by Faodiff DESC";
                sqlstr = "SELECT TOP 5 instname,Symbol,expdate,Ltp,faochange as Faodiff FROM CmotsDerivativeFuturesTopGainers_Nifty where cast(created_on as date) = cast(GETDATE() as date) order by faochange DESC";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsDerivativeFuturesTopGainers_Nifty Future_Gainer = new CmotsDerivativeFuturesTopGainers_Nifty();
                    Future_Gainer.instname = sdr["instname"].ToString();
                    Future_Gainer.Symbol = sdr["Symbol"].ToString();
                    Future_Gainer.expdate = sdr["expdate"].ToString();
                    Future_Gainer.Ltp = sdr["Ltp"].ToString();
                    Future_Gainer.Faodiff = Math.Round(Convert.ToDecimal(sdr["Faodiff"].ToString()), 2).ToString();
                    Future_Gainer_List.Add(Future_Gainer);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(Future_Gainer_List);
        }

        //[CustomActionFilter]
        public PartialViewResult OPTIONS_GAINERS()
        {
            List<CmotsDerivativeOptionsTopGainers_Nifty> Option_Gainer_List = new List<CmotsDerivativeOptionsTopGainers_Nifty>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                //sqlstr = "SELECT TOP 5 instname,Symbol,expdate,Ltp,Faodiff FROM CmotsDerivativeOptionsTopGainers_Nifty where cast(created_on as date) = cast(GETDATE() as date) order by Faodiff DESC";
                sqlstr = "SELECT TOP 5 instname,Symbol,expdate,Ltp,faochange as Faodiff FROM CmotsDerivativeOptionsTopGainers_Nifty where cast(created_on as date) = cast(GETDATE() as date) order by faochange DESC";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsDerivativeOptionsTopGainers_Nifty Option_Gainer = new CmotsDerivativeOptionsTopGainers_Nifty();
                    Option_Gainer.instname = sdr["instname"].ToString();
                    Option_Gainer.Symbol = sdr["Symbol"].ToString();
                    Option_Gainer.expdate = sdr["expdate"].ToString();
                    Option_Gainer.Ltp = sdr["Ltp"].ToString();
                    Option_Gainer.Faodiff = Math.Round(Convert.ToDecimal(sdr["Faodiff"].ToString()), 2).ToString();
                    Option_Gainer_List.Add(Option_Gainer);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(Option_Gainer_List);
        }

        //[CustomActionFilter]
        public PartialViewResult FUTURES_LOSERS()
        {
            List<CmotsDerivativeFuturesTopLosers_Nifty> Future_Loser_List = new List<CmotsDerivativeFuturesTopLosers_Nifty>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                //sqlstr = "SELECT TOP 5 instname,Symbol,expdate,Ltp,Faodiff FROM CmotsDerivativeFuturesTopLosers_Nifty where cast(created_on as date) = cast(GETDATE() as date) order by Faodiff DESC";
                sqlstr = "SELECT TOP 5 instname,Symbol,expdate,Ltp,faochange as Faodiff FROM CmotsDerivativeFuturesTopLosers_Nifty where cast(created_on as date) = cast(GETDATE() as date) order by faochange DESC";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsDerivativeFuturesTopLosers_Nifty Future_Loser = new CmotsDerivativeFuturesTopLosers_Nifty();
                    Future_Loser.instname = sdr["instname"].ToString();
                    Future_Loser.Symbol = sdr["Symbol"].ToString();
                    Future_Loser.expdate = sdr["expdate"].ToString();
                    Future_Loser.Ltp = sdr["Ltp"].ToString();
                    Future_Loser.Faodiff = Math.Round(Convert.ToDecimal(sdr["Faodiff"].ToString()), 2).ToString();
                    Future_Loser_List.Add(Future_Loser);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(Future_Loser_List);
        }

        //[CustomActionFilter]
        public PartialViewResult OPTIONS_LOSERS()
        {
            List<CmotsDerivativeOptionsTopLosers_Nifty> Option_Loser_List = new List<CmotsDerivativeOptionsTopLosers_Nifty>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                //sqlstr = "SELECT TOP 5 instname,Symbol,expdate,Ltp,Faodiff FROM CmotsDerivativeOptionsTopLosers_Nifty where cast(created_on as date) = cast(GETDATE() as date) order by Faodiff DESC";
                sqlstr = "SELECT TOP 5 instname,Symbol,expdate,Ltp,faochange as Faodiff FROM CmotsDerivativeOptionsTopLosers_Nifty where cast(created_on as date) = cast(GETDATE() as date) order by faochange DESC";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsDerivativeOptionsTopLosers_Nifty Option_Loser = new CmotsDerivativeOptionsTopLosers_Nifty();
                    Option_Loser.instname = sdr["instname"].ToString();
                    Option_Loser.Symbol = sdr["Symbol"].ToString();
                    Option_Loser.expdate = sdr["expdate"].ToString();
                    Option_Loser.Ltp = sdr["Ltp"].ToString();
                    Option_Loser.Faodiff = Math.Round(Convert.ToDecimal(sdr["Faodiff"].ToString()), 2).ToString();
                    Option_Loser_List.Add(Option_Loser);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(Option_Loser_List);
        }

        //[CustomActionFilter]
        public PartialViewResult Mutual_Fund()
        {
            return PartialView();
        }

        //[CustomActionFilter]
        public PartialViewResult Performance_Equity()
        {
            List<CmotsMFTopPerform_OneYearEquity> Performance_Equity_List = new List<CmotsMFTopPerform_OneYearEquity>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "SELECT TOP 5 Sch_name,oneyear from CmotsMFTopPerform_OneYearEquity where cast(created_on as date) = cast(GETDATE() as date) ORDER BY oneyear DESC";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsMFTopPerform_OneYearEquity Equity_List = new CmotsMFTopPerform_OneYearEquity();
                    Equity_List.Sch_Name = sdr["Sch_Name"].ToString();
                    Equity_List.oneyear = Math.Round(Convert.ToDecimal(sdr["oneyear"].ToString()), 2).ToString();
                    Performance_Equity_List.Add(Equity_List);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(Performance_Equity_List);
        }

        //[CustomActionFilter]
        public PartialViewResult Performance_DEBT()
        {
            List<CmotsMFTopPerform_OneYearDebt> Performance_DEBT_List = new List<CmotsMFTopPerform_OneYearDebt>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "SELECT TOP 5 Sch_name,oneyear from CmotsMFTopPerform_OneYearDebt where cast(created_on as date) = cast(GETDATE() as date) ORDER BY oneyear DESC";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsMFTopPerform_OneYearDebt DEBT_List = new CmotsMFTopPerform_OneYearDebt();
                    DEBT_List.Sch_Name = sdr["Sch_Name"].ToString();
                    DEBT_List.oneyear = Math.Round(Convert.ToDecimal(sdr["oneyear"].ToString()), 2).ToString();
                    Performance_DEBT_List.Add(DEBT_List);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(Performance_DEBT_List);
        }


        //[CustomActionFilter]
        public PartialViewResult Performance_Hybrid()
        {
            List<CmotsMFTopPerform_OneYearHybrid> Performance_Hybrid_List = new List<CmotsMFTopPerform_OneYearHybrid>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "SELECT TOP 5 Sch_name,oneyear from CmotsMFTopPerform_OneYearHybrid where cast(created_on as date) = cast(GETDATE() as date) ORDER BY oneyear DESC";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsMFTopPerform_OneYearHybrid Hybrid_List = new CmotsMFTopPerform_OneYearHybrid();
                    Hybrid_List.Sch_Name = sdr["Sch_Name"].ToString();
                    Hybrid_List.oneyear = Math.Round(Convert.ToDecimal(sdr["oneyear"].ToString()), 2).ToString();
                    Performance_Hybrid_List.Add(Hybrid_List);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(Performance_Hybrid_List);
        }

        //[CustomActionFilter]
        public PartialViewResult Mutual_NFO()
        {
            List<CmotsMFNewFundOffer> Mutual_NFO = new List<CmotsMFNewFundOffer>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "select TOP 5 Sch_Name,Launc_Date,CLDATE from CmotsMFNewFundOffer where cast(created_on as date) = cast(GETDATE() as date) ORDER BY Launc_Date DESC";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsMFNewFundOffer NFO_List = new CmotsMFNewFundOffer();
                    NFO_List.Sch_Name = sdr["Sch_Name"].ToString();
                    NFO_List.Launc_Date = sdr["Launc_Date"].ToString();
                    NFO_List.CLDATE = sdr["CLDATE"].ToString();
                    Mutual_NFO.Add(NFO_List);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(Mutual_NFO);
        }

        //[CustomActionFilter]
        public PartialViewResult Mutual_MF_News()
        {
            List<CmotsMFNews> Mutual_MF_News = new List<CmotsMFNews>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "SELECT TOP 5 Heading,Caption,Arttext from CmotsMFNews where cast(created_on as date) = cast(GETDATE() as date) ORDER BY Date DESC,Time DESC";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsMFNews MF_News_List = new CmotsMFNews();
                    MF_News_List.Heading = sdr["Heading"].ToString();
                    MF_News_List.Caption = sdr["Caption"].ToString();
                    MF_News_List.Arttext = sdr["Arttext"].ToString();
                    Mutual_MF_News.Add(MF_News_List);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(Mutual_MF_News);
        }

        //[CustomActionFilter]
        public PartialViewResult Mutual_Managers()
        {
            List<CmotsMFFundManagerInterview> Mutual_Managers = new List<CmotsMFFundManagerInterview>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "SELECT TOP 5 Heading,Caption,Arttext from CmotsMFFundManagerInterview where cast(created_on as date) = cast(GETDATE() as date) ORDER BY Date DESC,Time DESC";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsMFFundManagerInterview Managers_List = new CmotsMFFundManagerInterview();
                    Managers_List.Heading = sdr["Heading"].ToString();
                    Managers_List.Caption = sdr["Caption"].ToString();
                    Managers_List.Arttext = sdr["Arttext"].ToString();
                    Mutual_Managers.Add(Managers_List);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(Mutual_Managers);
        }

        //[CustomActionFilter]
        public PartialViewResult Commodities()
        {
            return PartialView();
        }

        ////[CustomActionFilter]
        //public PartialViewResult Commodities_Gain_Lose()
        //{
        //    List<CmotsCommoditiesGainersAndLosers> CommoditiesGainersAndLosers = new List<CmotsCommoditiesGainersAndLosers>();
        //    gConnection.Open();
        //    SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
        //    cmd.Connection = GlobalVariables.gConn;
        //    try
        //    {
        //        string sqlstr = "";
        //        sqlstr = "SELECT TOP 5 CommName,ClosePrice,OI  from CmotsCommoditiesGainersAndLosers where cast(created_on as date) = cast(GETDATE() as date) ORDER by Cast (OI AS INT) DESC";
        //        cmd.CommandText = sqlstr;
        //        cmd.Parameters.Clear();
        //        SqlDataReader sdr = cmd.ExecuteReader();
        //        while (sdr.Read())
        //        {
        //            CmotsCommoditiesGainersAndLosers GainersAndLosers = new CmotsCommoditiesGainersAndLosers();
        //            GainersAndLosers.CommName = sdr["CommName"].ToString();
        //            GainersAndLosers.ClosePrice = sdr["ClosePrice"].ToString();
        //            GainersAndLosers.OI = sdr["OI"].ToString();
        //            CommoditiesGainersAndLosers.Add(GainersAndLosers);
        //        }
        //        sdr.Close();
        //        gConnection.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        gConnection.Close();
        //        Global.ErrorHandlerClass.LogError(ex);
        //    }
        //    return PartialView(CommoditiesGainersAndLosers);
        //}

        //[CustomActionFilter]

        public PartialViewResult Commodities_Closed_Price()
        {
            List<CmotsCommoditiesClosingPrices> CommoditiesClosingPrices = new List<CmotsCommoditiesClosingPrices>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "SELECT TOP 5 CommName,ClosePrice,Diff FROM CmotsCommoditiesClosingPrices where cast(created_on as date) = cast(GETDATE() as date) ORDER by Cast (DIff AS INT) DESC";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCommoditiesClosingPrices ClosingPrices = new CmotsCommoditiesClosingPrices();
                    ClosingPrices.CommName = sdr["CommName"].ToString();
                    ClosingPrices.ClosePrice = sdr["ClosePrice"].ToString();
                    ClosingPrices.Diff = sdr["Diff"].ToString();
                    CommoditiesClosingPrices.Add(ClosingPrices);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(CommoditiesClosingPrices);
        }


        public PartialViewResult Commodities_Gain()
        {
            List<CmotsCommoditiesGainers> CommoditiesGainList = new List<CmotsCommoditiesGainers>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "select Top 5 CommName, Cast(Exp_Date as Date) as Exp_Date, ClosePrice as LTP, PrevClose, Round(Diff,2) as Diff, Round(Change,2) as Change From dbo.CmotsCommoditiesGainers";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCommoditiesGainers CommoditiesGain = new CmotsCommoditiesGainers();
                    CommoditiesGain.CommName = sdr["CommName"].ToString();
                    CommoditiesGain.ClosePrice = sdr["LTP"].ToString();
                    CommoditiesGain.Diff = sdr["Diff"].ToString();
                    CommoditiesGain.Exp_Date = sdr["Exp_Date"].ToString();
                    CommoditiesGain.PrevClose = sdr["PrevClose"].ToString();
                    CommoditiesGain.Change = sdr["Change"].ToString();
                    CommoditiesGainList.Add(CommoditiesGain);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(CommoditiesGainList);
        }

        public PartialViewResult Commodities_Lose()
        {
            List<CmotsCommoditiesLosers> CommoditiesGainList = new List<CmotsCommoditiesLosers>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "select Top 5 CommName, Cast(Exp_Date as Date) as Exp_Date, ClosePrice as LTP, PrevClose, Round(Diff,2) as Diff, Round(Change,2) as Change From dbo.CmotsCommoditiesLosers";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCommoditiesLosers CommoditiesGain = new CmotsCommoditiesLosers();
                    CommoditiesGain.CommName = sdr["CommName"].ToString();
                    CommoditiesGain.ClosePrice = sdr["LTP"].ToString();
                    CommoditiesGain.Diff = sdr["Diff"].ToString();
                    CommoditiesGain.Exp_Date = sdr["Exp_Date"].ToString();
                    CommoditiesGain.PrevClose = sdr["PrevClose"].ToString();
                    CommoditiesGain.Change = sdr["Change"].ToString();
                    CommoditiesGainList.Add(CommoditiesGain);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(CommoditiesGainList);
        }



        //[CustomActionFilter]
        public PartialViewResult Capital_Market()
        {
            return PartialView();
        }        

        //[CustomActionFilter]
        public PartialViewResult Capital_Market_Hot_Pursuit()
        {
            List<CmotsHotPursuit_stockAction> HotPursuit_stockAction = new List<CmotsHotPursuit_stockAction>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "SELECT TOP 5 heading,caption,arttext FROM CmotsHotPursuit_stockAction where cast(created_on as date) = cast(GETDATE() as date)";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsHotPursuit_stockAction stockAction = new CmotsHotPursuit_stockAction();
                    stockAction.heading = sdr["heading"].ToString();
                    stockAction.caption = sdr["caption"].ToString();
                    stockAction.arttext = sdr["arttext"].ToString();
                    HotPursuit_stockAction.Add(stockAction);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(HotPursuit_stockAction);
        }
        //[CustomActionFilter]
        public PartialViewResult Capital_Market_Corporate_News()
        {
            List<CmotsCorporateNews> CmotsCorporateNews = new List<CmotsCorporateNews>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "SELECT TOP 5 heading,caption,arttext FROM CmotsCorporateNews where cast(created_on as date) = cast(GETDATE() as date)";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCorporateNews CmotsCorporateNews_List = new CmotsCorporateNews();
                    CmotsCorporateNews_List.heading = sdr["heading"].ToString();
                    CmotsCorporateNews_List.caption = sdr["caption"].ToString();
                    CmotsCorporateNews_List.arttext = sdr["arttext"].ToString();
                    CmotsCorporateNews.Add(CmotsCorporateNews_List);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(CmotsCorporateNews);
        }

        //[CustomActionFilter]
        public PartialViewResult Capital_Market_Economy()
        {
            List<CmotsEconomyNews> EconomyNews = new List<CmotsEconomyNews>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "SELECT TOP 5 heading,arttext FROM CmotsEconomyNews where cast(created_on as date) = cast(GETDATE() as date)";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsEconomyNews EconomyNews_List = new CmotsEconomyNews();
                    EconomyNews_List.heading = sdr["heading"].ToString();
                    EconomyNews_List.arttext = sdr["arttext"].ToString();
                    EconomyNews.Add(EconomyNews_List);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(EconomyNews);
        }
        //[CustomActionFilter]
        public PartialViewResult Capital_Market_Stock_Alert()
        {
            List<CmotsStockAlert> StockAlert = new List<CmotsStockAlert>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "SELECT TOP 5 heading,arttext FROM CmotsStockAlert where cast(created_on as date) = cast(GETDATE() as date)";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsStockAlert StockAlert_List = new CmotsStockAlert();
                    StockAlert_List.heading = sdr["heading"].ToString();
                    StockAlert_List.arttext = sdr["arttext"].ToString();
                    StockAlert.Add(StockAlert_List);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(StockAlert);
        }

        //[CustomActionFilter]
        public PartialViewResult Home_Ticker()
        {            
            return PartialView();
        }

        //[CustomActionFilter]
        public PartialViewResult NSE_Ticker()
        {
            string data = "";
            try
            {
                string sqlstr = "select co_name,co_code, price,change from dbo.CmotsNSETicker order by co_name";
                gConnection.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Math.Round(Convert.ToDecimal(sdr["change"].ToString()), 2).ToString();
                    data = data + "<a style='color:#000;font-weight: bold;' href='https://www.XXXXXXX.XXX/Company/Company_Details?id=" + sdr["co_code"].ToString() + "'>";
                    data = data + sdr["co_name"].ToString();
                    data = data + "</a>";
                    data = data + " : ";                    
                    data = data + " ";
                    if (Math.Round(Convert.ToDecimal(sdr["change"].ToString()), 2).ToString().Contains('-'))
                    {
                        data = data + "<span style='font-weight: bold;'>";                        
                        data = data + sdr["price"].ToString();
                        data = data + " [";
                        data = data + Math.Round(Convert.ToDecimal(sdr["change"].ToString()), 2).ToString();
                        data = data + "%] ";
                        data = data + "</span>";
                    }
                    else
                    {
                        data = data + "<strong>";
                        data = data + sdr["price"].ToString();
                        data = data + " [";
                        data = data + Math.Round(Convert.ToDecimal(sdr["change"].ToString()), 2).ToString();
                        data = data + "%] ";
                        data = data + "</strong>";
                    }
                    data = data + " &nbsp&nbsp|&nbsp&nbsp ";
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {

            }
            ViewBag.Data = data;
            return PartialView();
        }
        //[CustomActionFilter]
        public PartialViewResult BSE_Ticker()
        {
            string data = "";
            try
            {
                string sqlstr = "select co_name,co_code, price,change from dbo.CmotsBSETicker order by co_name";
                gConnection.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    data = data + "<a style='color:#000;font-weight: bold;' href='https://www.XXXXXXX.XXX/Company/Company_Details?id=" + sdr["co_code"].ToString() + "'>";
                    data = data + sdr["co_name"].ToString();
                    data = data + "</a>";
                    data = data + " : ";
                    data = data + " ";
                    if (Math.Round(Convert.ToDecimal(sdr["change"].ToString()), 2).ToString().Contains('-'))
                    {
                        data = data + "<span style='font-weight: bold;'>";
                        data = data + sdr["price"].ToString();
                        data = data + " [";
                        data = data + Math.Round(Convert.ToDecimal(sdr["change"].ToString()), 2).ToString();
                        data = data + "%] ";
                        data = data + "</span>";
                    }
                    else
                    {
                        data = data + "<strong>";
                        data = data + sdr["price"].ToString();
                        data = data + " [";
                        data = data + Math.Round(Convert.ToDecimal(sdr["change"].ToString()), 2).ToString();
                        data = data + "%] ";
                        data = data + "</strong>";
                    }
                    data = data + " &nbsp&nbsp|&nbsp&nbsp ";
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {

            }
            ViewBag.Data = data;
            return PartialView();
        }





        #region IPO


        //[CustomActionFilter]
        public PartialViewResult IPO()
        {
            return PartialView();
        }


        //[CustomActionFilter]
        public PartialViewResult IPO_NewListing_NSE()
        {
            List<CmotsIPONewListingNSE> objCmotsList = new List<CmotsIPONewListingNSE>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "select Top 5 co_name, cast(listdate as DATE) as listdate, listprice, high, low, [close] from CmotsIPONewListingNSE order by cast(listdate as date) desc";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsIPONewListingNSE objCmots = new CmotsIPONewListingNSE();
                    objCmots.co_name = sdr["co_name"].ToString();
                    objCmots.listdate = sdr["listdate"].ToString();
                    objCmots.listprice = sdr["listprice"].ToString();
                    objCmots.high = sdr["high"].ToString();
                    objCmots.low = sdr["low"].ToString();
                    objCmots.close = sdr["close"].ToString();
                    //objCmots.low = Math.Round(Convert.ToDecimal(sdr["low"].ToString()), 2).ToString();
                    objCmotsList.Add(objCmots);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(objCmotsList);
        }


        //[CustomActionFilter]
        public PartialViewResult IPO_NewListing_BSE()
        {
            List<CmotsIPONewListingBSE> objCmotsList = new List<CmotsIPONewListingBSE>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "select Top 5 co_name, cast(listdate as DATE) as listdate, listprice, high, low, [close] from CmotsIPONewListingBSE order by cast(listdate as date) desc";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsIPONewListingBSE objCmots = new CmotsIPONewListingBSE();
                    objCmots.co_name = sdr["co_name"].ToString();
                    objCmots.listdate = sdr["listdate"].ToString();
                    objCmots.listprice = sdr["listprice"].ToString();
                    objCmots.high = sdr["high"].ToString();
                    objCmots.low = sdr["low"].ToString();
                    objCmots.close = sdr["close"].ToString();
                    //objCmots.low = Math.Round(Convert.ToDecimal(sdr["low"].ToString()), 2).ToString();
                    objCmotsList.Add(objCmots);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(objCmotsList);
        }


        //[CustomActionFilter]
        public PartialViewResult IPO_ClosedIssues()
        {
            List<CmotsIPOClosedIssues> objCmotsList = new List<CmotsIPOClosedIssues>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "select Top 5 lname, issuetype, cast(opendate as date) as opendate, cast(closdate as date) as closdate, issueprice from dbo.CmotsIPOClosedIssues";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsIPOClosedIssues objCmots = new CmotsIPOClosedIssues();
                    objCmots.lname = sdr["lname"].ToString();
                    objCmots.issuetype = sdr["issuetype"].ToString();
                    objCmots.opendate = sdr["opendate"].ToString();
                    objCmots.closdate = sdr["closdate"].ToString();
                    objCmots.issueprice = sdr["issueprice"].ToString();
                    //objCmots.low = Math.Round(Convert.ToDecimal(sdr["low"].ToString()), 2).ToString();
                    objCmotsList.Add(objCmots);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(objCmotsList);
        }


        //[CustomActionFilter]
        public PartialViewResult IPO_OpenIssues()
        {
            List<CmotsIPOOpenIssues> objCmotsList = new List<CmotsIPOOpenIssues>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "select Top 5 lname, issuetype, cast(opendate as date) as opendate, cast(closdate as date) as closdate, issueprice from dbo.CmotsIPOOpenIssues";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsIPOOpenIssues objCmots = new CmotsIPOOpenIssues();
                    objCmots.lname = sdr["lname"].ToString();
                    objCmots.issuetype = sdr["issuetype"].ToString();
                    objCmots.opendate = sdr["opendate"].ToString();
                    objCmots.closdate = sdr["closdate"].ToString();
                    objCmots.issueprice = sdr["issueprice"].ToString();
                    //objCmots.low = Math.Round(Convert.ToDecimal(sdr["low"].ToString()), 2).ToString();
                    objCmotsList.Add(objCmots);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(objCmotsList);
        }



        //[CustomActionFilter]
        public PartialViewResult IPO_ForthcomingIssues()
        {
            List<CmotsIPOForthcomingIssues> objCmotsList = new List<CmotsIPOForthcomingIssues>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "select Top 5 lname, issuetype, cast(opendate as date) as opendate, cast(closdate as DATE) as closdate, issueprice from CmotsIPOForthcomingIssues";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsIPOForthcomingIssues objCmots = new CmotsIPOForthcomingIssues();
                    objCmots.lname = sdr["lname"].ToString();
                    objCmots.issuetype = sdr["issuetype"].ToString();
                    objCmots.opendate = sdr["opendate"].ToString();
                    objCmots.closdate = sdr["closdate"].ToString();
                    objCmots.issueprice = sdr["issueprice"].ToString();
                    //objCmots.low = Math.Round(Convert.ToDecimal(sdr["low"].ToString()), 2).ToString();
                    objCmotsList.Add(objCmots);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(objCmotsList);
        }
        

        
        #endregion

    }
}