﻿using KR_Choksey.Global;
using KR_Choksey.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace KR_Choksey.Controllers
{
    public class LPController : Controller
    {

        #region SIP (Get)

        [HttpGet]
        public ActionResult SIP()
        {
            //System.Web.HttpContext.Current.Session["SIPContRetUrl"] = Convert.ToString(Session["ReturnUrl"]);
            return View();
        }

        #endregion


        #region SIP (Post)

        [HttpPost]
        //public string SIP(string Name, string EmailId, string PhoneNumber, string QuickTopic, string Aadhar_Pan, string PMutualFund, string PInsurance, string PFinancialPlanning, string PIFund, string PIPms, string PIBrokerage, string PILend, string PIHold, string PSubBrokerage)
        public ActionResult SIP(string Name, string EmailId, string PhoneNumber, string QuickTopic, string Aadhar_Pan, string PMutualFund, string PInsurance, string PFinancialPlanning, string PIFund, string PIPms, string PIBrokerage, string PILend, string PIHold, string PSubBrokerage)
        //public ActionResult SIP(KrcSIPContacts objQC)
        {
            GlobalFunction objGFunc = new GlobalFunction();
            string result = "";
            KrcQuickContacts objContact = new KrcQuickContacts();
            string ReturnUrl = Convert.ToString(Session["ReturnUrl"]);
            System.Web.HttpContext.Current.Session["SIPContRetUrl"] = ReturnUrl;

            string EMMutualFund = "";
            string EMInsurance = "";
            string EMFinancialPlanning = "";
            string EMIFund = "";
            string EMIPms = "";
            string EMIBrokerage = "";
            string EMILend = "";
            string EMIHold = "";
            string EMSubBrokerage = "";

            if (!String.IsNullOrEmpty(Request.Form["g-recaptcha-response"]))
            {
                try
                {
                    string Products = "";
                    // Mutual Fund
                    if (PMutualFund == null)
                    {
                        PMutualFund = "";
                    }
                    if (PMutualFund != "")
                    {
                        if (PMutualFund != "false")
                        {
                            Products = Products + PMutualFund;
                            EMMutualFund = "fp@XXXXXXX.XXX";
                        }
                    }

                    // Insurance
                    if (PInsurance == null)
                    {
                        PInsurance = "";
                    }
                    if (PInsurance != "")
                    {
                        if (PInsurance != "false")
                        {
                            Products = Products + PInsurance;
                            EMInsurance = "fp@XXXXXXX.XXX";
                        }
                    }

                    // Financial Planning
                    if (PFinancialPlanning == null)
                    {
                        PFinancialPlanning = "";
                    }
                    if (PFinancialPlanning != "")
                    {
                        if (PFinancialPlanning != "false")
                        {
                            Products = Products + PFinancialPlanning;
                            EMFinancialPlanning = "fp@XXXXXXX.XXX";
                        }
                    }


                    // I Fund
                    if (PIFund == null)
                    {
                        PIFund = "";
                    }
                    if (PIFund != "")
                    {
                        if (PIFund != "false")
                        {
                            Products = Products + PIFund;
                            EMIFund = "ifund@XXXXXXX.XXX";
                        }
                    }


                    // I Pms
                    if (PIPms == null)
                    {
                        PIPms = "";
                    }
                    if (PIPms != "")
                    {
                        if (PIPms != "false")
                        {
                            Products = Products + PIPms;
                            EMIPms = "pmsclient@XXXXXXX.XXX";
                        }
                    }


                    // I Brokerage
                    if (PIBrokerage == null)
                    {
                        PIBrokerage = "";
                    }
                    if (PIBrokerage != "")
                    {
                        if (PIBrokerage != "false")
                        {
                            Products = Products + PIBrokerage;
                            EMIBrokerage = "prahsant.shah@XXXXXXX.XXX";
                        }
                    }


                    // I Lend
                    if (PILend == null)
                    {
                        PILend = "";
                    }
                    if (PILend != "")
                    {
                        if (PILend != "false")
                        {
                            Products = Products + PILend;
                            EMILend = "rahul.pandit@XXXXXXX.XXX";
                        }
                    }


                    // I Hold
                    if (PIHold == null)
                    {
                        PIHold = "";
                    }
                    if (PIHold != "")
                    {
                        if (PIHold != "false")
                        {
                            Products = Products + PIHold;
                            EMIHold = "mona.zaveri@XXXXXXX.XXX";
                        }
                    }


                    // Sub Brokerage
                    if (PSubBrokerage == null)
                    {
                        PSubBrokerage = "";
                    }
                    if (PSubBrokerage != "")
                    {
                        if (PSubBrokerage != "false")
                        {
                            Products = Products + PSubBrokerage;
                            EMSubBrokerage = "npcare@XXXXXXX.XXX";
                        }
                    }

                    if (Aadhar_Pan == null)
                    {
                        Aadhar_Pan = "";
                    }
                    if (QuickTopic == null)
                    {
                        QuickTopic = "";
                    }

                    objContact.Name = Name;
                    objContact.EmailId = EmailId;
                    objContact.PhoneNumber = PhoneNumber;
                    objContact.QuickTopic = QuickTopic;
                    objContact.Aadhar_PAN = Aadhar_Pan;

                    if (objContact.PhoneNumber.Length != 10)
                    {
                        result = "Enter Valid 10 Digit Mobile Number!";
                    }
                    else if (IsValidEmail(objContact.EmailId) == false)
                    {
                        //result = "Enter Valid Email Address!";
                        @ViewBag.HideClass = "alert alert-danger";
                        @ViewBag.Message = "Enter Valid Email Address";
                    }
                    else
                    {

                        if (ModelState.IsValid)
                        {
                            bool res = true;
                            foreach (char c in objContact.PhoneNumber)
                            {
                                if (c < '0' || c > '9')
                                {
                                    res = false;
                                }
                            }

                            if (res == false || (objContact.PhoneNumber.ToString().Length != 10))
                            {
                                @ViewBag.HideClass = "alert alert-danger";
                                @ViewBag.Message = "Please Enter Valid Mobile Number.";
                                result = "Please Enter Valid Mobile Number.";
                            }
                            else
                            {
                                try
                                {
                                    // Insertion Code For QuickContact Table //
                                    gConnection.Open();
                                    string sqlstr = "insert into KrcQuickContacts(Name,EmailId,PhoneNumber,QuickTopic,Screen,CreatedBy,CreatedOn,Products,Aadhar_PAN)";
                                    sqlstr = sqlstr + " values (@QuickContactName,@QuickContactEmail,@QuickContactPhone,@QuickContactMessage,@Screen,@CreatedBy,GetDate(),@Products,@Aadhar_PAN)";
                                    //cmd.CommandText = sqlstr;
                                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@QuickContactName", objContact.Name);
                                    cmd.Parameters.AddWithValue("@QuickContactEmail", objContact.EmailId);
                                    cmd.Parameters.AddWithValue("@QuickContactPhone", objContact.PhoneNumber);
                                    cmd.Parameters.AddWithValue("@QuickContactMessage", objContact.QuickTopic);
                                    cmd.Parameters.AddWithValue("@Screen", ReturnUrl);
                                    cmd.Parameters.AddWithValue("@CreatedBy", "Online");
                                    cmd.Parameters.AddWithValue("@Products", Products);
                                    cmd.Parameters.AddWithValue("@Aadhar_Pan", objContact.Aadhar_PAN);
                                    cmd.ExecuteNonQuery();

                                    gConnection.Close();


                                    //Mail for Quick contact
                                    string FromMail = ConfigurationManager.AppSettings["smtpDisplayId"];
                                    string ToMail = "service@XXXXXXX.XXX";
                                    //string ToMail = "souravkuila65@gmail.com";
                                    //string Subject = "New contact request from " + objContact.Name;
                                    string Subject = "Product Enquiry received from DIMAG promotions - SIP Page";
                                    string Body = "Hi, Team KRChoksey";
                                    Body = Body + Environment.NewLine;

                                    string UserName = ConfigurationManager.AppSettings["smtpUser"];
                                    string Password = ConfigurationManager.AppSettings["smtpPass"];

                                    //Body = Body + "New contact request:";
                                    Body = Body + Environment.NewLine;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Name: " + objContact.Name;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Phone: " + objContact.PhoneNumber;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Email: " + objContact.EmailId;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Aadhar / PAN: " + objContact.Aadhar_PAN;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Message: " + objContact.QuickTopic;
                                    Body = Body + Environment.NewLine;
                                    if (Products == "")
                                    {
                                        Body = Body + "Products: Not Selected";
                                    }
                                    else
                                    {
                                        Body = Body + "Products: " + Products;
                                    }

                                    Body = Body + Environment.NewLine;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Thanks";
                                    Body = Body + Environment.NewLine;
                                    Body = Body + objContact.Name;
                                    string AttachmentPath = "";
                                    //bool resu = objGFunc.SendMail(FromMail, ToMail, Subject, Body, UserName, Password, AttachmentPath);
                                    bool resu = objGFunc.SendMailForQuickContact(FromMail, ToMail, Subject, Body, UserName, Password, AttachmentPath, EMMutualFund, EMInsurance, EMFinancialPlanning, EMIFund, EMIPms, EMIBrokerage, EMILend, EMIHold, EMSubBrokerage);
                                    if (resu == true)
                                    {
                                        InsertMailLog(FromMail, ToMail, "Success SIP");

                                        Subject = "KRChoksey: Request Received: SIP";
                                        //Body = "Dear " + objContact.Name + ",";
                                        //Body += Environment.NewLine;
                                        //Body += Environment.NewLine;
                                        //Body += "Thank you for contacting us. Our executive will get back to you within 24 hours.";
                                        //Body += Environment.NewLine;
                                        //Body += "You can also reach us at service@XXXXXXX.XXX or call us on 022-66965513/5";
                                        //Body += Environment.NewLine;
                                        //Body += Environment.NewLine;
                                        //Body += Environment.NewLine;
                                        //Body += "Thanks";
                                        //Body += Environment.NewLine;
                                        //Body += "Customer Support Team";
                                        //bool ack = objGFunc.SendAcknowledgementMail(FromMail, objContact.EmailId, Subject, Body, UserName, Password, AttachmentPath);

                                        StreamReader reader = new StreamReader(Server.MapPath("~/Global/LPAcknowledgement.html"));
                                        string readFile = reader.ReadToEnd();
                                        string myString = "";
                                        myString = readFile;
                                        myString = myString.Replace("$$Name$$", objContact.Name);

                                        bool ack = objGFunc.SendAcknowledgementMail_New(FromMail, objContact.EmailId, Subject, myString, UserName, Password, AttachmentPath);

                                        if (ack == true)
                                        {
                                            objGFunc.InsertAcknowledgementMailLog(FromMail, objContact.EmailId, "Success", "SIP Page");
                                        }
                                        else
                                        {
                                            objGFunc.InsertAcknowledgementMailLog(FromMail, objContact.EmailId, "Not Success", "SIP Page");
                                        }
                                    }
                                    else
                                    {
                                        InsertMailLog(FromMail, ToMail, "Not Success SIP");
                                    }

                                    ModelState.Clear();
                                    @ViewBag.HideClass = "alert alert-success";
                                    @ViewBag.Message = "Thanks for contacting us! Our team shall get back to you very soon.";
                                    result = "Success";
                                    //return RedirectToAction("SIP");

                                    //System.Web.HttpContext.Current.Session["QuickContUrl"] = Request.Url.AbsoluteUri;
                                }
                                catch (Exception ex)
                                {
                                    //transaction.Rollback();
                                    gConnection.Close();
                                    Global.ErrorHandlerClass.LogError(ex);
                                    @ViewBag.HideClass = "alert alert-danger";
                                    @ViewBag.Message = "Error!";
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Global.ErrorHandlerClass.LogError(ex);
                }
            }
            else
            {
                //result = "Please Check Captcha!";
                @ViewBag.HideClass = "alert alert-danger";
                @ViewBag.Message = "Error! Please Check Captcha.";
            }

            //return result;
            return View();
            //return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion





        #region Mutual Fund (Get)

        [HttpGet]
        public ActionResult MutualFund()
        {
            return View();
        }

        #endregion


        #region Mutual Fund (Post)

        [HttpPost]
        public ActionResult MutualFund(string Name, string EmailId, string PhoneNumber, string QuickTopic, string Aadhar_Pan, string PMutualFund, string PInsurance, string PFinancialPlanning, string PIFund, string PIPms, string PIBrokerage, string PILend, string PIHold, string PSubBrokerage)
        {
            GlobalFunction objGFunc = new GlobalFunction();
            string result = "";
            KrcQuickContacts objContact = new KrcQuickContacts();
            string ReturnUrl = Convert.ToString(Session["ReturnUrl"]);
            System.Web.HttpContext.Current.Session["SIPContRetUrl"] = ReturnUrl;

            string EMMutualFund = "";
            string EMInsurance = "";
            string EMFinancialPlanning = "";
            string EMIFund = "";
            string EMIPms = "";
            string EMIBrokerage = "";
            string EMILend = "";
            string EMIHold = "";
            string EMSubBrokerage = "";
            if (!String.IsNullOrEmpty(Request.Form["g-recaptcha-response"]))
            {
                try
                {
                    string Products = "";
                    // Mutual Fund
                    if (PMutualFund == null)
                    {
                        PMutualFund = "";
                    }
                    if (PMutualFund != "")
                    {
                        if (PMutualFund != "false")
                        {
                            Products = Products + PMutualFund;
                            EMMutualFund = "fp@XXXXXXX.XXX";
                        }
                    }

                    // Insurance
                    if (PInsurance == null)
                    {
                        PInsurance = "";
                    }
                    if (PInsurance != "")
                    {
                        if (PInsurance != "false")
                        {
                            Products = Products + PInsurance;
                            EMInsurance = "fp@XXXXXXX.XXX";
                        }
                    }

                    // Financial Planning
                    if (PFinancialPlanning == null)
                    {
                        PFinancialPlanning = "";
                    }
                    if (PFinancialPlanning != "")
                    {
                        if (PFinancialPlanning != "false")
                        {
                            Products = Products + PFinancialPlanning;
                            EMFinancialPlanning = "fp@XXXXXXX.XXX";
                        }
                    }


                    // I Fund
                    if (PIFund == null)
                    {
                        PIFund = "";
                    }
                    if (PIFund != "")
                    {
                        if (PIFund != "false")
                        {
                            Products = Products + PIFund;
                            EMIFund = "ifund@XXXXXXX.XXX";
                        }
                    }


                    // I Pms
                    if (PIPms == null)
                    {
                        PIPms = "";
                    }
                    if (PIPms != "")
                    {
                        if (PIPms != "false")
                        {
                            Products = Products + PIPms;
                            EMIPms = "pmsclient@XXXXXXX.XXX";
                        }
                    }


                    // I Brokerage
                    if (PIBrokerage == null)
                    {
                        PIBrokerage = "";
                    }
                    if (PIBrokerage != "")
                    {
                        if (PIBrokerage != "false")
                        {
                            Products = Products + PIBrokerage;
                            EMIBrokerage = "prahsant.shah@XXXXXXX.XXX";
                        }
                    }


                    // I Lend
                    if (PILend == null)
                    {
                        PILend = "";
                    }
                    if (PILend != "")
                    {
                        if (PILend != "false")
                        {
                            Products = Products + PILend;
                            EMILend = "rahul.pandit@XXXXXXX.XXX";
                        }
                    }


                    // I Hold
                    if (PIHold == null)
                    {
                        PIHold = "";
                    }
                    if (PIHold != "")
                    {
                        if (PIHold != "false")
                        {
                            Products = Products + PIHold;
                            EMIHold = "mona.zaveri@XXXXXXX.XXX";
                        }
                    }


                    // Sub Brokerage
                    if (PSubBrokerage == null)
                    {
                        PSubBrokerage = "";
                    }
                    if (PSubBrokerage != "")
                    {
                        if (PSubBrokerage != "false")
                        {
                            Products = Products + PSubBrokerage;
                            EMSubBrokerage = "npcare@XXXXXXX.XXX";
                        }
                    }


                    if (Aadhar_Pan == null)
                    {
                        Aadhar_Pan = "";
                    }
                    if (QuickTopic == null)
                    {
                        QuickTopic = "";
                    }
                    objContact.Name = Name;
                    objContact.EmailId = EmailId;
                    objContact.PhoneNumber = PhoneNumber;
                    objContact.QuickTopic = QuickTopic;
                    objContact.Aadhar_PAN = Aadhar_Pan;


                    if (objContact.PhoneNumber.Length != 10)
                    {
                        result = "Enter Valid 10 Digit Mobile Number!";
                    }
                    else if (IsValidEmail(objContact.EmailId) == false)
                    {
                        //result = "Enter Valid Email Address!";
                        @ViewBag.HideClass = "alert alert-danger";
                        @ViewBag.Message = "Enter Valid Email Address";
                    }
                    else
                    {

                        if (ModelState.IsValid)
                        {
                            bool res = true;
                            foreach (char c in objContact.PhoneNumber)
                            {
                                if (c < '0' || c > '9')
                                {
                                    res = false;
                                }
                            }

                            if (res == false || (objContact.PhoneNumber.ToString().Length != 10))
                            {
                                @ViewBag.HideClass = "alert alert-danger";
                                @ViewBag.Message = "Please Enter Valid Mobile Number.";
                                result = "Please Enter Valid Mobile Number.";
                            }
                            else
                            {
                                try
                                {
                                    // Insertion Code For QuickContact Table //
                                    gConnection.Open();
                                    string sqlstr = "insert into KrcQuickContacts(Name,EmailId,PhoneNumber,QuickTopic,Screen,CreatedBy,CreatedOn,Products,Aadhar_PAN)";
                                    sqlstr = sqlstr + " values (@QuickContactName,@QuickContactEmail,@QuickContactPhone,@QuickContactMessage,@Screen,@CreatedBy,GetDate(),@Products,@Aadhar_PAN)";
                                    //cmd.CommandText = sqlstr;
                                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@QuickContactName", objContact.Name);
                                    cmd.Parameters.AddWithValue("@QuickContactEmail", objContact.EmailId);
                                    cmd.Parameters.AddWithValue("@QuickContactPhone", objContact.PhoneNumber);
                                    cmd.Parameters.AddWithValue("@QuickContactMessage", objContact.QuickTopic);
                                    cmd.Parameters.AddWithValue("@Screen", ReturnUrl);
                                    cmd.Parameters.AddWithValue("@CreatedBy", "Online");
                                    cmd.Parameters.AddWithValue("@Products", Products);
                                    cmd.Parameters.AddWithValue("@Aadhar_Pan", objContact.Aadhar_PAN);
                                    cmd.ExecuteNonQuery();

                                    gConnection.Close();


                                    //Mail for Quick contact
                                    string FromMail = ConfigurationManager.AppSettings["smtpDisplayId"];
                                    string ToMail = "service@XXXXXXX.XXX";
                                    //string ToMail = "souravkuila65@gmail.com";
                                    //string Subject = "New contact request from " + objContact.Name;
                                    string Subject = "Product Enquiry received from DIMAG promotions - PMS Page";
                                    string Body = "Hi, Team KRChoksey";
                                    Body = Body + Environment.NewLine;

                                    string UserName = ConfigurationManager.AppSettings["smtpUser"];
                                    string Password = ConfigurationManager.AppSettings["smtpPass"];

                                    //Body = Body + "New contact request:";
                                    Body = Body + Environment.NewLine;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Name: " + objContact.Name;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Phone: " + objContact.PhoneNumber;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Email: " + objContact.EmailId;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Aadhar / PAN: " + objContact.Aadhar_PAN;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Message: " + objContact.QuickTopic;
                                    Body = Body + Environment.NewLine;
                                    if (Products == "")
                                    {
                                        Body = Body + "Products: Not Selected";
                                    }
                                    else
                                    {
                                        Body = Body + "Products: " + Products;
                                    }

                                    Body = Body + Environment.NewLine;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Thanks";
                                    Body = Body + Environment.NewLine;
                                    Body = Body + objContact.Name;
                                    string AttachmentPath = "";
                                    //bool resu = objGFunc.SendMail(FromMail, ToMail, Subject, Body, UserName, Password, AttachmentPath);
                                    bool resu = objGFunc.SendMailForQuickContact(FromMail, ToMail, Subject, Body, UserName, Password, AttachmentPath, EMMutualFund, EMInsurance, EMFinancialPlanning, EMIFund, EMIPms, EMIBrokerage, EMILend, EMIHold, EMSubBrokerage);
                                    if (resu == true)
                                    {
                                        InsertMailLog(FromMail, ToMail, "Success Mutual");

                                        Subject = "KRChoksey: Request Received: Mutual Fund";
                                        //Body = "Dear " + objContact.Name + ",";
                                        //Body += Environment.NewLine;
                                        //Body += Environment.NewLine;
                                        //Body += "Thank you for contacting us. Our executive will get back to you within 24 hours.";
                                        //Body += Environment.NewLine;
                                        //Body += "You can also reach us at service@XXXXXXX.XXX or call us on 022-66965513/5";
                                        //Body += Environment.NewLine;
                                        //Body += Environment.NewLine;
                                        //Body += Environment.NewLine;
                                        //Body += "Thanks";
                                        //Body += Environment.NewLine;
                                        //Body += "Customer Support Team";
                                        //bool ack = objGFunc.SendAcknowledgementMail(FromMail, objContact.EmailId, Subject, Body, UserName, Password, AttachmentPath);

                                        StreamReader reader = new StreamReader(Server.MapPath("~/Global/LPAcknowledgement.html"));
                                        string readFile = reader.ReadToEnd();
                                        string myString = "";
                                        myString = readFile;
                                        myString = myString.Replace("$$Name$$", objContact.Name);

                                        bool ack = objGFunc.SendAcknowledgementMail_New(FromMail, objContact.EmailId, Subject, myString, UserName, Password, AttachmentPath);

                                        if (ack == true)
                                        {
                                            objGFunc.InsertAcknowledgementMailLog(FromMail, objContact.EmailId, "Success", "Mutual Fund");
                                        }
                                        else
                                        {
                                            objGFunc.InsertAcknowledgementMailLog(FromMail, objContact.EmailId, "Not Success", "Mutual Fund");
                                        }
                                    }
                                    else
                                    {
                                        InsertMailLog(FromMail, ToMail, "Not Success Mutual");
                                    }
                                    ModelState.Clear();
                                    @ViewBag.HideClass = "alert alert-success";
                                    @ViewBag.Message = "Thanks for contacting us! Our team shall get back to you very soon.";
                                    result = "Success";
                                    //return RedirectToAction("SIP");

                                    //System.Web.HttpContext.Current.Session["QuickContUrl"] = Request.Url.AbsoluteUri;
                                }
                                catch (Exception ex)
                                {
                                    //transaction.Rollback();
                                    gConnection.Close();
                                    Global.ErrorHandlerClass.LogError(ex);
                                    @ViewBag.HideClass = "alert alert-danger";
                                    @ViewBag.Message = "Error!";
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Global.ErrorHandlerClass.LogError(ex);
                }
            }
            else
            {
                //result = "Error! Please check the capcha.";
                @ViewBag.HideClass = "alert alert-danger";
                @ViewBag.Message = "Error! Please Check Captcha.";
            }
            //return result;
            return View();
        }

        #endregion






        #region PMS (Get)

        [HttpGet]
        public ActionResult PMS()
        {
            //System.Web.HttpContext.Current.Session["SIPContRetUrl"] = Convert.ToString(Session["ReturnUrl"]);
            return View();
        }

        #endregion


        #region PMS (Post)

        [HttpPost]
        //public string SIP(string Name, string EmailId, string PhoneNumber, string QuickTopic, string Aadhar_Pan, string PMutualFund, string PInsurance, string PFinancialPlanning, string PIFund, string PIPms, string PIBrokerage, string PILend, string PIHold, string PSubBrokerage)
        public ActionResult PMS(string Name, string EmailId, string PhoneNumber, string QuickTopic, string Aadhar_Pan, string PMutualFund, string PInsurance, string PFinancialPlanning, string PIFund, string PIPms, string PIBrokerage, string PILend, string PIHold, string PSubBrokerage)
        //public ActionResult SIP(KrcSIPContacts objQC)
        {
            GlobalFunction objGFunc = new GlobalFunction();
            string result = "";
            KrcQuickContacts objContact = new KrcQuickContacts();
            string ReturnUrl = Convert.ToString(Session["ReturnUrl"]);
            System.Web.HttpContext.Current.Session["SIPContRetUrl"] = ReturnUrl;

            string EMMutualFund = "";
            string EMInsurance = "";
            string EMFinancialPlanning = "";
            string EMIFund = "";
            string EMIPms = "";
            string EMIBrokerage = "";
            string EMILend = "";
            string EMIHold = "";
            string EMSubBrokerage = "";

            if (!String.IsNullOrEmpty(Request.Form["g-recaptcha-response"]))
            {
                try
                {
                    string Products = "";
                    // Mutual Fund
                    if (PMutualFund == null)
                    {
                        PMutualFund = "";
                    }
                    if (PMutualFund != "")
                    {
                        if (PMutualFund != "false")
                        {
                            Products = Products + PMutualFund;
                            EMMutualFund = "fp@XXXXXXX.XXX";
                        }
                    }

                    // Insurance
                    if (PInsurance == null)
                    {
                        PInsurance = "";
                    }
                    if (PInsurance != "")
                    {
                        if (PInsurance != "false")
                        {
                            Products = Products + PInsurance;
                            EMInsurance = "fp@XXXXXXX.XXX";
                        }
                    }

                    // Financial Planning
                    if (PFinancialPlanning == null)
                    {
                        PFinancialPlanning = "";
                    }
                    if (PFinancialPlanning != "")
                    {
                        if (PFinancialPlanning != "false")
                        {
                            Products = Products + PFinancialPlanning;
                            EMFinancialPlanning = "fp@XXXXXXX.XXX";
                        }
                    }


                    // I Fund
                    if (PIFund == null)
                    {
                        PIFund = "";
                    }
                    if (PIFund != "")
                    {
                        if (PIFund != "false")
                        {
                            Products = Products + PIFund;
                            EMIFund = "ifund@XXXXXXX.XXX";
                        }
                    }


                    // I Pms
                    if (PIPms == null)
                    {
                        PIPms = "";
                    }
                    if (PIPms != "")
                    {
                        if (PIPms != "false")
                        {
                            Products = Products + PIPms;
                            EMIPms = "pmsclient@XXXXXXX.XXX";
                        }
                    }


                    // I Brokerage
                    if (PIBrokerage == null)
                    {
                        PIBrokerage = "";
                    }
                    if (PIBrokerage != "")
                    {
                        if (PIBrokerage != "false")
                        {
                            Products = Products + PIBrokerage;
                            EMIBrokerage = "prahsant.shah@XXXXXXX.XXX";
                        }
                    }


                    // I Lend
                    if (PILend == null)
                    {
                        PILend = "";
                    }
                    if (PILend != "")
                    {
                        if (PILend != "false")
                        {
                            Products = Products + PILend;
                            EMILend = "rahul.pandit@XXXXXXX.XXX";
                        }
                    }


                    // I Hold
                    if (PIHold == null)
                    {
                        PIHold = "";
                    }
                    if (PIHold != "")
                    {
                        if (PIHold != "false")
                        {
                            Products = Products + PIHold;
                            EMIHold = "mona.zaveri@XXXXXXX.XXX";
                        }
                    }


                    // Sub Brokerage
                    if (PSubBrokerage == null)
                    {
                        PSubBrokerage = "";
                    }
                    if (PSubBrokerage != "")
                    {
                        if (PSubBrokerage != "false")
                        {
                            Products = Products + PSubBrokerage;
                            EMSubBrokerage = "npcare@XXXXXXX.XXX";
                        }
                    }


                    if (Aadhar_Pan == null)
                    {
                        Aadhar_Pan = "";
                    }
                    if (QuickTopic == null)
                    {
                        QuickTopic = "";
                    }
                    objContact.Name = Name;
                    objContact.EmailId = EmailId;
                    objContact.PhoneNumber = PhoneNumber;
                    objContact.QuickTopic = QuickTopic;
                    objContact.Aadhar_PAN = Aadhar_Pan;

                    if (objContact.PhoneNumber.Length != 10)
                    {
                        //result = "Enter Valid 10 Digit Mobile Number!";
                        @ViewBag.HideClass = "alert alert-danger";
                        @ViewBag.Message = "Enter Valid 10 Digit Mobile Number";
                    }
                    else if (IsValidEmail(objContact.EmailId) == false)
                    {
                        //result = "Enter Valid Email Address!";
                        @ViewBag.HideClass = "alert alert-danger";
                        @ViewBag.Message = "Enter Valid Email Address";
                    }
                    else
                    {

                        if (ModelState.IsValid)
                        {
                            bool res = true;
                            foreach (char c in objContact.PhoneNumber)
                            {
                                if (c < '0' || c > '9')
                                {
                                    res = false;
                                }
                            }

                            if (res == false || (objContact.PhoneNumber.ToString().Length != 10))
                            {
                                @ViewBag.HideClass = "alert alert-danger";
                                @ViewBag.Message = "Please Enter Valid Mobile Number.";
                                result = "Please Enter Valid Mobile Number.";
                            }
                            else
                            {
                                try
                                {
                                    // Insertion Code For QuickContact Table //
                                    gConnection.Open();
                                    string sqlstr = "insert into KrcQuickContacts(Name,EmailId,PhoneNumber,QuickTopic,Screen,CreatedBy,CreatedOn,Products,Aadhar_PAN)";
                                    sqlstr = sqlstr + " values (@QuickContactName,@QuickContactEmail,@QuickContactPhone,@QuickContactMessage,@Screen,@CreatedBy,GetDate(),@Products,@Aadhar_PAN)";
                                    //cmd.CommandText = sqlstr;
                                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@QuickContactName", objContact.Name);
                                    cmd.Parameters.AddWithValue("@QuickContactEmail", objContact.EmailId);
                                    cmd.Parameters.AddWithValue("@QuickContactPhone", objContact.PhoneNumber);
                                    cmd.Parameters.AddWithValue("@QuickContactMessage", objContact.QuickTopic);
                                    cmd.Parameters.AddWithValue("@Screen", ReturnUrl);
                                    cmd.Parameters.AddWithValue("@CreatedBy", "Online");
                                    cmd.Parameters.AddWithValue("@Products", Products);
                                    cmd.Parameters.AddWithValue("@Aadhar_Pan", objContact.Aadhar_PAN);
                                    cmd.ExecuteNonQuery();

                                    gConnection.Close();


                                    //Mail for Quick contact
                                    string FromMail = ConfigurationManager.AppSettings["smtpDisplayId"];
                                    string ToMail = "service@XXXXXXX.XXX";
                                    //string ToMail = "souravkuila65@gmail.com";
                                    //string Subject = "New contact request from " + objContact.Name;
                                    string Subject = "Product Enquiry received from DIMAG promotions - PMS Page";
                                    string Body = "Hi, Team KRChoksey";
                                    Body = Body + Environment.NewLine;

                                    string UserName = ConfigurationManager.AppSettings["smtpUser"];
                                    string Password = ConfigurationManager.AppSettings["smtpPass"];

                                    //Body = Body + "New contact request:";
                                    Body = Body + Environment.NewLine;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Name: " + objContact.Name;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Phone: " + objContact.PhoneNumber;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Email: " + objContact.EmailId;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Aadhar / PAN: " + objContact.Aadhar_PAN;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Message: " + objContact.QuickTopic;
                                    Body = Body + Environment.NewLine;
                                    if (Products == "")
                                    {
                                        Body = Body + "Products: Not Selected";
                                    }
                                    else
                                    {
                                        Body = Body + "Products: " + Products;
                                    }

                                    Body = Body + Environment.NewLine;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Thanks";
                                    Body = Body + Environment.NewLine;
                                    Body = Body + objContact.Name;
                                    string AttachmentPath = "";

                                    EMIPms = "pmsclient@XXXXXXX.XXX"; // Must go mail to this email as instructed
                                    //bool resu = objGFunc.SendMail(FromMail, ToMail, Subject, Body, UserName, Password, AttachmentPath);
                                    bool resu = objGFunc.SendMailForQuickContact(FromMail, ToMail, Subject, Body, UserName, Password, AttachmentPath, EMMutualFund, EMInsurance, EMFinancialPlanning, EMIFund, EMIPms, EMIBrokerage, EMILend, EMIHold, EMSubBrokerage);
                                    
                                    if (resu == true)
                                    {
                                        InsertMailLog(FromMail, ToMail, "Success PMS");

                                        Subject = "KRChoksey: Request Received: PMS";
                                        //Body = "Dear " + objContact.Name + ",";
                                        //Body += Environment.NewLine;
                                        //Body += Environment.NewLine;
                                        //Body += "Thank you for contacting us. Our executive will get back to you within 24 hours.";
                                        //Body += Environment.NewLine;
                                        //Body += "You can also reach us at service@XXXXXXX.XXX or call us on 022-66965513/5";
                                        //Body += Environment.NewLine;
                                        //Body += Environment.NewLine;
                                        //Body += Environment.NewLine;
                                        //Body += "Thanks";
                                        //Body += Environment.NewLine;
                                        //Body += "Customer Support Team";

                                        StreamReader reader = new StreamReader(Server.MapPath("~/Global/LPAcknowledgement.html"));
                                        string readFile = reader.ReadToEnd();
                                        string myString = "";
                                        myString = readFile;
                                        myString = myString.Replace("$$Name$$", objContact.Name);

                                        bool ack = objGFunc.SendAcknowledgementMail_New(FromMail, objContact.EmailId, Subject, myString, UserName, Password, AttachmentPath);
                                        if (ack == true)
                                        {
                                            objGFunc.InsertAcknowledgementMailLog(FromMail, objContact.EmailId, "Success", "PMS Page");
                                        }
                                        else
                                        {
                                            objGFunc.InsertAcknowledgementMailLog(FromMail, objContact.EmailId, "Not Success", "PMS Page");
                                        }
                                    }
                                    else
                                    {
                                        InsertMailLog(FromMail, ToMail, "Not Success PMS");
                                    }

                                    ModelState.Clear();
                                    @ViewBag.HideClass = "alert alert-success";
                                    @ViewBag.Message = "Thanks for contacting us! Our team shall get back to you very soon.";
                                    result = "Success";
                                    //return RedirectToAction("SIP");

                                    //System.Web.HttpContext.Current.Session["QuickContUrl"] = Request.Url.AbsoluteUri;
                                }
                                catch (Exception ex)
                                {
                                    //transaction.Rollback();
                                    gConnection.Close();
                                    Global.ErrorHandlerClass.LogError(ex);
                                    @ViewBag.HideClass = "alert alert-danger";
                                    @ViewBag.Message = "Error!";
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Global.ErrorHandlerClass.LogError(ex);
                }
            }
            else
            {
                //result = "Please Check Captcha!";
                @ViewBag.HideClass = "alert alert-danger";
                @ViewBag.Message = "Error! Please Check Captcha.";
            }

            //return result;
            return View();
            //return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion





        #region ILend (Get)

        [HttpGet]
        public ActionResult ILend()
        {
            //System.Web.HttpContext.Current.Session["SIPContRetUrl"] = Convert.ToString(Session["ReturnUrl"]);
            return View();
        }

        #endregion


        #region ILend (Post)

        [HttpPost]
        //public string SIP(string Name, string EmailId, string PhoneNumber, string QuickTopic, string Aadhar_Pan, string PMutualFund, string PInsurance, string PFinancialPlanning, string PIFund, string PIPms, string PIBrokerage, string PILend, string PIHold, string PSubBrokerage)
        public ActionResult ILend(string Name, string EmailId, string PhoneNumber, string QuickTopic, string Aadhar_Pan, string PMutualFund, string PInsurance, string PFinancialPlanning, string PIFund, string PIPms, string PIBrokerage, string PILend, string PIHold, string PSubBrokerage)
        //public ActionResult SIP(KrcSIPContacts objQC)
        {
            GlobalFunction objGFunc = new GlobalFunction();
            string result = "";
            KrcQuickContacts objContact = new KrcQuickContacts();
            string ReturnUrl = Convert.ToString(Session["ReturnUrl"]);
            System.Web.HttpContext.Current.Session["SIPContRetUrl"] = ReturnUrl;

            string EMMutualFund = "";
            string EMInsurance = "";
            string EMFinancialPlanning = "";
            string EMIFund = "";
            string EMIPms = "";
            string EMIBrokerage = "";
            string EMILend = "";
            string EMIHold = "";
            string EMSubBrokerage = "";

            if (!String.IsNullOrEmpty(Request.Form["g-recaptcha-response"]))
            {
                try
                {
                    string Products = "";
                    // Mutual Fund
                    if (PMutualFund == null)
                    {
                        PMutualFund = "";
                    }
                    if (PMutualFund != "")
                    {
                        if (PMutualFund != "false")
                        {
                            Products = Products + PMutualFund;
                            EMMutualFund = "fp@XXXXXXX.XXX";
                        }
                    }

                    // Insurance
                    if (PInsurance == null)
                    {
                        PInsurance = "";
                    }
                    if (PInsurance != "")
                    {
                        if (PInsurance != "false")
                        {
                            Products = Products + PInsurance;
                            EMInsurance = "fp@XXXXXXX.XXX";
                        }
                    }

                    // Financial Planning
                    if (PFinancialPlanning == null)
                    {
                        PFinancialPlanning = "";
                    }
                    if (PFinancialPlanning != "")
                    {
                        if (PFinancialPlanning != "false")
                        {
                            Products = Products + PFinancialPlanning;
                            EMFinancialPlanning = "fp@XXXXXXX.XXX";
                        }
                    }


                    // I Fund
                    if (PIFund == null)
                    {
                        PIFund = "";
                    }
                    if (PIFund != "")
                    {
                        if (PIFund != "false")
                        {
                            Products = Products + PIFund;
                            EMIFund = "ifund@XXXXXXX.XXX";
                        }
                    }


                    // I Pms
                    if (PIPms == null)
                    {
                        PIPms = "";
                    }
                    if (PIPms != "")
                    {
                        if (PIPms != "false")
                        {
                            Products = Products + PIPms;
                            EMIPms = "pmsclient@XXXXXXX.XXX";
                        }
                    }


                    // I Brokerage
                    if (PIBrokerage == null)
                    {
                        PIBrokerage = "";
                    }
                    if (PIBrokerage != "")
                    {
                        if (PIBrokerage != "false")
                        {
                            Products = Products + PIBrokerage;
                            EMIBrokerage = "prahsant.shah@XXXXXXX.XXX";
                        }
                    }


                    // I Lend
                    if (PILend == null)
                    {
                        PILend = "";
                    }
                    if (PILend != "")
                    {
                        if (PILend != "false")
                        {
                            Products = Products + PILend;
                            EMILend = "rahul.pandit@XXXXXXX.XXX";
                        }
                    }


                    // I Hold
                    if (PIHold == null)
                    {
                        PIHold = "";
                    }
                    if (PIHold != "")
                    {
                        if (PIHold != "false")
                        {
                            Products = Products + PIHold;
                            EMIHold = "mona.zaveri@XXXXXXX.XXX";
                        }
                    }


                    // Sub Brokerage
                    if (PSubBrokerage == null)
                    {
                        PSubBrokerage = "";
                    }
                    if (PSubBrokerage != "")
                    {
                        if (PSubBrokerage != "false")
                        {
                            Products = Products + PSubBrokerage;
                            EMSubBrokerage = "npcare@XXXXXXX.XXX";
                        }
                    }


                    if (Aadhar_Pan == null)
                    {
                        Aadhar_Pan = "";
                    }
                    if (QuickTopic == null)
                    {
                        QuickTopic = "";
                    }
                    objContact.Name = Name;
                    objContact.EmailId = EmailId;
                    objContact.PhoneNumber = PhoneNumber;
                    objContact.QuickTopic = QuickTopic;
                    objContact.Aadhar_PAN = Aadhar_Pan;

                    if (objContact.PhoneNumber.Length != 10)
                    {
                        //result = "Enter Valid 10 Digit Mobile Number!";
                        @ViewBag.HideClass = "alert alert-danger";
                        @ViewBag.Message = "Enter Valid 10 Digit Mobile Number";
                    }
                    else if (IsValidEmail(objContact.EmailId) == false)
                    {
                        //result = "Enter Valid Email Address!";
                        @ViewBag.HideClass = "alert alert-danger";
                        @ViewBag.Message = "Enter Valid Email Address";
                    }
                    else
                    {

                        if (ModelState.IsValid)
                        {
                            bool res = true;
                            foreach (char c in objContact.PhoneNumber)
                            {
                                if (c < '0' || c > '9')
                                {
                                    res = false;
                                }
                            }

                            if (res == false || (objContact.PhoneNumber.ToString().Length != 10))
                            {
                                @ViewBag.HideClass = "alert alert-danger";
                                @ViewBag.Message = "Please Enter Valid Mobile Number.";
                                result = "Please Enter Valid Mobile Number.";
                            }
                            else
                            {
                                try
                                {
                                    // Insertion Code For QuickContact Table //
                                    gConnection.Open();
                                    string sqlstr = "insert into KrcQuickContacts(Name,EmailId,PhoneNumber,QuickTopic,Screen,CreatedBy,CreatedOn,Products,Aadhar_PAN)";
                                    sqlstr = sqlstr + " values (@QuickContactName,@QuickContactEmail,@QuickContactPhone,@QuickContactMessage,@Screen,@CreatedBy,GetDate(),@Products,@Aadhar_PAN)";
                                    //cmd.CommandText = sqlstr;
                                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@QuickContactName", objContact.Name);
                                    cmd.Parameters.AddWithValue("@QuickContactEmail", objContact.EmailId);
                                    cmd.Parameters.AddWithValue("@QuickContactPhone", objContact.PhoneNumber);
                                    cmd.Parameters.AddWithValue("@QuickContactMessage", objContact.QuickTopic);
                                    cmd.Parameters.AddWithValue("@Screen", ReturnUrl);
                                    cmd.Parameters.AddWithValue("@CreatedBy", "Online");
                                    cmd.Parameters.AddWithValue("@Products", Products);
                                    cmd.Parameters.AddWithValue("@Aadhar_Pan", objContact.Aadhar_PAN);
                                    cmd.ExecuteNonQuery();

                                    gConnection.Close();


                                    //Mail for Quick contact
                                    string FromMail = ConfigurationManager.AppSettings["smtpDisplayId"];
                                    string ToMail = "service@XXXXXXX.XXX";
                                    //string ToMail = "souravkuila65@gmail.com";
                                    //string Subject = "New contact request from " + objContact.Name;
                                    string Subject = "Product Enquiry received from DIMAG promotions - iLend Page";
                                    string Body = "Hi, Team KRChoksey";
                                    Body = Body + Environment.NewLine;

                                    string UserName = ConfigurationManager.AppSettings["smtpUser"];
                                    string Password = ConfigurationManager.AppSettings["smtpPass"];

                                    //Body = Body + "New contact request:";
                                    Body = Body + Environment.NewLine;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Name: " + objContact.Name;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Phone: " + objContact.PhoneNumber;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Email: " + objContact.EmailId;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Aadhar / PAN: " + objContact.Aadhar_PAN;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Message: " + objContact.QuickTopic;
                                    Body = Body + Environment.NewLine;
                                    if (Products == "")
                                    {
                                        Body = Body + "Products: Not Selected";
                                    }
                                    else
                                    {
                                        Body = Body + "Products: " + Products;
                                    }

                                    Body = Body + Environment.NewLine;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Thanks";
                                    Body = Body + Environment.NewLine;
                                    Body = Body + objContact.Name;
                                    string AttachmentPath = "";

                                    EMILend = "rahul.pandit@XXXXXXX.XXX"; // Must go mail to this email as instructed
                                    //bool resu = objGFunc.SendMail(FromMail, ToMail, Subject, Body, UserName, Password, AttachmentPath);
                                    bool resu = objGFunc.SendMailForQuickContact(FromMail, ToMail, Subject, Body, UserName, Password, AttachmentPath, EMMutualFund, EMInsurance, EMFinancialPlanning, EMIFund, EMIPms, EMIBrokerage, EMILend, EMIHold, EMSubBrokerage);

                                    if (resu == true)
                                    {
                                        InsertMailLog(FromMail, ToMail, "Success iLend");

                                        Subject = "KRChoksey: Request Received: iLend";
                                        //Body = "Dear " + objContact.Name + ",";
                                        //Body += Environment.NewLine;
                                        //Body += Environment.NewLine;
                                        //Body += "Thank you for contacting us. Our executive will get back to you within 24 hours.";
                                        //Body += Environment.NewLine;
                                        //Body += "You can also reach us at service@XXXXXXX.XXX or call us on 022-66965513/5";
                                        //Body += Environment.NewLine;
                                        //Body += Environment.NewLine;
                                        //Body += Environment.NewLine;
                                        //Body += "Thanks";
                                        //Body += Environment.NewLine;
                                        //Body += "Customer Support Team";

                                        StreamReader reader = new StreamReader(Server.MapPath("~/Global/LPAcknowledgement.html"));
                                        string readFile = reader.ReadToEnd();
                                        string myString = "";
                                        myString = readFile;
                                        myString = myString.Replace("$$Name$$", objContact.Name);

                                        bool ack = objGFunc.SendAcknowledgementMail_New(FromMail, objContact.EmailId, Subject, myString, UserName, Password, AttachmentPath);
                                        if (ack == true)
                                        {
                                            objGFunc.InsertAcknowledgementMailLog(FromMail, objContact.EmailId, "Success", "iLend Page");
                                        }
                                        else
                                        {
                                            objGFunc.InsertAcknowledgementMailLog(FromMail, objContact.EmailId, "Not Success", "iLend Page");
                                        }
                                    }
                                    else
                                    {
                                        InsertMailLog(FromMail, ToMail, "Not Success iLend");
                                    }

                                    ModelState.Clear();
                                    @ViewBag.HideClass = "alert alert-success";
                                    @ViewBag.Message = "Thanks for contacting us! Our team shall get back to you very soon.";
                                    result = "Success";
                                    //return RedirectToAction("SIP");

                                    //System.Web.HttpContext.Current.Session["QuickContUrl"] = Request.Url.AbsoluteUri;
                                }
                                catch (Exception ex)
                                {
                                    //transaction.Rollback();
                                    gConnection.Close();
                                    Global.ErrorHandlerClass.LogError(ex);
                                    @ViewBag.HideClass = "alert alert-danger";
                                    @ViewBag.Message = "Error!";
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Global.ErrorHandlerClass.LogError(ex);
                }
            }
            else
            {
                //result = "Please Check Captcha!";
                @ViewBag.HideClass = "alert alert-danger";
                @ViewBag.Message = "Error! Please Check Captcha.";
            }

            //return result;
            return View();
            //return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion





        #region IEquity (Get)

        [HttpGet]
        public ActionResult IEquity()
        {
            //System.Web.HttpContext.Current.Session["SIPContRetUrl"] = Convert.ToString(Session["ReturnUrl"]);
            return View();
        }

        #endregion


        #region IEquity (Post)

        [HttpPost]
        //public string SIP(string Name, string EmailId, string PhoneNumber, string QuickTopic, string Aadhar_Pan, string PMutualFund, string PInsurance, string PFinancialPlanning, string PIFund, string PIPms, string PIBrokerage, string PILend, string PIHold, string PSubBrokerage)
        public ActionResult IEquity(string Name, string EmailId, string PhoneNumber, string QuickTopic, string Aadhar_Pan, string PMutualFund, string PInsurance, string PFinancialPlanning, string PIFund, string PIPms, string PIBrokerage, string PILend, string PIHold, string PSubBrokerage)
        //public ActionResult SIP(KrcSIPContacts objQC)
        {
            GlobalFunction objGFunc = new GlobalFunction();
            string result = "";
            KrcQuickContacts objContact = new KrcQuickContacts();
            string ReturnUrl = Convert.ToString(Session["ReturnUrl"]);
            System.Web.HttpContext.Current.Session["SIPContRetUrl"] = ReturnUrl;

            string EMMutualFund = "";
            string EMInsurance = "";
            string EMFinancialPlanning = "";
            string EMIFund = "";
            string EMIPms = "";
            string EMIBrokerage = "";
            string EMILend = "";
            string EMIHold = "";
            string EMSubBrokerage = "";

            if (!String.IsNullOrEmpty(Request.Form["g-recaptcha-response"]))
            {
                try
                {
                    string Products = "";
                    // Mutual Fund
                    if (PMutualFund == null)
                    {
                        PMutualFund = "";
                    }
                    if (PMutualFund != "")
                    {
                        if (PMutualFund != "false")
                        {
                            Products = Products + PMutualFund;
                            EMMutualFund = "fp@XXXXXXX.XXX";
                        }
                    }

                    // Insurance
                    if (PInsurance == null)
                    {
                        PInsurance = "";
                    }
                    if (PInsurance != "")
                    {
                        if (PInsurance != "false")
                        {
                            Products = Products + PInsurance;
                            EMInsurance = "fp@XXXXXXX.XXX";
                        }
                    }

                    // Financial Planning
                    if (PFinancialPlanning == null)
                    {
                        PFinancialPlanning = "";
                    }
                    if (PFinancialPlanning != "")
                    {
                        if (PFinancialPlanning != "false")
                        {
                            Products = Products + PFinancialPlanning;
                            EMFinancialPlanning = "fp@XXXXXXX.XXX";
                        }
                    }


                    // I Fund
                    if (PIFund == null)
                    {
                        PIFund = "";
                    }
                    if (PIFund != "")
                    {
                        if (PIFund != "false")
                        {
                            Products = Products + PIFund;
                            EMIFund = "ifund@XXXXXXX.XXX";
                        }
                    }


                    // I Pms
                    if (PIPms == null)
                    {
                        PIPms = "";
                    }
                    if (PIPms != "")
                    {
                        if (PIPms != "false")
                        {
                            Products = Products + PIPms;
                            EMIPms = "pmsclient@XXXXXXX.XXX";
                        }
                    }


                    // I Brokerage
                    if (PIBrokerage == null)
                    {
                        PIBrokerage = "";
                    }
                    if (PIBrokerage != "")
                    {
                        if (PIBrokerage != "false")
                        {
                            Products = Products + PIBrokerage;
                            EMIBrokerage = "prahsant.shah@XXXXXXX.XXX";
                        }
                    }


                    // I Lend
                    if (PILend == null)
                    {
                        PILend = "";
                    }
                    if (PILend != "")
                    {
                        if (PILend != "false")
                        {
                            Products = Products + PILend;
                            EMILend = "rahul.pandit@XXXXXXX.XXX";
                        }
                    }


                    // I Hold
                    if (PIHold == null)
                    {
                        PIHold = "";
                    }
                    if (PIHold != "")
                    {
                        if (PIHold != "false")
                        {
                            Products = Products + PIHold;
                            EMIHold = "mona.zaveri@XXXXXXX.XXX";
                        }
                    }


                    // Sub Brokerage
                    if (PSubBrokerage == null)
                    {
                        PSubBrokerage = "";
                    }
                    if (PSubBrokerage != "")
                    {
                        if (PSubBrokerage != "false")
                        {
                            Products = Products + PSubBrokerage;
                            EMSubBrokerage = "npcare@XXXXXXX.XXX";
                        }
                    }


                    if (Aadhar_Pan == null)
                    {
                        Aadhar_Pan = "";
                    }
                    if (QuickTopic == null)
                    {
                        QuickTopic = "";
                    }
                    objContact.Name = Name;
                    objContact.EmailId = EmailId;
                    objContact.PhoneNumber = PhoneNumber;
                    objContact.QuickTopic = QuickTopic;
                    objContact.Aadhar_PAN = Aadhar_Pan;

                    if (objContact.PhoneNumber.Length != 10)
                    {
                        //result = "Enter Valid 10 Digit Mobile Number!";
                        @ViewBag.HideClass = "alert alert-danger";
                        @ViewBag.Message = "Enter Valid 10 Digit Mobile Number";
                    }
                    else if (IsValidEmail(objContact.EmailId) == false)
                    {
                        //result = "Enter Valid Email Address!";
                        @ViewBag.HideClass = "alert alert-danger";
                        @ViewBag.Message = "Enter Valid Email Address";
                    }
                    else
                    {

                        if (ModelState.IsValid)
                        {
                            bool res = true;
                            foreach (char c in objContact.PhoneNumber)
                            {
                                if (c < '0' || c > '9')
                                {
                                    res = false;
                                }
                            }

                            if (res == false || (objContact.PhoneNumber.ToString().Length != 10))
                            {
                                @ViewBag.HideClass = "alert alert-danger";
                                @ViewBag.Message = "Please Enter Valid Mobile Number.";
                                result = "Please Enter Valid Mobile Number.";
                            }
                            else
                            {
                                try
                                {
                                    // Insertion Code For QuickContact Table //
                                    gConnection.Open();
                                    string sqlstr = "insert into KrcQuickContacts(Name,EmailId,PhoneNumber,QuickTopic,Screen,CreatedBy,CreatedOn,Products,Aadhar_PAN)";
                                    sqlstr = sqlstr + " values (@QuickContactName,@QuickContactEmail,@QuickContactPhone,@QuickContactMessage,@Screen,@CreatedBy,GetDate(),@Products,@Aadhar_PAN)";
                                    //cmd.CommandText = sqlstr;
                                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@QuickContactName", objContact.Name);
                                    cmd.Parameters.AddWithValue("@QuickContactEmail", objContact.EmailId);
                                    cmd.Parameters.AddWithValue("@QuickContactPhone", objContact.PhoneNumber);
                                    cmd.Parameters.AddWithValue("@QuickContactMessage", objContact.QuickTopic);
                                    cmd.Parameters.AddWithValue("@Screen", ReturnUrl);
                                    cmd.Parameters.AddWithValue("@CreatedBy", "Online");
                                    cmd.Parameters.AddWithValue("@Products", Products);
                                    cmd.Parameters.AddWithValue("@Aadhar_Pan", objContact.Aadhar_PAN);
                                    cmd.ExecuteNonQuery();

                                    gConnection.Close();


                                    //Mail for Quick contact
                                    string FromMail = ConfigurationManager.AppSettings["smtpDisplayId"];
                                    string ToMail = "service@XXXXXXX.XXX";
                                    //string ToMail = "souravkuila65@gmail.com";
                                    //string Subject = "New contact request from " + objContact.Name;
                                    string Subject = "Product Enquiry received from DIMAG promotions - iEquity Page";
                                    string Body = "Hi, Team KRChoksey";
                                    Body = Body + Environment.NewLine;

                                    string UserName = ConfigurationManager.AppSettings["smtpUser"];
                                    string Password = ConfigurationManager.AppSettings["smtpPass"];

                                    //Body = Body + "New contact request:";
                                    Body = Body + Environment.NewLine;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Name: " + objContact.Name;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Phone: " + objContact.PhoneNumber;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Email: " + objContact.EmailId;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Aadhar / PAN: " + objContact.Aadhar_PAN;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Message: " + objContact.QuickTopic;
                                    Body = Body + Environment.NewLine;
                                    if (Products == "")
                                    {
                                        Body = Body + "Products: Not Selected";
                                    }
                                    else
                                    {
                                        Body = Body + "Products: " + Products;
                                    }

                                    Body = Body + Environment.NewLine;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Thanks";
                                    Body = Body + Environment.NewLine;
                                    Body = Body + objContact.Name;
                                    string AttachmentPath = "";

                                    string EMIEquity = "prahsant.shah@XXXXXXX.XXX"; // Must go mail to this email as instructed
                                    //bool resu = objGFunc.SendMail(FromMail, ToMail, Subject, Body, UserName, Password, AttachmentPath);
                                    bool resu = objGFunc.SendMailForQuickContact_IEquity(FromMail, ToMail, Subject, Body, UserName, Password, AttachmentPath, EMMutualFund, EMInsurance, EMFinancialPlanning, EMIFund, EMIPms, EMIBrokerage, EMILend, EMIHold, EMSubBrokerage, EMIEquity);

                                    if (resu == true)
                                    {
                                        InsertMailLog(FromMail, ToMail, "Success iEquity");

                                        Subject = "KRChoksey: Request Received: iEquity";
                                        //Body = "Dear " + objContact.Name + ",";
                                        //Body += Environment.NewLine;
                                        //Body += Environment.NewLine;
                                        //Body += "Thank you for contacting us. Our executive will get back to you within 24 hours.";
                                        //Body += Environment.NewLine;
                                        //Body += "You can also reach us at service@XXXXXXX.XXX or call us on 022-66965513/5";
                                        //Body += Environment.NewLine;
                                        //Body += Environment.NewLine;
                                        //Body += Environment.NewLine;
                                        //Body += "Thanks";
                                        //Body += Environment.NewLine;
                                        //Body += "Customer Support Team";

                                        StreamReader reader = new StreamReader(Server.MapPath("~/Global/LPAcknowledgement.html"));
                                        string readFile = reader.ReadToEnd();
                                        string myString = "";
                                        myString = readFile;
                                        myString = myString.Replace("$$Name$$", objContact.Name);

                                        bool ack = objGFunc.SendAcknowledgementMail_New(FromMail, objContact.EmailId, Subject, myString, UserName, Password, AttachmentPath);
                                        if (ack == true)
                                        {
                                            objGFunc.InsertAcknowledgementMailLog(FromMail, objContact.EmailId, "Success", "iEquity Page");
                                        }
                                        else
                                        {
                                            objGFunc.InsertAcknowledgementMailLog(FromMail, objContact.EmailId, "Not Success", "iLend Page");
                                        }
                                    }
                                    else
                                    {
                                        InsertMailLog(FromMail, ToMail, "Not Success iEquity");
                                    }

                                    ModelState.Clear();
                                    @ViewBag.HideClass = "alert alert-success";
                                    @ViewBag.Message = "Thanks for contacting us! Our team shall get back to you very soon.";
                                    result = "Success";
                                    //return RedirectToAction("SIP");

                                    //System.Web.HttpContext.Current.Session["QuickContUrl"] = Request.Url.AbsoluteUri;
                                }
                                catch (Exception ex)
                                {
                                    //transaction.Rollback();
                                    gConnection.Close();
                                    Global.ErrorHandlerClass.LogError(ex);
                                    @ViewBag.HideClass = "alert alert-danger";
                                    @ViewBag.Message = "Error!";
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Global.ErrorHandlerClass.LogError(ex);
                }
            }
            else
            {
                //result = "Please Check Captcha!";
                @ViewBag.HideClass = "alert alert-danger";
                @ViewBag.Message = "Error! Please Check Captcha.";
            }

            //return result;
            return View();
            //return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region Private Function

        private static bool IsPhoneNumber(string number)
        {
            //return Regex.Match(number, @"^[0-9]\d{10}$").Success;
            //Regex mobilePattern = new Regex(@"^[0-9]\d{10}$");
            Regex mobilePattern = new Regex(@"/^((\\+91-?)|0)?[0-9]{10}$");
            return !mobilePattern.IsMatch(number);
        }

        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }


        private void InsertMailLog(string FromMail, string ToMail, string Success)
        {
            try
            {
                gConnection.Open();
                string sqlstr = "Insert Into LandingPageMailLog(FromMail,ToMail,Success,CreatedOn)";
                sqlstr = sqlstr + " values (@FromMail,@ToMail,@Success,GetDate())";
                gConnection.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@FromMail", FromMail);
                cmd.Parameters.AddWithValue("@ToMail", ToMail);
                cmd.Parameters.AddWithValue("@Success", Success);
                cmd.ExecuteNonQuery();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
        }

        #endregion

    }
}