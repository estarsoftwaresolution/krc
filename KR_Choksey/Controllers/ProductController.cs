﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Global;
using KR_Choksey.Areas.Admin.Models;
using KR_Choksey.Global;
using System.Data.SqlClient;

namespace KR_Choksey.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        [CustomActionFilter]
        public ActionResult Financial_Planning()
        {
            return View();
        }

        [CustomActionFilter]
        public ActionResult Research()
        {
            List<ResearchRptUpload> objRptList = new List<ResearchRptUpload>();
            objRptList = GetLatest3ResearchReport();
            return View(objRptList);
        }

        [CustomActionFilter]
        public ActionResult i_Equity()
        {
            return View();
        }
        [CustomActionFilter]
        public ActionResult i_Derivatives()
        {
            return View();
        }

        [CustomActionFilter]
        public ActionResult i_Plan()
        {
            return View();
        }

        [CustomActionFilter]
        public ActionResult i_PMS()
        {
            List<ResearchRptUpload> objRptList = new List<ResearchRptUpload>();
            objRptList = GetLatest3PMSReport();
            return View(objRptList);
        }
        [CustomActionFilter]
        public ActionResult i_Fund()
        {
            return View();
        }

        [CustomActionFilter]
        public ActionResult i_Hold()
        {
            return View();
        }

        [CustomActionFilter]
        public ActionResult i_Lend()
        {
            return View();
        }

        [CustomActionFilter]
        public ActionResult Online_trading()
        {
            return View();
        }


        [CustomActionFilter]
        public ActionResult DD_DC()
        {
            return View();
        }


        private List<ResearchRptUpload> GetLatest3PMSReport()
        {
            List<ResearchRptUpload> objRptList = new List<ResearchRptUpload>();
            try
            {
                gConnection.Open();
                string sqlstr = "select Top 3 ReportName, ReportPath, CreatedOn, OriginalRptName from dbo.ResearchRptUpload where PMS_Research='PMS report' Order By CreatedOn Desc";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    ResearchRptUpload objRpt = new ResearchRptUpload();
                    objRpt.ReportName = sdr["ReportName"].ToString();
                    objRpt.ReportPath = sdr["ReportPath"].ToString();
                    objRpt.OriginalRptName = sdr["OriginalRptName"].ToString();
                    objRpt.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());

                    objRpt.ReportName = objRpt.ReportName + "( " + Convert.ToDateTime(sdr["CreatedOn"].ToString()) + " )";

                    objRptList.Add(objRpt);
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }

            return objRptList;
        }


        private List<ResearchRptUpload> GetLatest3ResearchReport()
        {
            List<ResearchRptUpload> objRptList = new List<ResearchRptUpload>();
            try
            {
                gConnection.Open();
                string sqlstr = "select Top 3 ReportName, ReportPath, CreatedOn, OriginalRptName from dbo.ResearchRptUpload where PMS_Research='Research report' Order By CreatedOn Desc";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    ResearchRptUpload objRpt = new ResearchRptUpload();
                    objRpt.ReportName = sdr["ReportName"].ToString();
                    objRpt.ReportPath = sdr["ReportPath"].ToString();
                    objRpt.OriginalRptName = sdr["OriginalRptName"].ToString();
                    objRpt.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());

                    objRpt.ReportName = objRpt.ReportName + "( " + Convert.ToDateTime(sdr["CreatedOn"].ToString()) + " )";

                    objRptList.Add(objRpt);
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }

            return objRptList;
        }



        // Old Pages//
        //[CustomActionFilter]
        //public ActionResult Mutual_Funds()
        //{
        //    return View();
        //}
        //[CustomActionFilter]
        //public ActionResult Insurance()
        //{
        //    return View();
        //}
        
        //[CustomActionFilter]
        //public ActionResult Bonds()
        //{
        //    return View();
        //}
        
        //[CustomActionFilter]
        //public ActionResult Shares()
        //{
        //    return View();
        //}
        
        
        //[CustomActionFilter]
        //public ActionResult Research_KRC_Blasters()
        //{
        //    return View();
        //}

        //[CustomActionFilter]
        //public ActionResult Research_Technicals()
        //{
        //    return View();
        //}        

        //[CustomActionFilter]
        //public ActionResult Research_Equity_Research()
        //{
        //    return View();
        //}
        //[CustomActionFilter]
        //public ActionResult Research_Strategy()
        //{
        //    return View();
        //}

        //[CustomActionFilter]
        //public ActionResult Research_Subscribe()
        //{
        //    return View();
        //}
        // Old Pages//
    }
}