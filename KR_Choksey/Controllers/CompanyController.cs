﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Global;
using KR_Choksey.Global;
using System.Data.SqlClient;
using KR_Choksey.Models;
using System.Net;
using System.Xml;

namespace KR_Choksey.Controllers
{
    public class CompanyController : Controller
    {
        // GET: Company
        [CustomActionFilter]
        public ActionResult Company_Details(string id)
        {
            id = HttpUtility.UrlDecode(id);
            CmotsCompanyMaster Cmp_Backgrnd = new CmotsCompanyMaster();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "SELECT companyname,sectorname FROM CmotsCompanyMaster where co_code = @CompCode";
                cmd.CommandText = sqlstr;
                cmd.Parameters.AddWithValue("@CompCode", id);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCompanyMaster Cmp_Name = new CmotsCompanyMaster();
                    Cmp_Name.companyname = sdr["companyname"].ToString();
                    Cmp_Name.sectorname = sdr["sectorname"].ToString();
                    Cmp_Backgrnd = Cmp_Name;
                }
                sdr.Close();
                gConnection.Close();


                // Update Data To UserNavigation For 
                string OldMenuName = "";
                string OldIP = "";
                string OldUserId = "";
                if (System.Web.HttpContext.Current.Session["OldMenuName"] != null)
                {
                    OldMenuName = System.Web.HttpContext.Current.Session["OldMenuName"].ToString();
                    OldIP = System.Web.HttpContext.Current.Session["OldIP"].ToString();
                    OldUserId = System.Web.HttpContext.Current.Session["OldUserId"].ToString();
                }
                GlobalFunction objGFunc = new GlobalFunction();
                if (System.Web.HttpContext.Current.Session["OldSignedUserId"] != null)
                {
                    OldUserId = System.Web.HttpContext.Current.Session["OldSignedUserId"].ToString();
                    if (OldUserId != "")
                    {
                        if (objGFunc.UpdateCompCodeForSignedInNavigation(OldUserId, OldMenuName, OldIP, id) == true)
                        {
                        }
                    }
                }
                
                if (OldMenuName != "")
                {
                    if (OldUserId != "")
                    {
                        if (objGFunc.UpdateCompCodeForNavigation(OldUserId, OldMenuName, OldIP, id) == true)
                        {
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View(Cmp_Backgrnd);
        }

        [CustomActionFilter]
        public ActionResult Company_Snapshot()
        {
            return View();
        }

        //[CustomActionFilter]
        public PartialViewResult Company_Search()
        {
            return PartialView();
        }

        //[CustomActionFilter]
        public PartialViewResult Company_Background(string id)
        {
            id = HttpUtility.UrlDecode(id);
            CmotsCompanyBackground Cmp_Backgrnd = new CmotsCompanyBackground();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "SELECT * FROM CmotsCompanyBackground where co_code = @CompCode";
                cmd.CommandText = sqlstr;
                cmd.Parameters.AddWithValue("@CompCode", id);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCompanyBackground All_Data = new CmotsCompanyBackground();
                    All_Data.AutoId = Convert.ToInt32(sdr["AutoId"].ToString());
                    All_Data.lname = sdr["lname"].ToString();
                    All_Data.isin = sdr["isin"].ToString();
                    All_Data.hse_s_name = sdr["hse_s_name"].ToString();
                    All_Data.inc_dt = sdr["inc_dt"].ToString();
                    All_Data.regadd1 = sdr["regadd1"].ToString();
                    All_Data.regadd2 = sdr["regadd2"].ToString();
                    All_Data.regdist = sdr["regdist"].ToString();
                    All_Data.regstate = sdr["regstate"].ToString();
                    All_Data.regpin = sdr["regpin"].ToString();
                    All_Data.tel1 = sdr["tel1"].ToString();
                    All_Data.ind_l_name = sdr["ind_l_name"].ToString();
                    All_Data.tel2 = sdr["tel2"].ToString();
                    All_Data.fax1 = sdr["fax1"].ToString();
                    All_Data.fax2 = sdr["fax2"].ToString();
                    All_Data.auditor = sdr["auditor"].ToString();
                    All_Data.fv = sdr["fv"].ToString();
                    All_Data.mkt_lot = sdr["mkt_lot"].ToString();
                    All_Data.chairman = sdr["chairman"].ToString();
                    All_Data.co_sec = sdr["co_sec"].ToString();
                    All_Data.co_code = sdr["co_code"].ToString();
                    All_Data.email = sdr["email"].ToString();
                    All_Data.internet = sdr["internet"].ToString();
                    All_Data.dir_name = sdr["dir_name"].ToString();
                    All_Data.dir_desg = sdr["dir_desg"].ToString();
                    Cmp_Backgrnd=All_Data;
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(Cmp_Backgrnd);
        }

        //[CustomActionFilter]
        public PartialViewResult BSE_Status(string id)
        {
            CmotsBSEQuoteDetails objBseQuote = new CmotsBSEQuoteDetails();

            try
            {
                string URL = "http://krchokseyapi.cmlinks.com/Company-Information/Company-Info.svc/getquotedetails/" + id + "/BSE";
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(URL);
                myRequest.Method = "GET";
                WebResponse myResponse = myRequest.GetResponse();

                XmlDataDocument xmldoc = new XmlDataDocument();

                xmldoc.Load(myResponse.GetResponseStream());

                XmlElement xelRoot = xmldoc.DocumentElement;
                XmlNodeList xnlNodes = xelRoot.SelectNodes("/response/data/getquotedetailslist/getquotedetails");

                
                foreach (XmlNode xndNode in xnlNodes)
                {
                    CmotsBSEQuoteDetails objCmots = new CmotsBSEQuoteDetails();
                    //objCmots.co_code = xndNode["co_code"].InnerText;
                    objCmots.xchng = xndNode["xchng"].InnerText;
                    objCmots.upd_time = xndNode["upd_time"].InnerText;



                    objCmots.open_price = xndNode["open_price"].InnerText;
                    objCmots.high_price = xndNode["high_price"].InnerText;

                    objCmots.low_price = xndNode["low_price"].InnerText;
                    objCmots.price = xndNode["price"].InnerText;
                    objCmots.bbuy_qty = xndNode["bbuy_qty"].InnerText;
                    objCmots.bbuy_price = xndNode["bbuy_price"].InnerText;
                    objCmots.bsell_qty = xndNode["bsell_qty"].InnerText;
                    objCmots.bsell_price = xndNode["bsell_price"].InnerText;
                    objCmots.volume = xndNode["volume"].InnerText;
                    //objCmots.prev_close = xndNode["prev_close"].InnerText;
                    objCmots.pricediff = xndNode["pricediff"].InnerText;
                    objCmots.pricediff = Math.Round(Convert.ToDecimal(objCmots.pricediff), 2).ToString();

                    objCmots.change = xndNode["change"].InnerText;

                    objCmots.change = Math.Round(Convert.ToDecimal(objCmots.change), 2).ToString();

                    objCmots.trd_qty = xndNode["trd_qty"].InnerText;
                    objCmots.hi_52_wk = xndNode["hi_52_wk"].InnerText;
                    objCmots.lo_52_wk = xndNode["lo_52_wk"].InnerText;
                    objCmots.h52date = xndNode["h52date"].InnerText;
                    objCmots.l52date = xndNode["l52date"].InnerText;
                    objCmots.trd_value = xndNode["trd_value"].InnerText;
                    objCmots.sc_group = xndNode["sc_group"].InnerText;
                    objCmots.complname = xndNode["complname"].InnerText;
                    //objCmots.sc_code = xndNode["sc_code"].InnerText;
                    //objCmots.listinfo = xndNode["listinfo"].InnerText;
                    //objCmots.isin = xndNode["isin"].InnerText;
                    objCmots.symbol = xndNode["symbol"].InnerText;
                    objBseQuote = objCmots;
                }

                myResponse.Close();
            }
            catch(Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(objBseQuote);
            //id = HttpUtility.UrlDecode(id);
            //CmotsBSEQuoteDetails BSE_QuoteDetails = new CmotsBSEQuoteDetails();
            //gConnection.Open();
            //SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            //cmd.Connection = GlobalVariables.gConn;
            //try
            //{
            //    string sqlstr = "";
            //    sqlstr = "select * from CmotsBSEQuoteDetails where co_code = @CompCode";
            //    cmd.CommandText = sqlstr;
            //    cmd.Parameters.AddWithValue("@CompCode", id);
            //    SqlDataReader sdr = cmd.ExecuteReader();
            //    while (sdr.Read())
            //    {
            //        CmotsBSEQuoteDetails All_Data = new CmotsBSEQuoteDetails();
            //        All_Data.price = sdr["price"].ToString();
            //        All_Data.pricediff = Math.Round(Convert.ToDecimal(sdr["pricediff"].ToString()), 2).ToString();
            //        All_Data.change = Math.Round(Convert.ToDecimal(sdr["change"].ToString()), 2).ToString();
            //        All_Data.upd_time = sdr["upd_time"].ToString();
            //        All_Data.open_price = sdr["open_price"].ToString();
            //        All_Data.high_price = sdr["high_price"].ToString();
            //        All_Data.low_price = sdr["low_price"].ToString();
            //        All_Data.volume = sdr["volume"].ToString();
            //        //All_Data.prev_close = sdr["prev_close"].ToString();
            //        All_Data.co_code = sdr["co_Code"].ToString();
            //        All_Data.AutoId = Convert.ToInt32(sdr["AutoId"].ToString());
            //        BSE_QuoteDetails = All_Data;
            //    }
            //    sdr.Close();
            //    gConnection.Close();
            //}
            //catch (Exception ex)
            //{
            //    gConnection.Close();
            //    Global.ErrorHandlerClass.LogError(ex);
            //}
            //return PartialView(BSE_QuoteDetails);
        }

        //[CustomActionFilter]
        public PartialViewResult NSE_Status(string id)
        {
            CmotsNSEQuoteDetails objNseQuote = new CmotsNSEQuoteDetails();
            try
            {
                string URL = "http://krchokseyapi.cmlinks.com/Company-Information/Company-Info.svc/getquotedetails/" + id + "/NSE";
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(URL);
                myRequest.Method = "GET";
                WebResponse myResponse = myRequest.GetResponse();

                XmlDataDocument xmldoc = new XmlDataDocument();

                xmldoc.Load(myResponse.GetResponseStream());

                XmlElement xelRoot = xmldoc.DocumentElement;
                XmlNodeList xnlNodes = xelRoot.SelectNodes("/response/data/getquotedetailslist/getquotedetails");


                foreach (XmlNode xndNode in xnlNodes)
                {
                    CmotsNSEQuoteDetails objCmots = new CmotsNSEQuoteDetails();
                    //objCmots.co_code = xndNode["co_code"].InnerText;
                    objCmots.xchng = xndNode["xchng"].InnerText;
                    objCmots.upd_time = xndNode["upd_time"].InnerText;

                    //objCmots.upd_time = Convert.ToDateTime(objCmots.upd_time).ToString();

                    //objCmots.upd_time_date = Convert.ToDateTime(objCmots.upd_time);

                    objCmots.open_price = xndNode["open_price"].InnerText;
                    objCmots.high_price = xndNode["high_price"].InnerText;

                    objCmots.low_price = xndNode["low_price"].InnerText;
                    objCmots.price = xndNode["price"].InnerText;
                    objCmots.bbuy_qty = xndNode["bbuy_qty"].InnerText;
                    objCmots.bbuy_price = xndNode["bbuy_price"].InnerText;
                    objCmots.bsell_qty = xndNode["bsell_qty"].InnerText;
                    objCmots.bsell_price = xndNode["bsell_price"].InnerText;
                    objCmots.volume = xndNode["volume"].InnerText;
                    //objCmots.prev_close = xndNode["prev_close"].InnerText;
                    objCmots.pricediff = xndNode["pricediff"].InnerText;

                    objCmots.pricediff = Math.Round(Convert.ToDecimal(objCmots.pricediff), 2).ToString();

                    objCmots.change = xndNode["change"].InnerText;

                    objCmots.change = Math.Round(Convert.ToDecimal(objCmots.change), 2).ToString();

                    objCmots.trd_qty = xndNode["trd_qty"].InnerText;
                    objCmots.hi_52_wk = xndNode["hi_52_wk"].InnerText;
                    objCmots.lo_52_wk = xndNode["lo_52_wk"].InnerText;
                    objCmots.h52date = xndNode["h52date"].InnerText;
                    objCmots.l52date = xndNode["l52date"].InnerText;
                    objCmots.trd_value = xndNode["trd_value"].InnerText;
                    objCmots.sc_group = xndNode["sc_group"].InnerText;
                    objCmots.complname = xndNode["complname"].InnerText;
                    //objCmots.sc_code = xndNode["sc_code"].InnerText;
                    //objCmots.listinfo = xndNode["listinfo"].InnerText;
                    //objCmots.isin = xndNode["isin"].InnerText;
                    objCmots.symbol = xndNode["symbol"].InnerText;
                    objNseQuote = objCmots;
                }

                myResponse.Close();
                
            }
            catch(Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(objNseQuote);
            //id = HttpUtility.UrlDecode(id);
            //CmotsNSEQuoteDetails NSE_QuoteDetails = new CmotsNSEQuoteDetails();
            //gConnection.Open();
            //SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            //cmd.Connection = GlobalVariables.gConn;
            //try
            //{
            //    string sqlstr = "";
            //    sqlstr = "select * from CmotsNSEQuoteDetails where co_code = @CompCode";
            //    cmd.CommandText = sqlstr;
            //    cmd.Parameters.AddWithValue("@CompCode", id);
            //    SqlDataReader sdr = cmd.ExecuteReader();
            //    while (sdr.Read())
            //    {
            //        CmotsNSEQuoteDetails All_Data = new CmotsNSEQuoteDetails();
            //        All_Data.price = sdr["price"].ToString();
            //        All_Data.pricediff = Math.Round(Convert.ToDecimal(sdr["pricediff"].ToString()), 2).ToString();
            //        All_Data.change = Math.Round(Convert.ToDecimal(sdr["change"].ToString()), 2).ToString();
            //        All_Data.upd_time = sdr["upd_time"].ToString();
            //        All_Data.open_price = sdr["open_price"].ToString();
            //        All_Data.high_price = sdr["high_price"].ToString();
            //        All_Data.low_price = sdr["low_price"].ToString();
            //        All_Data.volume = sdr["volume"].ToString();
            //        //All_Data.prev_close = sdr["prev_close"].ToString();
            //        All_Data.co_code = sdr["co_code"].ToString();
            //        All_Data.AutoId = Convert.ToInt32(sdr["AutoId"].ToString());
            //        NSE_QuoteDetails = All_Data;
            //    }
            //    sdr.Close();
            //    gConnection.Close();
            //}
            //catch (Exception ex)
            //{
            //    gConnection.Close();
            //    Global.ErrorHandlerClass.LogError(ex);
            //}
            //return PartialView(NSE_QuoteDetails);
        }

        //[CustomActionFilter]
        public PartialViewResult Company_BoardofDirectors(string id)
        {
            id = HttpUtility.UrlDecode(id);
            List<CmotsBoardOfDirectors> Cmp_Directors = new List<CmotsBoardOfDirectors>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "SELECT * FROM CmotsBoardOfDirectors where co_code = @CompCode";
                cmd.CommandText = sqlstr;
                cmd.Parameters.AddWithValue("@CompCode", id);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsBoardOfDirectors Dir_List = new CmotsBoardOfDirectors();
                    Dir_List.co_code = sdr["co_code"].ToString();
                    Dir_List.slno = sdr["slno"].ToString();
                    Dir_List.dir_name = sdr["dir_name"].ToString();
                    Dir_List.dir_desg = sdr["dir_desg"].ToString();
                    Dir_List.year = sdr["year"].ToString();
                    Cmp_Directors.Add(Dir_List);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(Cmp_Directors);
        }

        //[CustomActionFilter]
        public PartialViewResult Company_Financial_Ratios(string id)
        {
            id = HttpUtility.UrlDecode(id);
            List<CmotsKeyFinancialRatios> FinancialRatios = new List<CmotsKeyFinancialRatios>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            GlobalFunction objGFunc = new GlobalFunction();
            try
            {
                // Print Header
                string sqlstr1 = "";
                sqlstr1 = "SELECT columnname,period1,period2,period3,period4,period5,period6 FROM CmotsKeyFinancialRatios where co_code = @CompCode1 and rid=1";
                cmd.CommandText = sqlstr1;
                cmd.Parameters.AddWithValue("@CompCode1", id);
                SqlDataReader sdr1 = cmd.ExecuteReader();
                while (sdr1.Read())
                {
                    ViewBag.Head1 = sdr1["columnname"].ToString();
                    string mnth1 = sdr1["period1"].ToString().Substring(sdr1["period1"].ToString().Length - 2);
                    mnth1 = objGFunc.MonthName(mnth1);
                    ViewBag.Head2 = mnth1 + " - " + sdr1["period1"].ToString().Substring(0, 4);
                    string mnth2 = sdr1["period2"].ToString().Substring(sdr1["period2"].ToString().Length - 2);
                    mnth2 = objGFunc.MonthName(mnth2);
                    ViewBag.Head3 = mnth1 + " - " + sdr1["period2"].ToString().Substring(0, 4);
                    string mnth3 = sdr1["period3"].ToString().Substring(sdr1["period3"].ToString().Length - 2);
                    mnth3 = objGFunc.MonthName(mnth3);
                    ViewBag.Head4 = mnth1 + " - " + sdr1["period3"].ToString().Substring(0, 4);
                    string mnth4 = sdr1["period4"].ToString().Substring(sdr1["period4"].ToString().Length - 2);
                    mnth4 = objGFunc.MonthName(mnth4);
                    ViewBag.Head5 = mnth1 + " - " + sdr1["period4"].ToString().Substring(0, 4);
                    string mnth5 = sdr1["period5"].ToString().Substring(sdr1["period5"].ToString().Length - 2);
                    mnth5 = objGFunc.MonthName(mnth5);
                    ViewBag.Head6 = mnth1 + " - " + sdr1["period5"].ToString().Substring(0, 4);
                    string mnth6 = sdr1["period6"].ToString().Substring(sdr1["period6"].ToString().Length - 2);
                    mnth6 = objGFunc.MonthName(mnth6);
                    ViewBag.Head7 = mnth1 + " - " + sdr1["period6"].ToString().Substring(0, 4);
                }
                sdr1.Close();
                // Print Table
                string sqlstr2 = "";
                sqlstr2 = "SELECT columnname,period1,period2,period3,period4,period5,period6 FROM CmotsKeyFinancialRatios where co_code = @CompCode2 and rid!=1";
                cmd.CommandText = sqlstr2;
                cmd.Parameters.AddWithValue("@CompCode2", id);
                SqlDataReader sdr2 = cmd.ExecuteReader();
                while (sdr2.Read())
                {
                    CmotsKeyFinancialRatios FinancialRatios_List = new CmotsKeyFinancialRatios();
                    FinancialRatios_List.columnname = sdr2["columnname"].ToString();
                    FinancialRatios_List.period1 = sdr2["period1"].ToString();
                    FinancialRatios_List.period2 = sdr2["period2"].ToString();
                    FinancialRatios_List.period3 = sdr2["period3"].ToString();
                    FinancialRatios_List.period4 = sdr2["period4"].ToString();
                    FinancialRatios_List.period5 = sdr2["period5"].ToString();
                    FinancialRatios_List.period6 = sdr2["period6"].ToString();
                    FinancialRatios.Add(FinancialRatios_List);
                }
                sdr2.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(FinancialRatios);
        }

        //[CustomActionFilter]
        public PartialViewResult Company_News(string id)
        {
            id = HttpUtility.UrlDecode(id);
            List<CmotsCompanyNews> Cmp_News = new List<CmotsCompanyNews>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "Select CmotsCompanyNews.date AS date, CmotsCompanyNews.caption AS caption, CmotsCompanyNews.sr_no, CmotsCompanyNews_Details.memo AS memo From CmotsCompanyNews Left Outer Join CmotsCompanyNews_Details On CmotsCompanyNews.sr_no = CmotsCompanyNews_Details.sr_no WHERE CmotsCompanyNews.co_code = @CompCode";
                cmd.CommandText = sqlstr;
                cmd.Parameters.AddWithValue("@CompCode", id);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCompanyNews News_List = new CmotsCompanyNews();
                    News_List.date = sdr["date"].ToString();
                    News_List.caption = sdr["caption"].ToString();
                    News_List.memo = sdr["memo"].ToString();
                    Cmp_News.Add(News_List);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(Cmp_News);
        }

        //[CustomActionFilter]
        public PartialViewResult Company_Share_Holding(string id)
        {
            id = HttpUtility.UrlDecode(id);
            CmotsShareHoldingPattern ShareHolding = new CmotsShareHoldingPattern();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "SELECT TOP 1 * FROM CmotsShareHoldingPattern WHERE co_code=@CompCode ORDER BY yearmonth DESC";                
                cmd.CommandText = sqlstr;
                cmd.Parameters.AddWithValue("@CompCode", id);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsShareHoldingPattern All_Data = new CmotsShareHoldingPattern();
                    All_Data.total_non_promoter_1 = sdr["total_non_promoter_1"].ToString();
                    All_Data.total_non_promoter_2 = sdr["total_non_promoter_2"].ToString();
                    All_Data.total_of_promoter_1 = sdr["total_of_promoter_1"].ToString();
                    All_Data.total_of_promoter_2 = sdr["total_of_promoter_2"].ToString();
                    All_Data.non_promoter_institution_1 = sdr["non_promoter_institution_1"].ToString();
                    All_Data.non_promoter_institution_2 = sdr["non_promoter_institution_2"].ToString();
                    All_Data.grand_total_1 = sdr["grand_total_1"].ToString();
                    All_Data.grand_total_2 = sdr["grand_total_2"].ToString();
                    All_Data.co_code = sdr["co_code"].ToString();
                    ShareHolding = All_Data;
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(ShareHolding);
        }

        //[CustomActionFilter]
        public PartialViewResult Company_MF_Holdings(string id)
        {
            id = HttpUtility.UrlDecode(id);
            List<CmotsMFHoldings> Cmp_MF_Holdings = new List<CmotsMFHoldings>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "SELECT date,caption FROM CmotsCompanyNews where co_code = @CompCode";
                cmd.CommandText = sqlstr;
                cmd.Parameters.AddWithValue("@CompCode", id);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsMFHoldings MF_Holdings = new CmotsMFHoldings();
                    //Share_Holdings.date = sdr["date"].ToString();
                    //Share_Holdings.caption = sdr["caption"].ToString();
                    Cmp_MF_Holdings.Add(MF_Holdings);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView(Cmp_MF_Holdings);
        }

        //[CustomActionFilter]
        public PartialViewResult Company_Quote()
        {
            return PartialView();
        }        

        //[CustomActionFilter]
        public PartialViewResult Company_Equity()
        {
            return PartialView();
        }       

        //[CustomActionFilter]
        public PartialViewResult Company_Derivatives()
        {
            return PartialView();
        }        

        //[CustomActionFilter]
        public PartialViewResult Company_MF_ETFs()
        {
            return PartialView();
        }

        //[CustomActionFilter]
        public PartialViewResult Company_Debt_Others()
        {
            return PartialView();
        }        

        #region Get CompanyName
        [HttpPost]
        public JsonResult GetCompanyName(string Prefix)
        {
            //Note : you can bind same list from database 
            List<SelectListItem> objRowsList = new List<SelectListItem>();

            try
            {
                gConnection.Open();
                string sqlstr = "SELECT TOP 5 Isnull(Companyname,'') as CompanyName,Isnull(co_code,'') as CompanyCode From dbo.CmotsCompanyMaster where Isnull(Companyname,'') Like @CompCode";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@CompCode", "%" + Prefix + "%");
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {                  
                    objRowsList.Add(new SelectListItem
                    {
                        Text = sdr["CompanyName"].ToString(),
                        Value = sdr["CompanyCode"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
                @ViewBag.HideClass = "alert alert-danger";
                @ViewBag.Message = "Error!";
            }
            var Data = objRowsList;
            return Json(Data, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}