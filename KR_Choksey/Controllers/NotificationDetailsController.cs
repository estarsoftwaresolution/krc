﻿using KR_Choksey.Global;
using KR_Choksey.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KR_Choksey.Controllers
{
    public class NotificationDetailsController : Controller
    {
        // GET: NotificationDetails
        public ActionResult HotPursuit(string _id)
        {
            CmotsHotPursuit_stockAction objHotP = new CmotsHotPursuit_stockAction();
            string id = Encryption.decrypt(_id);
            //string id = "909080";
            try
            {
                string sqlstr = "select sno, Isnull(section_name,'') as section_name, date, time, Isnull(heading,'') as heading, ";
                sqlstr = sqlstr + " Isnull(caption,'') as caption, Isnull(arttext,'') as arttext, co_code from dbo.CmotsHotPursuit_stockAction ";
                sqlstr = sqlstr + " where sno=@sno";
                gConnection.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@sno", id);
                SqlDataReader sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    objHotP.sno = sdr["sno"].ToString();
                    objHotP.section_name = sdr["section_name"].ToString();
                    objHotP.date = sdr["date"].ToString();
                    objHotP.time = sdr["time"].ToString();
                    objHotP.heading = sdr["heading"].ToString();
                    objHotP.caption = sdr["caption"].ToString();
                    objHotP.arttext = sdr["arttext"].ToString();
                    objHotP.co_code = sdr["co_Code"].ToString();
                }
                gConnection.Close();
                DateTime dt = Convert.ToDateTime(objHotP.date).Add(TimeSpan.Parse(objHotP.time));
                objHotP.date = dt.ToString();
                ViewData["arttext"] = objHotP.arttext;
            }
            catch(Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return View(objHotP);
        }


        public ActionResult CorporateNews(string _id)
        {
            CmotsCorporateNews objCmot = new CmotsCorporateNews();
            string id = Encryption.decrypt(_id);
            //string id = "909080";
            try
            {
                string sqlstr = "select sno, Isnull(section_name,'') as section_name, date, time, Isnull(heading,'') as heading, ";
                sqlstr = sqlstr + " Isnull(caption,'') as caption, Isnull(arttext,'') as arttext from dbo.CmotsCorporateNews where sno = @sno";
                gConnection.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@sno", id);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objCmot.sno = sdr["sno"].ToString();
                    objCmot.section_name = sdr["section_name"].ToString();
                    objCmot.date = sdr["date"].ToString();
                    objCmot.time = sdr["time"].ToString();
                    objCmot.heading = sdr["heading"].ToString();
                    objCmot.caption = sdr["caption"].ToString();
                    objCmot.arttext = sdr["arttext"].ToString();
                }
                gConnection.Close();
                DateTime dt = Convert.ToDateTime(objCmot.date).Add(TimeSpan.Parse(objCmot.time));
                objCmot.date = dt.ToString();
                ViewData["arttext"] = objCmot.arttext;
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return View(objCmot);
        }


        public ActionResult EconomyNews(string _id)
        {
            CmotsEconomyNews objCmot = new CmotsEconomyNews();
            string id = Encryption.decrypt(_id);
            //string id = "909080";
            try
            {
                string sqlstr = "select sno, Isnull(section_name,'') as section_name, date, time, Isnull(heading,'') as heading, ";
                sqlstr = sqlstr + " Isnull(caption,'') as caption, Isnull(arttext,'') as arttext from dbo.CmotsEconomyNews where sno = @sno";
                gConnection.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@sno", id);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objCmot.sno = sdr["sno"].ToString();
                    objCmot.section_name = sdr["section_name"].ToString();
                    objCmot.date = sdr["date"].ToString();
                    objCmot.time = sdr["time"].ToString();
                    objCmot.heading = sdr["heading"].ToString();
                    objCmot.caption = sdr["caption"].ToString();
                    objCmot.arttext = sdr["arttext"].ToString();
                }
                gConnection.Close();
                DateTime dt = Convert.ToDateTime(objCmot.date).Add(TimeSpan.Parse(objCmot.time));
                objCmot.date = dt.ToString();
                ViewData["arttext"] = objCmot.arttext;
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return View(objCmot);
        }



        public ActionResult StockAlertNews(string _id)
        {
            CmotsStockAlert objCmot = new CmotsStockAlert();
            string id = Encryption.decrypt(_id);
            //string id = "909080";
            try
            {
                string sqlstr = "select sno, Isnull(section_name,'') as section_name, date, time, Isnull(heading,'') as heading, ";
                sqlstr = sqlstr + " Isnull(caption,'') as caption, Isnull(arttext,'') as arttext from dbo.CmotsStockAlert where sno = @sno";
                gConnection.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@sno", id);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objCmot.sno = sdr["sno"].ToString();
                    objCmot.section_name = sdr["section_name"].ToString();
                    objCmot.date = sdr["date"].ToString();
                    objCmot.time = sdr["time"].ToString();
                    objCmot.heading = sdr["heading"].ToString();
                    objCmot.caption = sdr["caption"].ToString();
                    objCmot.arttext = sdr["arttext"].ToString();
                }
                gConnection.Close();
                DateTime dt = Convert.ToDateTime(objCmot.date).Add(TimeSpan.Parse(objCmot.time));
                objCmot.date = dt.ToString();
                ViewData["arttext"] = objCmot.arttext;
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return View(objCmot);
        }
    }
}