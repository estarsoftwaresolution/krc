﻿using KR_Choksey.Global;
using KR_Choksey.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace KR_Choksey.Controllers
{
    public class OnlineCMOTSApiController : Controller
    {

        #region Get BSE Online Data

        [HttpGet]       
        //[AllowCrossSiteJson]
        public ActionResult GetBSEData()
        {
            return View();
        }


        [HttpPost]
        public ActionResult GetBSEData(string lname, string currprice, string currtime, string prevclose)
        {
            try
            {
                bool result = false;
                gConnection.Open();
                string sqlstr = "select AutoId,lname ,currprice,currtime ,prevclose from BSEOnlinePrice where ";
                sqlstr = sqlstr + " lname = @lname and @currprice=@currprice and currtime=@currtime";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@lname", lname);
                cmd.Parameters.AddWithValue("@currprice", currprice);
                cmd.Parameters.AddWithValue("@currtime", currtime);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    result = true;
                }
                sdr.Close();
                if (result == false)
                {
                    sqlstr = "Insert Into BSEOnlinePrice (lname ,currprice,currtime ,prevclose) ";
                    sqlstr = sqlstr + " values (@lname ,@currprice,@currtime ,@prevclose)";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.AddWithValue("@lname", lname);
                    cmd.Parameters.AddWithValue("@currprice", currprice);
                    cmd.Parameters.AddWithValue("@currtime", currtime);
                    cmd.Parameters.AddWithValue("@prevclose", prevclose);
                    cmd.ExecuteNonQuery();
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {

            }
            return View();
        }


        #endregion
        

        #region Get NSE Online Data

        [HttpGet]
        //[AllowCrossSiteJson]
        public ActionResult GetNSEData()
        {
            return View();
        }


        [HttpPost]
        public ActionResult GetNSEData(string lname, string currprice, string currtime, string prevclose)
        {
            try
            {
                bool result = false;
                gConnection.Open();
                string sqlstr = "select AutoId,lname ,currprice,currtime ,prevclose from NSEOnlinePrice where ";
                sqlstr = sqlstr + " lname = @lname and @currprice=@currprice and currtime=@currtime";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@lname", lname);
                cmd.Parameters.AddWithValue("@currprice", currprice);
                cmd.Parameters.AddWithValue("@currtime", currtime);
                SqlDataReader sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    result = true;
                }
                sdr.Close();
                if(result == false)
                {
                    sqlstr = "Insert Into NSEOnlinePrice (lname ,currprice,currtime ,prevclose) ";
                    sqlstr = sqlstr + " values (@lname ,@currprice,@currtime ,@prevclose)";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.AddWithValue("@lname", lname);
                    cmd.Parameters.AddWithValue("@currprice", currprice);
                    cmd.Parameters.AddWithValue("@currtime", currtime);
                    cmd.Parameters.AddWithValue("@prevclose", prevclose);
                    cmd.ExecuteNonQuery();
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {

            }

                return View();
        }

        #endregion




        #region NSE Price Gainers Data

        [HttpGet]
        public ActionResult GetNSENetPriceGainersData()
        {
            return View();
        }


        [HttpPost]
        public ActionResult GetNSENetPriceGainersData(string result1)
        {
            try
            {
                bool result = false;
                List<NSENetPriceGainersData> objGrpList = new List<NSENetPriceGainersData>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(result1);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    NSENetPriceGainersData objNSE = new NSENetPriceGainersData();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {

                        var appName = app.Key.ToString();
                        if (appName.ToUpper() == "stk_exchng".ToUpper())
                        {
                            objNSE.stk_exchng = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "sc_code".ToUpper())
                        {
                            objNSE.sc_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_code".ToUpper())
                        {
                            objNSE.co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "symbol".ToUpper())
                        {
                            objNSE.symbol = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "isin".ToUpper())
                        {
                            objNSE.isin = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_name".ToUpper())
                        {
                            objNSE.co_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "sc_group".ToUpper())
                        {
                            objNSE.sc_group = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "upd_time".ToUpper())
                        {
                            objNSE.upd_time = Convert.ToDateTime(app.Value.ToString());
                        }
                        else if (appName.ToUpper() == "open_price".ToUpper())
                        {
                            objNSE.open_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "high_price".ToUpper())
                        {
                            objNSE.high_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "low_price".ToUpper())
                        {
                            objNSE.low_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "close_price".ToUpper())
                        {
                            objNSE.close_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bbuy_qty".ToUpper())
                        {
                            objNSE.bbuy_qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bbuy_price".ToUpper())
                        {
                            objNSE.bbuy_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bsell_qty".ToUpper())
                        {
                            objNSE.bsell_qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bsell_price".ToUpper())
                        {
                            objNSE.bsell_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Prevclose".ToUpper())
                        {
                            objNSE.Prevclose = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Perchg".ToUpper())
                        {
                            objNSE.Perchg = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Netchg".ToUpper())
                        {
                            objNSE.Netchg = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "vol_traded".ToUpper())
                        {
                            objNSE.vol_traded = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "pervol".ToUpper())
                        {
                            objNSE.pervol = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "val_traded".ToUpper())
                        {
                            objNSE.val_traded = app.Value.ToString();

                        }
                        else if (appName.ToUpper() == "fiftytwoweekhigh".ToUpper())
                        {
                            objNSE.fiftytwoweekhigh = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "fiftytwoweeklow".ToUpper())
                        {
                            objNSE.fiftytwoweeklow = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "prev_vol_traded".ToUpper())
                        {
                            objNSE.prev_vol_traded = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "prev_value_traded".ToUpper())
                        {
                            objNSE.prev_value_traded = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "prevdate".ToUpper())
                        {
                            objNSE.prevdate = Convert.ToDateTime(app.Value.ToString());
                        }
                        else if (appName.ToUpper() == "lname".ToUpper())
                        {
                            objNSE.lname = app.Value.ToString();
                        }
                    }
                    objGrpList.Add(objNSE);
                }


                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    for (int i = 0; i < objGrpList.Count; i++)
                    {
                        if (objGrpList[i].stk_exchng != "")
                        {
                            sqlstr = "select AutoId, Isnull(stk_exchange,'') As stk_exchange, lname from ";
                            sqlstr = sqlstr + " NSENetPriceGainersData where upd_time=@upd_time And stk_exchange=@stk_exchange and co_code=@co_code";
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@upd_time", objGrpList[i].upd_time);
                            cmd.Parameters.AddWithValue("@stk_exchange", objGrpList[i].stk_exchng);
                            cmd.Parameters.AddWithValue("@co_code", objGrpList[i].co_code);
                            SqlDataReader sdr = cmd.ExecuteReader();
                            while (sdr.Read())
                            {
                                result = true;
                            }
                            sdr.Close();

                            if (result == false)
                            {
                                sqlstr = "insert into NSENetPriceGainersData(stk_exchange,sc_code,co_code,symbol,co_name,sc_group,upd_time,open_price,";
                                sqlstr = sqlstr + " high_price,low_price,close_price,bbuy_qty,bbuy_price,bsell_qty,bsell_price,Prevclose,Perchg,Netchg,vol_traded,";
                                sqlstr = sqlstr + " pervol,val_traded,fiftytwoweekhigh,fiftytwoweeklow,prev_vol_traded,prev_value_traded,prevdate,lname, created_on	)";
                                sqlstr = sqlstr + " values (@stk_exchange,@sc_code,@co_code,@symbol,@co_name,@sc_group,@upd_time,@open_price,";
                                sqlstr = sqlstr + " @high_price,@low_price,@close_price,@bbuy_qty,@bbuy_price,@bsell_qty,@bsell_price,@Prevclose,@Perchg,@Netchg,@vol_traded,";
                                sqlstr = sqlstr + " @pervol,@val_traded,@fiftytwoweekhigh,@fiftytwoweeklow,@prev_vol_traded,@prev_value_traded,@prevdate,@lname, GetDate()	)";
                                cmd.Parameters.Clear();
                                cmd.CommandText = sqlstr;
                                cmd.Parameters.AddWithValue("@stk_exchange", objGrpList[i].stk_exchng);
                                cmd.Parameters.AddWithValue("@sc_code", objGrpList[i].sc_code);
                                cmd.Parameters.AddWithValue("@co_code", objGrpList[i].co_code);
                                cmd.Parameters.AddWithValue("@symbol", objGrpList[i].symbol);
                                //cmd.Parameters.AddWithValue("@isin", isin);
                                cmd.Parameters.AddWithValue("@co_name", objGrpList[i].co_name);
                                cmd.Parameters.AddWithValue("@sc_group", objGrpList[i].sc_group);
                                cmd.Parameters.AddWithValue("@upd_time", objGrpList[i].upd_time);
                                cmd.Parameters.AddWithValue("@open_price", objGrpList[i].open_price);
                                cmd.Parameters.AddWithValue("@high_price", objGrpList[i].high_price);
                                cmd.Parameters.AddWithValue("@low_price", objGrpList[i].low_price);
                                cmd.Parameters.AddWithValue("@close_price", objGrpList[i].close_price);
                                cmd.Parameters.AddWithValue("@bbuy_qty", objGrpList[i].bbuy_qty);
                                cmd.Parameters.AddWithValue("@bbuy_price", objGrpList[i].bbuy_price);
                                cmd.Parameters.AddWithValue("@bsell_qty", objGrpList[i].bsell_qty);
                                cmd.Parameters.AddWithValue("@bsell_price", objGrpList[i].bsell_price);
                                cmd.Parameters.AddWithValue("@Prevclose", objGrpList[i].Prevclose);
                                cmd.Parameters.AddWithValue("@Perchg", objGrpList[i].Perchg);
                                cmd.Parameters.AddWithValue("@Netchg", objGrpList[i].Netchg);
                                cmd.Parameters.AddWithValue("@vol_traded", objGrpList[i].vol_traded);
                                cmd.Parameters.AddWithValue("@pervol", objGrpList[i].pervol);
                                cmd.Parameters.AddWithValue("@val_traded", objGrpList[i].val_traded);
                                cmd.Parameters.AddWithValue("@fiftytwoweekhigh", objGrpList[i].fiftytwoweekhigh);
                                cmd.Parameters.AddWithValue("@fiftytwoweeklow", objGrpList[i].fiftytwoweeklow);
                                cmd.Parameters.AddWithValue("@prev_vol_traded", objGrpList[i].prev_vol_traded);
                                cmd.Parameters.AddWithValue("@prev_value_traded", objGrpList[i].prev_value_traded);
                                cmd.Parameters.AddWithValue("@prevdate", objGrpList[i].prevdate);
                                cmd.Parameters.AddWithValue("@lname", objGrpList[i].lname);
                                cmd.ExecuteNonQuery();

                            }
                            result = false;
                        }
                    }
                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View();
        }


        //[HttpPost]
        //public ActionResult GetNSENetPriceGainersData(string stk_exchange, string sc_code,string co_code,string symbol,string co_name,string sc_group,string upd_time,
        //        string open_price,string high_price,string low_price,string close_price,string bbuy_qty,string bbuy_price,string bsell_qty,string bsell_price,
        //        string prevclose,string prevdate,string perchg,string netchg,string vol_traded,string pervol,string val_traded,string fiftytwoweekhigh,string fiftytwoweeklow,
        //        string prev_vol_traded, string prev_value_traded, string offerprice, string offerqty, string lname)
        //{
        //    try
        //    {
        //        if(bsell_qty == null)
        //        {
        //            bsell_qty = "0";
        //        }
        //        if(bsell_price == null)
        //        {
        //            bsell_price = "0";
        //        }


        //        bool result = false;
        //        gConnection.Open();
        //        string sqlstr = "select AutoId, Isnull(stk_exchange,'') As stk_exchange, lname from ";
        //        sqlstr = sqlstr + " NSENetPriceGainersData where upd_time=@upd_time And stk_exchange=@stk_exchange";
        //        SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
        //        cmd.Parameters.AddWithValue("@upd_time", upd_time);
        //        cmd.Parameters.AddWithValue("@stk_exchange", stk_exchange);
        //        SqlDataReader sdr = cmd.ExecuteReader();
        //        while(sdr.Read())
        //        {
        //            result = true;
        //        }
        //        sdr.Close();

        //        if(result == false)
        //        {
        //            sqlstr = "insert into NSENetPriceGainersData(stk_exchange,sc_code,co_code,symbol,co_name,sc_group,upd_time,open_price,";
        //            sqlstr = sqlstr + " high_price,low_price,close_price,bbuy_qty,bbuy_price,bsell_qty,bsell_price,Prevclose,Perchg,Netchg,vol_traded,";
        //            sqlstr = sqlstr + " pervol,val_traded,fiftytwoweekhigh,fiftytwoweeklow,prev_vol_traded,prev_value_traded,prevdate,lname	)";
        //            sqlstr = sqlstr + " values (@stk_exchange,@sc_code,@co_code,@symbol,@co_name,@sc_group,@upd_time,@open_price,";
        //            sqlstr = sqlstr + " @high_price,@low_price,@close_price,@bbuy_qty,@bbuy_price,@bsell_qty,@bsell_price,@Prevclose,@Perchg,@Netchg,@vol_traded,";
        //            sqlstr = sqlstr + " @pervol,@val_traded,@fiftytwoweekhigh,@fiftytwoweeklow,@prev_vol_traded,@prev_value_traded,@prevdate,@lname	)";
        //            cmd.Parameters.Clear();
        //            cmd.CommandText = sqlstr;
        //            cmd.Parameters.AddWithValue("@stk_exchange", stk_exchange);
        //            cmd.Parameters.AddWithValue("@sc_code", sc_code);
        //            cmd.Parameters.AddWithValue("@co_code", co_code);
        //            cmd.Parameters.AddWithValue("@symbol", symbol);
        //            //cmd.Parameters.AddWithValue("@isin", isin);
        //            cmd.Parameters.AddWithValue("@co_name", co_name);
        //            cmd.Parameters.AddWithValue("@sc_group", sc_group);
        //            cmd.Parameters.AddWithValue("@upd_time", upd_time);
        //            cmd.Parameters.AddWithValue("@open_price", open_price);
        //            cmd.Parameters.AddWithValue("@high_price", high_price);
        //            cmd.Parameters.AddWithValue("@low_price", low_price);
        //            cmd.Parameters.AddWithValue("@close_price", close_price);
        //            cmd.Parameters.AddWithValue("@bbuy_qty", bbuy_qty);
        //            cmd.Parameters.AddWithValue("@bbuy_price", bbuy_price);
        //            cmd.Parameters.AddWithValue("@bsell_qty", bsell_qty);
        //            cmd.Parameters.AddWithValue("@bsell_price", bsell_price);
        //            cmd.Parameters.AddWithValue("@Prevclose", prevclose);
        //            cmd.Parameters.AddWithValue("@Perchg", perchg);
        //            cmd.Parameters.AddWithValue("@Netchg", netchg);
        //            cmd.Parameters.AddWithValue("@vol_traded", vol_traded);
        //            cmd.Parameters.AddWithValue("@pervol", pervol);
        //            cmd.Parameters.AddWithValue("@val_traded", val_traded);
        //            cmd.Parameters.AddWithValue("@fiftytwoweekhigh", fiftytwoweekhigh);
        //            cmd.Parameters.AddWithValue("@fiftytwoweeklow", fiftytwoweeklow);
        //            cmd.Parameters.AddWithValue("@prev_vol_traded", prev_vol_traded);
        //            cmd.Parameters.AddWithValue("@prev_value_traded", prev_value_traded);
        //            cmd.Parameters.AddWithValue("@prevdate", prevdate);
        //            cmd.Parameters.AddWithValue("@lname", lname);
        //            cmd.ExecuteNonQuery();
        //            gConnection.Close();
        //        }
        //        sqlstr = "";
        //    }
        //    catch(Exception ex)
        //    {

        //    }
        //    return View();
        //}


        #endregion
        

        #region NSE Price Losers Data

        [HttpGet]
        public ActionResult GetNSENetPriceLosersData()
        {
            return View();
        }


        [HttpPost]
        public ActionResult GetNSENetPriceLosersData(string result1)
        {
            try
            {
                bool result = false;
                List<NSENetPriceLosersData> objGrpList = new List<NSENetPriceLosersData>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(result1);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    NSENetPriceLosersData objNSE = new NSENetPriceLosersData();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {

                        var appName = app.Key.ToString();
                        if (appName.ToUpper() == "stk_exchng".ToUpper())
                        {
                            objNSE.stk_exchng = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "sc_code".ToUpper())
                        {
                            objNSE.sc_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_code".ToUpper())
                        {
                            objNSE.co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "symbol".ToUpper())
                        {
                            objNSE.symbol = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "isin".ToUpper())
                        {
                            objNSE.isin = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_name".ToUpper())
                        {
                            objNSE.co_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "sc_group".ToUpper())
                        {
                            objNSE.sc_group = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "upd_time".ToUpper())
                        {
                            objNSE.upd_time = Convert.ToDateTime(app.Value.ToString());
                        }
                        else if (appName.ToUpper() == "open_price".ToUpper())
                        {
                            objNSE.open_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "high_price".ToUpper())
                        {
                            objNSE.high_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "low_price".ToUpper())
                        {
                            objNSE.low_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "close_price".ToUpper())
                        {
                            objNSE.close_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bbuy_qty".ToUpper())
                        {
                            objNSE.bbuy_qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bbuy_price".ToUpper())
                        {
                            objNSE.bbuy_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bsell_qty".ToUpper())
                        {
                            objNSE.bsell_qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bsell_price".ToUpper())
                        {
                            objNSE.bsell_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Prevclose".ToUpper())
                        {
                            objNSE.Prevclose = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Perchg".ToUpper())
                        {
                            objNSE.Perchg = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Netchg".ToUpper())
                        {
                            objNSE.Netchg = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "vol_traded".ToUpper())
                        {
                            objNSE.vol_traded = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "pervol".ToUpper())
                        {
                            objNSE.pervol = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "val_traded".ToUpper())
                        {
                            objNSE.val_traded = app.Value.ToString();

                        }
                        else if (appName.ToUpper() == "fiftytwoweekhigh".ToUpper())
                        {
                            objNSE.fiftytwoweekhigh = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "fiftytwoweeklow".ToUpper())
                        {
                            objNSE.fiftytwoweeklow = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "prev_vol_traded".ToUpper())
                        {
                            objNSE.prev_vol_traded = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "prev_value_traded".ToUpper())
                        {
                            objNSE.prev_value_traded = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "prevdate".ToUpper())
                        {
                            objNSE.prevdate = Convert.ToDateTime(app.Value.ToString());
                        }
                        else if (appName.ToUpper() == "lname".ToUpper())
                        {
                            objNSE.lname = app.Value.ToString();
                        }
                    }
                    objGrpList.Add(objNSE);
                }


                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    for (int i = 0; i < objGrpList.Count; i++)
                    {
                        if (objGrpList[i].stk_exchng != "")
                        {
                            sqlstr = "select AutoId, Isnull(stk_exchange,'') As stk_exchange, lname from ";
                            sqlstr = sqlstr + " NSENetPriceLosersData where upd_time=@upd_time And stk_exchange=@stk_exchange and co_code=@co_code";
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@upd_time", objGrpList[i].upd_time);
                            cmd.Parameters.AddWithValue("@stk_exchange", objGrpList[i].stk_exchng);
                            cmd.Parameters.AddWithValue("@co_code", objGrpList[i].co_code);
                            SqlDataReader sdr = cmd.ExecuteReader();
                            while (sdr.Read())
                            {
                                result = true;
                            }
                            sdr.Close();

                            if (result == false)
                            {
                                sqlstr = "insert into NSENetPriceLosersData(stk_exchange,sc_code,co_code,symbol,co_name,sc_group,upd_time,open_price,";
                                sqlstr = sqlstr + " high_price,low_price,close_price,bbuy_qty,bbuy_price,bsell_qty,bsell_price,Prevclose,Perchg,Netchg,vol_traded,";
                                sqlstr = sqlstr + " pervol,val_traded,fiftytwoweekhigh,fiftytwoweeklow,prev_vol_traded,prev_value_traded,prevdate,lname, created_on	)";
                                sqlstr = sqlstr + " values (@stk_exchange,@sc_code,@co_code,@symbol,@co_name,@sc_group,@upd_time,@open_price,";
                                sqlstr = sqlstr + " @high_price,@low_price,@close_price,@bbuy_qty,@bbuy_price,@bsell_qty,@bsell_price,@Prevclose,@Perchg,@Netchg,@vol_traded,";
                                sqlstr = sqlstr + " @pervol,@val_traded,@fiftytwoweekhigh,@fiftytwoweeklow,@prev_vol_traded,@prev_value_traded,@prevdate,@lname, GetDate()	)";
                                cmd.Parameters.Clear();
                                cmd.CommandText = sqlstr;
                                cmd.Parameters.AddWithValue("@stk_exchange", objGrpList[i].stk_exchng);
                                cmd.Parameters.AddWithValue("@sc_code", objGrpList[i].sc_code);
                                cmd.Parameters.AddWithValue("@co_code", objGrpList[i].co_code);
                                cmd.Parameters.AddWithValue("@symbol", objGrpList[i].symbol);
                                //cmd.Parameters.AddWithValue("@isin", isin);
                                cmd.Parameters.AddWithValue("@co_name", objGrpList[i].co_name);
                                cmd.Parameters.AddWithValue("@sc_group", objGrpList[i].sc_group);
                                cmd.Parameters.AddWithValue("@upd_time", objGrpList[i].upd_time);
                                cmd.Parameters.AddWithValue("@open_price", objGrpList[i].open_price);
                                cmd.Parameters.AddWithValue("@high_price", objGrpList[i].high_price);
                                cmd.Parameters.AddWithValue("@low_price", objGrpList[i].low_price);
                                cmd.Parameters.AddWithValue("@close_price", objGrpList[i].close_price);
                                cmd.Parameters.AddWithValue("@bbuy_qty", objGrpList[i].bbuy_qty);
                                cmd.Parameters.AddWithValue("@bbuy_price", objGrpList[i].bbuy_price);
                                cmd.Parameters.AddWithValue("@bsell_qty", objGrpList[i].bsell_qty);
                                cmd.Parameters.AddWithValue("@bsell_price", objGrpList[i].bsell_price);
                                cmd.Parameters.AddWithValue("@Prevclose", objGrpList[i].Prevclose);
                                cmd.Parameters.AddWithValue("@Perchg", objGrpList[i].Perchg);
                                cmd.Parameters.AddWithValue("@Netchg", objGrpList[i].Netchg);
                                cmd.Parameters.AddWithValue("@vol_traded", objGrpList[i].vol_traded);
                                cmd.Parameters.AddWithValue("@pervol", objGrpList[i].pervol);
                                cmd.Parameters.AddWithValue("@val_traded", objGrpList[i].val_traded);
                                cmd.Parameters.AddWithValue("@fiftytwoweekhigh", objGrpList[i].fiftytwoweekhigh);
                                cmd.Parameters.AddWithValue("@fiftytwoweeklow", objGrpList[i].fiftytwoweeklow);
                                cmd.Parameters.AddWithValue("@prev_vol_traded", objGrpList[i].prev_vol_traded);
                                cmd.Parameters.AddWithValue("@prev_value_traded", objGrpList[i].prev_value_traded);
                                cmd.Parameters.AddWithValue("@prevdate", objGrpList[i].prevdate);
                                cmd.Parameters.AddWithValue("@lname", objGrpList[i].lname);
                                cmd.ExecuteNonQuery();

                            }
                            result = false;
                        }
                    }
                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {

            }
            return View();
        }


        //[HttpPost]
        //public ActionResult GetNSENetPriceLosersData(string stk_exchange, string sc_code, string co_code, string symbol, string co_name, string sc_group, string upd_time,
        //        string open_price, string high_price, string low_price, string close_price, string bbuy_qty, string bbuy_price, string bsell_qty, string bsell_price,
        //        string prevclose, string prevdate, string perchg, string netchg, string vol_traded, string pervol, string val_traded, string fiftytwoweekhigh, string fiftytwoweeklow,
        //        string prev_vol_traded, string prev_value_traded, string offerprice, string offerqty, string lname)
        //{
        //    try
        //    {
        //        if (bsell_qty == null)
        //        {
        //            bsell_qty = "0";
        //        }
        //        if (bsell_price == null)
        //        {
        //            bsell_price = "0";
        //        }


        //        bool result = false;
        //        gConnection.Open();
        //        string sqlstr = "select AutoId, Isnull(stk_exchange,'') As stk_exchange, lname from ";
        //        sqlstr = sqlstr + " NSENetPriceLosersData where upd_time=@upd_time And stk_exchange=@stk_exchange";
        //        SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
        //        cmd.Parameters.AddWithValue("@upd_time", upd_time);
        //        cmd.Parameters.AddWithValue("@stk_exchange", stk_exchange);
        //        SqlDataReader sdr = cmd.ExecuteReader();
        //        while (sdr.Read())
        //        {
        //            result = true;
        //        }
        //        sdr.Close();

        //        if (result == false)
        //        {
        //            sqlstr = "insert into NSENetPriceLosersData(stk_exchange,sc_code,co_code,symbol,co_name,sc_group,upd_time,open_price,";
        //            sqlstr = sqlstr + " high_price,low_price,close_price,bbuy_qty,bbuy_price,bsell_qty,bsell_price,Prevclose,Perchg,Netchg,vol_traded,";
        //            sqlstr = sqlstr + " pervol,val_traded,fiftytwoweekhigh,fiftytwoweeklow,prev_vol_traded,prev_value_traded,prevdate,lname	)";
        //            sqlstr = sqlstr + " values (@stk_exchange,@sc_code,@co_code,@symbol,@co_name,@sc_group,@upd_time,@open_price,";
        //            sqlstr = sqlstr + " @high_price,@low_price,@close_price,@bbuy_qty,@bbuy_price,@bsell_qty,@bsell_price,@Prevclose,@Perchg,@Netchg,@vol_traded,";
        //            sqlstr = sqlstr + " @pervol,@val_traded,@fiftytwoweekhigh,@fiftytwoweeklow,@prev_vol_traded,@prev_value_traded,@prevdate,@lname	)";
        //            cmd.Parameters.Clear();
        //            cmd.CommandText = sqlstr;
        //            cmd.Parameters.AddWithValue("@stk_exchange", stk_exchange);
        //            cmd.Parameters.AddWithValue("@sc_code", sc_code);
        //            cmd.Parameters.AddWithValue("@co_code", co_code);
        //            cmd.Parameters.AddWithValue("@symbol", symbol);
        //            //cmd.Parameters.AddWithValue("@isin", isin);
        //            cmd.Parameters.AddWithValue("@co_name", co_name);
        //            cmd.Parameters.AddWithValue("@sc_group", sc_group);
        //            cmd.Parameters.AddWithValue("@upd_time", upd_time);
        //            cmd.Parameters.AddWithValue("@open_price", open_price);
        //            cmd.Parameters.AddWithValue("@high_price", high_price);
        //            cmd.Parameters.AddWithValue("@low_price", low_price);
        //            cmd.Parameters.AddWithValue("@close_price", close_price);
        //            cmd.Parameters.AddWithValue("@bbuy_qty", bbuy_qty);
        //            cmd.Parameters.AddWithValue("@bbuy_price", bbuy_price);
        //            cmd.Parameters.AddWithValue("@bsell_qty", bsell_qty);
        //            cmd.Parameters.AddWithValue("@bsell_price", bsell_price);
        //            cmd.Parameters.AddWithValue("@Prevclose", prevclose);
        //            cmd.Parameters.AddWithValue("@Perchg", perchg);
        //            cmd.Parameters.AddWithValue("@Netchg", netchg);
        //            cmd.Parameters.AddWithValue("@vol_traded", vol_traded);
        //            cmd.Parameters.AddWithValue("@pervol", pervol);
        //            cmd.Parameters.AddWithValue("@val_traded", val_traded);
        //            cmd.Parameters.AddWithValue("@fiftytwoweekhigh", fiftytwoweekhigh);
        //            cmd.Parameters.AddWithValue("@fiftytwoweeklow", fiftytwoweeklow);
        //            cmd.Parameters.AddWithValue("@prev_vol_traded", prev_vol_traded);
        //            cmd.Parameters.AddWithValue("@prev_value_traded", prev_value_traded);
        //            cmd.Parameters.AddWithValue("@prevdate", prevdate);
        //            cmd.Parameters.AddWithValue("@lname", lname);
        //            cmd.ExecuteNonQuery();
        //            gConnection.Close();
        //        }
        //        sqlstr = "";
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return View();
        //}


        #endregion




        #region BSE Price Gainers Data

        [HttpGet]
        public ActionResult GetBSENetPriceGainersData()
        {
            return View();
        }


        [HttpPost]
        public ActionResult GetBSENetPriceGainersData(string result1)
        {
            try
            {
                bool result = false;
                List<BSENetPriceGainersData> objGrpList = new List<BSENetPriceGainersData>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(result1);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    BSENetPriceGainersData objBSE = new BSENetPriceGainersData();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {

                        var appName = app.Key.ToString();
                        if (appName.ToUpper() == "stk_exchange".ToUpper())
                        {
                            objBSE.stk_exchange = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "sc_code".ToUpper())
                        {
                            objBSE.sc_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_code".ToUpper())
                        {
                            objBSE.co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "symbol".ToUpper())
                        {
                            objBSE.symbol = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "isin".ToUpper())
                        {
                            objBSE.isin = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_name".ToUpper())
                        {
                            objBSE.co_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "sc_group".ToUpper())
                        {
                            objBSE.sc_group = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "upd_time".ToUpper())
                        {
                            objBSE.upd_time = Convert.ToDateTime(app.Value.ToString());
                        }
                        else if (appName.ToUpper() == "open_price".ToUpper())
                        {
                            objBSE.open_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "high_price".ToUpper())
                        {
                            objBSE.high_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "low_price".ToUpper())
                        {
                            objBSE.low_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "close_price".ToUpper())
                        {
                            objBSE.close_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bbuy_qty".ToUpper())
                        {
                            objBSE.bbuy_qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bbuy_price".ToUpper())
                        {
                            objBSE.bbuy_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bsell_qty".ToUpper())
                        {
                            objBSE.bsell_qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bsell_price".ToUpper())
                        {
                            objBSE.bsell_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Prevclose".ToUpper())
                        {
                            objBSE.Prevclose = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Perchg".ToUpper())
                        {
                            objBSE.Perchg = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Netchg".ToUpper())
                        {
                            objBSE.Netchg = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "vol_traded".ToUpper())
                        {
                            objBSE.vol_traded = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "pervol".ToUpper())
                        {
                            objBSE.pervol = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "val_traded".ToUpper())
                        {
                            objBSE.val_traded = app.Value.ToString();

                        }
                        else if (appName.ToUpper() == "fiftytwoweekhigh".ToUpper())
                        {
                            objBSE.fiftytwoweekhigh = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "fiftytwoweeklow".ToUpper())
                        {
                            objBSE.fiftytwoweeklow = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "prev_vol_traded".ToUpper())
                        {
                            objBSE.prev_vol_traded = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "prev_value_traded".ToUpper())
                        {
                            objBSE.prev_value_traded = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "prevdate".ToUpper())
                        {
                            objBSE.prevdate = Convert.ToDateTime(app.Value.ToString());
                        }
                        else if (appName.ToUpper() == "lname".ToUpper())
                        {
                            objBSE.lname = app.Value.ToString();
                        }
                    }
                    objGrpList.Add(objBSE);
                }


                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    for (int i = 0; i < objGrpList.Count; i++)
                    {
                        if (objGrpList[i].stk_exchange != "")
                        {
                            sqlstr = "select AutoId, Isnull(stk_exchange,'') As stk_exchange, lname from ";
                            sqlstr = sqlstr + " BSENetPriceGainersData where upd_time=@upd_time And stk_exchange=@stk_exchange and co_code=@co_code";
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@upd_time", objGrpList[i].upd_time);
                            cmd.Parameters.AddWithValue("@stk_exchange", objGrpList[i].stk_exchange);
                            cmd.Parameters.AddWithValue("@co_code", objGrpList[i].co_code);
                            SqlDataReader sdr = cmd.ExecuteReader();
                            while (sdr.Read())
                            {
                                result = true;
                            }
                            sdr.Close();

                            if (result == false)
                            {
                                sqlstr = "insert into BSENetPriceGainersData(stk_exchange,sc_code,co_code,symbol,co_name,sc_group,upd_time,open_price,";
                                sqlstr = sqlstr + " high_price,low_price,close_price,bbuy_qty,bbuy_price,bsell_qty,bsell_price,Prevclose,Perchg,Netchg,vol_traded,";
                                sqlstr = sqlstr + " pervol,val_traded,fiftytwoweekhigh,fiftytwoweeklow,prev_vol_traded,prev_value_traded,prevdate,lname, created_on	)";
                                sqlstr = sqlstr + " values (@stk_exchange,@sc_code,@co_code,@symbol,@co_name,@sc_group,@upd_time,@open_price,";
                                sqlstr = sqlstr + " @high_price,@low_price,@close_price,@bbuy_qty,@bbuy_price,@bsell_qty,@bsell_price,@Prevclose,@Perchg,@Netchg,@vol_traded,";
                                sqlstr = sqlstr + " @pervol,@val_traded,@fiftytwoweekhigh,@fiftytwoweeklow,@prev_vol_traded,@prev_value_traded,@prevdate,@lname, GetDate()	)";
                                cmd.Parameters.Clear();
                                cmd.CommandText = sqlstr;
                                cmd.Parameters.AddWithValue("@stk_exchange", objGrpList[i].stk_exchange);
                                cmd.Parameters.AddWithValue("@sc_code", objGrpList[i].sc_code);
                                cmd.Parameters.AddWithValue("@co_code", objGrpList[i].co_code);
                                cmd.Parameters.AddWithValue("@symbol", objGrpList[i].symbol);
                                //cmd.Parameters.AddWithValue("@isin", isin);
                                cmd.Parameters.AddWithValue("@co_name", objGrpList[i].co_name);
                                cmd.Parameters.AddWithValue("@sc_group", objGrpList[i].sc_group);
                                cmd.Parameters.AddWithValue("@upd_time", objGrpList[i].upd_time);
                                cmd.Parameters.AddWithValue("@open_price", objGrpList[i].open_price);
                                cmd.Parameters.AddWithValue("@high_price", objGrpList[i].high_price);
                                cmd.Parameters.AddWithValue("@low_price", objGrpList[i].low_price);
                                cmd.Parameters.AddWithValue("@close_price", objGrpList[i].close_price);
                                cmd.Parameters.AddWithValue("@bbuy_qty", objGrpList[i].bbuy_qty);
                                cmd.Parameters.AddWithValue("@bbuy_price", objGrpList[i].bbuy_price);
                                cmd.Parameters.AddWithValue("@bsell_qty", objGrpList[i].bsell_qty);
                                cmd.Parameters.AddWithValue("@bsell_price", objGrpList[i].bsell_price);
                                cmd.Parameters.AddWithValue("@Prevclose", objGrpList[i].Prevclose);
                                cmd.Parameters.AddWithValue("@Perchg", objGrpList[i].Perchg);
                                cmd.Parameters.AddWithValue("@Netchg", objGrpList[i].Netchg);
                                cmd.Parameters.AddWithValue("@vol_traded", objGrpList[i].vol_traded);
                                cmd.Parameters.AddWithValue("@pervol", objGrpList[i].pervol);
                                cmd.Parameters.AddWithValue("@val_traded", objGrpList[i].val_traded);
                                cmd.Parameters.AddWithValue("@fiftytwoweekhigh", objGrpList[i].fiftytwoweekhigh);
                                cmd.Parameters.AddWithValue("@fiftytwoweeklow", objGrpList[i].fiftytwoweeklow);
                                cmd.Parameters.AddWithValue("@prev_vol_traded", objGrpList[i].prev_vol_traded);
                                cmd.Parameters.AddWithValue("@prev_value_traded", objGrpList[i].prev_value_traded);
                                cmd.Parameters.AddWithValue("@prevdate", objGrpList[i].prevdate);
                                cmd.Parameters.AddWithValue("@lname", objGrpList[i].lname);
                                cmd.ExecuteNonQuery();

                            }
                            result = false;
                        }
                    }
                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {

            }
            return View();
        }



        //[HttpPost]
        //public ActionResult GetBSENetPriceGainersData(string stk_exchange, string sc_code, string co_code, string symbol, string co_name, string sc_group, string upd_time,
        //        string open_price, string high_price, string low_price, string close_price, string bbuy_qty, string bbuy_price, string bsell_qty, string bsell_price,
        //        string prevclose, string prevdate, string perchg, string netchg, string vol_traded, string pervol, string val_traded, string fiftytwoweekhigh, string fiftytwoweeklow,
        //        string prev_vol_traded, string prev_value_traded, string offerprice, string offerqty, string lname)
        //{
        //    try
        //    {
        //        if (bsell_qty == null)
        //        {
        //            bsell_qty = "0";
        //        }
        //        if (bsell_price == null)
        //        {
        //            bsell_price = "0";
        //        }


        //        bool result = false;
        //        gConnection.Open();
        //        string sqlstr = "select AutoId, Isnull(stk_exchange,'') As stk_exchange, lname from ";
        //        sqlstr = sqlstr + " BSENetPriceGainersData where upd_time=@upd_time And stk_exchange=@stk_exchange";
        //        SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
        //        cmd.Parameters.AddWithValue("@upd_time", upd_time);
        //        cmd.Parameters.AddWithValue("@stk_exchange", stk_exchange);
        //        SqlDataReader sdr = cmd.ExecuteReader();
        //        while (sdr.Read())
        //        {
        //            result = true;
        //        }
        //        sdr.Close();

        //        if (result == false)
        //        {
        //            sqlstr = "insert into BSENetPriceGainersData(stk_exchange,sc_code,co_code,symbol,co_name,sc_group,upd_time,open_price,";
        //            sqlstr = sqlstr + " high_price,low_price,close_price,bbuy_qty,bbuy_price,bsell_qty,bsell_price,Prevclose,Perchg,Netchg,vol_traded,";
        //            sqlstr = sqlstr + " pervol,val_traded,fiftytwoweekhigh,fiftytwoweeklow,prev_vol_traded,prev_value_traded,prevdate,lname	)";
        //            sqlstr = sqlstr + " values (@stk_exchange,@sc_code,@co_code,@symbol,@co_name,@sc_group,@upd_time,@open_price,";
        //            sqlstr = sqlstr + " @high_price,@low_price,@close_price,@bbuy_qty,@bbuy_price,@bsell_qty,@bsell_price,@Prevclose,@Perchg,@Netchg,@vol_traded,";
        //            sqlstr = sqlstr + " @pervol,@val_traded,@fiftytwoweekhigh,@fiftytwoweeklow,@prev_vol_traded,@prev_value_traded,@prevdate,@lname	)";
        //            cmd.Parameters.Clear();
        //            cmd.CommandText = sqlstr;
        //            cmd.Parameters.AddWithValue("@stk_exchange", stk_exchange);
        //            cmd.Parameters.AddWithValue("@sc_code", sc_code);
        //            cmd.Parameters.AddWithValue("@co_code", co_code);
        //            cmd.Parameters.AddWithValue("@symbol", symbol);
        //            //cmd.Parameters.AddWithValue("@isin", isin);
        //            cmd.Parameters.AddWithValue("@co_name", co_name);
        //            cmd.Parameters.AddWithValue("@sc_group", sc_group);
        //            cmd.Parameters.AddWithValue("@upd_time", upd_time);
        //            cmd.Parameters.AddWithValue("@open_price", open_price);
        //            cmd.Parameters.AddWithValue("@high_price", high_price);
        //            cmd.Parameters.AddWithValue("@low_price", low_price);
        //            cmd.Parameters.AddWithValue("@close_price", close_price);
        //            cmd.Parameters.AddWithValue("@bbuy_qty", bbuy_qty);
        //            cmd.Parameters.AddWithValue("@bbuy_price", bbuy_price);
        //            cmd.Parameters.AddWithValue("@bsell_qty", bsell_qty);
        //            cmd.Parameters.AddWithValue("@bsell_price", bsell_price);
        //            cmd.Parameters.AddWithValue("@Prevclose", prevclose);
        //            cmd.Parameters.AddWithValue("@Perchg", perchg);
        //            cmd.Parameters.AddWithValue("@Netchg", netchg);
        //            cmd.Parameters.AddWithValue("@vol_traded", vol_traded);
        //            cmd.Parameters.AddWithValue("@pervol", pervol);
        //            cmd.Parameters.AddWithValue("@val_traded", val_traded);
        //            cmd.Parameters.AddWithValue("@fiftytwoweekhigh", fiftytwoweekhigh);
        //            cmd.Parameters.AddWithValue("@fiftytwoweeklow", fiftytwoweeklow);
        //            cmd.Parameters.AddWithValue("@prev_vol_traded", prev_vol_traded);
        //            cmd.Parameters.AddWithValue("@prev_value_traded", prev_value_traded);
        //            cmd.Parameters.AddWithValue("@prevdate", prevdate);
        //            cmd.Parameters.AddWithValue("@lname", lname);
        //            cmd.ExecuteNonQuery();
        //            gConnection.Close();
        //        }
        //        sqlstr = "";
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return View();
        //}

        #endregion


        #region BSE Price Losers Data

        [HttpGet]
        public ActionResult GetBSENetPriceLosersData()
        {
            return View();
        }


        [HttpPost]
        public ActionResult GetBSENetPriceLosersData(string result1)
        {
            try
            {                
                bool result = false;
                List<BSENetPriceLosersData> objGrpList = new List<BSENetPriceLosersData>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(result1);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    BSENetPriceLosersData objBSE = new BSENetPriceLosersData();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {

                        var appName = app.Key.ToString();
                        if (appName.ToUpper() == "stk_exchange".ToUpper())
                        {
                            objBSE.stk_exchange = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "sc_code".ToUpper())
                        {
                            objBSE.sc_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_code".ToUpper())
                        {
                            objBSE.co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "symbol".ToUpper())
                        {
                            objBSE.symbol = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "isin".ToUpper())
                        {
                            objBSE.isin = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_name".ToUpper())
                        {
                            objBSE.co_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "sc_group".ToUpper())
                        {
                            objBSE.sc_group = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "upd_time".ToUpper())
                        {
                            objBSE.upd_time = Convert.ToDateTime(app.Value.ToString());
                        }
                        else if (appName.ToUpper() == "open_price".ToUpper())
                        {
                            objBSE.open_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "high_price".ToUpper())
                        {
                            objBSE.high_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "low_price".ToUpper())
                        {
                            objBSE.low_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "close_price".ToUpper())
                        {
                            objBSE.close_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bbuy_qty".ToUpper())
                        {
                            objBSE.bbuy_qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bbuy_price".ToUpper())
                        {
                            objBSE.bbuy_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bsell_qty".ToUpper())
                        {
                            objBSE.bsell_qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bsell_price".ToUpper())
                        {
                            objBSE.bsell_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Prevclose".ToUpper())
                        {
                            objBSE.Prevclose = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Perchg".ToUpper())
                        {
                            objBSE.Perchg = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Netchg".ToUpper())
                        {
                            objBSE.Netchg = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "vol_traded".ToUpper())
                        {
                            objBSE.vol_traded = app.Value.ToString();                            
                        }
                        else if (appName.ToUpper() == "pervol".ToUpper())
                        {
                            objBSE.pervol = app.Value.ToString();                            
                        }
                        else if (appName.ToUpper() == "val_traded".ToUpper())
                        {
                            objBSE.val_traded = app.Value.ToString();                            

                        }
                        else if (appName.ToUpper() == "fiftytwoweekhigh".ToUpper())
                        {
                            objBSE.fiftytwoweekhigh = app.Value.ToString();                            
                        }
                        else if (appName.ToUpper() == "fiftytwoweeklow".ToUpper())
                        {
                            objBSE.fiftytwoweeklow = app.Value.ToString();                            
                        }
                        else if (appName.ToUpper() == "prev_vol_traded".ToUpper())
                        {
                            objBSE.prev_vol_traded = app.Value.ToString();                            
                        }
                        else if (appName.ToUpper() == "prev_value_traded".ToUpper())
                        {
                            objBSE.prev_value_traded = app.Value.ToString();                            
                        }
                        else if (appName.ToUpper() == "prevdate".ToUpper())
                        {
                            objBSE.prevdate = Convert.ToDateTime(app.Value.ToString());                       
                        }
                        else if (appName.ToUpper() == "lname".ToUpper())
                        {
                            objBSE.lname = app.Value.ToString();                            
                        }                       
                    }
                    objGrpList.Add(objBSE);
                }


                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    for (int i = 0; i < objGrpList.Count; i++)
                    {
                        if (objGrpList[i].stk_exchange != "")
                        {
                            sqlstr = "select AutoId, Isnull(stk_exchange,'') As stk_exchange, lname from ";
                            sqlstr = sqlstr + " BSENetPriceLosersData where upd_time=@upd_time And stk_exchange=@stk_exchange and co_code=@co_code";
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@upd_time", objGrpList[i].upd_time);
                            cmd.Parameters.AddWithValue("@stk_exchange", objGrpList[i].stk_exchange);
                            cmd.Parameters.AddWithValue("@co_code", objGrpList[i].co_code);
                            SqlDataReader sdr = cmd.ExecuteReader();
                            while (sdr.Read())
                            {
                                result = true;
                            }
                            sdr.Close();

                            if (result == false)
                            {
                                        sqlstr = "insert into BSENetPriceLosersData(stk_exchange,sc_code,co_code,symbol,co_name,sc_group,upd_time,open_price,";
                                        sqlstr = sqlstr + " high_price,low_price,close_price,bbuy_qty,bbuy_price,bsell_qty,bsell_price,Prevclose,Perchg,Netchg,vol_traded,";
                                        sqlstr = sqlstr + " pervol,val_traded,fiftytwoweekhigh,fiftytwoweeklow,prev_vol_traded,prev_value_traded,prevdate,lname, created_on	)";
                                        sqlstr = sqlstr + " values (@stk_exchange,@sc_code,@co_code,@symbol,@co_name,@sc_group,@upd_time,@open_price,";
                                        sqlstr = sqlstr + " @high_price,@low_price,@close_price,@bbuy_qty,@bbuy_price,@bsell_qty,@bsell_price,@Prevclose,@Perchg,@Netchg,@vol_traded,";
                                        sqlstr = sqlstr + " @pervol,@val_traded,@fiftytwoweekhigh,@fiftytwoweeklow,@prev_vol_traded,@prev_value_traded,@prevdate,@lname, GetDate()	)";
                                        cmd.Parameters.Clear();
                                        cmd.CommandText = sqlstr;
                                        cmd.Parameters.AddWithValue("@stk_exchange", objGrpList[i].stk_exchange);
                                        cmd.Parameters.AddWithValue("@sc_code", objGrpList[i].sc_code);
                                        cmd.Parameters.AddWithValue("@co_code", objGrpList[i].co_code);
                                        cmd.Parameters.AddWithValue("@symbol", objGrpList[i].symbol);
                                        //cmd.Parameters.AddWithValue("@isin", isin);
                                        cmd.Parameters.AddWithValue("@co_name", objGrpList[i].co_name);
                                        cmd.Parameters.AddWithValue("@sc_group", objGrpList[i].sc_group);
                                        cmd.Parameters.AddWithValue("@upd_time", objGrpList[i].upd_time);
                                        cmd.Parameters.AddWithValue("@open_price", objGrpList[i].open_price);
                                        cmd.Parameters.AddWithValue("@high_price", objGrpList[i].high_price);
                                        cmd.Parameters.AddWithValue("@low_price", objGrpList[i].low_price);
                                        cmd.Parameters.AddWithValue("@close_price", objGrpList[i].close_price);
                                        cmd.Parameters.AddWithValue("@bbuy_qty", objGrpList[i].bbuy_qty);
                                        cmd.Parameters.AddWithValue("@bbuy_price", objGrpList[i].bbuy_price);
                                        cmd.Parameters.AddWithValue("@bsell_qty", objGrpList[i].bsell_qty);
                                        cmd.Parameters.AddWithValue("@bsell_price", objGrpList[i].bsell_price);
                                        cmd.Parameters.AddWithValue("@Prevclose", objGrpList[i].Prevclose);
                                        cmd.Parameters.AddWithValue("@Perchg", objGrpList[i].Perchg);
                                        cmd.Parameters.AddWithValue("@Netchg", objGrpList[i].Netchg);
                                        cmd.Parameters.AddWithValue("@vol_traded", objGrpList[i].vol_traded);
                                        cmd.Parameters.AddWithValue("@pervol", objGrpList[i].pervol);
                                        cmd.Parameters.AddWithValue("@val_traded", objGrpList[i].val_traded);
                                        cmd.Parameters.AddWithValue("@fiftytwoweekhigh", objGrpList[i].fiftytwoweekhigh);
                                        cmd.Parameters.AddWithValue("@fiftytwoweeklow", objGrpList[i].fiftytwoweeklow);
                                        cmd.Parameters.AddWithValue("@prev_vol_traded", objGrpList[i].prev_vol_traded);
                                        cmd.Parameters.AddWithValue("@prev_value_traded", objGrpList[i].prev_value_traded);
                                        cmd.Parameters.AddWithValue("@prevdate", objGrpList[i].prevdate);
                                        cmd.Parameters.AddWithValue("@lname", objGrpList[i].lname);
                                        cmd.ExecuteNonQuery();
                            
                            }
                            result = false;
                        }
                    }
                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {

            }
            return View();
        }



        //[HttpPost]
        //public ActionResult GetBSENetPriceLosersData(string stk_exchange, string sc_code, string co_code, string symbol, string co_name, string sc_group, string upd_time,
        //        string open_price, string high_price, string low_price, string close_price, string bbuy_qty, string bbuy_price, string bsell_qty, string bsell_price,
        //        string prevclose, string prevdate, string perchg, string netchg, string vol_traded, string pervol, string val_traded, string fiftytwoweekhigh, string fiftytwoweeklow,
        //        string prev_vol_traded, string prev_value_traded, string offerprice, string offerqty, string lname)
        //{
        //    try
        //    {
        //        if (bsell_qty == null)
        //        {
        //            bsell_qty = "0";
        //        }
        //        if (bsell_price == null)
        //        {
        //            bsell_price = "0";
        //        }


        //        bool result = false;
        //        gConnection.Open();
        //        string sqlstr = "select AutoId, Isnull(stk_exchange,'') As stk_exchange, lname from ";
        //        sqlstr = sqlstr + " BSENetPriceLosersData where upd_time=@upd_time And stk_exchange=@stk_exchange";
        //        SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
        //        cmd.Parameters.AddWithValue("@upd_time", upd_time);
        //        cmd.Parameters.AddWithValue("@stk_exchange", stk_exchange);
        //        SqlDataReader sdr = cmd.ExecuteReader();
        //        while (sdr.Read())
        //        {
        //            result = true;
        //        }
        //        sdr.Close();

        //        if (result == false)
        //        {
        //            sqlstr = "insert into BSENetPriceLosersData(stk_exchange,sc_code,co_code,symbol,co_name,sc_group,upd_time,open_price,";
        //            sqlstr = sqlstr + " high_price,low_price,close_price,bbuy_qty,bbuy_price,bsell_qty,bsell_price,Prevclose,Perchg,Netchg,vol_traded,";
        //            sqlstr = sqlstr + " pervol,val_traded,fiftytwoweekhigh,fiftytwoweeklow,prev_vol_traded,prev_value_traded,prevdate,lname	)";
        //            sqlstr = sqlstr + " values (@stk_exchange,@sc_code,@co_code,@symbol,@co_name,@sc_group,@upd_time,@open_price,";
        //            sqlstr = sqlstr + " @high_price,@low_price,@close_price,@bbuy_qty,@bbuy_price,@bsell_qty,@bsell_price,@Prevclose,@Perchg,@Netchg,@vol_traded,";
        //            sqlstr = sqlstr + " @pervol,@val_traded,@fiftytwoweekhigh,@fiftytwoweeklow,@prev_vol_traded,@prev_value_traded,@prevdate,@lname	)";
        //            cmd.Parameters.Clear();
        //            cmd.CommandText = sqlstr;
        //            cmd.Parameters.AddWithValue("@stk_exchange", stk_exchange);
        //            cmd.Parameters.AddWithValue("@sc_code", sc_code);
        //            cmd.Parameters.AddWithValue("@co_code", co_code);
        //            cmd.Parameters.AddWithValue("@symbol", symbol);
        //            //cmd.Parameters.AddWithValue("@isin", isin);
        //            cmd.Parameters.AddWithValue("@co_name", co_name);
        //            cmd.Parameters.AddWithValue("@sc_group", sc_group);
        //            cmd.Parameters.AddWithValue("@upd_time", upd_time);
        //            cmd.Parameters.AddWithValue("@open_price", open_price);
        //            cmd.Parameters.AddWithValue("@high_price", high_price);
        //            cmd.Parameters.AddWithValue("@low_price", low_price);
        //            cmd.Parameters.AddWithValue("@close_price", close_price);
        //            cmd.Parameters.AddWithValue("@bbuy_qty", bbuy_qty);
        //            cmd.Parameters.AddWithValue("@bbuy_price", bbuy_price);
        //            cmd.Parameters.AddWithValue("@bsell_qty", bsell_qty);
        //            cmd.Parameters.AddWithValue("@bsell_price", bsell_price);
        //            cmd.Parameters.AddWithValue("@Prevclose", prevclose);
        //            cmd.Parameters.AddWithValue("@Perchg", perchg);
        //            cmd.Parameters.AddWithValue("@Netchg", netchg);
        //            cmd.Parameters.AddWithValue("@vol_traded", vol_traded);
        //            cmd.Parameters.AddWithValue("@pervol", pervol);
        //            cmd.Parameters.AddWithValue("@val_traded", val_traded);
        //            cmd.Parameters.AddWithValue("@fiftytwoweekhigh", fiftytwoweekhigh);
        //            cmd.Parameters.AddWithValue("@fiftytwoweeklow", fiftytwoweeklow);
        //            cmd.Parameters.AddWithValue("@prev_vol_traded", prev_vol_traded);
        //            cmd.Parameters.AddWithValue("@prev_value_traded", prev_value_traded);
        //            cmd.Parameters.AddWithValue("@prevdate", prevdate);
        //            cmd.Parameters.AddWithValue("@lname", lname);
        //            cmd.ExecuteNonQuery();
        //            gConnection.Close();
        //        }
        //        sqlstr = "";
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return View();
        //}

        #endregion




        #region NSE Index Movers

        [HttpGet]
        public ActionResult GetNSEIndexMovers()
        {
            return View();
        }


        [HttpPost]
        public ActionResult GetNSEIndexMovers(string result1)
        {
            try
            {
                bool result = false;
                List<NSEIndexMovers> objGrpList = new List<NSEIndexMovers>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(result1);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    NSEIndexMovers objNSE = new NSEIndexMovers();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        

                        var appName = app.Key.ToString();
                        if(appName.ToUpper() == "co_code".ToUpper())
                        {
                            objNSE.Co_Code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "comp_name".ToUpper())
                        {
                            objNSE.comp_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "upd_time".ToUpper())
                        {
                            objNSE.upd_time = Convert.ToDateTime(app.Value.ToString());
                        }
                        else if (appName.ToUpper() == "open_price".ToUpper())
                        {
                            objNSE.open_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "high_price".ToUpper())
                        {
                            objNSE.high_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "low_price".ToUpper())
                        {
                            objNSE.low_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "price".ToUpper())
                        {
                            objNSE.price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "pricediff".ToUpper())
                        {
                            objNSE.pricediff = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "change".ToUpper())
                        {
                            objNSE.change = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Contribution".ToUpper())
                        {
                            objNSE.Contribution = app.Value.ToString();
                        }
                        
                        
                        var description = app.Value.ToString();                        
                    }
                    objGrpList.Add(objNSE);
                }




                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    for (int i = 0; i < objGrpList.Count; i++)
                    {
                        if (objGrpList[i].Co_Code != "")
                        {
                            sqlstr = "select AutoId, Co_code from NSEIndexMovers where Co_Code=@Co_code and upd_time=@upd_time";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@Co_code", objGrpList[i].Co_Code);
                            cmd.Parameters.AddWithValue("@upd_time", objGrpList[i].upd_time);
                            SqlDataReader sdr = cmd.ExecuteReader();
                            while (sdr.Read())
                            {
                                result = true;
                            }
                            sdr.Close();

                            if (result == false)
                            {
                                sqlstr = "Insert Into NSEIndexMovers(Co_Code,comp_name,upd_time,open_price,";
                                sqlstr = sqlstr + " high_price,low_price,price,pricediff,change,Contribution,created_on)";
                                sqlstr = sqlstr + " values(@Co_Code,@comp_name,@upd_time,@open_price,";
                                sqlstr = sqlstr + " @high_price,@low_price,@price,@pricediff,@change,@Contribution,GETDATE())";
                                cmd.Parameters.Clear();
                                cmd.CommandText = sqlstr;
                                cmd.Parameters.AddWithValue("@Co_Code", objGrpList[i].Co_Code);
                                cmd.Parameters.AddWithValue("@comp_name", objGrpList[i].comp_name);
                                cmd.Parameters.AddWithValue("@upd_time", objGrpList[i].upd_time);
                                cmd.Parameters.AddWithValue("@open_price", objGrpList[i].open_price);
                                cmd.Parameters.AddWithValue("@high_price", objGrpList[i].high_price);
                                cmd.Parameters.AddWithValue("@low_price", objGrpList[i].low_price);
                                cmd.Parameters.AddWithValue("@price", objGrpList[i].price);
                                cmd.Parameters.AddWithValue("@pricediff", objGrpList[i].pricediff);
                                cmd.Parameters.AddWithValue("@change", objGrpList[i].change);
                                cmd.Parameters.AddWithValue("@Contribution", objGrpList[i].Contribution);

                                cmd.ExecuteNonQuery();
                            }
                            result = false;
                        }
                    }
                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            
                
                //gConnection.Open();
                //string sqlstr = "select AutoId, Co_code from NSEIndexMovers where Co_Code=@Co_code and upd_time=@upd_time";
                //SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                //cmd.Parameters.AddWithValue("@Co_code", Co_Code);
                //cmd.Parameters.AddWithValue("@upd_time", upd_time);
                //SqlDataReader sdr = cmd.ExecuteReader();
                //while (sdr.Read())
                //{
                //    result = true;
                //}
                //sdr.Close();

                //if (result == false)
                //{
                //    sqlstr = "Insert Into NSEIndexMovers(Co_Code,comp_name,upd_time,open_price,";
                //    sqlstr = sqlstr + " high_price,low_price,price,pricediff,change,Contribution,created_on)";
                //    sqlstr = sqlstr + " values(@Co_Code,@comp_name,@upd_time,@open_price,";
                //    sqlstr = sqlstr + " @high_price,@low_price,@price,@pricediff,@change,@Contribution,GETDATE())";
                //    cmd.Parameters.Clear();
                //    cmd.CommandText = sqlstr;
                //    cmd.Parameters.AddWithValue("@Co_Code", Co_Code);
                //    cmd.Parameters.AddWithValue("@comp_name", comp_name);
                //    cmd.Parameters.AddWithValue("@upd_time", upd_time);
                //    cmd.Parameters.AddWithValue("@open_price", open_price);
                //    cmd.Parameters.AddWithValue("@high_price", High_Price);
                //    cmd.Parameters.AddWithValue("@low_price", Low_Price);
                //    cmd.Parameters.AddWithValue("@price", price);
                //    cmd.Parameters.AddWithValue("@pricediff", Pricediff);
                //    cmd.Parameters.AddWithValue("@change", change);
                //    cmd.Parameters.AddWithValue("@Contribution", Contribution);

                //    cmd.ExecuteNonQuery();
                //}


               // gConnection.Close();

            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View();
        }


        //[HttpPost]
        //public ActionResult GetNSEIndexMovers(string Co_Code, string comp_name, string upd_time, string open_price, string High_Price,
        //     string Low_Price, string price, string Pricediff, string change, string Contribution)
        //{
        //    try
        //    {
        //        bool result = false;
        //        gConnection.Open();
        //        string sqlstr = "select AutoId, Co_code from NSEIndexMovers where Co_Code=@Co_code and upd_time=@upd_time";
        //        SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
        //        cmd.Parameters.AddWithValue("@Co_code", Co_Code);
        //        cmd.Parameters.AddWithValue("@upd_time", upd_time);
        //        SqlDataReader sdr = cmd.ExecuteReader();
        //        while (sdr.Read())
        //        {
        //            result = true;
        //        }
        //        sdr.Close();

        //        if (result == false)
        //        {
        //            sqlstr = "Insert Into NSEIndexMovers(Co_Code,comp_name,upd_time,open_price,";
        //            sqlstr = sqlstr + " high_price,low_price,price,pricediff,change,Contribution,created_on)";
        //            sqlstr = sqlstr + " values(@Co_Code,@comp_name,@upd_time,@open_price,";
        //            sqlstr = sqlstr + " @high_price,@low_price,@price,@pricediff,@change,@Contribution,GETDATE())";
        //            cmd.Parameters.Clear();
        //            cmd.CommandText = sqlstr;
        //            cmd.Parameters.AddWithValue("@Co_Code", Co_Code);
        //            cmd.Parameters.AddWithValue("@comp_name", comp_name);
        //            cmd.Parameters.AddWithValue("@upd_time", upd_time);
        //            cmd.Parameters.AddWithValue("@open_price", open_price);
        //            cmd.Parameters.AddWithValue("@high_price", High_Price);
        //            cmd.Parameters.AddWithValue("@low_price", Low_Price);
        //            cmd.Parameters.AddWithValue("@price", price);
        //            cmd.Parameters.AddWithValue("@pricediff", Pricediff);
        //            cmd.Parameters.AddWithValue("@change", change);
        //            cmd.Parameters.AddWithValue("@Contribution", Contribution);
                    
        //            cmd.ExecuteNonQuery();
        //        }


        //        gConnection.Close();

        //    }
        //    catch (Exception ex)
        //    {
        //        Global.ErrorHandlerClass.LogError(ex);
        //    }
        //    return View();
        //}


        #endregion




        #region NSE Group List

        [HttpGet]
        public ActionResult GetNSEGroupList()
        {
            return View();
        }



        [HttpPost]
        public ActionResult GetNSEGroupList(string group)
        {
            try
            {
                bool result = false;
                List<NSEGroupList> objGrpList = new List<NSEGroupList>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        var description = app.Value;
                        string m = description.ToString();
                        NSEGroupList objNSE = new NSEGroupList();
                        objNSE.group_name = m;
                        objGrpList.Add(objNSE);
                    }
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    for (int i = 0; i < objGrpList.Count; i++)
                    {
                        if (objGrpList[i].group_name != "")
                        {
                            sqlstr = " select * from NSEGroupList where group_name=@group_name";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@group_name", group);
                            SqlDataReader sdr = cmd.ExecuteReader();
                            while (sdr.Read())
                            {
                                result = true;
                            }
                            sdr.Close();

                            if (result == false)
                            {
                                sqlstr = "Insert Into NSEGroupList(group_name, created_on) values (@group_name, GetDate())";
                                cmd.CommandText = sqlstr;
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@group_name", objGrpList[i].group_name);
                                cmd.ExecuteNonQuery();
                            }
                            result = false;
                        }
                    }
                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            return View();


        }


        #endregion





        #region BSE Group List

        [HttpGet]
        public ActionResult GetBSEGroupList()
        {
            return View();
        }



        [HttpPost]
        public ActionResult GetBSEGroupList(string group)
        {
            try
            {
                bool result = false;
                List<BSEGroupList> objGrpList = new List<BSEGroupList>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        var description = app.Value;
                        string m = description.ToString();
                        BSEGroupList objBSE = new BSEGroupList();
                        objBSE.group_name = m;
                        objGrpList.Add(objBSE);
                    }
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    for (int i = 0; i < objGrpList.Count; i++)
                    {
                        if (objGrpList[i].group_name != "")
                        {
                            sqlstr = " select * from BSEGroupList where group_name=@group_name";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@group_name", group);
                            SqlDataReader sdr = cmd.ExecuteReader();
                            while (sdr.Read())
                            {
                                result = true;
                            }
                            sdr.Close();

                            if (result == false)
                            {
                                sqlstr = "Insert Into BSEGroupList(group_name, created_on) values (@group_name, GetDate())";
                                cmd.CommandText = sqlstr;
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@group_name", objGrpList[i].group_name);
                                cmd.ExecuteNonQuery();
                            }
                            result = false;
                        }
                    }
                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            return View();


        }


        #endregion



        #region Cmots Company Master

        [HttpGet]
        public ActionResult GetCmotsCompanyList()
        {
            return View();
        }



        [HttpPost]
        public ActionResult GetCmotsCompanyList(string group)
        {
            try
            {
                bool result = false;
                List<CmotsCompanyMaster> objCmpList = new List<CmotsCompanyMaster>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsCompanyMaster objComp = new CmotsCompanyMaster();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "co_code".ToUpper())
                        {
                            objComp.co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bsecode".ToUpper())
                        {
                            objComp.bsecode = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "nsesymbol".ToUpper())
                        {
                            objComp.nsesymbol = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "companyname".ToUpper())
                        {
                            objComp.companyname = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "companyshortname".ToUpper())
                        {
                            objComp.companyshortname = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "categoryname".ToUpper())
                        {
                            objComp.categoryname = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "isin".ToUpper())
                        {
                            objComp.isin = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bsegroup".ToUpper())
                        {
                            objComp.bsegroup = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "mcaptype".ToUpper())
                        {
                            objComp.mcaptype = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "sectorcode".ToUpper())
                        {
                            objComp.sectorcode = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "sectorname".ToUpper())
                        {
                            objComp.sectorname = app.Value.ToString();
                        }
                    }
                    objCmpList.Add(objComp);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    foreach (var data in objCmpList)
                    {
                        if (data.co_code != "")
                        {
                            sqlstr = " select * from CmotsCompanyMaster where co_code=@co_code";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@co_code", data.co_code);
                            SqlDataReader sdr = cmd.ExecuteReader();
                            while (sdr.Read())
                            {
                                result = true;
                            }
                            sdr.Close();

                            if (result == false)
                            {
                                sqlstr = "insert into CmotsCompanyMaster(co_code,bsecode,nsesymbol,companyname,companyshortname,";
                                sqlstr = sqlstr + " categoryname,isin,bsegroup,mcaptype,sectorcode,sectorname,created_by,created_on)";
                                sqlstr = sqlstr + "values (@co_code,@bsecode,@nsesymbol,@companyname,@companyshortname,";
                                sqlstr = sqlstr + " @categoryname,@isin,@bsegroup,@mcaptype,@sectorcode,@sectorname,@created_by,GetDate())";
                                cmd.CommandText = sqlstr;
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@co_code", data.co_code);
                                cmd.Parameters.AddWithValue("@bsecode", data.bsecode);
                                cmd.Parameters.AddWithValue("@nsesymbol", data.nsesymbol);
                                cmd.Parameters.AddWithValue("@companyname", data.companyname);
                                cmd.Parameters.AddWithValue("@companyshortname", data.companyshortname);
                                cmd.Parameters.AddWithValue("@categoryname", data.categoryname);
                                cmd.Parameters.AddWithValue("@isin", data.isin);
                                cmd.Parameters.AddWithValue("@bsegroup", data.bsegroup);
                                cmd.Parameters.AddWithValue("@mcaptype", data.mcaptype);
                                cmd.Parameters.AddWithValue("@sectorcode", data.sectorcode);
                                cmd.Parameters.AddWithValue("@sectorname", data.sectorname);
                                cmd.Parameters.AddWithValue("@created_by", "Online");
                                cmd.ExecuteNonQuery();
                            }
                            else if (result == true)
                            {
                                sqlstr = "update CmotsCompanyMaster set bsecode=@bsecode,nsesymbol=@nsesymbol,companyname=@companyname,companyshortname=@companyshortname,";
                                sqlstr = sqlstr + " categoryname=@categoryname,isin=@isin,bsegroup=@bsegroup,mcaptype=@mcaptype,sectorcode=@sectorcode,sectorname=@sectorname,";
                                sqlstr = sqlstr + " updated_by=@updated_by, updated_on = GetDate() where co_code=@co_code";
                                cmd.CommandText = sqlstr;
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@co_code", data.co_code);
                                cmd.Parameters.AddWithValue("@bsecode", data.bsecode);
                                cmd.Parameters.AddWithValue("@nsesymbol", data.nsesymbol);
                                cmd.Parameters.AddWithValue("@companyname", data.companyname);
                                cmd.Parameters.AddWithValue("@companyshortname", data.companyshortname);
                                cmd.Parameters.AddWithValue("@categoryname", data.categoryname);
                                cmd.Parameters.AddWithValue("@isin", data.isin);
                                cmd.Parameters.AddWithValue("@bsegroup", data.bsegroup);
                                cmd.Parameters.AddWithValue("@mcaptype", data.mcaptype);
                                cmd.Parameters.AddWithValue("@sectorcode", data.sectorcode);
                                cmd.Parameters.AddWithValue("@sectorname", data.sectorname);
                                cmd.Parameters.AddWithValue("@updated_by", "Online");
                                cmd.ExecuteNonQuery();
                            }
                            result = false;
                        }
                    }

                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            return View();


        }


        #endregion




        #region Cmots Get NSE Quote Details

        [HttpGet]
        public ActionResult GetCmotsNSEQuoteDetails()
        {
            List<CmotsCompanyMaster> objCmotsCompLits = new List<CmotsCompanyMaster>();
            try
            {
                gConnection.Open();
                string sqlstr = "select co_code from dbo.CmotsCompanyMaster where ISNULL(nsesymbol,'') != '' order by co_code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    CmotsCompanyMaster objCmots = new CmotsCompanyMaster();
                    objCmots.co_code = sdr["co_code"].ToString();
                    objCmotsCompLits.Add(objCmots);
                }
                gConnection.Close();

            }
            catch(Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View(objCmotsCompLits);

        }



        [HttpPost]
        public void GetCmotsNSEQuoteDetails(string group, string comp_code)
        {
            try
            {
                bool result = false;
                CmotsNSEQuoteDetails objNseQuote = new CmotsNSEQuoteDetails();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    //CmotsCompanyMaster objComp = new CmotsCompanyMaster();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "xchng".ToUpper())
                        {
                            objNseQuote.xchng = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "upd_time".ToUpper())
                        {
                            objNseQuote.upd_time = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "open_price".ToUpper())
                        {
                            objNseQuote.open_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "price".ToUpper())
                        {
                            objNseQuote.price = app.Value.ToString();
                        }


                        else if (appName.ToUpper() == "high_price".ToUpper())
                        {
                            objNseQuote.high_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "low_price".ToUpper())
                        {
                            objNseQuote.low_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bbuy_qty".ToUpper())
                        {
                            objNseQuote.bbuy_qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bbuy_price".ToUpper())
                        {
                            objNseQuote.bbuy_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bsell_qty".ToUpper())
                        {
                            objNseQuote.bsell_qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bsell_price".ToUpper())
                        {
                            objNseQuote.bsell_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "oldprice".ToUpper())
                        {
                            objNseQuote.oldprice = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "pricediff".ToUpper())
                        {
                            objNseQuote.pricediff = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "volume".ToUpper())
                        {
                            objNseQuote.volume = app.Value.ToString();

                        }
                        else if (appName.ToUpper() == "value".ToUpper())
                        {
                            objNseQuote.value = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "change".ToUpper())
                        {
                            objNseQuote.change = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "net_trdqty".ToUpper())
                        {
                            objNseQuote.trd_qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "hi_52_wk".ToUpper())
                        {
                            objNseQuote.hi_52_wk = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "lo_52_wk".ToUpper())
                        {
                            objNseQuote.lo_52_wk = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "h52date".ToUpper())
                        {
                            objNseQuote.h52date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "l52date".ToUpper())
                        {
                            objNseQuote.l52date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_name".ToUpper())
                        {
                            objNseQuote.co_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "complname".ToUpper())
                        {
                            objNseQuote.complname = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "symbol".ToUpper())
                        {
                            objNseQuote.symbol = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "sc_group".ToUpper())
                        {
                            objNseQuote.sc_group = app.Value.ToString();
                        }
                       
                    }                   
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    sqlstr = " select * from CmotsNSEQuoteDetails where co_code=@co_code and xchng=@xchng";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.AddWithValue("@co_code", comp_code);
                    cmd.Parameters.AddWithValue("@xchng", objNseQuote.xchng);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        result = true;
                    }
                    sdr.Close();

                    if (result == false)
                    {
                        sqlstr = "Insert into CmotsNSEQuoteDetails(co_code, xchng,upd_time,open_price,high_price,";
                        sqlstr = sqlstr + " low_price,price,bbuy_qty,bbuy_price,bsell_qty,bsell_price,oldprice,pricediff,";
                        sqlstr = sqlstr + " volume,value,change,trd_qty,hi_52_wk,lo_52_wk,h52date,l52date,co_name,";
                        sqlstr = sqlstr + " complname,symbol,sc_group,created_by,created_on)";
                        sqlstr = sqlstr + " values (@co_code, @xchng,@upd_time,@open_price,@high_price,";
                        sqlstr = sqlstr + " @low_price,@price,@bbuy_qty,@bbuy_price,@bsell_qty,@bsell_price,@oldprice,@pricediff,";
                        sqlstr = sqlstr + " @volume,@value,@change,@trd_qty,@hi_52_wk,@lo_52_wk,@h52date,@l52date,@co_name,";
                        sqlstr = sqlstr + " @complname,@symbol,@sc_group,@created_by,GetDate())";
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@co_code", comp_code);
                        cmd.Parameters.AddWithValue("@xchng", objNseQuote.xchng);
                        cmd.Parameters.AddWithValue("@upd_time", objNseQuote.upd_time);
                        cmd.Parameters.AddWithValue("@open_price", objNseQuote.open_price);
                        cmd.Parameters.AddWithValue("@high_price", objNseQuote.high_price);
                        cmd.Parameters.AddWithValue("@low_price", objNseQuote.low_price);
                        cmd.Parameters.AddWithValue("@price", objNseQuote.price);
                        cmd.Parameters.AddWithValue("@bbuy_qty", objNseQuote.bbuy_qty);
                        cmd.Parameters.AddWithValue("@bbuy_price", objNseQuote.bbuy_price);
                        cmd.Parameters.AddWithValue("@bsell_qty", objNseQuote.bsell_qty);
                        cmd.Parameters.AddWithValue("@bsell_price", objNseQuote.bsell_price);
                        cmd.Parameters.AddWithValue("@oldprice", objNseQuote.oldprice);
                        cmd.Parameters.AddWithValue("@pricediff", objNseQuote.pricediff);
                        cmd.Parameters.AddWithValue("@volume", objNseQuote.volume);
                        cmd.Parameters.AddWithValue("@value", objNseQuote.value);
                        cmd.Parameters.AddWithValue("@change", objNseQuote.change);
                        cmd.Parameters.AddWithValue("@trd_qty", objNseQuote.trd_qty);
                        cmd.Parameters.AddWithValue("@hi_52_wk", objNseQuote.hi_52_wk);
                        cmd.Parameters.AddWithValue("@lo_52_wk", objNseQuote.lo_52_wk);
                        cmd.Parameters.AddWithValue("@h52date", objNseQuote.h52date);
                        cmd.Parameters.AddWithValue("@l52date", objNseQuote.l52date);
                        //cmd.Parameters.AddWithValue("@trd_value", objNseQuote.trd_value);
                        cmd.Parameters.AddWithValue("@co_name", objNseQuote.co_name);
                        cmd.Parameters.AddWithValue("@complname", objNseQuote.complname);
                        cmd.Parameters.AddWithValue("@symbol", objNseQuote.symbol);
                        cmd.Parameters.AddWithValue("@sc_group", objNseQuote.sc_group);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();
                    }
                    else if(result == true)
                    {
                        sqlstr = "update CmotsNSEQuoteDetails set upd_time=@upd_time,open_price=@open_price,high_price=@high_price,";
                        sqlstr = sqlstr + " low_price=@low_price,price=@price,bbuy_qty=@bbuy_qty,bbuy_price=@bbuy_price,";
                        sqlstr = sqlstr + " bsell_qty=@bsell_qty,bsell_price=@bsell_price,oldprice=@oldprice,pricediff=@pricediff,";
                        sqlstr = sqlstr + " volume=@volume,value=@value,change=@change,trd_qty=@trd_qty,hi_52_wk=@hi_52_wk,";
                        sqlstr = sqlstr + " lo_52_wk=@lo_52_wk,h52date=@h52date,l52date=@l52date,co_name=@co_name,";
                        sqlstr = sqlstr + " complname=@complname,symbol=@symbol,sc_group=@sc_group,updated_by=@updated_by, ";
                        sqlstr = sqlstr + "  updated_on=GetDate() where co_code=@co_code and xchng=@xchng";
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@co_code", comp_code);
                        cmd.Parameters.AddWithValue("@xchng", objNseQuote.xchng);
                        cmd.Parameters.AddWithValue("@upd_time", objNseQuote.upd_time);
                        cmd.Parameters.AddWithValue("@open_price", objNseQuote.open_price);
                        cmd.Parameters.AddWithValue("@high_price", objNseQuote.high_price);
                        cmd.Parameters.AddWithValue("@low_price", objNseQuote.low_price);
                        cmd.Parameters.AddWithValue("@price", objNseQuote.price);
                        cmd.Parameters.AddWithValue("@bbuy_qty", objNseQuote.bbuy_qty);
                        cmd.Parameters.AddWithValue("@bbuy_price", objNseQuote.bbuy_price);
                        cmd.Parameters.AddWithValue("@bsell_qty", objNseQuote.bsell_qty);
                        cmd.Parameters.AddWithValue("@bsell_price", objNseQuote.bsell_price);
                        cmd.Parameters.AddWithValue("@oldprice", objNseQuote.oldprice);
                        cmd.Parameters.AddWithValue("@pricediff", objNseQuote.pricediff);
                        cmd.Parameters.AddWithValue("@volume", objNseQuote.volume);
                        cmd.Parameters.AddWithValue("@value", objNseQuote.value);
                        cmd.Parameters.AddWithValue("@change", objNseQuote.change);
                        cmd.Parameters.AddWithValue("@trd_qty", objNseQuote.trd_qty);
                        cmd.Parameters.AddWithValue("@hi_52_wk", objNseQuote.hi_52_wk);
                        cmd.Parameters.AddWithValue("@lo_52_wk", objNseQuote.lo_52_wk);
                        cmd.Parameters.AddWithValue("@h52date", objNseQuote.h52date);
                        cmd.Parameters.AddWithValue("@l52date", objNseQuote.l52date);
                        //cmd.Parameters.AddWithValue("@trd_value", objNseQuote.trd_value);
                        cmd.Parameters.AddWithValue("@co_name", objNseQuote.co_name);
                        cmd.Parameters.AddWithValue("@complname", objNseQuote.complname);
                        cmd.Parameters.AddWithValue("@symbol", objNseQuote.symbol);
                        cmd.Parameters.AddWithValue("@sc_group", objNseQuote.sc_group);
                        cmd.Parameters.AddWithValue("@updated_by", "Online");
                        cmd.ExecuteNonQuery();
                    }
                    result = false;


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";
                    
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion


        #region Cmots Get BSE Quote Details

        [HttpGet]
        public ActionResult GetCmotsBSEQuoteDetails()
        {
            List<CmotsCompanyMaster> objCmotsCompLits = new List<CmotsCompanyMaster>();
            try
            {
                gConnection.Open();
                string sqlstr = "select co_code from dbo.CmotsCompanyMaster where ISNULL(bsecode,'') != '' order by co_code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCompanyMaster objCmots = new CmotsCompanyMaster();
                    objCmots.co_code = sdr["co_code"].ToString();
                    objCmotsCompLits.Add(objCmots);
                }
                gConnection.Close();

            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View(objCmotsCompLits);

        }



        [HttpPost]
        public void GetCmotsBSEQuoteDetails(string group, string comp_code)
        {
            try
            {
                bool result = false;
                CmotsBSEQuoteDetails objBseQuote = new CmotsBSEQuoteDetails();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    //CmotsCompanyMaster objComp = new CmotsCompanyMaster();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "xchng".ToUpper())
                        {
                            objBseQuote.xchng = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "upd_time".ToUpper())
                        {
                            objBseQuote.upd_time = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "open_price".ToUpper())
                        {
                            objBseQuote.open_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "price".ToUpper())
                        {
                            objBseQuote.price = app.Value.ToString();
                        }


                        else if (appName.ToUpper() == "high_price".ToUpper())
                        {
                            objBseQuote.high_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "low_price".ToUpper())
                        {
                            objBseQuote.low_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bbuy_qty".ToUpper())
                        {
                            objBseQuote.bbuy_qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bbuy_price".ToUpper())
                        {
                            objBseQuote.bbuy_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bsell_qty".ToUpper())
                        {
                            objBseQuote.bsell_qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "bsell_price".ToUpper())
                        {
                            objBseQuote.bsell_price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "oldprice".ToUpper())
                        {
                            objBseQuote.oldprice = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "pricediff".ToUpper())
                        {
                            objBseQuote.pricediff = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "volume".ToUpper())
                        {
                            objBseQuote.volume = app.Value.ToString();

                        }
                        else if (appName.ToUpper() == "value".ToUpper())
                        {
                            objBseQuote.value = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "change".ToUpper())
                        {
                            objBseQuote.change = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "trd_qty".ToUpper())
                        {
                            objBseQuote.trd_qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "hi_52_wk".ToUpper())
                        {
                            objBseQuote.hi_52_wk = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "lo_52_wk".ToUpper())
                        {
                            objBseQuote.lo_52_wk = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "h52date".ToUpper())
                        {
                            objBseQuote.h52date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "l52date".ToUpper())
                        {
                            objBseQuote.l52date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_name".ToUpper())
                        {
                            objBseQuote.co_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "complname".ToUpper())
                        {
                            objBseQuote.complname = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "symbol".ToUpper())
                        {
                            objBseQuote.symbol = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "sc_group".ToUpper())
                        {
                            objBseQuote.sc_group = app.Value.ToString();
                        }

                    }
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    sqlstr = " select * from CmotsBSEQuoteDetails where co_code=@co_code and xchng=@xchng";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.AddWithValue("@co_code", comp_code);
                    cmd.Parameters.AddWithValue("@xchng", objBseQuote.xchng);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        result = true;
                    }
                    sdr.Close();

                    if (result == false)
                    {
                        sqlstr = "Insert into CmotsBSEQuoteDetails(co_code, xchng,upd_time,open_price,high_price,";
                        sqlstr = sqlstr + " low_price,price,bbuy_qty,bbuy_price,bsell_qty,bsell_price,oldprice,pricediff,";
                        sqlstr = sqlstr + " volume,value,change,trd_qty,hi_52_wk,lo_52_wk,h52date,l52date,co_name,";
                        sqlstr = sqlstr + " complname,symbol,sc_group,created_by,created_on)";
                        sqlstr = sqlstr + " values (@co_code, @xchng,@upd_time,@open_price,@high_price,";
                        sqlstr = sqlstr + " @low_price,@price,@bbuy_qty,@bbuy_price,@bsell_qty,@bsell_price,@oldprice,@pricediff,";
                        sqlstr = sqlstr + " @volume,@value,@change,@trd_qty,@hi_52_wk,@lo_52_wk,@h52date,@l52date,@co_name,";
                        sqlstr = sqlstr + " @complname,@symbol,@sc_group,@created_by,GetDate())";
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@co_code", comp_code);
                        cmd.Parameters.AddWithValue("@xchng", objBseQuote.xchng);
                        cmd.Parameters.AddWithValue("@upd_time", objBseQuote.upd_time);
                        cmd.Parameters.AddWithValue("@open_price", objBseQuote.open_price);
                        cmd.Parameters.AddWithValue("@high_price", objBseQuote.high_price);
                        cmd.Parameters.AddWithValue("@low_price", objBseQuote.low_price);
                        cmd.Parameters.AddWithValue("@price", objBseQuote.price);
                        cmd.Parameters.AddWithValue("@bbuy_qty", objBseQuote.bbuy_qty);
                        cmd.Parameters.AddWithValue("@bbuy_price", objBseQuote.bbuy_price);
                        cmd.Parameters.AddWithValue("@bsell_qty", objBseQuote.bsell_qty);
                        cmd.Parameters.AddWithValue("@bsell_price", objBseQuote.bsell_price);
                        cmd.Parameters.AddWithValue("@oldprice", objBseQuote.oldprice);
                        cmd.Parameters.AddWithValue("@pricediff", objBseQuote.pricediff);
                        cmd.Parameters.AddWithValue("@volume", objBseQuote.volume);
                        cmd.Parameters.AddWithValue("@value", objBseQuote.value);
                        cmd.Parameters.AddWithValue("@change", objBseQuote.change);
                        cmd.Parameters.AddWithValue("@trd_qty", objBseQuote.trd_qty);
                        cmd.Parameters.AddWithValue("@hi_52_wk", objBseQuote.hi_52_wk);
                        cmd.Parameters.AddWithValue("@lo_52_wk", objBseQuote.lo_52_wk);
                        cmd.Parameters.AddWithValue("@h52date", objBseQuote.h52date);
                        cmd.Parameters.AddWithValue("@l52date", objBseQuote.l52date);
                        //cmd.Parameters.AddWithValue("@trd_value", objNseQuote.trd_value);
                        cmd.Parameters.AddWithValue("@co_name", objBseQuote.co_name);
                        cmd.Parameters.AddWithValue("@complname", objBseQuote.complname);
                        cmd.Parameters.AddWithValue("@symbol", objBseQuote.symbol);
                        cmd.Parameters.AddWithValue("@sc_group", objBseQuote.sc_group);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();
                    }
                    result = false;


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion




        #region Cmots Get Company Background

        [HttpGet]
        public ActionResult GetCmotsCompanyBackground()
        {
            List<CmotsCompanyMaster> objCmotsCompLits = new List<CmotsCompanyMaster>();
            try
            {
                gConnection.Open();
                string sqlstr = "select co_code from dbo.CmotsCompanyMaster order by co_code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCompanyMaster objCmots = new CmotsCompanyMaster();
                    objCmots.co_code = sdr["co_code"].ToString();
                    objCmotsCompLits.Add(objCmots);
                }
                gConnection.Close();

            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View(objCmotsCompLits);

        }



        [HttpPost]
        public void GetCmotsCompanyBackground(string group, string comp_code)
        {
            try
            {
                group = "[" + group + "]";
                bool result = false;
                CmotsCompanyBackground objCompBkg = new CmotsCompanyBackground();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    //CmotsCompanyMaster objComp = new CmotsCompanyMaster();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "lname".ToUpper())
                        {
                            objCompBkg.lname = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "isin".ToUpper())
                        {
                            objCompBkg.isin = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "hse_s_name".ToUpper())
                        {
                            objCompBkg.hse_s_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "inc_dt".ToUpper())
                        {
                            objCompBkg.inc_dt = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "regadd1".ToUpper())
                        {
                            objCompBkg.regadd1 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "regadd2".ToUpper())
                        {
                            objCompBkg.regadd2 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "regdist".ToUpper())
                        {
                            objCompBkg.regdist = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "regstate".ToUpper())
                        {
                            objCompBkg.regstate = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "regpin".ToUpper())
                        {
                            objCompBkg.regpin = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "tel1".ToUpper())
                        {
                            objCompBkg.tel1 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "ind_l_name".ToUpper())
                        {
                            objCompBkg.ind_l_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "tel2".ToUpper())
                        {
                            objCompBkg.tel2 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "fax1".ToUpper())
                        {
                            objCompBkg.fax1 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "fax2".ToUpper())
                        {
                            objCompBkg.fax2 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "auditor".ToUpper())
                        {
                            objCompBkg.auditor = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "fv".ToUpper())
                        {
                            objCompBkg.fv = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "mkt_lot".ToUpper())
                        {
                            objCompBkg.mkt_lot = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "chairman".ToUpper())
                        {
                            objCompBkg.chairman = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_sec".ToUpper())
                        {
                            objCompBkg.co_sec = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_code".ToUpper())
                        {
                            objCompBkg.co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "email".ToUpper())
                        {
                            objCompBkg.email = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "internet".ToUpper())
                        {
                            objCompBkg.internet = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "dir_name".ToUpper())
                        {
                            objCompBkg.dir_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "dir_desg".ToUpper())
                        {
                            objCompBkg.dir_desg = app.Value.ToString();
                        }
                    }
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    sqlstr = " select * from CmotsCompanyBackground where co_code=@co_code ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.AddWithValue("@co_code", objCompBkg.co_code);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        result = true;
                    }
                    sdr.Close();

                    if (result == false)
                    {
                        sqlstr = "insert into CmotsCompanyBackground(lname, isin, hse_s_name, inc_dt, regadd1, regadd2, regdist,";
                        sqlstr = sqlstr + " regstate, regpin, tel1, ind_l_name, tel2, fax1, fax2, auditor, fv, mkt_lot, chairman, co_sec,";
                        sqlstr = sqlstr + " co_code, email, internet, dir_name, dir_desg, created_by, created_on)";
                        sqlstr = sqlstr + " values (@lname, @isin, @hse_s_name, @inc_dt, @regadd1, @regadd2, @regdist,";
                        sqlstr = sqlstr + " @regstate, @regpin, @tel1, @ind_l_name, @tel2, @fax1, @fax2, @auditor, @fv, @mkt_lot, @chairman, @co_sec,";
                        sqlstr = sqlstr + " @co_code, @email, @internet, @dir_name, @dir_desg, @created_by, GetDate())";
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@lname", objCompBkg.lname);
                        cmd.Parameters.AddWithValue("@isin", objCompBkg.isin);
                        cmd.Parameters.AddWithValue("@hse_s_name", objCompBkg.hse_s_name);
                        cmd.Parameters.AddWithValue("@inc_dt", objCompBkg.inc_dt);
                        cmd.Parameters.AddWithValue("@regadd1", objCompBkg.regadd1);
                        cmd.Parameters.AddWithValue("@regadd2", objCompBkg.regadd2);
                        cmd.Parameters.AddWithValue("@regdist", objCompBkg.regdist);
                        cmd.Parameters.AddWithValue("@regstate", objCompBkg.regstate);
                        cmd.Parameters.AddWithValue("@regpin", objCompBkg.regpin);
                        cmd.Parameters.AddWithValue("@tel1", objCompBkg.tel1);
                        cmd.Parameters.AddWithValue("@ind_l_name", objCompBkg.ind_l_name);
                        cmd.Parameters.AddWithValue("@tel2", objCompBkg.tel2);
                        cmd.Parameters.AddWithValue("@fax1", objCompBkg.fax1);
                        cmd.Parameters.AddWithValue("@fax2", objCompBkg.fax2);
                        cmd.Parameters.AddWithValue("@auditor", objCompBkg.auditor);
                        cmd.Parameters.AddWithValue("@fv", objCompBkg.fv);
                        cmd.Parameters.AddWithValue("@mkt_lot", objCompBkg.mkt_lot);
                        cmd.Parameters.AddWithValue("@chairman", objCompBkg.chairman);
                        cmd.Parameters.AddWithValue("@co_sec", objCompBkg.co_sec);
                        cmd.Parameters.AddWithValue("@co_code", objCompBkg.co_code);
                        cmd.Parameters.AddWithValue("@email", objCompBkg.email);
                        cmd.Parameters.AddWithValue("@internet", objCompBkg.internet);
                        cmd.Parameters.AddWithValue("@dir_name", objCompBkg.dir_name);
                        cmd.Parameters.AddWithValue("@dir_desg", objCompBkg.dir_desg);

                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();
                    }
                    else if (result == true)
                    {
                        sqlstr = "update CmotsCompanyBackground set lname=@lname, isin=@isin, hse_s_name=@hse_s_name, inc_dt=@inc_dt, ";
                        sqlstr = sqlstr + " regadd1=@regadd1, regadd2=@regadd2, regdist=@regdist,regstate=@regstate, regpin=@regpin, ";
                        sqlstr = sqlstr + " tel1=@tel1, ind_l_name=@ind_l_name, tel2=@tel2, fax1=@fax1, fax2=@fax2, auditor=@auditor, ";
                        sqlstr = sqlstr + " fv=@fv, mkt_lot=@mkt_lot, chairman=@chairman, co_sec=@co_sec,email=@email, ";
                        sqlstr = sqlstr + " internet=@internet, dir_name=@dir_name, dir_desg=@dir_desg, updated_by=@updated_by,";
                        sqlstr = sqlstr + "  updated_on = GetDate() where co_code=@co_code ";
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@lname", objCompBkg.lname);
                        cmd.Parameters.AddWithValue("@isin", objCompBkg.isin);
                        cmd.Parameters.AddWithValue("@hse_s_name", objCompBkg.hse_s_name);
                        cmd.Parameters.AddWithValue("@inc_dt", objCompBkg.inc_dt);
                        cmd.Parameters.AddWithValue("@regadd1", objCompBkg.regadd1);
                        cmd.Parameters.AddWithValue("@regadd2", objCompBkg.regadd2);
                        cmd.Parameters.AddWithValue("@regdist", objCompBkg.regdist);
                        cmd.Parameters.AddWithValue("@regstate", objCompBkg.regstate);
                        cmd.Parameters.AddWithValue("@regpin", objCompBkg.regpin);
                        cmd.Parameters.AddWithValue("@tel1", objCompBkg.tel1);
                        cmd.Parameters.AddWithValue("@ind_l_name", objCompBkg.ind_l_name);
                        cmd.Parameters.AddWithValue("@tel2", objCompBkg.tel2);
                        cmd.Parameters.AddWithValue("@fax1", objCompBkg.fax1);
                        cmd.Parameters.AddWithValue("@fax2", objCompBkg.fax2);
                        cmd.Parameters.AddWithValue("@auditor", objCompBkg.auditor);
                        cmd.Parameters.AddWithValue("@fv", objCompBkg.fv);
                        cmd.Parameters.AddWithValue("@mkt_lot", objCompBkg.mkt_lot);
                        cmd.Parameters.AddWithValue("@chairman", objCompBkg.chairman);
                        cmd.Parameters.AddWithValue("@co_sec", objCompBkg.co_sec);
                        cmd.Parameters.AddWithValue("@co_code", objCompBkg.co_code);
                        cmd.Parameters.AddWithValue("@email", objCompBkg.email);
                        cmd.Parameters.AddWithValue("@internet", objCompBkg.internet);
                        cmd.Parameters.AddWithValue("@dir_name", objCompBkg.dir_name);
                        cmd.Parameters.AddWithValue("@dir_desg", objCompBkg.dir_desg);
                        cmd.Parameters.AddWithValue("@updated_by", "Online");
                        cmd.ExecuteNonQuery();
                    }
                    result = false;


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion



        #region Cmots Get Board Of Directors

        [HttpGet]
        public ActionResult GetCmotsBoardOfDirectors()
        {
            List<CmotsCompanyMaster> objCmotsCompLits = new List<CmotsCompanyMaster>();
            try
            {
                gConnection.Open();
                string sqlstr = "select  co_code from dbo.CmotsCompanyMaster order by co_code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCompanyMaster objCmots = new CmotsCompanyMaster();
                    objCmots.co_code = sdr["co_code"].ToString();
                    objCmotsCompLits.Add(objCmots);
                }
                gConnection.Close();

            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View(objCmotsCompLits);

        }



        [HttpPost]
        public void GetCmotsBoardOfDirectors(string group, string comp_code)
        {
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsBoardOfDirectors> objBoDLst = new List<CmotsBoardOfDirectors>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsBoardOfDirectors objBoD = new CmotsBoardOfDirectors();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "slno".ToUpper())
                        {
                            objBoD.slno = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "dir_name".ToUpper())
                        {
                            objBoD.dir_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "dir_desg".ToUpper())
                        {
                            objBoD.dir_desg = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "year".ToUpper())
                        {
                            objBoD.year = app.Value.ToString();
                        }                        
                    }
                    objBoDLst.Add(objBoD);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    sqlstr = " select * from CmotsBoardOfDirectors where co_code=@co_code ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.AddWithValue("@co_code", comp_code);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        result = true;
                    }
                    sdr.Close();

                    if (result == false)
                    {
                        foreach(var mod in objBoDLst)
                        {
                            sqlstr = "insert into CmotsBoardOfDirectors(co_code, slno, dir_name, dir_desg, year, created_by, created_on)";
                            sqlstr = sqlstr + " values (@co_code, @slno, @dir_name, @dir_desg, @year, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@co_code", comp_code);
                            cmd.Parameters.AddWithValue("@slno", mod.slno);
                            cmd.Parameters.AddWithValue("@dir_name", mod.dir_name);
                            cmd.Parameters.AddWithValue("@dir_desg", mod.dir_desg);
                            cmd.Parameters.AddWithValue("@year", mod.year);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }
                        
                    }
                    else if (result == true)
                    {
                        sqlstr = "delete from CmotsBoardOfDirectors where co_code=@co_code";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@co_code", comp_code);
                        cmd.ExecuteNonQuery();


                        foreach (var mod in objBoDLst)
                        {
                            sqlstr = "insert into CmotsBoardOfDirectors(co_code, slno, dir_name, dir_desg, year, created_by, created_on)";
                            sqlstr = sqlstr + " values (@co_code, @slno, @dir_name, @dir_desg, @year, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@co_code", comp_code);
                            cmd.Parameters.AddWithValue("@slno", mod.slno);
                            cmd.Parameters.AddWithValue("@dir_name", mod.dir_name);
                            cmd.Parameters.AddWithValue("@dir_desg", mod.dir_desg);
                            cmd.Parameters.AddWithValue("@year", mod.year);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }
                    }
                    result = false;


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion


        // Error In API
        #region Cmots Get Key Financial Ratios

        [HttpGet]
        public ActionResult GetCmotsKeyFinancialRatios()
        {
            List<CmotsCompanyMaster> objCmotsCompLits = new List<CmotsCompanyMaster>();
            try
            {
                gConnection.Open();
                string sqlstr = "select co_code from dbo.CmotsCompanyMaster order by co_code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCompanyMaster objCmots = new CmotsCompanyMaster();
                    objCmots.co_code = sdr["co_code"].ToString();
                    objCmotsCompLits.Add(objCmots);
                }
                gConnection.Close();

            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View(objCmotsCompLits);

        }



        [HttpPost]
        public void GetCmotsKeyFinancialRatios(string group, string comp_code)
        {
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsKeyFinancialRatios> objKFRLst = new List<CmotsKeyFinancialRatios>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    int i = 1;
                    CmotsKeyFinancialRatios objKFR = new CmotsKeyFinancialRatios();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "columnname".ToUpper())
                        {
                            objKFR.columnname = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "rid".ToUpper())
                        {
                            objKFR.rid = Convert.ToInt32(app.Value.ToString());
                        }
                        else
                        {
                            if(i==1)
                            {
                                objKFR.period1 = app.Value.ToString();
                            }
                            else if (i == 2)
                            {
                                objKFR.period2 = app.Value.ToString();
                            }
                            else if (i == 3)
                            {
                                objKFR.period3 = app.Value.ToString();
                            }
                            else if (i == 4)
                            {
                                objKFR.period4 = app.Value.ToString();
                            }
                            else if (i == 5)
                            {
                                objKFR.period5 = app.Value.ToString();
                            }
                            else if (i == 6)
                            {
                                objKFR.period6 = app.Value.ToString();
                            }
                            i = i + 1;
                        }
                        
                    }
                    objKFRLst.Add(objKFR);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    sqlstr = " select * from CmotsKeyFinancialRatios where co_code=@co_code ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.AddWithValue("@co_code", comp_code);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        result = true;
                    }
                    sdr.Close();

                    if (result == false)
                    {
                        foreach (var mod in objKFRLst)
                        {
                            sqlstr = "insert into CmotsKeyFinancialRatios(co_code, columnname, rid, period1, ";
                            sqlstr = sqlstr + " period2, period3, period4, period5, period6, created_by, created_on)";
                            sqlstr = sqlstr + " values (@co_code, @columnname, @rid, @period1, ";
                            sqlstr = sqlstr + " @period2, @period3, @period4, @period5, @period6, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@co_code", comp_code);
                            cmd.Parameters.AddWithValue("@columnname", mod.columnname);
                            cmd.Parameters.AddWithValue("@rid", mod.rid);
                            cmd.Parameters.AddWithValue("@period1", mod.period1);
                            cmd.Parameters.AddWithValue("@period2", mod.period2);
                            cmd.Parameters.AddWithValue("@period3", mod.period3);
                            cmd.Parameters.AddWithValue("@period4", mod.period4);
                            cmd.Parameters.AddWithValue("@period5", mod.period5);
                            cmd.Parameters.AddWithValue("@period6", mod.period6);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }

                    }
                    else if (result == true)
                    {
                        sqlstr = "delete from CmotsKeyFinancialRatios where co_code=@co_code";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@co_code", comp_code);
                        cmd.ExecuteNonQuery();

                        foreach (var mod in objKFRLst)
                        {
                            sqlstr = "insert into CmotsKeyFinancialRatios(co_code, columnname, rid, period1, ";
                            sqlstr = sqlstr + " period2, period3, period4, period5, period6, created_by, created_on)";
                            sqlstr = sqlstr + " values (@co_code, @columnname, @rid, @period1, ";
                            sqlstr = sqlstr + " @period2, @period3, @period4, @period5, @period6, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@co_code", comp_code);
                            cmd.Parameters.AddWithValue("@columnname", mod.columnname);
                            cmd.Parameters.AddWithValue("@rid", mod.rid);
                            cmd.Parameters.AddWithValue("@period1", mod.period1);
                            cmd.Parameters.AddWithValue("@period2", mod.period2);
                            cmd.Parameters.AddWithValue("@period3", mod.period3);
                            cmd.Parameters.AddWithValue("@period4", mod.period4);
                            cmd.Parameters.AddWithValue("@period5", mod.period5);
                            cmd.Parameters.AddWithValue("@period6", mod.period6);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }
                    }
                    result = false;


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion



        #region Cmots Get Cmots Company Chart 1D

        [HttpGet]
        public ActionResult GetCmotsCompanyChart1D()
        {
            List<CmotsCompanyMaster> objCmotsCompLits = new List<CmotsCompanyMaster>();
            try
            {
                gConnection.Open();
                string sqlstr = "select co_code from dbo.CmotsCompanyMaster order by co_code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCompanyMaster objCmots = new CmotsCompanyMaster();
                    objCmots.co_code = sdr["co_code"].ToString();
                    objCmotsCompLits.Add(objCmots);
                }
                gConnection.Close();

            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View(objCmotsCompLits);

        }
        

        [HttpPost]
        public void GetCmotsCompanyChart1D(string group, string comp_code)
        {
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsCompanyChart1D> objCCC1DLst = new List<CmotsCompanyChart1D>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsCompanyChart1D objCCC1D = new CmotsCompanyChart1D();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "upd_date".ToUpper())
                        {
                            objCCC1D.upd_date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "price".ToUpper())
                        {
                            objCCC1D.price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "volume".ToUpper())
                        {
                            objCCC1D.volume = app.Value.ToString();
                        }
                    }
                    objCCC1DLst.Add(objCCC1D);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    sqlstr = " select * from CmotsCompanyChart1D where co_code=@co_code ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.AddWithValue("@co_code", comp_code);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        result = true;
                    }
                    sdr.Close();

                    if (result == false)
                    {
                        foreach (var mod in objCCC1DLst)
                        {
                            sqlstr = "insert into CmotsCompanyChart1D(co_code, upd_date, price, volume, created_by, created_on)";
                            sqlstr = sqlstr + " values (@co_code, @upd_date, @price, @volume, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@co_code", comp_code);
                            cmd.Parameters.AddWithValue("@upd_date", mod.upd_date);
                            cmd.Parameters.AddWithValue("@price", mod.price);
                            cmd.Parameters.AddWithValue("@volume", mod.volume);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }

                    }
                    else if (result == true)
                    {
                        sqlstr = "delete from CmotsCompanyChart1D where co_code=@co_code";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@co_code", comp_code);
                        cmd.ExecuteNonQuery();

                        foreach (var mod in objCCC1DLst)
                        {
                            sqlstr = "insert into CmotsCompanyChart1D(co_code, upd_date, price, volume, created_by, created_on)";
                            sqlstr = sqlstr + " values (@co_code, @upd_date, @price, @volume, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@co_code", comp_code);
                            cmd.Parameters.AddWithValue("@upd_date", mod.upd_date);
                            cmd.Parameters.AddWithValue("@price", mod.price);
                            cmd.Parameters.AddWithValue("@volume", mod.volume);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }
                    }
                    result = false;


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion

        #region Cmots Get Cmots Company Chart 5D

        [HttpGet]
        public ActionResult GetCmotsCompanyChart5D()
        {
            List<CmotsCompanyMaster> objCmotsCompLits = new List<CmotsCompanyMaster>();
            try
            {
                gConnection.Open();
                string sqlstr = "select co_code from dbo.CmotsCompanyMaster order by co_code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCompanyMaster objCmots = new CmotsCompanyMaster();
                    objCmots.co_code = sdr["co_code"].ToString();
                    objCmotsCompLits.Add(objCmots);
                }
                gConnection.Close();

            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View(objCmotsCompLits);

        }


        [HttpPost]
        public void GetCmotsCompanyChart5D(string group, string comp_code)
        {
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsCompanyChart5D> objCCC5DLst = new List<CmotsCompanyChart5D>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsCompanyChart5D objCCC5D = new CmotsCompanyChart5D();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "upd_date".ToUpper())
                        {
                            objCCC5D.upd_date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "price".ToUpper())
                        {
                            objCCC5D.price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "volume".ToUpper())
                        {
                            objCCC5D.volume = app.Value.ToString();
                        }
                    }
                    objCCC5DLst.Add(objCCC5D);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    sqlstr = " select * from CmotsCompanyChart5D where co_code=@co_code ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.AddWithValue("@co_code", comp_code);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        result = true;
                    }
                    sdr.Close();

                    if (result == false)
                    {
                        foreach (var mod in objCCC5DLst)
                        {
                            sqlstr = "insert into CmotsCompanyChart5D(co_code, upd_date, price, volume, created_by, created_on)";
                            sqlstr = sqlstr + " values (@co_code, @upd_date, @price, @volume, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@co_code", comp_code);
                            cmd.Parameters.AddWithValue("@upd_date", mod.upd_date);
                            cmd.Parameters.AddWithValue("@price", mod.price);
                            cmd.Parameters.AddWithValue("@volume", mod.volume);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }

                    }
                    else if (result == true)
                    {
                        sqlstr = "delete from CmotsCompanyChart5D where co_code=@co_code";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@co_code", comp_code);
                        cmd.ExecuteNonQuery();

                        foreach (var mod in objCCC5DLst)
                        {
                            sqlstr = "insert into CmotsCompanyChart5D(co_code, upd_date, price, volume, created_by, created_on)";
                            sqlstr = sqlstr + " values (@co_code, @upd_date, @price, @volume, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@co_code", comp_code);
                            cmd.Parameters.AddWithValue("@upd_date", mod.upd_date);
                            cmd.Parameters.AddWithValue("@price", mod.price);
                            cmd.Parameters.AddWithValue("@volume", mod.volume);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }
                    }
                    result = false;


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion

        #region Cmots Get Cmots Company Chart 3M

        [HttpGet]
        public ActionResult GetCmotsCompanyChart3M()
        {
            List<CmotsCompanyMaster> objCmotsCompLits = new List<CmotsCompanyMaster>();
            try
            {
                gConnection.Open();
                string sqlstr = "select co_code from dbo.CmotsCompanyMaster order by co_code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCompanyMaster objCmots = new CmotsCompanyMaster();
                    objCmots.co_code = sdr["co_code"].ToString();
                    objCmotsCompLits.Add(objCmots);
                }
                gConnection.Close();

            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View(objCmotsCompLits);

        }


        [HttpPost]
        public void GetCmotsCompanyChart3M(string group, string comp_code)
        {
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsCompanyChart3M> objCCC3MLst = new List<CmotsCompanyChart3M>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsCompanyChart3M objCCC3M = new CmotsCompanyChart3M();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "upd_date".ToUpper())
                        {
                            objCCC3M.upd_date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "price".ToUpper())
                        {
                            objCCC3M.price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "volume".ToUpper())
                        {
                            objCCC3M.volume = app.Value.ToString();
                        }
                    }
                    objCCC3MLst.Add(objCCC3M);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    sqlstr = " select * from CmotsCompanyChart3M where co_code=@co_code ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.AddWithValue("@co_code", comp_code);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        result = true;
                    }
                    sdr.Close();

                    if (result == false)
                    {
                        foreach (var mod in objCCC3MLst)
                        {
                            sqlstr = "insert into CmotsCompanyChart3M(co_code, upd_date, price, volume, created_by, created_on)";
                            sqlstr = sqlstr + " values (@co_code, @upd_date, @price, @volume, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@co_code", comp_code);
                            cmd.Parameters.AddWithValue("@upd_date", mod.upd_date);
                            cmd.Parameters.AddWithValue("@price", mod.price);
                            cmd.Parameters.AddWithValue("@volume", mod.volume);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }

                    }
                    else if (result == true)
                    {
                        sqlstr = "delete from CmotsCompanyChart3M where co_code=@co_code";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@co_code", comp_code);
                        cmd.ExecuteNonQuery();

                        foreach (var mod in objCCC3MLst)
                        {
                            sqlstr = "insert into CmotsCompanyChart3M(co_code, upd_date, price, volume, created_by, created_on)";
                            sqlstr = sqlstr + " values (@co_code, @upd_date, @price, @volume, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@co_code", comp_code);
                            cmd.Parameters.AddWithValue("@upd_date", mod.upd_date);
                            cmd.Parameters.AddWithValue("@price", mod.price);
                            cmd.Parameters.AddWithValue("@volume", mod.volume);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }
                    }
                    result = false;


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion

        #region Cmots Get Cmots Company Chart 1Y

        [HttpGet]
        public ActionResult GetCmotsCompanyChart1Y()
        {
            List<CmotsCompanyMaster> objCmotsCompLits = new List<CmotsCompanyMaster>();
            try
            {
                gConnection.Open();
                string sqlstr = "select co_code from dbo.CmotsCompanyMaster order by co_code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCompanyMaster objCmots = new CmotsCompanyMaster();
                    objCmots.co_code = sdr["co_code"].ToString();
                    objCmotsCompLits.Add(objCmots);
                }
                gConnection.Close();

            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View(objCmotsCompLits);

        }


        [HttpPost]
        public void GetCmotsCompanyChart1Y(string group, string comp_code)
        {
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsCompanyChart1Y> objCCC1YLst = new List<CmotsCompanyChart1Y>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsCompanyChart1Y objCCC1Y = new CmotsCompanyChart1Y();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "upd_date".ToUpper())
                        {
                            objCCC1Y.upd_date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "price".ToUpper())
                        {
                            objCCC1Y.price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "volume".ToUpper())
                        {
                            objCCC1Y.volume = app.Value.ToString();
                        }
                    }
                    objCCC1YLst.Add(objCCC1Y);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    sqlstr = " select * from CmotsCompanyChart1Y where co_code=@co_code ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.AddWithValue("@co_code", comp_code);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        result = true;
                    }
                    sdr.Close();

                    if (result == false)
                    {
                        foreach (var mod in objCCC1YLst)
                        {
                            sqlstr = "insert into CmotsCompanyChart1Y(co_code, upd_date, price, volume, created_by, created_on)";
                            sqlstr = sqlstr + " values (@co_code, @upd_date, @price, @volume, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@co_code", comp_code);
                            cmd.Parameters.AddWithValue("@upd_date", mod.upd_date);
                            cmd.Parameters.AddWithValue("@price", mod.price);
                            cmd.Parameters.AddWithValue("@volume", mod.volume);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }

                    }
                    else if (result == true)
                    {
                        sqlstr = "delete from CmotsCompanyChart1Y where co_code=@co_code";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@co_code", comp_code);
                        cmd.ExecuteNonQuery();

                        foreach (var mod in objCCC1YLst)
                        {
                            sqlstr = "insert into CmotsCompanyChart1Y(co_code, upd_date, price, volume, created_by, created_on)";
                            sqlstr = sqlstr + " values (@co_code, @upd_date, @price, @volume, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@co_code", comp_code);
                            cmd.Parameters.AddWithValue("@upd_date", mod.upd_date);
                            cmd.Parameters.AddWithValue("@price", mod.price);
                            cmd.Parameters.AddWithValue("@volume", mod.volume);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }
                    }
                    result = false;


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion

        #region Cmots Get Cmots Company Chart 5Y

        [HttpGet]
        public ActionResult GetCmotsCompanyChart5Y()
        {
            List<CmotsCompanyMaster> objCmotsCompLits = new List<CmotsCompanyMaster>();
            try
            {
                gConnection.Open();
                string sqlstr = "select co_code from dbo.CmotsCompanyMaster order by co_code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCompanyMaster objCmots = new CmotsCompanyMaster();
                    objCmots.co_code = sdr["co_code"].ToString();
                    objCmotsCompLits.Add(objCmots);
                }
                gConnection.Close();

            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View(objCmotsCompLits);

        }


        [HttpPost]
        public void GetCmotsCompanyChart5Y(string group, string comp_code)
        {
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsCompanyChart5Y> objCCC5YLst = new List<CmotsCompanyChart5Y>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsCompanyChart5Y objCCC5Y = new CmotsCompanyChart5Y();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "upd_date".ToUpper())
                        {
                            objCCC5Y.upd_date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "price".ToUpper())
                        {
                            objCCC5Y.price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "volume".ToUpper())
                        {
                            objCCC5Y.volume = app.Value.ToString();
                        }
                    }
                    objCCC5YLst.Add(objCCC5Y);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    sqlstr = " select * from CmotsCompanyChart5Y where co_code=@co_code ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.AddWithValue("@co_code", comp_code);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        result = true;
                    }
                    sdr.Close();

                    if (result == false)
                    {
                        foreach (var mod in objCCC5YLst)
                        {
                            sqlstr = "insert into CmotsCompanyChart5Y(co_code, upd_date, price, volume, created_by, created_on)";
                            sqlstr = sqlstr + " values (@co_code, @upd_date, @price, @volume, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@co_code", comp_code);
                            cmd.Parameters.AddWithValue("@upd_date", mod.upd_date);
                            cmd.Parameters.AddWithValue("@price", mod.price);
                            cmd.Parameters.AddWithValue("@volume", mod.volume);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }

                    }
                    else if (result == true)
                    {
                        sqlstr = "delete from CmotsCompanyChart5Y where co_code=@co_code";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@co_code", comp_code);
                        cmd.ExecuteNonQuery();

                        foreach (var mod in objCCC5YLst)
                        {
                            sqlstr = "insert into CmotsCompanyChart5Y(co_code, upd_date, price, volume, created_by, created_on)";
                            sqlstr = sqlstr + " values (@co_code, @upd_date, @price, @volume, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@co_code", comp_code);
                            cmd.Parameters.AddWithValue("@upd_date", mod.upd_date);
                            cmd.Parameters.AddWithValue("@price", mod.price);
                            cmd.Parameters.AddWithValue("@volume", mod.volume);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }
                    }
                    result = false;


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion




       
        #region Cmots Get Cmots Company News

        [HttpGet]
        public ActionResult GetCmotsCompanyNews()
        {
            List<CmotsCompanyMaster> objCmotsCompLits = new List<CmotsCompanyMaster>();
            try
            {
                gConnection.Open();
                string sqlstr = "select co_code from dbo.CmotsCompanyMaster order by co_code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCompanyMaster objCmots = new CmotsCompanyMaster();
                    objCmots.co_code = sdr["co_code"].ToString();
                    objCmotsCompLits.Add(objCmots);
                }
                gConnection.Close();

            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View(objCmotsCompLits);

        }




        [HttpPost]        
        public void GetCmotsCompanyNews(string group, string comp_code)
        {

            //string m = RemoveSpecialCharacters(group);
            //m = m.Replace(System.Environment.NewLine, " ");
            ////m = m.Replace('"', ''');
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsCompanyNews> objCCNLst = new List<CmotsCompanyNews>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsCompanyNews objCCN = new CmotsCompanyNews();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "sr_no".ToUpper())
                        {
                            objCCN.sr_no = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "date".ToUpper())
                        {
                            objCCN.date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "caption".ToUpper())
                        {
                            objCCN.caption = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_code".ToUpper())
                        {
                            objCCN.co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "yrc".ToUpper())
                        {
                            objCCN.yrc = app.Value.ToString();
                        }   
                    }
                    objCCNLst.Add(objCCN);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    sqlstr = " select * from CmotsCompanyNews where co_code=@co_code ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                     cmd.Parameters.AddWithValue("@co_code", comp_code);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        result = true;
                    }
                    sdr.Close();

                    if (result == false)
                    {
                        foreach (var mod in objCCNLst)
                        {
                            sqlstr = "insert into dbo.CmotsCompanyNews(sr_no,co_code,date,caption,yrc,created_by,created_on)";
                            sqlstr = sqlstr + " values (@sr_no,@co_code,@date,@caption,@yrc, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@sr_no", mod.sr_no);
                            cmd.Parameters.AddWithValue("@co_code", mod.co_code);
                            cmd.Parameters.AddWithValue("@caption", mod.caption);
                            cmd.Parameters.AddWithValue("@date", mod.date);
                            cmd.Parameters.AddWithValue("@yrc", mod.yrc);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }

                    }
                    else if (result == true)
                    {
                        sqlstr = "delete from CmotsCompanyNews where co_code=@co_code";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                         cmd.Parameters.AddWithValue("@co_code", comp_code);
                        cmd.ExecuteNonQuery();

                        foreach (var mod in objCCNLst)
                        {
                            sqlstr = "insert into dbo.CmotsCompanyNews(sr_no,co_code,date,caption,yrc,created_by,created_on)";
                            sqlstr = sqlstr + " values (@sr_no,@co_code,@date,@caption,@yrc, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@sr_no", mod.sr_no);
                            cmd.Parameters.AddWithValue("@co_code", mod.co_code);
                            cmd.Parameters.AddWithValue("@caption", mod.caption);
                            cmd.Parameters.AddWithValue("@date", mod.date);
                            cmd.Parameters.AddWithValue("@yrc", mod.yrc);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }
                    }
                    result = false;


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

        }






        [HttpGet]
        [ValidateInput(false)]
        public ActionResult GetCmotsCompanyNews_Details()
        {
            List<CmotsCompanyNews> objCmotsCompLits = new List<CmotsCompanyNews>();
            try
            {
                gConnection.Open();
                string sqlstr = "select sr_no from dbo.CmotsCompanyNews order by co_code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCompanyNews objCmots = new CmotsCompanyNews();
                    objCmots.sr_no = sdr["sr_no"].ToString();
                    objCmotsCompLits.Add(objCmots);
                }
                gConnection.Close();

            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View(objCmotsCompLits);

        }




        [HttpPost]
        public void GetCmotsCompanyNews_Details(string group, string sr_no1)        
        {

            //string m = RemoveSpecialCharacters(group);
            //m = m.Replace(System.Environment.NewLine, " ");
            ////m = m.Replace('"', ''');
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsCompanyNews_Details> objCCNLst = new List<CmotsCompanyNews_Details>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsCompanyNews_Details objCCN = new CmotsCompanyNews_Details();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "sr_no".ToUpper())
                        {
                            objCCN.sr_no = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "date".ToUpper())
                        {
                            objCCN.date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "caption".ToUpper())
                        {
                            objCCN.caption = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_code".ToUpper())
                        {
                            objCCN.co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "memo".ToUpper())
                        {
                            objCCN.memo = app.Value.ToString();
                        }
                    }
                    objCCNLst.Add(objCCN);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    foreach (var mod in objCCNLst)
                    {
                        sqlstr = "delete from CmotsCompanyNews_Details where sr_no=@sr_no";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@sr_no", sr_no1);
                        cmd.ExecuteNonQuery();


                        sqlstr = "insert into dbo.CmotsCompanyNews_Details(sr_no,date,caption,memo,created_by,created_on)";
                        sqlstr = sqlstr + " values (@sr_no,@date,@caption,@memo, @created_by, GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@sr_no", mod.sr_no);
                        //cmd.Parameters.AddWithValue("@co_code", mod.co_code);
                        cmd.Parameters.AddWithValue("@caption", mod.caption);
                        cmd.Parameters.AddWithValue("@date", mod.date);
                        cmd.Parameters.AddWithValue("@memo", mod.memo);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();
                    }
                   // result = false;


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

        }



        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            string rem = "";
            foreach (char c in str)
            {
                if ((c == '<') || (c == '>') || (c == '-'))
                {                   
                }
                else
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        #endregion




        #region Cmots MF Holdings

        [HttpGet]
        public ActionResult GetCmotsMFHoldings()
        {
            List<CmotsCompanyMaster> objCmotsCompLits = new List<CmotsCompanyMaster>();
            try
            {
                gConnection.Open();
                string sqlstr = "select co_code from dbo.CmotsCompanyMaster order by co_code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCompanyMaster objCmots = new CmotsCompanyMaster();
                    objCmots.co_code = sdr["co_code"].ToString();
                    objCmotsCompLits.Add(objCmots);
                }
                gConnection.Close();

            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View(objCmotsCompLits);

        }



        [HttpPost]
        public void GetCmotsMFHoldings(string group, string comp_code)
        {           
            try
            {
                
                bool result = false;
                List<CmotsMFHoldings> objCmotsMFHoldLst = new List<CmotsMFHoldings>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsMFHoldings objCmotsMFHold = new CmotsMFHoldings();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "sch_name".ToUpper())
                        {
                            objCmotsMFHold.sch_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "mf_schcode".ToUpper())
                        {
                            objCmotsMFHold.mf_schcode = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "invdate".ToUpper())
                        {
                            objCmotsMFHold.invdate = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "mktvalue".ToUpper())
                        {
                            objCmotsMFHold.mktvalue = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_code".ToUpper())
                        {
                            objCmotsMFHold.co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_name".ToUpper())
                        {
                            objCmotsMFHold.co_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "perc_hold".ToUpper())
                        {
                            objCmotsMFHold.perc_hold = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "totnav".ToUpper())
                        {
                            objCmotsMFHold.totnav = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "sch_type".ToUpper())
                        {
                            objCmotsMFHold.sch_type = app.Value.ToString();
                        }
                    }
                    objCmotsMFHoldLst.Add(objCmotsMFHold);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    sqlstr = " select * from CmotsMFHoldings where co_code=@co_code ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.AddWithValue("@co_code", comp_code);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        result = true;
                    }
                    sdr.Close();

                    if (result == false)
                    {
                        foreach (var mod in objCmotsMFHoldLst)
                        {
                            sqlstr = "insert into CmotsMFHoldings(sch_name,mf_schcode,invdate,mktvalue,";
                            sqlstr = sqlstr + " co_code,co_name,perc_hold,totnav,sch_type,created_by,created_on)";
                            sqlstr = sqlstr + " values (@sch_name,@mf_schcode,@invdate,@mktvalue,";
                            sqlstr = sqlstr + " @co_code,@co_name,@perc_hold,@totnav,@sch_type,@created_by,GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@sch_name", mod.sch_name);
                            cmd.Parameters.AddWithValue("@mf_schcode", mod.mf_schcode);
                            cmd.Parameters.AddWithValue("@invdate", mod.invdate);
                            cmd.Parameters.AddWithValue("@mktvalue", mod.mktvalue);
                            cmd.Parameters.AddWithValue("@co_code", mod.co_code);
                            cmd.Parameters.AddWithValue("@co_name", mod.co_name);
                            cmd.Parameters.AddWithValue("@perc_hold", mod.perc_hold);
                            cmd.Parameters.AddWithValue("@totnav", mod.totnav);
                            cmd.Parameters.AddWithValue("@sch_type", mod.sch_type);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }

                    }
                    else if (result == true)
                    {
                        sqlstr = "delete from CmotsMFHoldings where co_code=@co_code";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@co_code", comp_code);
                        cmd.ExecuteNonQuery();

                        foreach (var mod in objCmotsMFHoldLst)
                        {
                            sqlstr = "insert into CmotsMFHoldings(sch_name,mf_schcode,invdate,mktvalue,";
                            sqlstr = sqlstr + " co_code,co_name,perc_hold,totnav,sch_type,created_by,created_on)";
                            sqlstr = sqlstr + " values (@sch_name,@mf_schcode,@invdate,@mktvalue,";
                            sqlstr = sqlstr + " @co_code,@co_name,@perc_hold,@totnav,@sch_type,@created_by,GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@sch_name", mod.sch_name);
                            cmd.Parameters.AddWithValue("@mf_schcode", mod.mf_schcode);
                            cmd.Parameters.AddWithValue("@invdate", mod.invdate);
                            cmd.Parameters.AddWithValue("@mktvalue", mod.mktvalue);
                            cmd.Parameters.AddWithValue("@co_code", mod.co_code);
                            cmd.Parameters.AddWithValue("@co_name", mod.co_name);
                            cmd.Parameters.AddWithValue("@perc_hold", mod.perc_hold);
                            cmd.Parameters.AddWithValue("@totnav", mod.totnav);
                            cmd.Parameters.AddWithValue("@sch_type", mod.sch_type);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }
                    }
                    result = false;


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion


        #region Cmots Share Holding Pattern

        [HttpGet]
        public ActionResult GetCmotsShareHoldingPattern()
        {
            List<CmotsCompanyMaster> objCmotsCompLits = new List<CmotsCompanyMaster>();
            try
            {
                gConnection.Open();
                string sqlstr = "select co_code from dbo.CmotsCompanyMaster order by co_code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CmotsCompanyMaster objCmots = new CmotsCompanyMaster();
                    objCmots.co_code = sdr["co_code"].ToString();
                    objCmotsCompLits.Add(objCmots);
                }
                gConnection.Close();

            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return View(objCmotsCompLits);

        }



        [HttpPost]
        public void GetCmotsShareHoldingPattern(string group, string comp_code)
        {
            try
            {
                bool result = false;
                List<CmotsShareHoldingPattern> objCmotsShareHoldLst = new List<CmotsShareHoldingPattern>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsShareHoldingPattern objCmotsShareHold = new CmotsShareHoldingPattern();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "type".ToUpper())
                        {
                            objCmotsShareHold.type = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "yearmonth".ToUpper())
                        {
                            objCmotsShareHold.yearmonth = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "total-foreign-promoter-and-group-1".ToUpper())
                        {
                            objCmotsShareHold.total_foreign_promoter_and_group_1 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "total-foreign-promoter-and-group-2".ToUpper())
                        {
                            objCmotsShareHold.total_foreign_promoter_and_group_2 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "total-foreign-promoter-and-group-3".ToUpper())
                        {
                            objCmotsShareHold.total_foreign_promoter_and_group_3 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "total-foreign-promoter-and-group-4".ToUpper())
                        {
                            objCmotsShareHold.total_foreign_promoter_and_group_4 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "indian-promoter-and-group-1".ToUpper())
                        {
                            objCmotsShareHold.indian_promoter_and_group_1 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "indian-promoter-and-group-2".ToUpper())
                        {
                            objCmotsShareHold.indian_promoter_and_group_2 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "indian-promoter-and-group-3".ToUpper())
                        {
                            objCmotsShareHold.indian_promoter_and_group_3 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "indian-promoter-and-group-4".ToUpper())
                        {
                            objCmotsShareHold.indian_promoter_and_group_4 = app.Value.ToString();
                        }

                        else if (appName.ToUpper() == "total-of-promoter-1".ToUpper())
                        {
                            objCmotsShareHold.total_of_promoter_1 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "total-of-promoter-2".ToUpper())
                        {
                            objCmotsShareHold.total_of_promoter_2 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "total-of-promoter-3".ToUpper())
                        {
                            objCmotsShareHold.total_of_promoter_3 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "total-of-promoter-4".ToUpper())
                        {
                            objCmotsShareHold.total_of_promoter_4 = app.Value.ToString();
                        }


                        else if (appName.ToUpper() == "non-promoter-institution-1".ToUpper())
                        {
                            objCmotsShareHold.non_promoter_institution_1 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "non-promoter-institution-2".ToUpper())
                        {
                            objCmotsShareHold.non_promoter_institution_2 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "non-promoter-institution-3".ToUpper())
                        {
                            objCmotsShareHold.non_promoter_institution_3 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "non-promoter-institution-4".ToUpper())
                        {
                            objCmotsShareHold.non_promoter_institution_4 = app.Value.ToString();
                        }


                        else if (appName.ToUpper() == "total-non-promoter-1".ToUpper())
                        {
                            objCmotsShareHold.total_non_promoter_1 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "total-non-promoter-2".ToUpper())
                        {
                            objCmotsShareHold.total_non_promoter_2 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "total-non-promoter-3".ToUpper())
                        {
                            objCmotsShareHold.total_non_promoter_3 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "total-non-promoter-4".ToUpper())
                        {
                            objCmotsShareHold.total_non_promoter_4 = app.Value.ToString();
                        }



                        else if (appName.ToUpper() == "total-promoter-and-non-promoter-1".ToUpper())
                        {
                            objCmotsShareHold.total_promoter_and_non_promoter_1 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "total-promoter-and-non-promoter-2".ToUpper())
                        {
                            objCmotsShareHold.total_promoter_and_non_promoter_2 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "total-promoter-and-non-promoter-3".ToUpper())
                        {
                            objCmotsShareHold.total_promoter_and_non_promoter_3 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "total-promoter-and-non-promoter-4".ToUpper())
                        {
                            objCmotsShareHold.total_promoter_and_non_promoter_4 = app.Value.ToString();
                        }



                        else if (appName.ToUpper() == "custodiansagainst-depository-receipts-1".ToUpper())
                        {
                            objCmotsShareHold.custodiansagainst_depository_receipts_1 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "custodiansagainst-depository-receipts-2".ToUpper())
                        {
                            objCmotsShareHold.custodiansagainst_depository_receipts_2 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "custodiansagainst-depository-receipts-3".ToUpper())
                        {
                            objCmotsShareHold.custodiansagainst_depository_receipts_3 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "custodiansagainst-depository-receipts-4".ToUpper())
                        {
                            objCmotsShareHold.custodiansagainst_depository_receipts_4 = app.Value.ToString();
                        }



                        else if (appName.ToUpper() == "grand-total-1".ToUpper())
                        {
                            objCmotsShareHold.grand_total_1 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "grand-total-2".ToUpper())
                        {
                            objCmotsShareHold.grand_total_2 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "grand-total-3".ToUpper())
                        {
                            objCmotsShareHold.grand_total_3 = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "grand-total-4".ToUpper())
                        {
                            objCmotsShareHold.grand_total_4 = app.Value.ToString();
                        }
                    }
                    objCmotsShareHoldLst.Add(objCmotsShareHold);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    sqlstr = " select * from CmotsShareHoldingPattern where co_code=@co_code ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.AddWithValue("@co_code", comp_code);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        result = true;
                    }
                    sdr.Close();

                    if (result == false)
                    {
                        foreach (var mod in objCmotsShareHoldLst)
                        {
                            sqlstr = "insert into CmotsShareHoldingPattern(type, yearmonth, total_foreign_promoter_and_group_1,total_foreign_promoter_and_group_2,";
                            sqlstr = sqlstr + " total_foreign_promoter_and_group_3, total_foreign_promoter_and_group_4,indian_promoter_and_group_1,indian_promoter_and_group_2,";
                            sqlstr = sqlstr + " indian_promoter_and_group_3, indian_promoter_and_group_4, total_of_promoter_1, total_of_promoter_2, total_of_promoter_3, ";
                            sqlstr = sqlstr + " total_of_promoter_4, non_promoter_institution_1, non_promoter_institution_2, non_promoter_non_institution_3, ";
                            sqlstr = sqlstr + " non_promoter_non_institution_4, total_non_promoter_1, total_non_promoter_2, total_non_promoter_3, total_non_promoter_4,";
                            sqlstr = sqlstr + " total_promoter_and_non_promoter_1, total_promoter_and_non_promoter_2, total_promoter_and_non_promoter_3, ";
                            sqlstr = sqlstr + " total_promoter_and_non_promoter_4, custodiansagainst_depository_receipts_1, custodiansagainst_depository_receipts_2,";
                            sqlstr = sqlstr + " custodiansagainst_depository_receipts_3, custodiansagainst_depository_receipts_4, grand_total_1, grand_total_2,";
                            sqlstr = sqlstr + " grand_total_3, grand_total_4,co_code, created_by, created_on)";
                            sqlstr = sqlstr + " values (@type, @yearmonth, @total_foreign_promoter_and_group_1,@total_foreign_promoter_and_group_2,";
                            sqlstr = sqlstr + " @total_foreign_promoter_and_group_3, @total_foreign_promoter_and_group_4,@indian_promoter_and_group_1,@indian_promoter_and_group_2,";
                            sqlstr = sqlstr + " @indian_promoter_and_group_3, @indian_promoter_and_group_4, @total_of_promoter_1, @total_of_promoter_2, @total_of_promoter_3, ";
                            sqlstr = sqlstr + " @total_of_promoter_4, @non_promoter_institution_1, @non_promoter_institution_2, @non_promoter_institution_3, ";
                            sqlstr = sqlstr + " @non_promoter_institution_4, @total_non_promoter_1, @total_non_promoter_2, @total_non_promoter_3, @total_non_promoter_4,";
                            sqlstr = sqlstr + " @total_promoter_and_non_promoter_1, @total_promoter_and_non_promoter_2, @total_promoter_and_non_promoter_3, ";
                            sqlstr = sqlstr + " @total_promoter_and_non_promoter_4, @custodiansagainst_depository_receipts_1, @custodiansagainst_depository_receipts_2,";
                            sqlstr = sqlstr + " @custodiansagainst_depository_receipts_3, @custodiansagainst_depository_receipts_4, @grand_total_1, @grand_total_2,";
                            sqlstr = sqlstr + " @grand_total_3, @grand_total_4,@co_code, @created_by, GetDate() )";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@type", mod.type);
                            cmd.Parameters.AddWithValue("@yearmonth", mod.yearmonth);
                            cmd.Parameters.AddWithValue("@total_foreign_promoter_and_group_1", mod.total_foreign_promoter_and_group_1);
                            cmd.Parameters.AddWithValue("@total_foreign_promoter_and_group_2", mod.total_foreign_promoter_and_group_2);
                            cmd.Parameters.AddWithValue("@total_foreign_promoter_and_group_3", mod.total_foreign_promoter_and_group_3);
                            cmd.Parameters.AddWithValue("@total_foreign_promoter_and_group_4", mod.total_foreign_promoter_and_group_4);
                            cmd.Parameters.AddWithValue("@indian_promoter_and_group_1", mod.indian_promoter_and_group_1);
                            cmd.Parameters.AddWithValue("@indian_promoter_and_group_2", mod.indian_promoter_and_group_2);
                            cmd.Parameters.AddWithValue("@indian_promoter_and_group_3", mod.indian_promoter_and_group_3);

                            cmd.Parameters.AddWithValue("@indian_promoter_and_group_4", mod.indian_promoter_and_group_4);
                            cmd.Parameters.AddWithValue("@total_of_promoter_1", mod.total_of_promoter_1);
                            cmd.Parameters.AddWithValue("@total_of_promoter_2", mod.total_of_promoter_2);
                            cmd.Parameters.AddWithValue("@total_of_promoter_3", mod.total_of_promoter_3);
                            cmd.Parameters.AddWithValue("@total_of_promoter_4", mod.total_of_promoter_4);
                            cmd.Parameters.AddWithValue("@non_promoter_institution_1", mod.non_promoter_institution_1);
                            cmd.Parameters.AddWithValue("@non_promoter_institution_2", mod.non_promoter_institution_2);
                            cmd.Parameters.AddWithValue("@non_promoter_institution_3", mod.non_promoter_institution_3);
                            cmd.Parameters.AddWithValue("@non_promoter_institution_4", mod.non_promoter_institution_4);
                            cmd.Parameters.AddWithValue("@total_non_promoter_1", mod.total_non_promoter_1);
                            cmd.Parameters.AddWithValue("@total_non_promoter_2", mod.total_non_promoter_2);
                            cmd.Parameters.AddWithValue("@total_non_promoter_3", mod.total_non_promoter_3);
                            cmd.Parameters.AddWithValue("@total_non_promoter_4", mod.total_non_promoter_4);
                            cmd.Parameters.AddWithValue("@total_promoter_and_non_promoter_1", mod.total_promoter_and_non_promoter_1);
                            cmd.Parameters.AddWithValue("@total_promoter_and_non_promoter_2", mod.total_promoter_and_non_promoter_2);
                            cmd.Parameters.AddWithValue("@total_promoter_and_non_promoter_3", mod.total_promoter_and_non_promoter_3);
                            cmd.Parameters.AddWithValue("@total_promoter_and_non_promoter_4", mod.total_promoter_and_non_promoter_4);
                            cmd.Parameters.AddWithValue("@custodiansagainst_depository_receipts_1", mod.custodiansagainst_depository_receipts_1);
                            cmd.Parameters.AddWithValue("@custodiansagainst_depository_receipts_2", mod.custodiansagainst_depository_receipts_2);
                            cmd.Parameters.AddWithValue("@custodiansagainst_depository_receipts_3", mod.custodiansagainst_depository_receipts_3);
                            cmd.Parameters.AddWithValue("@custodiansagainst_depository_receipts_4", mod.custodiansagainst_depository_receipts_4);
                            cmd.Parameters.AddWithValue("@grand_total_1", mod.grand_total_1);
                            cmd.Parameters.AddWithValue("@grand_total_2", mod.grand_total_2);
                            cmd.Parameters.AddWithValue("@grand_total_3", mod.grand_total_3);
                            cmd.Parameters.AddWithValue("@grand_total_4", mod.grand_total_4);
                            cmd.Parameters.AddWithValue("@co_code", comp_code);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }

                    }
                    else if (result == true)
                    {
                        sqlstr = "delete from CmotsShareHoldingPattern where co_code=@co_code";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@co_code", comp_code);
                        cmd.ExecuteNonQuery();

                        foreach (var mod in objCmotsShareHoldLst)
                        {
                            sqlstr = "insert into CmotsShareHoldingPattern(type, yearmonth, total_foreign_promoter_and_group_1,total_foreign_promoter_and_group_2,";
                            sqlstr = sqlstr + " total_foreign_promoter_and_group_3, total_foreign_promoter_and_group_4,indian_promoter_and_group_1,indian_promoter_and_group_2,";
                            sqlstr = sqlstr + " indian_promoter_and_group_3, indian_promoter_and_group_4, total_of_promoter_1, total_of_promoter_2, total_of_promoter_3, ";
                            sqlstr = sqlstr + " total_of_promoter_4, non_promoter_institution_1, non_promoter_institution_2, non_promoter_non_institution_3, ";
                            sqlstr = sqlstr + " non_promoter_non_institution_4, total_non_promoter_1, total_non_promoter_2, total_non_promoter_3, total_non_promoter_4,";
                            sqlstr = sqlstr + " total_promoter_and_non_promoter_1, total_promoter_and_non_promoter_2, total_promoter_and_non_promoter_3, ";
                            sqlstr = sqlstr + " total_promoter_and_non_promoter_4, custodiansagainst_depository_receipts_1, custodiansagainst_depository_receipts_2,";
                            sqlstr = sqlstr + " custodiansagainst_depository_receipts_3, custodiansagainst_depository_receipts_4, grand_total_1, grand_total_2,";
                            sqlstr = sqlstr + " grand_total_3, grand_total_4,co_code, created_by, created_on)";
                            sqlstr = sqlstr + " values (@type, @yearmonth, @total_foreign_promoter_and_group_1,@total_foreign_promoter_and_group_2,";
                            sqlstr = sqlstr + " @total_foreign_promoter_and_group_3, @total_foreign_promoter_and_group_4,@indian_promoter_and_group_1,@indian_promoter_and_group_2,";
                            sqlstr = sqlstr + " @indian_promoter_and_group_3, @indian_promoter_and_group_4, @total_of_promoter_1, @total_of_promoter_2, @total_of_promoter_3, ";
                            sqlstr = sqlstr + " @total_of_promoter_4, @non_promoter_institution_1, @non_promoter_institution_2, @non_promoter_institution_3, ";
                            sqlstr = sqlstr + " @non_promoter_institution_4, @total_non_promoter_1, @total_non_promoter_2, @total_non_promoter_3, @total_non_promoter_4,";
                            sqlstr = sqlstr + " @total_promoter_and_non_promoter_1, @total_promoter_and_non_promoter_2, @total_promoter_and_non_promoter_3, ";
                            sqlstr = sqlstr + " @total_promoter_and_non_promoter_4, @custodiansagainst_depository_receipts_1, @custodiansagainst_depository_receipts_2,";
                            sqlstr = sqlstr + " @custodiansagainst_depository_receipts_3, @custodiansagainst_depository_receipts_4, @grand_total_1, @grand_total_2,";
                            sqlstr = sqlstr + " @grand_total_3, @grand_total_4,@co_code, @created_by, GetDate() )";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@type", mod.type);
                            cmd.Parameters.AddWithValue("@yearmonth", mod.yearmonth);
                            cmd.Parameters.AddWithValue("@total_foreign_promoter_and_group_1", mod.total_foreign_promoter_and_group_1);
                            cmd.Parameters.AddWithValue("@total_foreign_promoter_and_group_2", mod.total_foreign_promoter_and_group_2);
                            cmd.Parameters.AddWithValue("@total_foreign_promoter_and_group_3", mod.total_foreign_promoter_and_group_3);
                            cmd.Parameters.AddWithValue("@total_foreign_promoter_and_group_4", mod.total_foreign_promoter_and_group_4);
                            cmd.Parameters.AddWithValue("@indian_promoter_and_group_1", mod.indian_promoter_and_group_1);
                            cmd.Parameters.AddWithValue("@indian_promoter_and_group_2", mod.indian_promoter_and_group_2);
                            cmd.Parameters.AddWithValue("@indian_promoter_and_group_3", mod.indian_promoter_and_group_3);

                            cmd.Parameters.AddWithValue("@indian_promoter_and_group_4", mod.indian_promoter_and_group_4);
                            cmd.Parameters.AddWithValue("@total_of_promoter_1", mod.total_of_promoter_1);
                            cmd.Parameters.AddWithValue("@total_of_promoter_2", mod.total_of_promoter_2);
                            cmd.Parameters.AddWithValue("@total_of_promoter_3", mod.total_of_promoter_3);
                            cmd.Parameters.AddWithValue("@total_of_promoter_4", mod.total_of_promoter_4);
                            cmd.Parameters.AddWithValue("@non_promoter_institution_1", mod.non_promoter_institution_1);
                            cmd.Parameters.AddWithValue("@non_promoter_institution_2", mod.non_promoter_institution_2);
                            cmd.Parameters.AddWithValue("@non_promoter_institution_3", mod.non_promoter_institution_3);
                            cmd.Parameters.AddWithValue("@non_promoter_institution_4", mod.non_promoter_institution_4);
                            cmd.Parameters.AddWithValue("@total_non_promoter_1", mod.total_non_promoter_1);
                            cmd.Parameters.AddWithValue("@total_non_promoter_2", mod.total_non_promoter_2);
                            cmd.Parameters.AddWithValue("@total_non_promoter_3", mod.total_non_promoter_3);
                            cmd.Parameters.AddWithValue("@total_non_promoter_4", mod.total_non_promoter_4);
                            cmd.Parameters.AddWithValue("@total_promoter_and_non_promoter_1", mod.total_promoter_and_non_promoter_1);
                            cmd.Parameters.AddWithValue("@total_promoter_and_non_promoter_2", mod.total_promoter_and_non_promoter_2);
                            cmd.Parameters.AddWithValue("@total_promoter_and_non_promoter_3", mod.total_promoter_and_non_promoter_3);
                            cmd.Parameters.AddWithValue("@total_promoter_and_non_promoter_4", mod.total_promoter_and_non_promoter_4);
                            cmd.Parameters.AddWithValue("@custodiansagainst_depository_receipts_1", mod.custodiansagainst_depository_receipts_1);
                            cmd.Parameters.AddWithValue("@custodiansagainst_depository_receipts_2", mod.custodiansagainst_depository_receipts_2);
                            cmd.Parameters.AddWithValue("@custodiansagainst_depository_receipts_3", mod.custodiansagainst_depository_receipts_3);
                            cmd.Parameters.AddWithValue("@custodiansagainst_depository_receipts_4", mod.custodiansagainst_depository_receipts_4);
                            cmd.Parameters.AddWithValue("@grand_total_1", mod.grand_total_1);
                            cmd.Parameters.AddWithValue("@grand_total_2", mod.grand_total_2);
                            cmd.Parameters.AddWithValue("@grand_total_3", mod.grand_total_3);
                            cmd.Parameters.AddWithValue("@grand_total_4", mod.grand_total_4);
                            cmd.Parameters.AddWithValue("@co_code", comp_code);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }
                    }
                    result = false;


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion





        #region Cmots NSE Ticker

        [HttpGet]
        public ActionResult GetCmotsNSETicker()
        {           
            return View();
        }



        [HttpPost]
        public void GetCmotsNSETicker(string group)
        {
            try
            {
                //bool result = false;
                List<CmotsNSETicker> objCmotsTickLst = new List<CmotsNSETicker>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsNSETicker objCmotsNSETick = new CmotsNSETicker();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "co_code".ToUpper())
                        {
                            objCmotsNSETick.co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_name".ToUpper())
                        {
                            objCmotsNSETick.co_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "price".ToUpper())
                        {
                            objCmotsNSETick.price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "price_diff".ToUpper())
                        {
                            objCmotsNSETick.price_diff = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "tr_date".ToUpper())
                        {
                            objCmotsNSETick.tr_date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "change".ToUpper())
                        {
                            objCmotsNSETick.change = app.Value.ToString();
                        }
                        
                    }
                    objCmotsTickLst.Add(objCmotsNSETick);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    //sqlstr = " select * from CmotsShareHoldingPattern where co_code=@co_code ";
                    //cmd.Parameters.Clear();
                    //cmd.CommandText = sqlstr;
                    //cmd.Parameters.AddWithValue("@co_code", comp_code);
                    //SqlDataReader sdr = cmd.ExecuteReader();
                    //while (sdr.Read())
                    //{
                    //    result = true;
                    //}
                    //sdr.Close();

                    //if (result == false)
                    //{
                    //    foreach (var mod in objCmotsTickLst)
                    //    {
                    //        sqlstr = "Insert Into CmotsNSETicker(co_code, co_name, price, price_diff, tr_date, change, created_by, created_on)";
                    //        sqlstr = sqlstr + " values (@co_code, @co_name, @price, @price_diff, @tr_date, @change, @created_by, GetDate())";
                    //        cmd.Parameters.Clear();
                    //        cmd.CommandText = sqlstr;
                    //        cmd.Parameters.AddWithValue("@co_code", mod.co_code);
                    //        cmd.Parameters.AddWithValue("@co_name", mod.co_name);
                    //        cmd.Parameters.AddWithValue("@price", mod.price);
                    //        cmd.Parameters.AddWithValue("@price_diff", mod.price_diff);
                    //        cmd.Parameters.AddWithValue("@tr_date", mod.tr_date);
                    //        cmd.Parameters.AddWithValue("@change", mod.change);
                    //        cmd.Parameters.AddWithValue("@created_by", "Online");
                    //        cmd.ExecuteNonQuery();
                    //    }

                    //}
                    //else if (result == true)
                    //{
                    //    sqlstr = "delete from CmotsNSETicker where co_code=@co_code";
                    //    cmd.Parameters.Clear();
                    //    cmd.CommandText = sqlstr;
                    //    cmd.Parameters.AddWithValue("@co_code", comp_code);
                    //    cmd.ExecuteNonQuery();

                    //    foreach (var mod in objCmotsTickLst)
                    //    {
                    //        sqlstr = "Insert Into CmotsNSETicker(co_code, co_name, price, price_diff, tr_date, change, created_by, created_on)";
                    //        sqlstr = sqlstr + " values (@co_code, @co_name, @price, @price_diff, @tr_date, @change, @created_by, GetDate())";
                    //        cmd.Parameters.Clear();
                    //        cmd.CommandText = sqlstr;
                    //        cmd.Parameters.AddWithValue("@co_code", mod.co_code);
                    //        cmd.Parameters.AddWithValue("@co_name", mod.co_name);
                    //        cmd.Parameters.AddWithValue("@price", mod.price);
                    //        cmd.Parameters.AddWithValue("@price_diff", mod.price_diff);
                    //        cmd.Parameters.AddWithValue("@tr_date", mod.tr_date);
                    //        cmd.Parameters.AddWithValue("@change", mod.change);
                    //        cmd.Parameters.AddWithValue("@created_by", "Online");
                    //        cmd.ExecuteNonQuery();
                    //    }
                    //}
                    //result = false;


                    for (int i = 0; i < objCmotsTickLst.Count; i++ )
                    {
                        if(objCmotsTickLst[i].co_code != "")
                        {
                            sqlstr = "delete from CmotsNSETicker";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.ExecuteNonQuery();
                        }
                        i = objCmotsTickLst.Count;
                    }


                        foreach (var mod in objCmotsTickLst)
                        {
                            sqlstr = "Insert Into CmotsNSETicker(co_code, co_name, price, price_diff, tr_date, change, created_by, created_on)";
                            sqlstr = sqlstr + " values (@co_code, @co_name, @price, @price_diff, @tr_date, @change, @created_by, GetDate())";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Parameters.AddWithValue("@co_code", mod.co_code);
                            cmd.Parameters.AddWithValue("@co_name", mod.co_name);
                            cmd.Parameters.AddWithValue("@price", mod.price);
                            cmd.Parameters.AddWithValue("@price_diff", mod.price_diff);
                            cmd.Parameters.AddWithValue("@tr_date", mod.tr_date);
                            cmd.Parameters.AddWithValue("@change", mod.change);
                            cmd.Parameters.AddWithValue("@created_by", "Online");
                            cmd.ExecuteNonQuery();
                        }

                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion

        #region Cmots BSE Ticker

        [HttpGet]
        public ActionResult GetCmotsBSETicker()
        {
            return View();
        }



        [HttpPost]
        public void GetCmotsBSETicker(string group)
        {
            try
            {
                //bool result = false;
                List<CmotsBSETicker> objCmotsTickLst = new List<CmotsBSETicker>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsBSETicker objCmotsBSETick = new CmotsBSETicker();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "co_code".ToUpper())
                        {
                            objCmotsBSETick.co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_name".ToUpper())
                        {
                            objCmotsBSETick.co_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "price".ToUpper())
                        {
                            objCmotsBSETick.price = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "price_diff".ToUpper())
                        {
                            objCmotsBSETick.price_diff = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "tr_date".ToUpper())
                        {
                            objCmotsBSETick.tr_date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "change".ToUpper())
                        {
                            objCmotsBSETick.change = app.Value.ToString();
                        }

                    }
                    objCmotsTickLst.Add(objCmotsBSETick);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    //sqlstr = " select * from CmotsShareHoldingPattern where co_code=@co_code ";
                    //cmd.Parameters.Clear();
                    //cmd.CommandText = sqlstr;
                    //cmd.Parameters.AddWithValue("@co_code", comp_code);
                    //SqlDataReader sdr = cmd.ExecuteReader();
                    //while (sdr.Read())
                    //{
                    //    result = true;
                    //}
                    //sdr.Close();

                    //if (result == false)
                    //{
                    //    foreach (var mod in objCmotsTickLst)
                    //    {
                    //        sqlstr = "Insert Into CmotsNSETicker(co_code, co_name, price, price_diff, tr_date, change, created_by, created_on)";
                    //        sqlstr = sqlstr + " values (@co_code, @co_name, @price, @price_diff, @tr_date, @change, @created_by, GetDate())";
                    //        cmd.Parameters.Clear();
                    //        cmd.CommandText = sqlstr;
                    //        cmd.Parameters.AddWithValue("@co_code", mod.co_code);
                    //        cmd.Parameters.AddWithValue("@co_name", mod.co_name);
                    //        cmd.Parameters.AddWithValue("@price", mod.price);
                    //        cmd.Parameters.AddWithValue("@price_diff", mod.price_diff);
                    //        cmd.Parameters.AddWithValue("@tr_date", mod.tr_date);
                    //        cmd.Parameters.AddWithValue("@change", mod.change);
                    //        cmd.Parameters.AddWithValue("@created_by", "Online");
                    //        cmd.ExecuteNonQuery();
                    //    }

                    //}
                    //else if (result == true)
                    //{
                    //    sqlstr = "delete from CmotsNSETicker where co_code=@co_code";
                    //    cmd.Parameters.Clear();
                    //    cmd.CommandText = sqlstr;
                    //    cmd.Parameters.AddWithValue("@co_code", comp_code);
                    //    cmd.ExecuteNonQuery();

                    //    foreach (var mod in objCmotsTickLst)
                    //    {
                    //        sqlstr = "Insert Into CmotsNSETicker(co_code, co_name, price, price_diff, tr_date, change, created_by, created_on)";
                    //        sqlstr = sqlstr + " values (@co_code, @co_name, @price, @price_diff, @tr_date, @change, @created_by, GetDate())";
                    //        cmd.Parameters.Clear();
                    //        cmd.CommandText = sqlstr;
                    //        cmd.Parameters.AddWithValue("@co_code", mod.co_code);
                    //        cmd.Parameters.AddWithValue("@co_name", mod.co_name);
                    //        cmd.Parameters.AddWithValue("@price", mod.price);
                    //        cmd.Parameters.AddWithValue("@price_diff", mod.price_diff);
                    //        cmd.Parameters.AddWithValue("@tr_date", mod.tr_date);
                    //        cmd.Parameters.AddWithValue("@change", mod.change);
                    //        cmd.Parameters.AddWithValue("@created_by", "Online");
                    //        cmd.ExecuteNonQuery();
                    //    }
                    //}
                    //result = false;

                    for (int i = 0; i < objCmotsTickLst.Count; i++)
                    {
                        if (objCmotsTickLst[i].co_code != "")
                        {
                            sqlstr = "delete from CmotsBSETicker";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.ExecuteNonQuery();
                        }
                        i = objCmotsTickLst.Count;
                    }

                    foreach (var mod in objCmotsTickLst)
                    {
                        sqlstr = "Insert Into CmotsBSETicker(co_code, co_name, price, price_diff, tr_date, change, created_by, created_on)";
                        sqlstr = sqlstr + " values (@co_code, @co_name, @price, @price_diff, @tr_date, @change, @created_by, GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@co_code", mod.co_code);
                        cmd.Parameters.AddWithValue("@co_name", mod.co_name);
                        cmd.Parameters.AddWithValue("@price", mod.price);
                        cmd.Parameters.AddWithValue("@price_diff", mod.price_diff);
                        cmd.Parameters.AddWithValue("@tr_date", mod.tr_date);
                        cmd.Parameters.AddWithValue("@change", mod.change);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion





        #region Cmots MF Top Perform One Year Equity

        [HttpGet]
        public ActionResult GetCmotsMFTopPerformOneYearEquity()
        {
            return View();
        }



        [HttpPost]
        public void GetCmotsMFTopPerformOneYearEquity(string group)
        {
            try
            {
                //bool result = false;
                List<CmotsMFTopPerform_OneYearEquity> objCmotsTPLst = new List<CmotsMFTopPerform_OneYearEquity>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsMFTopPerform_OneYearEquity objCmotsTP = new CmotsMFTopPerform_OneYearEquity();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "Mf_SchCode".ToUpper())
                        {
                            objCmotsTP.Mf_SchCode = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "SchClCode".ToUpper())
                        {
                            objCmotsTP.SchClCode = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Scheme".ToUpper())
                        {
                            objCmotsTP.Scheme = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "TypeName".ToUpper())
                        {
                            objCmotsTP.TypeName = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Sch_Name".ToUpper())
                        {
                            objCmotsTP.Sch_Name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "NavDate".ToUpper())
                        {
                            objCmotsTP.NavDate = app.Value.ToString();
                        }

                        else if (appName.ToUpper() == "NavRs".ToUpper())
                        {
                            objCmotsTP.NavRs = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "pReturn".ToUpper())
                        {
                            objCmotsTP.pReturn = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "oneyear".ToUpper())
                        {
                            objCmotsTP.oneyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "threeyear".ToUpper())
                        {
                            objCmotsTP.threeyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "fiveyear".ToUpper())
                        {
                            objCmotsTP.fiveyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "inception".ToUpper())
                        {
                            objCmotsTP.inception = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "prevnavrs".ToUpper())
                        {
                            objCmotsTP.prevnavrs = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "navchange".ToUpper())
                        {
                            objCmotsTP.navchange = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "navperchange".ToUpper())
                        {
                            objCmotsTP.navperchange = app.Value.ToString();
                        }
                    }
                    objCmotsTPLst.Add(objCmotsTP);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    for (int i = 0; i < objCmotsTPLst.Count; i++ )
                    {
                        if(objCmotsTPLst[i].Mf_SchCode != "")
                        {
                            sqlstr = "delete from CmotsMFTopPerform_OneYearEquity";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.ExecuteNonQuery();
                        }
                        i = objCmotsTPLst.Count;
                    }
                        

                    foreach (var mod in objCmotsTPLst)
                    {
                        sqlstr = "insert into CmotsMFTopPerform_OneYearEquity(Mf_SchCode, SchClCode, Scheme, TypeName, Sch_Name,NavDate, ";
                        sqlstr = sqlstr + " NavRs, pReturn, oneyear, threeyear, fiveyear, inception, prevnavrs, navchange, navperchange, created_by, created_on)";
                        sqlstr = sqlstr + " values (@Mf_SchCode, @SchClCode, @Scheme, @TypeName, @Sch_Name,@NavDate, @NavRs, @pReturn, @oneyear, ";
                        sqlstr = sqlstr + " @threeyear, @fiveyear, @inception, @prevnavrs, @navchange, @navperchange, @created_by,  GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@Mf_SchCode", mod.Mf_SchCode);
                        cmd.Parameters.AddWithValue("@SchClCode", mod.SchClCode);
                        cmd.Parameters.AddWithValue("@Scheme", mod.Scheme);
                        cmd.Parameters.AddWithValue("@TypeName", mod.TypeName);
                        cmd.Parameters.AddWithValue("@Sch_Name", mod.Sch_Name);
                        cmd.Parameters.AddWithValue("@NavDate", mod.NavDate);
                        cmd.Parameters.AddWithValue("@NavRs", mod.NavRs);
                        cmd.Parameters.AddWithValue("@pReturn", mod.pReturn);
                        cmd.Parameters.AddWithValue("@oneyear", mod.oneyear);
                        cmd.Parameters.AddWithValue("@threeyear", mod.threeyear);
                        cmd.Parameters.AddWithValue("@fiveyear", mod.fiveyear);
                        cmd.Parameters.AddWithValue("@inception", mod.inception);
                        cmd.Parameters.AddWithValue("@prevnavrs", mod.prevnavrs);
                        cmd.Parameters.AddWithValue("@navchange", mod.navchange);
                        cmd.Parameters.AddWithValue("@navperchange", mod.navperchange);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion

        #region Cmots MF Top Perform One Week Equity

        [HttpGet]
        public ActionResult GetCmotsMFTopPerformOneWeekEquity()
        {
            return View();
        }



        [HttpPost]
        public void GetCmotsMFTopPerformOneWeekEquity(string group)
        {
            try
            {
                //bool result = false;
                List<CmotsMFTopPerform_OneWeekEquity> objCmotsTPLst = new List<CmotsMFTopPerform_OneWeekEquity>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsMFTopPerform_OneWeekEquity objCmotsTP = new CmotsMFTopPerform_OneWeekEquity();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "Mf_SchCode".ToUpper())
                        {
                            objCmotsTP.Mf_SchCode = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "SchClCode".ToUpper())
                        {
                            objCmotsTP.SchClCode = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Scheme".ToUpper())
                        {
                            objCmotsTP.Scheme = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "TypeName".ToUpper())
                        {
                            objCmotsTP.TypeName = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Sch_Name".ToUpper())
                        {
                            objCmotsTP.Sch_Name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "NavDate".ToUpper())
                        {
                            objCmotsTP.NavDate = app.Value.ToString();
                        }

                        else if (appName.ToUpper() == "NavRs".ToUpper())
                        {
                            objCmotsTP.NavRs = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "pReturn".ToUpper())
                        {
                            objCmotsTP.pReturn = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "oneyear".ToUpper())
                        {
                            objCmotsTP.oneyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "threeyear".ToUpper())
                        {
                            objCmotsTP.threeyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "fiveyear".ToUpper())
                        {
                            objCmotsTP.fiveyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "inception".ToUpper())
                        {
                            objCmotsTP.inception = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "prevnavrs".ToUpper())
                        {
                            objCmotsTP.prevnavrs = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "navchange".ToUpper())
                        {
                            objCmotsTP.navchange = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "navperchange".ToUpper())
                        {
                            objCmotsTP.navperchange = app.Value.ToString();
                        }
                    }
                    objCmotsTPLst.Add(objCmotsTP);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    for (int i = 0; i < objCmotsTPLst.Count; i++)
                    {
                        if (objCmotsTPLst[i].Mf_SchCode != "")
                        {
                            sqlstr = "delete from CmotsMFTopPerform_OneWeekEquity";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.ExecuteNonQuery();
                        }
                        i = objCmotsTPLst.Count;
                    }


                    foreach (var mod in objCmotsTPLst)
                    {
                        sqlstr = "insert into CmotsMFTopPerform_OneWeekEquity(Mf_SchCode, SchClCode, Scheme, TypeName, Sch_Name,NavDate, ";
                        sqlstr = sqlstr + " NavRs, pReturn, oneyear, threeyear, fiveyear, inception, prevnavrs, navchange, navperchange, created_by, created_on)";
                        sqlstr = sqlstr + " values (@Mf_SchCode, @SchClCode, @Scheme, @TypeName, @Sch_Name,@NavDate, @NavRs, @pReturn, @oneyear, ";
                        sqlstr = sqlstr + " @threeyear, @fiveyear, @inception, @prevnavrs, @navchange, @navperchange, @created_by,  GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@Mf_SchCode", mod.Mf_SchCode);
                        cmd.Parameters.AddWithValue("@SchClCode", mod.SchClCode);
                        cmd.Parameters.AddWithValue("@Scheme", mod.Scheme);
                        cmd.Parameters.AddWithValue("@TypeName", mod.TypeName);
                        cmd.Parameters.AddWithValue("@Sch_Name", mod.Sch_Name);
                        cmd.Parameters.AddWithValue("@NavDate", mod.NavDate);
                        cmd.Parameters.AddWithValue("@NavRs", mod.NavRs);
                        cmd.Parameters.AddWithValue("@pReturn", mod.pReturn);
                        cmd.Parameters.AddWithValue("@oneyear", mod.oneyear);
                        cmd.Parameters.AddWithValue("@threeyear", mod.threeyear);
                        cmd.Parameters.AddWithValue("@fiveyear", mod.fiveyear);
                        cmd.Parameters.AddWithValue("@inception", mod.inception);
                        cmd.Parameters.AddWithValue("@prevnavrs", mod.prevnavrs);
                        cmd.Parameters.AddWithValue("@navchange", mod.navchange);
                        cmd.Parameters.AddWithValue("@navperchange", mod.navperchange);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion
        
        #region Cmots MF Top Perform One Year Debt

        [HttpGet]
        public ActionResult GetCmotsMFTopPerformOneYearDebt()
        {
            return View();
        }



        [HttpPost]
        public void GetCmotsMFTopPerformOneYearDebt(string group)
        {
            try
            {
                //bool result = false;
                List<CmotsMFTopPerform_OneYearDebt> objCmotsTPLst = new List<CmotsMFTopPerform_OneYearDebt>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsMFTopPerform_OneYearDebt objCmotsTP = new CmotsMFTopPerform_OneYearDebt();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "Mf_SchCode".ToUpper())
                        {
                            objCmotsTP.Mf_SchCode = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "SchClCode".ToUpper())
                        {
                            objCmotsTP.SchClCode = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Scheme".ToUpper())
                        {
                            objCmotsTP.Scheme = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "TypeName".ToUpper())
                        {
                            objCmotsTP.TypeName = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Sch_Name".ToUpper())
                        {
                            objCmotsTP.Sch_Name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "NavDate".ToUpper())
                        {
                            objCmotsTP.NavDate = app.Value.ToString();
                        }

                        else if (appName.ToUpper() == "NavRs".ToUpper())
                        {
                            objCmotsTP.NavRs = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "pReturn".ToUpper())
                        {
                            objCmotsTP.pReturn = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "oneyear".ToUpper())
                        {
                            objCmotsTP.oneyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "threeyear".ToUpper())
                        {
                            objCmotsTP.threeyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "fiveyear".ToUpper())
                        {
                            objCmotsTP.fiveyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "inception".ToUpper())
                        {
                            objCmotsTP.inception = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "prevnavrs".ToUpper())
                        {
                            objCmotsTP.prevnavrs = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "navchange".ToUpper())
                        {
                            objCmotsTP.navchange = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "navperchange".ToUpper())
                        {
                            objCmotsTP.navperchange = app.Value.ToString();
                        }
                    }
                    objCmotsTPLst.Add(objCmotsTP);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    for (int i = 0; i < objCmotsTPLst.Count; i++)
                    {
                        if (objCmotsTPLst[i].Mf_SchCode != "")
                        {
                            sqlstr = "delete from CmotsMFTopPerform_OneYearDebt";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.ExecuteNonQuery();
                        }
                        i = objCmotsTPLst.Count;
                    }


                    foreach (var mod in objCmotsTPLst)
                    {
                        sqlstr = "insert into CmotsMFTopPerform_OneYearDebt(Mf_SchCode, SchClCode, Scheme, TypeName, Sch_Name,NavDate, ";
                        sqlstr = sqlstr + " NavRs, pReturn, oneyear, threeyear, fiveyear, inception, prevnavrs, navchange, navperchange, created_by, created_on)";
                        sqlstr = sqlstr + " values (@Mf_SchCode, @SchClCode, @Scheme, @TypeName, @Sch_Name,@NavDate, @NavRs, @pReturn, @oneyear, ";
                        sqlstr = sqlstr + " @threeyear, @fiveyear, @inception, @prevnavrs, @navchange, @navperchange, @created_by,  GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@Mf_SchCode", mod.Mf_SchCode);
                        cmd.Parameters.AddWithValue("@SchClCode", mod.SchClCode);
                        cmd.Parameters.AddWithValue("@Scheme", mod.Scheme);
                        cmd.Parameters.AddWithValue("@TypeName", mod.TypeName);
                        cmd.Parameters.AddWithValue("@Sch_Name", mod.Sch_Name);
                        cmd.Parameters.AddWithValue("@NavDate", mod.NavDate);
                        cmd.Parameters.AddWithValue("@NavRs", mod.NavRs);
                        cmd.Parameters.AddWithValue("@pReturn", mod.pReturn);
                        cmd.Parameters.AddWithValue("@oneyear", mod.oneyear);
                        cmd.Parameters.AddWithValue("@threeyear", mod.threeyear);
                        cmd.Parameters.AddWithValue("@fiveyear", mod.fiveyear);
                        cmd.Parameters.AddWithValue("@inception", mod.inception);
                        cmd.Parameters.AddWithValue("@prevnavrs", mod.prevnavrs);
                        cmd.Parameters.AddWithValue("@navchange", mod.navchange);
                        cmd.Parameters.AddWithValue("@navperchange", mod.navperchange);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion

        #region Cmots MF Top Perform One Week Debt

        [HttpGet]
        public ActionResult GetCmotsMFTopPerformOneWeekDebt()
        {
            return View();
        }



        [HttpPost]
        public void GetCmotsMFTopPerformOneWeekDebt(string group)
        {
            try
            {
                //bool result = false;
                List<CmotsMFTopPerform_OneWeekDebt> objCmotsTPLst = new List<CmotsMFTopPerform_OneWeekDebt>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsMFTopPerform_OneWeekDebt objCmotsTP = new CmotsMFTopPerform_OneWeekDebt();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "Mf_SchCode".ToUpper())
                        {
                            objCmotsTP.Mf_SchCode = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "SchClCode".ToUpper())
                        {
                            objCmotsTP.SchClCode = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Scheme".ToUpper())
                        {
                            objCmotsTP.Scheme = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "TypeName".ToUpper())
                        {
                            objCmotsTP.TypeName = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Sch_Name".ToUpper())
                        {
                            objCmotsTP.Sch_Name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "NavDate".ToUpper())
                        {
                            objCmotsTP.NavDate = app.Value.ToString();
                        }

                        else if (appName.ToUpper() == "NavRs".ToUpper())
                        {
                            objCmotsTP.NavRs = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "pReturn".ToUpper())
                        {
                            objCmotsTP.pReturn = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "oneyear".ToUpper())
                        {
                            objCmotsTP.oneyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "threeyear".ToUpper())
                        {
                            objCmotsTP.threeyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "fiveyear".ToUpper())
                        {
                            objCmotsTP.fiveyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "inception".ToUpper())
                        {
                            objCmotsTP.inception = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "prevnavrs".ToUpper())
                        {
                            objCmotsTP.prevnavrs = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "navchange".ToUpper())
                        {
                            objCmotsTP.navchange = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "navperchange".ToUpper())
                        {
                            objCmotsTP.navperchange = app.Value.ToString();
                        }
                    }
                    objCmotsTPLst.Add(objCmotsTP);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    for (int i = 0; i < objCmotsTPLst.Count; i++)
                    {
                        if (objCmotsTPLst[i].Mf_SchCode != "")
                        {
                            sqlstr = "delete from CmotsMFTopPerform_OneWeekDebt";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.ExecuteNonQuery();
                        }
                        i = objCmotsTPLst.Count;
                    }


                    foreach (var mod in objCmotsTPLst)
                    {
                        sqlstr = "insert into CmotsMFTopPerform_OneWeekDebt(Mf_SchCode, SchClCode, Scheme, TypeName, Sch_Name,NavDate, ";
                        sqlstr = sqlstr + " NavRs, pReturn, oneyear, threeyear, fiveyear, inception, prevnavrs, navchange, navperchange, created_by, created_on)";
                        sqlstr = sqlstr + " values (@Mf_SchCode, @SchClCode, @Scheme, @TypeName, @Sch_Name,@NavDate, @NavRs, @pReturn, @oneyear, ";
                        sqlstr = sqlstr + " @threeyear, @fiveyear, @inception, @prevnavrs, @navchange, @navperchange, @created_by,  GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@Mf_SchCode", mod.Mf_SchCode);
                        cmd.Parameters.AddWithValue("@SchClCode", mod.SchClCode);
                        cmd.Parameters.AddWithValue("@Scheme", mod.Scheme);
                        cmd.Parameters.AddWithValue("@TypeName", mod.TypeName);
                        cmd.Parameters.AddWithValue("@Sch_Name", mod.Sch_Name);
                        cmd.Parameters.AddWithValue("@NavDate", mod.NavDate);
                        cmd.Parameters.AddWithValue("@NavRs", mod.NavRs);
                        cmd.Parameters.AddWithValue("@pReturn", mod.pReturn);
                        cmd.Parameters.AddWithValue("@oneyear", mod.oneyear);
                        cmd.Parameters.AddWithValue("@threeyear", mod.threeyear);
                        cmd.Parameters.AddWithValue("@fiveyear", mod.fiveyear);
                        cmd.Parameters.AddWithValue("@inception", mod.inception);
                        cmd.Parameters.AddWithValue("@prevnavrs", mod.prevnavrs);
                        cmd.Parameters.AddWithValue("@navchange", mod.navchange);
                        cmd.Parameters.AddWithValue("@navperchange", mod.navperchange);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion
        
        #region Cmots MF Top Perform One Year Hybrid

        [HttpGet]
        public ActionResult GetCmotsMFTopPerformOneYearHybrid()
        {
            return View();
        }



        [HttpPost]
        public void GetCmotsMFTopPerformOneYearHybrid(string group)
        {
            try
            {
                //bool result = false;
                List<CmotsMFTopPerform_OneYearHybrid> objCmotsTPLst = new List<CmotsMFTopPerform_OneYearHybrid>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsMFTopPerform_OneYearHybrid objCmotsTP = new CmotsMFTopPerform_OneYearHybrid();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "Mf_SchCode".ToUpper())
                        {
                            objCmotsTP.Mf_SchCode = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "SchClCode".ToUpper())
                        {
                            objCmotsTP.SchClCode = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Scheme".ToUpper())
                        {
                            objCmotsTP.Scheme = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "TypeName".ToUpper())
                        {
                            objCmotsTP.TypeName = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Sch_Name".ToUpper())
                        {
                            objCmotsTP.Sch_Name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "NavDate".ToUpper())
                        {
                            objCmotsTP.NavDate = app.Value.ToString();
                        }

                        else if (appName.ToUpper() == "NavRs".ToUpper())
                        {
                            objCmotsTP.NavRs = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "pReturn".ToUpper())
                        {
                            objCmotsTP.pReturn = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "oneyear".ToUpper())
                        {
                            objCmotsTP.oneyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "threeyear".ToUpper())
                        {
                            objCmotsTP.threeyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "fiveyear".ToUpper())
                        {
                            objCmotsTP.fiveyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "inception".ToUpper())
                        {
                            objCmotsTP.inception = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "prevnavrs".ToUpper())
                        {
                            objCmotsTP.prevnavrs = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "navchange".ToUpper())
                        {
                            objCmotsTP.navchange = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "navperchange".ToUpper())
                        {
                            objCmotsTP.navperchange = app.Value.ToString();
                        }
                    }
                    objCmotsTPLst.Add(objCmotsTP);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    for (int i = 0; i < objCmotsTPLst.Count; i++)
                    {
                        if (objCmotsTPLst[i].Mf_SchCode != "")
                        {
                            sqlstr = "delete from CmotsMFTopPerform_OneYearHybrid";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.ExecuteNonQuery();
                        }
                        i = objCmotsTPLst.Count;
                    }


                    foreach (var mod in objCmotsTPLst)
                    {
                        sqlstr = "insert into CmotsMFTopPerform_OneYearHybrid(Mf_SchCode, SchClCode, Scheme, TypeName, Sch_Name,NavDate, ";
                        sqlstr = sqlstr + " NavRs, pReturn, oneyear, threeyear, fiveyear, inception, prevnavrs, navchange, navperchange, created_by, created_on)";
                        sqlstr = sqlstr + " values (@Mf_SchCode, @SchClCode, @Scheme, @TypeName, @Sch_Name,@NavDate, @NavRs, @pReturn, @oneyear, ";
                        sqlstr = sqlstr + " @threeyear, @fiveyear, @inception, @prevnavrs, @navchange, @navperchange, @created_by,  GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@Mf_SchCode", mod.Mf_SchCode);
                        cmd.Parameters.AddWithValue("@SchClCode", mod.SchClCode);
                        cmd.Parameters.AddWithValue("@Scheme", mod.Scheme);
                        cmd.Parameters.AddWithValue("@TypeName", mod.TypeName);
                        cmd.Parameters.AddWithValue("@Sch_Name", mod.Sch_Name);
                        cmd.Parameters.AddWithValue("@NavDate", mod.NavDate);
                        cmd.Parameters.AddWithValue("@NavRs", mod.NavRs);
                        cmd.Parameters.AddWithValue("@pReturn", mod.pReturn);
                        cmd.Parameters.AddWithValue("@oneyear", mod.oneyear);
                        cmd.Parameters.AddWithValue("@threeyear", mod.threeyear);
                        cmd.Parameters.AddWithValue("@fiveyear", mod.fiveyear);
                        cmd.Parameters.AddWithValue("@inception", mod.inception);
                        cmd.Parameters.AddWithValue("@prevnavrs", mod.prevnavrs);
                        cmd.Parameters.AddWithValue("@navchange", mod.navchange);
                        cmd.Parameters.AddWithValue("@navperchange", mod.navperchange);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion

        #region Cmots MF Top Perform One Week Hybrid

        [HttpGet]
        public ActionResult GetCmotsMFTopPerformOneWeekHybrid()
        {
            return View();
        }



        [HttpPost]
        public void GetCmotsMFTopPerformOneWeekHybrid(string group)
        {
            try
            {
                //bool result = false;
                List<CmotsMFTopPerform_OneWeekHybrid> objCmotsTPLst = new List<CmotsMFTopPerform_OneWeekHybrid>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsMFTopPerform_OneWeekHybrid objCmotsTP = new CmotsMFTopPerform_OneWeekHybrid();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "Mf_SchCode".ToUpper())
                        {
                            objCmotsTP.Mf_SchCode = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "SchClCode".ToUpper())
                        {
                            objCmotsTP.SchClCode = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Scheme".ToUpper())
                        {
                            objCmotsTP.Scheme = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "TypeName".ToUpper())
                        {
                            objCmotsTP.TypeName = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Sch_Name".ToUpper())
                        {
                            objCmotsTP.Sch_Name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "NavDate".ToUpper())
                        {
                            objCmotsTP.NavDate = app.Value.ToString();
                        }

                        else if (appName.ToUpper() == "NavRs".ToUpper())
                        {
                            objCmotsTP.NavRs = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "pReturn".ToUpper())
                        {
                            objCmotsTP.pReturn = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "oneyear".ToUpper())
                        {
                            objCmotsTP.oneyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "threeyear".ToUpper())
                        {
                            objCmotsTP.threeyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "fiveyear".ToUpper())
                        {
                            objCmotsTP.fiveyear = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "inception".ToUpper())
                        {
                            objCmotsTP.inception = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "prevnavrs".ToUpper())
                        {
                            objCmotsTP.prevnavrs = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "navchange".ToUpper())
                        {
                            objCmotsTP.navchange = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "navperchange".ToUpper())
                        {
                            objCmotsTP.navperchange = app.Value.ToString();
                        }
                    }
                    objCmotsTPLst.Add(objCmotsTP);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    for (int i = 0; i < objCmotsTPLst.Count; i++)
                    {
                        if (objCmotsTPLst[i].Mf_SchCode != "")
                        {
                            sqlstr = "delete from CmotsMFTopPerform_OneWeekHybrid";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.ExecuteNonQuery();
                        }
                        i = objCmotsTPLst.Count;
                    }


                    foreach (var mod in objCmotsTPLst)
                    {
                        sqlstr = "insert into CmotsMFTopPerform_OneWeekHybrid(Mf_SchCode, SchClCode, Scheme, TypeName, Sch_Name,NavDate, ";
                        sqlstr = sqlstr + " NavRs, pReturn, oneyear, threeyear, fiveyear, inception, prevnavrs, navchange, navperchange, created_by, created_on)";
                        sqlstr = sqlstr + " values (@Mf_SchCode, @SchClCode, @Scheme, @TypeName, @Sch_Name,@NavDate, @NavRs, @pReturn, @oneyear, ";
                        sqlstr = sqlstr + " @threeyear, @fiveyear, @inception, @prevnavrs, @navchange, @navperchange, @created_by,  GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@Mf_SchCode", mod.Mf_SchCode);
                        cmd.Parameters.AddWithValue("@SchClCode", mod.SchClCode);
                        cmd.Parameters.AddWithValue("@Scheme", mod.Scheme);
                        cmd.Parameters.AddWithValue("@TypeName", mod.TypeName);
                        cmd.Parameters.AddWithValue("@Sch_Name", mod.Sch_Name);
                        cmd.Parameters.AddWithValue("@NavDate", mod.NavDate);
                        cmd.Parameters.AddWithValue("@NavRs", mod.NavRs);
                        cmd.Parameters.AddWithValue("@pReturn", mod.pReturn);
                        cmd.Parameters.AddWithValue("@oneyear", mod.oneyear);
                        cmd.Parameters.AddWithValue("@threeyear", mod.threeyear);
                        cmd.Parameters.AddWithValue("@fiveyear", mod.fiveyear);
                        cmd.Parameters.AddWithValue("@inception", mod.inception);
                        cmd.Parameters.AddWithValue("@prevnavrs", mod.prevnavrs);
                        cmd.Parameters.AddWithValue("@navchange", mod.navchange);
                        cmd.Parameters.AddWithValue("@navperchange", mod.navperchange);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion
        
        #region Cmots MF New Fund Offer

        [HttpGet]
        public ActionResult GetCmotsMFNewFundOffer()
        {
            return View();
        }



        [HttpPost]
        public void GetCmotsMFNewFundOffer(string group)
        {
            try
            {
                //bool result = false;
                List<CmotsMFNewFundOffer> objCmotsNFOLst = new List<CmotsMFNewFundOffer>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsMFNewFundOffer objCmotsNFO = new CmotsMFNewFundOffer();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "Mf_SchCode".ToUpper())
                        {
                            objCmotsNFO.Mf_SchCode = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "LName".ToUpper())
                        {
                            objCmotsNFO.LName = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Sch_Name".ToUpper())
                        {
                            objCmotsNFO.Sch_Name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Launc_Date".ToUpper())
                        {
                            objCmotsNFO.Launc_Date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "CLDATE".ToUpper())
                        {
                            objCmotsNFO.CLDATE = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "MININVT".ToUpper())
                        {
                            objCmotsNFO.MININVT = app.Value.ToString();
                        }

                        else if (appName.ToUpper() == "OFFERPRICE".ToUpper())
                        {
                            objCmotsNFO.OFFERPRICE = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "SCHEME".ToUpper())
                        {
                            objCmotsNFO.SCHEME = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Objective".ToUpper())
                        {
                            objCmotsNFO.Objective = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "SIZE".ToUpper())
                        {
                            objCmotsNFO.SIZE = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "VClass".ToUpper())
                        {
                            objCmotsNFO.VClass = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "TypeName".ToUpper())
                        {
                            objCmotsNFO.TypeName = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "VClassCode".ToUpper())
                        {
                            objCmotsNFO.VClassCode = app.Value.ToString();
                        }
                        
                    }
                    objCmotsNFOLst.Add(objCmotsNFO);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    for (int i = 0; i < objCmotsNFOLst.Count; i++)
                    {
                        if (objCmotsNFOLst[i].Mf_SchCode != "")
                        {
                            sqlstr = "delete from CmotsMFNewFundOffer";
                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.ExecuteNonQuery();
                        }
                        i = objCmotsNFOLst.Count;
                    }


                    foreach (var mod in objCmotsNFOLst)
                    {
                        sqlstr = "insert into CmotsMFNewFundOffer(Mf_SchCode, LName, Sch_Name, Launc_Date, CLDATE, MININVT, OFFERPRICE,";
                        sqlstr = sqlstr + " SCHEME, Objective, SIZE, VClass, TypeName, VClassCode, created_by, created_on)";
                        sqlstr = sqlstr + " values (@Mf_SchCode, @LName, @Sch_Name, @Launc_Date, @CLDATE, @MININVT, @OFFERPRICE,";
                        sqlstr = sqlstr + " @SCHEME, @Objective, @SIZE, @VClass, @TypeName, @VClassCode, @created_by,  GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@Mf_SchCode", mod.Mf_SchCode);
                        cmd.Parameters.AddWithValue("@LName", mod.LName);
                        cmd.Parameters.AddWithValue("@Sch_Name", mod.Sch_Name);
                        cmd.Parameters.AddWithValue("@Launc_Date", mod.Launc_Date);
                        cmd.Parameters.AddWithValue("@CLDATE", mod.CLDATE);
                        cmd.Parameters.AddWithValue("@MININVT", mod.MININVT);
                        cmd.Parameters.AddWithValue("@OFFERPRICE", mod.OFFERPRICE);
                        cmd.Parameters.AddWithValue("@SCHEME", mod.SCHEME);
                        cmd.Parameters.AddWithValue("@Objective", mod.Objective);
                        cmd.Parameters.AddWithValue("@SIZE", mod.SIZE);
                        cmd.Parameters.AddWithValue("@VClass", mod.VClass);
                        cmd.Parameters.AddWithValue("@TypeName", mod.TypeName);
                        cmd.Parameters.AddWithValue("@VClassCode", mod.VClassCode);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            //return View();


        }


        #endregion

        #region Cmots Get Cmots MF News

        [HttpGet]
        public ActionResult GetCmotsMFNews()
        {
            return View();
        }


        [HttpPost]
        [ValidateInput(false)]
        public void GetCmotsMFNews(string group)
        {

            //string m = RemoveSpecialCharacters(group);
            //m = m.Replace(System.Environment.NewLine, " ");
            ////m = m.Replace('"', ''');
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsMFNews> objCCNLst = new List<CmotsMFNews>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsMFNews objCCN = new CmotsMFNews();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "Sno".ToUpper())
                        {
                            objCCN.Sno = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Section_name".ToUpper())
                        {
                            objCCN.Section_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Date".ToUpper())
                        {
                            objCCN.Date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Time".ToUpper())
                        {
                            objCCN.Time = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Heading".ToUpper())
                        {
                            objCCN.Heading = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Caption".ToUpper())
                        {
                            objCCN.Caption = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Arttext".ToUpper())
                        {
                            objCCN.Arttext = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Co_code".ToUpper())
                        {
                            objCCN.Co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Ind_code".ToUpper())
                        {
                            objCCN.Ind_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Flag".ToUpper())
                        {
                            objCCN.Flag = app.Value.ToString();
                        }
                    }
                    objCCNLst.Add(objCCN);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";
                    
                    sqlstr = "delete from CmotsMFNews ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.ExecuteNonQuery();

                    foreach (var mod in objCCNLst)
                    {
                        sqlstr = "Insert into CmotsMFNews(Sno, Section_name, Date, Time, Heading,";
                        sqlstr = sqlstr + " Caption, Arttext, Co_code, Ind_code, Flag, created_by, created_on)";
                        sqlstr = sqlstr + " values (@Sno, @Section_name, @Date, @Time, @Heading,";
                        sqlstr = sqlstr + " @Caption, @Arttext, @Co_code, @Ind_code, @Flag, @created_by, GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@Sno", mod.Sno);
                        cmd.Parameters.AddWithValue("@Section_name", mod.Section_name);
                        cmd.Parameters.AddWithValue("@Date", mod.Date);
                        cmd.Parameters.AddWithValue("@Time", mod.Time);
                        cmd.Parameters.AddWithValue("@Heading", mod.Heading);
                        cmd.Parameters.AddWithValue("@Caption", mod.Caption);
                        cmd.Parameters.AddWithValue("@Arttext", mod.Arttext);
                        cmd.Parameters.AddWithValue("@Co_code", mod.Co_code);
                        cmd.Parameters.AddWithValue("@Ind_code", mod.Ind_code);
                        cmd.Parameters.AddWithValue("@Flag", mod.Flag);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

        }



        #endregion

        #region Cmots Get Cmots MF Fund Manager Interview

        [HttpGet]
        public ActionResult GetCmotsMFFundManagerInterview()
        {
            return View();
        }


        [HttpPost]
        [ValidateInput(false)]
        public void GetCmotsMFFundManagerInterview(string group)
        {

            //string m = RemoveSpecialCharacters(group);
            //m = m.Replace(System.Environment.NewLine, " ");
            ////m = m.Replace('"', ''');
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsMFFundManagerInterview> objLst = new List<CmotsMFFundManagerInterview>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsMFFundManagerInterview obj = new CmotsMFFundManagerInterview();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "Sno".ToUpper())
                        {
                            obj.Sno = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Section_name".ToUpper())
                        {
                            obj.Section_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Date".ToUpper())
                        {
                            obj.Date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Time".ToUpper())
                        {
                            obj.Time = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Heading".ToUpper())
                        {
                            obj.Heading = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Caption".ToUpper())
                        {
                            obj.Caption = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Arttext".ToUpper())
                        {
                            obj.Arttext = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Co_code".ToUpper())
                        {
                            obj.Co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Ind_code".ToUpper())
                        {
                            obj.Ind_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Flag".ToUpper())
                        {
                            obj.Flag = app.Value.ToString();
                        }
                    }
                    objLst.Add(obj);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";

                    sqlstr = "delete from CmotsMFFundManagerInterview ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.ExecuteNonQuery();

                    foreach (var mod in objLst)
                    {
                        sqlstr = "Insert into CmotsMFFundManagerInterview(Sno, Section_name, Date, Time, Heading,";
                        sqlstr = sqlstr + " Caption, Arttext, Co_code, Ind_code, Flag, created_by, created_on)";
                        sqlstr = sqlstr + " values (@Sno, @Section_name, @Date, @Time, @Heading,";
                        sqlstr = sqlstr + " @Caption, @Arttext, @Co_code, @Ind_code, @Flag, @created_by, GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@Sno", mod.Sno);
                        cmd.Parameters.AddWithValue("@Section_name", mod.Section_name);
                        cmd.Parameters.AddWithValue("@Date", mod.Date);
                        cmd.Parameters.AddWithValue("@Time", mod.Time);
                        cmd.Parameters.AddWithValue("@Heading", mod.Heading);
                        cmd.Parameters.AddWithValue("@Caption", mod.Caption);
                        cmd.Parameters.AddWithValue("@Arttext", mod.Arttext);
                        cmd.Parameters.AddWithValue("@Co_code", mod.Co_code);
                        cmd.Parameters.AddWithValue("@Ind_code", mod.Ind_code);
                        cmd.Parameters.AddWithValue("@Flag", mod.Flag);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

        }



        #endregion





        #region Cmots Get Cmots Hot Pursuit Stock Action

        [HttpGet]
        public ActionResult GetCmotsHotPursuit_StockAction()
        {
            
            return View();

        }




        [HttpPost]
        [ValidateInput(false)]
        public void GetCmotsHotPursuit_StockAction(string group)
        {

            //string m = RemoveSpecialCharacters(group);
            //m = m.Replace(System.Environment.NewLine, " ");
            ////m = m.Replace('"', ''');
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsHotPursuit_stockAction> objCCNLst = new List<CmotsHotPursuit_stockAction>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsHotPursuit_stockAction objCCN = new CmotsHotPursuit_stockAction();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "sno".ToUpper())
                        {
                            objCCN.sno = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "section_name".ToUpper())
                        {
                            objCCN.section_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "date".ToUpper())
                        {
                            objCCN.date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "time".ToUpper())
                        {
                            objCCN.time = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "heading".ToUpper())
                        {
                            objCCN.heading  = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "caption".ToUpper())
                        {
                            objCCN.caption = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "arttext".ToUpper())
                        {
                            objCCN.arttext = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_code".ToUpper())
                        {
                            objCCN.co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "ind_code".ToUpper())
                        {
                            objCCN.ind_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "flag".ToUpper())
                        {
                            objCCN.flag = app.Value.ToString();
                        }
                    }
                    objCCNLst.Add(objCCN);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";



                    sqlstr = "delete from CmotsHotPursuit_stockAction ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.ExecuteNonQuery();

                    foreach (var mod in objCCNLst)
                    {
                        sqlstr = "insert into CmotsHotPursuit_stockAction(sno,section_name,date,time,heading,caption,arttext,co_code,ind_code,flag,created_by,created_on)";
                        sqlstr = sqlstr + " values (@sno,@section_name,@date,@time,@heading,@caption,@arttext,@co_code,@ind_code,@flag, @created_by, GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@sno", mod.sno);
                        cmd.Parameters.AddWithValue("@section_name", mod.section_name);
                        cmd.Parameters.AddWithValue("@date", mod.date);
                        cmd.Parameters.AddWithValue("@time", mod.time);
                        cmd.Parameters.AddWithValue("@heading", mod.heading);
                        cmd.Parameters.AddWithValue("@caption", mod.caption);
                        cmd.Parameters.AddWithValue("@arttext", mod.arttext);
                        cmd.Parameters.AddWithValue("@co_code", mod.co_code);
                        cmd.Parameters.AddWithValue("@ind_code", mod.ind_code);
                        cmd.Parameters.AddWithValue("@flag", mod.flag);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();

                    }


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

        }



        #endregion


        #region Cmots Get Cmots Corporate News

        [HttpGet]
        public ActionResult GetCmotsCorporateNews()
        {

            return View();

        }




        [HttpPost]
        [ValidateInput(false)]
        public void GetCmotsCorporateNews(string group)
        {

            //string m = RemoveSpecialCharacters(group);
            //m = m.Replace(System.Environment.NewLine, " ");
            ////m = m.Replace('"', ''');
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsCorporateNews> objCCNLst = new List<CmotsCorporateNews>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsCorporateNews objCCN = new CmotsCorporateNews();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "sno".ToUpper())
                        {
                            objCCN.sno = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "section_name".ToUpper())
                        {
                            objCCN.section_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "date".ToUpper())
                        {
                            objCCN.date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "time".ToUpper())
                        {
                            objCCN.time = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "heading".ToUpper())
                        {
                            objCCN.heading = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "caption".ToUpper())
                        {
                            objCCN.caption = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "arttext".ToUpper())
                        {
                            objCCN.arttext = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_code".ToUpper())
                        {
                            objCCN.co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "ind_code".ToUpper())
                        {
                            objCCN.ind_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "flag".ToUpper())
                        {
                            objCCN.flag = app.Value.ToString();
                        }
                    }
                    objCCNLst.Add(objCCN);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";



                    sqlstr = "delete from CmotsCorporateNews ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.ExecuteNonQuery();

                    foreach (var mod in objCCNLst)
                    {
                        sqlstr = "insert into CmotsCorporateNews(sno,section_name,date,time,heading,caption,arttext,co_code,ind_code,flag,created_by,created_on)";
                        sqlstr = sqlstr + " values (@sno,@section_name,@date,@time,@heading,@caption,@arttext,@co_code,@ind_code,@flag, @created_by, GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@sno", mod.sno);
                        cmd.Parameters.AddWithValue("@section_name", mod.section_name);
                        cmd.Parameters.AddWithValue("@date", mod.date);
                        cmd.Parameters.AddWithValue("@time", mod.time);
                        cmd.Parameters.AddWithValue("@heading", mod.heading);
                        cmd.Parameters.AddWithValue("@caption", mod.caption);
                        cmd.Parameters.AddWithValue("@arttext", mod.arttext);
                        cmd.Parameters.AddWithValue("@co_code", mod.co_code);
                        cmd.Parameters.AddWithValue("@ind_code", mod.ind_code);
                        cmd.Parameters.AddWithValue("@flag", mod.flag);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();

                    }


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

        }



        #endregion


        #region Cmots Get Cmots Economy News

        [HttpGet]
        public ActionResult GetCmotsEconomyNews()
        {

            return View();

        }




        [HttpPost]
        [ValidateInput(false)]
        public void GetCmotsEconomyNews(string group)
        {

            //string m = RemoveSpecialCharacters(group);
            //m = m.Replace(System.Environment.NewLine, " ");
            ////m = m.Replace('"', ''');
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsEconomyNews> objCCNLst = new List<CmotsEconomyNews>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsEconomyNews objCCN = new CmotsEconomyNews();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "sno".ToUpper())
                        {
                            objCCN.sno = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "section_name".ToUpper())
                        {
                            objCCN.section_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "date".ToUpper())
                        {
                            objCCN.date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "time".ToUpper())
                        {
                            objCCN.time = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "heading".ToUpper())
                        {
                            objCCN.heading = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "caption".ToUpper())
                        {
                            objCCN.caption = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "arttext".ToUpper())
                        {
                            objCCN.arttext = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_code".ToUpper())
                        {
                            objCCN.co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "ind_code".ToUpper())
                        {
                            objCCN.ind_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "flag".ToUpper())
                        {
                            objCCN.flag = app.Value.ToString();
                        }
                    }
                    objCCNLst.Add(objCCN);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";



                    sqlstr = "delete from CmotsEconomyNews ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.ExecuteNonQuery();

                    foreach (var mod in objCCNLst)
                    {
                        sqlstr = "insert into CmotsEconomyNews(sno,section_name,date,time,heading,caption,arttext,co_code,ind_code,flag,created_by,created_on)";
                        sqlstr = sqlstr + " values (@sno,@section_name,@date,@time,@heading,@caption,@arttext,@co_code,@ind_code,@flag, @created_by, GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@sno", mod.sno);
                        cmd.Parameters.AddWithValue("@section_name", mod.section_name);
                        cmd.Parameters.AddWithValue("@date", mod.date);
                        cmd.Parameters.AddWithValue("@time", mod.time);
                        cmd.Parameters.AddWithValue("@heading", mod.heading);
                        cmd.Parameters.AddWithValue("@caption", mod.caption);
                        cmd.Parameters.AddWithValue("@arttext", mod.arttext);
                        cmd.Parameters.AddWithValue("@co_code", mod.co_code);
                        cmd.Parameters.AddWithValue("@ind_code", mod.ind_code);
                        cmd.Parameters.AddWithValue("@flag", mod.flag);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();

                    }


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

        }



        #endregion


        #region Cmots Get Cmots Stock Alert

        [HttpGet]
        public ActionResult GetCmotsStockAlert()
        {

            return View();

        }
        

        [HttpPost]
        [ValidateInput(false)]
        public void GetCmotsStockAlert(string group)
        {

            //string m = RemoveSpecialCharacters(group);
            //m = m.Replace(System.Environment.NewLine, " ");
            ////m = m.Replace('"', ''');
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsStockAlert> objCCNLst = new List<CmotsStockAlert>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsStockAlert objCCN = new CmotsStockAlert();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "sno".ToUpper())
                        {
                            objCCN.sno = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "section_name".ToUpper())
                        {
                            objCCN.section_name = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "date".ToUpper())
                        {
                            objCCN.date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "time".ToUpper())
                        {
                            objCCN.time = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "heading".ToUpper())
                        {
                            objCCN.heading = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "caption".ToUpper())
                        {
                            objCCN.caption = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "arttext".ToUpper())
                        {
                            objCCN.arttext = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "co_code".ToUpper())
                        {
                            objCCN.co_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "ind_code".ToUpper())
                        {
                            objCCN.ind_code = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "flag".ToUpper())
                        {
                            objCCN.flag = app.Value.ToString();
                        }
                    }
                    objCCNLst.Add(objCCN);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";



                    sqlstr = "delete from CmotsStockAlert ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.ExecuteNonQuery();

                    foreach (var mod in objCCNLst)
                    {
                        sqlstr = "insert into CmotsStockAlert(sno,section_name,date,time,heading,caption,arttext,co_code,ind_code,flag,created_by,created_on)";
                        sqlstr = sqlstr + " values (@sno,@section_name,@date,@time,@heading,@caption,@arttext,@co_code,@ind_code,@flag, @created_by, GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@sno", mod.sno);
                        cmd.Parameters.AddWithValue("@section_name", mod.section_name);
                        cmd.Parameters.AddWithValue("@date", mod.date);
                        cmd.Parameters.AddWithValue("@time", mod.time);
                        cmd.Parameters.AddWithValue("@heading", mod.heading);
                        cmd.Parameters.AddWithValue("@caption", mod.caption);
                        cmd.Parameters.AddWithValue("@arttext", mod.arttext);
                        cmd.Parameters.AddWithValue("@co_code", mod.co_code);
                        cmd.Parameters.AddWithValue("@ind_code", mod.ind_code);
                        cmd.Parameters.AddWithValue("@flag", mod.flag);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();

                    }


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

        }



        #endregion







        #region Cmots Get Cmots Derivative Futures TopGainers_Nifty

        [HttpGet]
        public ActionResult GetDerivativeFuturesTopGainers_Nifty()
        {

            return View();

        }


        [HttpPost]
        public void GetDerivativeFuturesTopGainers_Nifty(string group)
        {

            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsDerivativeFuturesTopGainers_Nifty> objLst = new List<CmotsDerivativeFuturesTopGainers_Nifty>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsDerivativeFuturesTopGainers_Nifty obj = new CmotsDerivativeFuturesTopGainers_Nifty();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "Prevltp".ToUpper())
                        {
                            obj.Prevltp = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Ltp".ToUpper())
                        {
                            obj.Ltp = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Faodiff".ToUpper())
                        {
                            obj.Faodiff = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "faochange".ToUpper())
                        {
                            obj.faochange = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "instname".ToUpper())
                        {
                            obj.instname = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Symbol".ToUpper())
                        {
                            obj.Symbol = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "expdate".ToUpper())
                        {
                            obj.expdate = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "strikeprice".ToUpper())
                        {
                            obj.strikeprice = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "opttype".ToUpper())
                        {
                            obj.opttype = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "updtime".ToUpper())
                        {
                            obj.updtime = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Qty".ToUpper())
                        {
                            obj.Qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "openinterest".ToUpper())
                        {
                            obj.openinterest = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "chgopenint".ToUpper())
                        {
                            obj.chgopenint = app.Value.ToString();
                        }
                    }
                    objLst.Add(obj);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";



                    sqlstr = "delete from CmotsDerivativeFuturesTopGainers_Nifty ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.ExecuteNonQuery();

                    foreach (var mod in objLst)
                    {
                        sqlstr = "insert into CmotsDerivativeFuturesTopGainers_Nifty(Prevltp, Ltp, Faodiff, faochange, instname,";
                        sqlstr = sqlstr + " Symbol, expdate, strikeprice, opttype, updtime, Qty, openinterest, chgopenint, created_by, created_on)";
                        sqlstr = sqlstr + " values (@Prevltp, @Ltp, @Faodiff, @faochange, @instname,";
                        sqlstr = sqlstr + " @Symbol, @expdate, @strikeprice, @opttype, @updtime, @Qty, @openinterest, @chgopenint, @created_by, GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@Prevltp", mod.Prevltp);
                        cmd.Parameters.AddWithValue("@Ltp", mod.Ltp);
                        cmd.Parameters.AddWithValue("@Faodiff", mod.Faodiff);
                        cmd.Parameters.AddWithValue("@faochange", mod.faochange);
                        cmd.Parameters.AddWithValue("@instname", mod.instname);
                        cmd.Parameters.AddWithValue("@Symbol", mod.Symbol);
                        cmd.Parameters.AddWithValue("@expdate", mod.expdate);
                        cmd.Parameters.AddWithValue("@strikeprice", mod.strikeprice);
                        cmd.Parameters.AddWithValue("@opttype", mod.opttype);
                        cmd.Parameters.AddWithValue("@updtime", mod.updtime);
                        cmd.Parameters.AddWithValue("@Qty", mod.Qty);
                        cmd.Parameters.AddWithValue("@openinterest", mod.openinterest);
                        cmd.Parameters.AddWithValue("@chgopenint", mod.chgopenint);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();

                    }


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

        }



        #endregion

        #region Cmots Get Cmots Derivative Futures TopLosers_Nifty

        [HttpGet]
        public ActionResult GetDerivativeFuturesTopLosers_Nifty()
        {

            return View();

        }


        [HttpPost]
        public void GetDerivativeFuturesTopLosers_Nifty(string group)
        {

            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsDerivativeFuturesTopLosers_Nifty> objLst = new List<CmotsDerivativeFuturesTopLosers_Nifty>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsDerivativeFuturesTopLosers_Nifty obj = new CmotsDerivativeFuturesTopLosers_Nifty();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "Prevltp".ToUpper())
                        {
                            obj.Prevltp = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Ltp".ToUpper())
                        {
                            obj.Ltp = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Faodiff".ToUpper())
                        {
                            obj.Faodiff = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "faochange".ToUpper())
                        {
                            obj.faochange = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "instname".ToUpper())
                        {
                            obj.instname = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Symbol".ToUpper())
                        {
                            obj.Symbol = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "expdate".ToUpper())
                        {
                            obj.expdate = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "strikeprice".ToUpper())
                        {
                            obj.strikeprice = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "opttype".ToUpper())
                        {
                            obj.opttype = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "updtime".ToUpper())
                        {
                            obj.updtime = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Qty".ToUpper())
                        {
                            obj.Qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "openinterest".ToUpper())
                        {
                            obj.openinterest = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "chgopenint".ToUpper())
                        {
                            obj.chgopenint = app.Value.ToString();
                        }
                    }
                    objLst.Add(obj);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";



                    sqlstr = "delete from CmotsDerivativeFuturesTopLosers_Nifty ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.ExecuteNonQuery();

                    foreach (var mod in objLst)
                    {
                        sqlstr = "insert into CmotsDerivativeFuturesTopLosers_Nifty(Prevltp, Ltp, Faodiff, faochange, instname,";
                        sqlstr = sqlstr + " Symbol, expdate, strikeprice, opttype, updtime, Qty, openinterest, chgopenint, created_by, created_on)";
                        sqlstr = sqlstr + " values (@Prevltp, @Ltp, @Faodiff, @faochange, @instname,";
                        sqlstr = sqlstr + " @Symbol, @expdate, @strikeprice, @opttype, @updtime, @Qty, @openinterest, @chgopenint, @created_by, GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@Prevltp", mod.Prevltp);
                        cmd.Parameters.AddWithValue("@Ltp", mod.Ltp);
                        cmd.Parameters.AddWithValue("@Faodiff", mod.Faodiff);
                        cmd.Parameters.AddWithValue("@faochange", mod.faochange);
                        cmd.Parameters.AddWithValue("@instname", mod.instname);
                        cmd.Parameters.AddWithValue("@Symbol", mod.Symbol);
                        cmd.Parameters.AddWithValue("@expdate", mod.expdate);
                        cmd.Parameters.AddWithValue("@strikeprice", mod.strikeprice);
                        cmd.Parameters.AddWithValue("@opttype", mod.opttype);
                        cmd.Parameters.AddWithValue("@updtime", mod.updtime);
                        cmd.Parameters.AddWithValue("@Qty", mod.Qty);
                        cmd.Parameters.AddWithValue("@openinterest", mod.openinterest);
                        cmd.Parameters.AddWithValue("@chgopenint", mod.chgopenint);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();

                    }


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

        }



        #endregion

        #region Cmots Get Cmots Derivative Options TopGainers_Nifty

        [HttpGet]
        public ActionResult GetDerivativeOptionsTopGainers_Nifty()
        {

            return View();

        }


        [HttpPost]
        public void GetDerivativeOptionsTopGainers_Nifty(string group)
        {

            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsDerivativeOptionsTopGainers_Nifty> objLst = new List<CmotsDerivativeOptionsTopGainers_Nifty>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsDerivativeOptionsTopGainers_Nifty obj = new CmotsDerivativeOptionsTopGainers_Nifty();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "Prevltp".ToUpper())
                        {
                            obj.Prevltp = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Ltp".ToUpper())
                        {
                            obj.Ltp = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Faodiff".ToUpper())
                        {
                            obj.Faodiff = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "faochange".ToUpper())
                        {
                            obj.faochange = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "instname".ToUpper())
                        {
                            obj.instname = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Symbol".ToUpper())
                        {
                            obj.Symbol = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "expdate".ToUpper())
                        {
                            obj.expdate = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "strikeprice".ToUpper())
                        {
                            obj.strikeprice = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "opttype".ToUpper())
                        {
                            obj.opttype = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "updtime".ToUpper())
                        {
                            obj.updtime = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Qty".ToUpper())
                        {
                            obj.Qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "openinterest".ToUpper())
                        {
                            obj.openinterest = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "chgopenint".ToUpper())
                        {
                            obj.chgopenint = app.Value.ToString();
                        }
                    }
                    objLst.Add(obj);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";



                    sqlstr = "delete from CmotsDerivativeOptionsTopGainers_Nifty ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.ExecuteNonQuery();

                    foreach (var mod in objLst)
                    {
                        sqlstr = "insert into CmotsDerivativeOptionsTopGainers_Nifty(Prevltp, Ltp, Faodiff, faochange, instname,";
                        sqlstr = sqlstr + " Symbol, expdate, strikeprice, opttype, updtime, Qty, openinterest, chgopenint, created_by, created_on)";
                        sqlstr = sqlstr + " values (@Prevltp, @Ltp, @Faodiff, @faochange, @instname,";
                        sqlstr = sqlstr + " @Symbol, @expdate, @strikeprice, @opttype, @updtime, @Qty, @openinterest, @chgopenint, @created_by, GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@Prevltp", mod.Prevltp);
                        cmd.Parameters.AddWithValue("@Ltp", mod.Ltp);
                        cmd.Parameters.AddWithValue("@Faodiff", mod.Faodiff);
                        cmd.Parameters.AddWithValue("@faochange", mod.faochange);
                        cmd.Parameters.AddWithValue("@instname", mod.instname);
                        cmd.Parameters.AddWithValue("@Symbol", mod.Symbol);
                        cmd.Parameters.AddWithValue("@expdate", mod.expdate);
                        cmd.Parameters.AddWithValue("@strikeprice", mod.strikeprice);
                        cmd.Parameters.AddWithValue("@opttype", mod.opttype);
                        cmd.Parameters.AddWithValue("@updtime", mod.updtime);
                        cmd.Parameters.AddWithValue("@Qty", mod.Qty);
                        cmd.Parameters.AddWithValue("@openinterest", mod.openinterest);
                        cmd.Parameters.AddWithValue("@chgopenint", mod.chgopenint);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();

                    }


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

        }



        #endregion

        #region Cmots Get Cmots Derivative Options TopLosers_Nifty

        [HttpGet]
        public ActionResult GetDerivativeOptionsTopLosers_Nifty()
        {

            return View();

        }


        [HttpPost]
        public void GetDerivativeOptionsTopLosers_Nifty(string group)
        {

            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsDerivativeOptionsTopLosers_Nifty> objLst = new List<CmotsDerivativeOptionsTopLosers_Nifty>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsDerivativeOptionsTopLosers_Nifty obj = new CmotsDerivativeOptionsTopLosers_Nifty();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "Prevltp".ToUpper())
                        {
                            obj.Prevltp = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Ltp".ToUpper())
                        {
                            obj.Ltp = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Faodiff".ToUpper())
                        {
                            obj.Faodiff = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "faochange".ToUpper())
                        {
                            obj.faochange = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "instname".ToUpper())
                        {
                            obj.instname = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Symbol".ToUpper())
                        {
                            obj.Symbol = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "expdate".ToUpper())
                        {
                            obj.expdate = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "strikeprice".ToUpper())
                        {
                            obj.strikeprice = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "opttype".ToUpper())
                        {
                            obj.opttype = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "updtime".ToUpper())
                        {
                            obj.updtime = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Qty".ToUpper())
                        {
                            obj.Qty = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "openinterest".ToUpper())
                        {
                            obj.openinterest = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "chgopenint".ToUpper())
                        {
                            obj.chgopenint = app.Value.ToString();
                        }
                    }
                    objLst.Add(obj);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";



                    sqlstr = "delete from CmotsDerivativeOptionsTopLosers_Nifty ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.ExecuteNonQuery();

                    foreach (var mod in objLst)
                    {
                        sqlstr = "insert into CmotsDerivativeOptionsTopLosers_Nifty(Prevltp, Ltp, Faodiff, faochange, instname,";
                        sqlstr = sqlstr + " Symbol, expdate, strikeprice, opttype, updtime, Qty, openinterest, chgopenint, created_by, created_on)";
                        sqlstr = sqlstr + " values (@Prevltp, @Ltp, @Faodiff, @faochange, @instname,";
                        sqlstr = sqlstr + " @Symbol, @expdate, @strikeprice, @opttype, @updtime, @Qty, @openinterest, @chgopenint, @created_by, GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@Prevltp", mod.Prevltp);
                        cmd.Parameters.AddWithValue("@Ltp", mod.Ltp);
                        cmd.Parameters.AddWithValue("@Faodiff", mod.Faodiff);
                        cmd.Parameters.AddWithValue("@faochange", mod.faochange);
                        cmd.Parameters.AddWithValue("@instname", mod.instname);
                        cmd.Parameters.AddWithValue("@Symbol", mod.Symbol);
                        cmd.Parameters.AddWithValue("@expdate", mod.expdate);
                        cmd.Parameters.AddWithValue("@strikeprice", mod.strikeprice);
                        cmd.Parameters.AddWithValue("@opttype", mod.opttype);
                        cmd.Parameters.AddWithValue("@updtime", mod.updtime);
                        cmd.Parameters.AddWithValue("@Qty", mod.Qty);
                        cmd.Parameters.AddWithValue("@openinterest", mod.openinterest);
                        cmd.Parameters.AddWithValue("@chgopenint", mod.chgopenint);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();

                    }


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

        }



        #endregion





        #region Cmots Get Commodities Gainers And Losers

        [HttpGet]
        public ActionResult GetCommoditiesGainersAndLosers()
        {
            return View();
        }


        [HttpPost]
        public void GetCommoditiesGainersAndLosers(string group)
        {
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsCommoditiesGainersAndLosers> objLst = new List<CmotsCommoditiesGainersAndLosers>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsCommoditiesGainersAndLosers obj = new CmotsCommoditiesGainersAndLosers();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "Symbol".ToUpper())
                        {
                            obj.Symbol = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "CommName".ToUpper())
                        {
                            obj.CommName = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Category".ToUpper())
                        {
                            obj.Category = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Unit".ToUpper())
                        {
                            obj.Unit = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Trd_Date".ToUpper())
                        {
                            obj.Trd_Date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Exp_Date".ToUpper())
                        {
                            obj.Exp_Date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "OpenPrice".ToUpper())
                        {
                            obj.OpenPrice = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "HPrice".ToUpper())
                        {
                            obj.HPrice = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "LPrice".ToUpper())
                        {
                            obj.LPrice = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "ClosePrice".ToUpper())
                        {
                            obj.ClosePrice = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "PrevClose".ToUpper())
                        {
                            obj.PrevClose = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "PrevClosedate".ToUpper())
                        {
                            obj.PrevClosedate = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Volume".ToUpper())
                        {
                            obj.Volume = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "TrdVal".ToUpper())
                        {
                            obj.TrdVal = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Center".ToUpper())
                        {
                            obj.Center = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "OI".ToUpper())
                        {
                            obj.OI = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Diff".ToUpper())
                        {
                            obj.Diff = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Change".ToUpper())
                        {
                            obj.Change = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "MaxDate".ToUpper())
                        {
                            obj.MaxDate = app.Value.ToString();
                        }
                    }
                    objLst.Add(obj);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";



                    sqlstr = "delete from CmotsCommoditiesGainersAndLosers ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.ExecuteNonQuery();

                    foreach (var mod in objLst)
                    {
                        sqlstr = "insert into CmotsCommoditiesGainersAndLosers(Symbol, CommName, Category, Unit, Trd_Date, Exp_Date,";
                        sqlstr = sqlstr + " OpenPrice, HPrice, LPrice, ClosePrice, PrevClose, PrevClosedate, Volume, TrdVal, Center, OI,";
                        sqlstr = sqlstr + " Diff, Change, MaxDate, created_by, created_on)";
                        sqlstr = sqlstr + " values (@Symbol, @CommName, @Category, @Unit, @Trd_Date, @Exp_Date,";
                        sqlstr = sqlstr + " @OpenPrice, @HPrice, @LPrice, @ClosePrice, @PrevClose, @PrevClosedate, @Volume, @TrdVal, @Center, @OI,";
                        sqlstr = sqlstr + " @Diff, @Change, @MaxDate, @created_by, GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@Symbol", mod.Symbol);
                        cmd.Parameters.AddWithValue("@CommName", mod.CommName);
                        cmd.Parameters.AddWithValue("@Category", mod.Category);
                        cmd.Parameters.AddWithValue("@Unit", mod.Unit);
                        cmd.Parameters.AddWithValue("@Trd_Date", mod.Trd_Date);
                        cmd.Parameters.AddWithValue("@Exp_Date", mod.Exp_Date);
                        cmd.Parameters.AddWithValue("@OpenPrice", mod.OpenPrice);
                        cmd.Parameters.AddWithValue("@HPrice", mod.HPrice);
                        cmd.Parameters.AddWithValue("@LPrice", mod.LPrice);
                        cmd.Parameters.AddWithValue("@ClosePrice", mod.ClosePrice);
                        cmd.Parameters.AddWithValue("@PrevClose", mod.PrevClose);
                        cmd.Parameters.AddWithValue("@PrevClosedate", mod.PrevClosedate);
                        cmd.Parameters.AddWithValue("@Volume", mod.Volume);
                        cmd.Parameters.AddWithValue("@TrdVal", mod.TrdVal);
                        cmd.Parameters.AddWithValue("@Center", mod.Center);
                        cmd.Parameters.AddWithValue("@OI", mod.OI);
                        cmd.Parameters.AddWithValue("@Diff", mod.Diff);
                        cmd.Parameters.AddWithValue("@Change", mod.Change);
                        cmd.Parameters.AddWithValue("@MaxDate", mod.MaxDate);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();

                    }


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

        }



        #endregion

        #region Cmots Get Commodities Closing Prices

        [HttpGet]
        public ActionResult GetCommoditiesClosingPrices()
        {
            return View();
        }


        [HttpPost]
        public void GetCommoditiesClosingPrices(string group)
        {
            try
            {
                //group = "[" + group + "]";
                bool result = false;
                List<CmotsCommoditiesClosingPrices> objLst = new List<CmotsCommoditiesClosingPrices>();
                var objects = Newtonsoft.Json.Linq.JArray.Parse(group);

                foreach (Newtonsoft.Json.Linq.JObject root in objects)
                {
                    CmotsCommoditiesClosingPrices obj = new CmotsCommoditiesClosingPrices();
                    foreach (KeyValuePair<String, Newtonsoft.Json.Linq.JToken> app in root)
                    {
                        var appName = app.Key;
                        if (appName.ToUpper() == "Symbol".ToUpper())
                        {
                            obj.Symbol = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "CommName".ToUpper())
                        {
                            obj.CommName = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Category".ToUpper())
                        {
                            obj.Category = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Unit".ToUpper())
                        {
                            obj.Unit = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Trd_Date".ToUpper())
                        {
                            obj.Trd_Date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Exp_Date".ToUpper())
                        {
                            obj.Exp_Date = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "OpenPrice".ToUpper())
                        {
                            obj.OpenPrice = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "HPrice".ToUpper())
                        {
                            obj.HPrice = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "LPrice".ToUpper())
                        {
                            obj.LPrice = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "ClosePrice".ToUpper())
                        {
                            obj.ClosePrice = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "PrevClose".ToUpper())
                        {
                            obj.PrevClose = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "PrevClosedate".ToUpper())
                        {
                            obj.PrevClosedate = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Volume".ToUpper())
                        {
                            obj.Volume = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "TrdVal".ToUpper())
                        {
                            obj.TrdVal = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Center".ToUpper())
                        {
                            obj.Center = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "OI".ToUpper())
                        {
                            obj.OI = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Diff".ToUpper())
                        {
                            obj.Diff = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "Change".ToUpper())
                        {
                            obj.Change = app.Value.ToString();
                        }
                        else if (appName.ToUpper() == "MaxDate".ToUpper())
                        {
                            obj.MaxDate = app.Value.ToString();
                        }
                    }
                    objLst.Add(obj);
                }

                // *************************** Insert Data ************************** //

                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "";



                    sqlstr = "delete from CmotsCommoditiesClosingPrices ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.ExecuteNonQuery();

                    foreach (var mod in objLst)
                    {
                        sqlstr = "insert into CmotsCommoditiesClosingPrices(Symbol, CommName, Category, Unit, Trd_Date, Exp_Date,";
                        sqlstr = sqlstr + " OpenPrice, HPrice, LPrice, ClosePrice, PrevClose, PrevClosedate, Volume, TrdVal, Center, OI,";
                        sqlstr = sqlstr + " Diff, Change, MaxDate, created_by, created_on)";
                        sqlstr = sqlstr + " values (@Symbol, @CommName, @Category, @Unit, @Trd_Date, @Exp_Date,";
                        sqlstr = sqlstr + " @OpenPrice, @HPrice, @LPrice, @ClosePrice, @PrevClose, @PrevClosedate, @Volume, @TrdVal, @Center, @OI,";
                        sqlstr = sqlstr + " @Diff, @Change, @MaxDate, @created_by, GetDate())";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Parameters.AddWithValue("@Symbol", mod.Symbol);
                        cmd.Parameters.AddWithValue("@CommName", mod.CommName);
                        cmd.Parameters.AddWithValue("@Category", mod.Category);
                        cmd.Parameters.AddWithValue("@Unit", mod.Unit);
                        cmd.Parameters.AddWithValue("@Trd_Date", mod.Trd_Date);
                        cmd.Parameters.AddWithValue("@Exp_Date", mod.Exp_Date);
                        cmd.Parameters.AddWithValue("@OpenPrice", mod.OpenPrice);
                        cmd.Parameters.AddWithValue("@HPrice", mod.HPrice);
                        cmd.Parameters.AddWithValue("@LPrice", mod.LPrice);
                        cmd.Parameters.AddWithValue("@ClosePrice", mod.ClosePrice);
                        cmd.Parameters.AddWithValue("@PrevClose", mod.PrevClose);
                        cmd.Parameters.AddWithValue("@PrevClosedate", mod.PrevClosedate);
                        cmd.Parameters.AddWithValue("@Volume", mod.Volume);
                        cmd.Parameters.AddWithValue("@TrdVal", mod.TrdVal);
                        cmd.Parameters.AddWithValue("@Center", mod.Center);
                        cmd.Parameters.AddWithValue("@OI", mod.OI);
                        cmd.Parameters.AddWithValue("@Diff", mod.Diff);
                        cmd.Parameters.AddWithValue("@Change", mod.Change);
                        cmd.Parameters.AddWithValue("@MaxDate", mod.MaxDate);
                        cmd.Parameters.AddWithValue("@created_by", "Online");
                        cmd.ExecuteNonQuery();

                    }


                    transaction.Commit();
                    gConnection.Close();
                    @ViewBag.HideClass = "alert alert-success";
                    @ViewBag.Message = "Insertion Done Successfully!";


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

        }



        #endregion

    }

}