﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Global;
using KR_Choksey.Models;
using KR_Choksey.Global;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Text;
using System.Web.Security;
using System.Data;
using System.Security.Cryptography;

namespace KR_Choksey.Controllers
{
    public class AccountController : Controller
    {
        DropDownSelection objDrop = new DropDownSelection();
        GlobalFunction objGFunc = new GlobalFunction();

        #region Sign Up (Get)

        [HttpGet]
        [CustomActionFilter]
        public ActionResult SignUp()
        {
            @ViewBag.CapImg = RandomFunction();
            return View();
        }

        #endregion


        #region Sign Up (Post)
        [HttpPost]
        [CustomActionFilter]
        public ActionResult SignUp(Client_Master objCliMa, FormCollection frm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // Check For Captcha Matching
                    //string capImg = frm["CapImg"].ToString();
                    //string capText = frm["CapText"].ToString();
                    //if (capImg != capText)
                    //{
                    //    @ViewBag.HideClass = "alert alert-danger";
                    //    @ViewBag.Message = "Error! Entered Text Not Matching";
                    //}
                    //if (!String.IsNullOrEmpty(Request.Form["g-recaptcha-response"]))
                    //{
                    //    @ViewBag.HideClass = "alert alert-danger";
                    //    @ViewBag.Message = "Error! Please check the capcha";
                    //}
                    if (!String.IsNullOrEmpty(Request.Form["g-recaptcha-response"]))
                    {

                        bool res = true;
                        foreach (char c in objCliMa.bo_mobile)
                        {
                            if (c < '0' || c > '9')
                            {
                                res = false;
                            }
                        }

                        if (res == false || (objCliMa.bo_mobile.Length != 10))
                        {
                            @ViewBag.HideClass = "alert alert-danger";
                            @ViewBag.Message = "Please Enter Valid Mobile Number.";
                        }
                        else
                        {
                            // Check If Mobile Already Exists //
                            if (CheckIfMobileExists(objCliMa.bo_mobile) == true)
                            {
                                @ViewBag.HideClass = "alert alert-danger";
                                @ViewBag.Message = "Mobile Number Already Registered With Us.";
                            }
                            else if (CheckIfEmailExists(objCliMa.bo_email) == true)
                            {
                                @ViewBag.HideClass = "alert alert-danger";
                                @ViewBag.Message = "Email Already Registered With Us.";
                            }
                            else
                            {
                                gConnection.Open();
                                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                                SqlTransaction transaction;
                                transaction = GlobalVariables.gConn.BeginTransaction();
                                cmd.Transaction = transaction;
                                cmd.Connection = GlobalVariables.gConn;
                                try
                                {
                                    // Insertion Code For Client_Master Table //
                                    string sqlstr = "insert into Client_Master(bo_client_code,first_name,last_name,bo_email,bo_mobile)";
                                    sqlstr = sqlstr + " values (@bo_client_code,@first_name,@last_name,@bo_email,@bo_mobile)";
                                    cmd.CommandText = sqlstr;
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@bo_client_code", "");
                                    cmd.Parameters.AddWithValue("@first_name", objCliMa.first_name);
                                    cmd.Parameters.AddWithValue("@last_name", objCliMa.last_name);
                                    cmd.Parameters.AddWithValue("@bo_email", objCliMa.bo_email);
                                    cmd.Parameters.AddWithValue("@bo_mobile", objCliMa.bo_mobile);
                                    cmd.ExecuteNonQuery();

                                    // Get client_master_code from Client_Master //
                                    sqlstr = "select Max(client_master_code) as client_master_code from Client_Master where first_name=@first_name";
                                    cmd.CommandText = sqlstr;
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@first_name", objCliMa.first_name);
                                    SqlDataReader sdr = cmd.ExecuteReader();
                                    while (sdr.Read())
                                    {
                                        objCliMa.client_master_code = Convert.ToInt32(sdr["client_master_code"].ToString());
                                    }
                                    sdr.Close();

                                    // Insert Code For client_login Table //
                                    sqlstr = "Insert into client_login(username,password,creation_date,FK_client_master_code)";
                                    sqlstr = sqlstr + " values (@username,@password,GetDate(),@FK_client_master_code)";
                                    cmd.CommandText = sqlstr;
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@username", objCliMa.bo_email);
                                    cmd.Parameters.AddWithValue("@password", "");
                                    cmd.Parameters.AddWithValue("@FK_client_master_code", objCliMa.client_master_code);
                                    cmd.ExecuteNonQuery();

                                    transaction.Commit();

                                    @ViewBag.HideClass = "alert alert-success";
                                    @ViewBag.Message = "Thanks For Registering With Us. Check Your Email For Further Process.";


                                    //Host = ,
                                    string FromMail = ConfigurationManager.AppSettings["smtpDisplayId"];
                                    string ToMail = objCliMa.bo_email;
                                    string Subject = "Welcome At KRChoksey";
                                    string Body = "Hi " + objCliMa.first_name;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Thanks for registering with us.";
                                    Body = Body + Environment.NewLine;
                                    string UserName = ConfigurationManager.AppSettings["smtpUser"];
                                    string Password = ConfigurationManager.AppSettings["smtpPass"];
                                    string Link = ConfigurationManager.AppSettings["smtpLink"];
                                    string Param = "?_UserName=" + Global.Encryption.encrypt(objCliMa.bo_email) + "&_ClientMasterCode=" + Global.Encryption.encrypt(objCliMa.client_master_code.ToString());
                                    //Param = Global.Encryption.encrypt(Link);
                                    Link = Link + Param;
                                    Body = Body + "Click On Below Link To Proceed Further";
                                    Body = Body + Environment.NewLine;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + Link;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Thanks";
                                    Body = Body + Environment.NewLine;
                                    Body = Body + "Team KRChoksey";
                                    string AttachmentPath = "";
                                    bool resu = objGFunc.SendMail(FromMail, ToMail, Subject, Body, UserName, Password, AttachmentPath);
                                    ModelState.Clear();

                                    // Send SMS
                                    string SMSBody = "Dear @Name, welcome to KRChoksey. Please check your email to set your password & start creating wealth with KRChoksey.";
                                    string MessageBody = SMSBody.Replace("@Name", objCliMa.first_name + " " + objCliMa.last_name);
                                    objGFunc.SendSMS(objCliMa.bo_mobile.ToString(), MessageBody);
                                }
                                catch (Exception ex)
                                {
                                    transaction.Rollback();
                                    gConnection.Close();
                                    Global.ErrorHandlerClass.LogError(ex);
                                    @ViewBag.HideClass = "alert alert-danger";
                                    @ViewBag.Message = "Error!";
                                }
                            }
                        }
                    }
                    else
                    {
                        @ViewBag.HideClass = "alert alert-danger";
                        @ViewBag.Message = "Error! Please check the capcha";
                    }
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            @ViewBag.CapImg = RandomFunction();
            return View();
        }

        #endregion        


        #region User Details (Get)

        [HttpGet]
        [CustomActionFilter]
        public ActionResult User_Details(string _ID)
        {
            string ID = Encryption.decrypt(_ID);
            Client_Master objCliMas = new Client_Master();
            //ID = "1014";
            objCliMas = objGFunc.GetClientMasterFromClientId(ID);
            objCliMas.bo_dob = DateTime.Now;

            ViewData["IncomeSlab1"] = objDrop.IncomeSlabList();
            ViewData["Occupation1"] = objDrop.OccupationList();
            ViewData["City1"] = objDrop.GetAllCityList();
            ViewData["State1"] = objDrop.GetAllStateList("");
            ViewData["Country1"] = objDrop.GetAllCountryList("");
            return View(objCliMas);
        }

        #endregion


        #region User Details (Post)

        [HttpPost]
        [CustomActionFilter]
        public ActionResult User_Details(Client_Master objCliMa)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    objCliMa = NullToBlabk(objCliMa);

                    gConnection.Open();
                    string sqlstr = "update dbo.Client_Master set bo_addr1=@bo_addr1, bo_addr2=@bo_addr2, bo_addr3=@bo_addr3, bo_city=@bo_city,";
                    sqlstr = sqlstr + " bo_state=@bo_state, bo_country=@bo_country, bo_zip=@bo_zip, bo_dob=@bo_dob, bo_pan=@bo_pan,";
                    sqlstr = sqlstr + " aadhaar=@aadhaar, income_slab=@income_slab, occupation=@occupation, bo_modified_by=@bo_modified_by,";
                    sqlstr = sqlstr + " bo_modified_on=GETDATE() where client_master_code=@client_master_code";
                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                    cmd.Parameters.AddWithValue("@bo_addr1", objCliMa.bo_addr1);
                    cmd.Parameters.AddWithValue("@bo_addr2", objCliMa.bo_addr2);
                    cmd.Parameters.AddWithValue("@bo_addr3", objCliMa.bo_addr3);
                    cmd.Parameters.AddWithValue("@bo_city", objCliMa.bo_city);
                    cmd.Parameters.AddWithValue("@bo_state", objCliMa.bo_state);
                    cmd.Parameters.AddWithValue("@bo_country", objCliMa.bo_country);
                    cmd.Parameters.AddWithValue("@bo_zip", objCliMa.bo_zip);
                    cmd.Parameters.AddWithValue("@bo_dob", objCliMa.bo_dob);
                    cmd.Parameters.AddWithValue("@bo_pan", objCliMa.bo_pan);
                    cmd.Parameters.AddWithValue("@aadhaar", objCliMa.aadhaar);
                    cmd.Parameters.AddWithValue("@income_slab", objCliMa.income_slab);
                    cmd.Parameters.AddWithValue("@occupation", objCliMa.occupation);
                    cmd.Parameters.AddWithValue("@client_master_code", objCliMa.client_master_code);
                    cmd.Parameters.AddWithValue("@bo_modified_by", objCliMa.client_master_code);
                    cmd.ExecuteNonQuery();

                    gConnection.Close();
                    ModelState.Clear();

                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            ViewData["IncomeSlab1"] = objDrop.IncomeSlabList();
            ViewData["Occupation1"] = objDrop.OccupationList();
            ViewData["City1"] = objDrop.GetAllCityList();
            ViewData["State1"] = objDrop.GetAllStateList("");
            ViewData["Country1"] = objDrop.GetAllCountryList("");
            return View();
        }

        #endregion



        [HttpGet]
        //[CustomActionFilter]
        public ActionResult Login()
        {
            return PartialView();
        }




        #region Login (Post)

        [HttpPost]
        public string Login(string UserName, string Password)
        {
            string ReturnUrl = "";
            bool result = false;
            
            //ReturnUrl = "http://localhost:11546/Account/User_Dashboard";
            if (UserName != "" & Password != "")
            {
                try
                {
                    
                    string sqlstr = "select l.FK_client_master_code, l.username from client_login l ";
                    sqlstr = sqlstr + " where  ";
                    sqlstr = sqlstr + " l.username = @bo_email and l.password=HASHBYTES('SHA1', @password)";
                    gConnection.Open();
                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                    
                    // Add the input parameter and set its properties.
                    SqlParameter parameter = new SqlParameter();
                    parameter.ParameterName = "@bo_email";
                    parameter.SqlDbType = SqlDbType.VarChar;
                    parameter.Direction = ParameterDirection.Input;
                    parameter.Value = UserName;
                    // Add the parameter to the Parameters collection. 
                    cmd.Parameters.Add(parameter);


                    // Add the input parameter and set its properties.
                    SqlParameter parameter1 = new SqlParameter();
                    parameter1.ParameterName = "@password";
                    parameter1.SqlDbType = SqlDbType.VarChar;
                    parameter1.Direction = ParameterDirection.Input;
                    parameter1.Value = Password;

                    // Add the parameter to the Parameters collection. 
                    cmd.Parameters.Add(parameter1);


                    //cmd.CommandTimeout = 120;
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                       
                        System.Web.HttpContext.Current.Session["UserId"] = UserName.ToUpper();

                        bool res = UpdateLastLogin(UserName);
                        result = true;
                        //return Redirect(ReturnUrl);
                    }
                    gConnection.Close();
                    //return RedirectResult(ReturnUrl);
                    if(result == false)
                    {
                        return "Wrong Password";
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandlerClass.LogError(ex);
                    return "";
                }
            }
            else
            {
                //ErrorHandlerClass.LogError(ex);
                return "";
            }
            //return Redirect(ReturnUrl);
            //return PartialView();
            return ReturnUrl;
        }

        #endregion



        #region Enter Password (Get)

        [HttpGet]
        [CustomActionFilter]
        public ActionResult Enter_Password(string _UserName, string _ClientMasterCode)
        {
            string UserName = Encryption.decrypt(_UserName);
            string ClientMasterCode = Encryption.decrypt(_ClientMasterCode);
            client_login objCliLog = new client_login();
            objCliLog.username = UserName;
            objCliLog.FK_client_master_code = Convert.ToInt32(ClientMasterCode);
            return View(objCliLog);
        }

        #endregion


        #region Enter Password (Post)

        [HttpPost]
        [CustomActionFilter]
        public ActionResult Enter_Password(client_login objCliLog)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    if (objCliLog.password == objCliLog.password_confirm)
                    {
                        gConnection.Open();
                        string sqlstr = "update client_login set password=HASHBYTES('SHA1',@password), web_modify_on=GETDATE() where ";
                        sqlstr = sqlstr + " username=@username and FK_client_master_code=@client_master_code";
                        SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                        cmd.Parameters.AddWithValue("@password", objCliLog.password);
                        cmd.Parameters.AddWithValue("@username", objCliLog.username);
                        cmd.Parameters.AddWithValue("@client_master_code", objCliLog.FK_client_master_code);
                        cmd.ExecuteNonQuery();

                        gConnection.Close();
                        @ViewBag.HideClass = "alert alert-success";
                        @ViewBag.Message = "Password Entered Successfully.";
                        //ModelState.Clear();
                        //return View("User_Details", objCliLog);
                        return RedirectToAction("User_Details", new
                        {
                            _ID = Encryption.encrypt(objCliLog.FK_client_master_code.ToString())
                        });
                        Session["UserId"] = objCliLog.username;
                    }
                    else
                    {
                        @ViewBag.HideClass = "alert alert-danger";
                        @ViewBag.Message = "Please Not Matching.";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return View();
        }

        #endregion



        #region Private Function For The Controller


        public JsonResult GetAllDetailsByPinCode(string PinCode)
        {
            IndianPostalDetails objIPD = new IndianPostalDetails();
            try
            {
                string sqlstr = "select Isnull(officename,'') as officename, Isnull(pincode,'') as pincode, Isnull(officeType,'') As officeType, ";
                sqlstr = sqlstr + " Isnull(DeliveryStatus,'') As DeliveryStatus, Isnull(DivisionName,'') As DivisionName, Isnull(RegionName,'') As RegionName, ";
                sqlstr = sqlstr + " Isnull(CircleName,'') As CircleName, Isnull(Taluk,'') as Taluk, Isnull(DistrictName,'') As DistrictName, ";
                sqlstr = sqlstr + " Isnull(StateName,'') as StateName, Isnull(CountryName,'') As CountryName from IndianPostalDetails Where Isnull(pincode,'')=@pincode";
                gConnection.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@pincode", PinCode);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objIPD.officename = sdr["officename"].ToString();
                    objIPD.pincode = sdr["pincode"].ToString();
                    objIPD.officeType = sdr["officeType"].ToString();
                    objIPD.DeliveryStatus = sdr["DeliveryStatus"].ToString();
                    objIPD.DivisionName = sdr["DivisionName"].ToString();
                    objIPD.RegionName = sdr["RegionName"].ToString();
                    objIPD.CircleName = sdr["CircleName"].ToString();
                    objIPD.Taluk = sdr["Taluk"].ToString();
                    objIPD.DistrictName = sdr["DistrictName"].ToString();
                    objIPD.StateName = sdr["StateName"].ToString();
                    objIPD.CountryName = sdr["CountryName"].ToString();
                }

                gConnection.Close();

            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            var data = objIPD;
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        private bool CheckIfMobileExists(string Mobile)
        {
            bool result = false;
            try
            {
                gConnection.Open();
                string sqlstr = "select * from dbo.Client_Master Where bo_mobile=@bo_mobile";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@bo_mobile", Mobile);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    result = true;
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }


            return result;
        }

        private bool CheckIfEmailExists(string Email)
        {
            bool result = false;
            try
            {
                gConnection.Open();
                string sqlstr = "select * from dbo.Client_Master Where bo_email = @bo_email";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@bo_email", Email);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    result = true;
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return result;
        }

        private Client_Master NullToBlabk(Client_Master objCliMa)
        {
            if (objCliMa.bo_addr1 == null)
            {
                objCliMa.bo_addr1 = "";
            }
            if (objCliMa.bo_addr2 == null)
            {
                objCliMa.bo_addr2 = "";
            }
            if (objCliMa.bo_addr3 == null)
            {
                objCliMa.bo_addr3 = "";
            }
            if (objCliMa.bo_city == null)
            {
                objCliMa.bo_city = "";
            }
            if (objCliMa.bo_state == null)
            {
                objCliMa.bo_state = "";
            }
            if (objCliMa.bo_country == null)
            {
                objCliMa.bo_country = "";
            }
            if (objCliMa.bo_zip == null)
            {
                objCliMa.bo_zip = "";
            }
            if (objCliMa.bo_dob == null)
            {
                objCliMa.bo_dob = Convert.ToDateTime("01/01/1900");
            }
            if (objCliMa.bo_pan == null)
            {
                objCliMa.bo_pan = "";
            }
            if (objCliMa.aadhaar == null)
            {
                objCliMa.aadhaar = "";
            }
            if (objCliMa.income_slab == null)
            {
                objCliMa.income_slab = "";
            }
            if (objCliMa.occupation == null)
            {
                objCliMa.occupation = "";
            }
            return objCliMa;

        }


        private string RandomFunction()
        {
            Random random = new Random();
            //string combination = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
            string combination = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789abcdefghijklmnpqrstuvwxyz";
            StringBuilder captcha = new StringBuilder();
            for (int i = 0; i < 6; i++)
                captcha.Append(combination[random.Next(combination.Length)]);
            string Var = captcha.ToString();

            return Var;
        }

        #endregion


        public ActionResult Log_Out()
        {
            Session.Clear();
            Session.RemoveAll();
            return RedirectToAction("Index", "Home");
        }


        [HttpGet]
        public PartialViewResult Footer_Sign_Up()
        {
            return PartialView();
        }


        [HttpPost]
        public PartialViewResult Footer_Sign_Up(Client_Master objCliMa, FormCollection frm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    bool res = true;
                    foreach (char c in objCliMa.bo_mobile)
                    {
                        if (c < '0' || c > '9')
                        {
                            res = false;
                        }
                    }

                    if (res == false || (objCliMa.bo_mobile.Length != 10))
                    {
                        @ViewBag.HideClass = "alert alert-danger";
                        @ViewBag.Message = "Please Enter Valid Mobile Number.";
                    }
                    else
                    {
                        // Check If Mobile Already Exists //
                        if (CheckIfMobileExists(objCliMa.bo_mobile) == true)
                        {
                            @ViewBag.HideClass = "alert alert-danger";
                            @ViewBag.Message = "Mobile Number Already Registered With Us.";
                        }
                        else if (CheckIfEmailExists(objCliMa.bo_email) == true)
                        {
                            @ViewBag.HideClass = "alert alert-danger";
                            @ViewBag.Message = "Email Already Registered With Us.";
                        }
                        else
                        {
                            gConnection.Open();
                            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                            SqlTransaction transaction;
                            transaction = GlobalVariables.gConn.BeginTransaction();
                            cmd.Transaction = transaction;
                            cmd.Connection = GlobalVariables.gConn;
                            try
                            {
                                // Insertion Code For Client_Master Table //
                                string sqlstr = "insert into Client_Master(bo_client_code,first_name,last_name,bo_email,bo_mobile)";
                                sqlstr = sqlstr + " values (@bo_client_code,@first_name,@last_name,@bo_email,@bo_mobile)";
                                cmd.CommandText = sqlstr;
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@bo_client_code", "");
                                cmd.Parameters.AddWithValue("@first_name", objCliMa.first_name);
                                cmd.Parameters.AddWithValue("@last_name", objCliMa.last_name);
                                cmd.Parameters.AddWithValue("@bo_email", objCliMa.bo_email);
                                cmd.Parameters.AddWithValue("@bo_mobile", objCliMa.bo_mobile);
                                cmd.ExecuteNonQuery();

                                // Get client_master_code from Client_Master //
                                sqlstr = "select Max(client_master_code) as client_master_code from Client_Master where first_name=@first_name";
                                cmd.CommandText = sqlstr;
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@first_name", objCliMa.first_name);
                                SqlDataReader sdr = cmd.ExecuteReader();
                                while (sdr.Read())
                                {
                                    objCliMa.client_master_code = Convert.ToInt32(sdr["client_master_code"].ToString());
                                }
                                sdr.Close();

                                // Insert Code For client_login Table //
                                sqlstr = "Insert into client_login(username,password,creation_date,FK_client_master_code)";
                                sqlstr = sqlstr + " values (@username,@password,GetDate(),@FK_client_master_code)";
                                cmd.CommandText = sqlstr;
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@username", objCliMa.bo_email);
                                cmd.Parameters.AddWithValue("@password", "");
                                cmd.Parameters.AddWithValue("@FK_client_master_code", objCliMa.client_master_code);
                                cmd.ExecuteNonQuery();

                                transaction.Commit();

                                @ViewBag.HideClass = "alert alert-success";
                                @ViewBag.Message = "Thanks For Registering With Us. Check Your Email For Further Process.";


                                //Host = ,
                                string FromMail = ConfigurationManager.AppSettings["smtpDisplayId"];
                                string ToMail = objCliMa.bo_email;
                                string Subject = "Welcome At KRChoksey";
                                string Body = "Hi " + objCliMa.first_name;
                                Body = Body + Environment.NewLine;
                                Body = Body + "Thanks for registering with us.";
                                Body = Body + Environment.NewLine;
                                string UserName = ConfigurationManager.AppSettings["smtpUser"];
                                string Password = ConfigurationManager.AppSettings["smtpPass"];
                                string Link = ConfigurationManager.AppSettings["smtpLink"];
                                string Param = "?_UserName=" + Global.Encryption.encrypt(objCliMa.bo_email) + "&_ClientMasterCode=" + Global.Encryption.encrypt(objCliMa.client_master_code.ToString());
                                //Param = Global.Encryption.encrypt(Link);
                                Link = Link + Param;
                                Body = Body + "Click On Below Link To Proceed Further";
                                Body = Body + Environment.NewLine;
                                Body = Body + Environment.NewLine;
                                Body = Body + Link;
                                Body = Body + Environment.NewLine;
                                Body = Body + Environment.NewLine;
                                Body = Body + "Thanks";
                                Body = Body + Environment.NewLine;
                                Body = Body + "Team KRChoksey";
                                string AttachmentPath = "";
                                bool resu = objGFunc.SendMail(FromMail, ToMail, Subject, Body, UserName, Password, AttachmentPath);
                                ModelState.Clear();
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                gConnection.Close();
                                Global.ErrorHandlerClass.LogError(ex);
                                @ViewBag.HideClass = "alert alert-danger";
                                @ViewBag.Message = "Error!";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return PartialView();
        }
        

        [HttpGet]
        [CustomActionFilter]
        public ActionResult Forgot_Password()
        {
            return View();
        }

        [HttpPost]
        [CustomActionFilter]
        public ActionResult Forgot_Password(FormCollection frm)
        {
            try
            {
                string ClientMasterCode = "";
                string email = frm["EmailId"].ToString();
                if (email != "")
                {
                    gConnection.Open();
                    string sqlstr = "select ISNULL(client_master_code,0) as client_master_code, ";
                    sqlstr = sqlstr + " bo_email from dbo.Client_Master where bo_email=@bo_email";
                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                    cmd.Parameters.AddWithValue("@bo_email", email);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        ClientMasterCode = sdr["client_master_code"].ToString();
                    }
                    gConnection.Close();

                    if (ClientMasterCode != "")
                    {

                        //Host = ,
                        string FromMail = ConfigurationManager.AppSettings["smtpDisplayId"];
                        string ToMail = email;
                        string Subject = "KRChoksey: Change Password";
                        string Body = "Hi ";
                        Body = Body + Environment.NewLine;
                        Body = Body + "Please click On below link to change your password.";
                        Body = Body + Environment.NewLine;
                        string UserName = ConfigurationManager.AppSettings["smtpUser"];
                        string Password = ConfigurationManager.AppSettings["smtpPass"];
                        string Link = ConfigurationManager.AppSettings["smtpLink"];
                        string Param = "?_UserName=" + Global.Encryption.encrypt(email) + "&_ClientMasterCode=" + Global.Encryption.encrypt(ClientMasterCode.ToString());
                        //Param = Global.Encryption.encrypt(Link);
                        Link = Link + Param;
                        //Body = Body + "Click On Below Link To Proceed Further";
                        //Body = Body + Environment.NewLine;
                        Body = Body + Environment.NewLine;
                        Body = Body + Link;
                        Body = Body + Environment.NewLine;
                        Body = Body + Environment.NewLine;
                        Body = Body + "Thanks";
                        Body = Body + Environment.NewLine;
                        Body = Body + "Team KRChoksey";
                        string AttachmentPath = "";
                        bool resu = objGFunc.SendMail(FromMail, ToMail, Subject, Body, UserName, Password, AttachmentPath);

                        ViewBag.Message = "Visit your email for reset link !";
                        ViewBag.Hideclass = "alert-alert-success";
                    }
                    else
                    {
                        ViewBag.Message = "Please Enter Valid Email Id";
                        ViewBag.Hideclass = "alert-alert-danger";
                    }
                }
                else
                {
                    ViewBag.Message = "Please Enter Valid Email Id";
                    ViewBag.Hideclass = "alert-alert-danger";
                }
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return View();
        }

                
        [CustomActionFilter]
        public ActionResult User_Dashboard()
        {
            return View();
        }

        public PartialViewResult Dashboard_Details(string UserIdSigned)
        {
            UserIdSigned = System.Web.HttpContext.Current.Session["UserId"].ToString();
            List<Client_Master> objCMList = new List<Client_Master>();

            try
            {
                Client_Master objCM = new Client_Master();
                gConnection.Open();
                string sqlstr = "select bo_long_name, bo_client_code,bo_short_name, bo_addr1,bo_addr2,bo_addr3,bo_city,bo_state,bo_zip,bo_country,";
                sqlstr = sqlstr + " bo_pan,bo_email,bo_mobile, bo_bank_name, bo_client_dp_id from client_master cm, client_login cl ";
                sqlstr = sqlstr + " where cm.client_master_code=cl.FK_client_master_code and cl.username=@username";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@username", UserIdSigned);
                SqlDataReader sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    
                    objCM.bo_long_name = sdr["bo_long_name"].ToString();
                    objCM.bo_client_code = sdr["bo_client_code"].ToString();
                    objCM.bo_addr1 = sdr["bo_addr1"].ToString();
                    objCM.bo_addr2 = sdr["bo_addr2"].ToString();
                    objCM.bo_addr3 = sdr["bo_addr3"].ToString();
                    objCM.bo_city = sdr["bo_city"].ToString();
                    objCM.bo_state = sdr["bo_state"].ToString();
                    objCM.bo_zip = sdr["bo_zip"].ToString();
                    objCM.bo_country = sdr["bo_country"].ToString();
                    objCM.bo_pan = sdr["bo_pan"].ToString();
                    objCM.bo_email = sdr["bo_email"].ToString();
                    objCM.bo_mobile = sdr["bo_mobile"].ToString();
                    objCM.bo_bank_name = sdr["bo_bank_name"].ToString();
                    objCM.bo_client_dp_id = sdr["bo_client_dp_id"].ToString();
                    objCM.bo_short_name = sdr["bo_short_name"].ToString();

                    objCM.bo_addr1 = objCM.bo_addr1 + " " + objCM.bo_addr2 + " " + objCM.bo_addr3;
                    objCMList.Add(objCM);
                }
                gConnection.Close();
                //string data = Encryption_Cmots(objCM.bo_client_code, "KRCPF");
                string data = Encryption_Cmots(UserIdSigned, "KRCPF");
                @ViewBag.ClientCode = Encryption_Cmots(UserIdSigned, "KRCPF");
            }
            catch(Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }

            return PartialView(objCMList);
        }


        public PartialViewResult ViewDashboard(string UserIdSigned)
        {
            UserIdSigned = System.Web.HttpContext.Current.Session["UserId"].ToString();
            
            try
            {
                Client_Master objCM = new Client_Master();
                gConnection.Open();
                string sqlstr = "select bo_long_name, bo_client_code  from client_master cm, client_login cl ";
                sqlstr = sqlstr + " where cm.client_master_code=cl.FK_client_master_code and cl.username=@username";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@username", UserIdSigned);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objCM.bo_long_name = sdr["bo_long_name"].ToString();
                    objCM.bo_client_code = sdr["bo_client_code"].ToString();                    
                }
                gConnection.Close();
                @ViewBag.ClientCodeD = objCM.bo_client_code;
                @ViewBag.ClientNameD = objCM.bo_long_name;
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }

            return PartialView();
        }

        public PartialViewResult ViewDashboard_Demat(string UserIdSigned)
        {
            PagedClientHold objPGClHd = new PagedClientHold();
            List<ClientHold> objCHList = new List<ClientHold>();
            UserIdSigned = System.Web.HttpContext.Current.Session["UserId"].ToString();

            try
            {
                gConnection.Open();
                string sqlstr = "Select AutoId,Client_Code,Branch_Code,Symbol,ISIN_Code,Quantity,BOD_Value,HoldingValue, bo_long_name ";
                sqlstr = sqlstr + " from ClientHold CH, Client_Master CM, Client_Login CL where CH.client_Code = CM.bo_client_code ";
                sqlstr = sqlstr + " and CL.FK_client_master_code=CM.client_master_code and username=@Client_Code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@Client_Code", UserIdSigned);
                SqlDataReader sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    ClientHold objCH = new ClientHold();
                    objCH.AutoId = Convert.ToInt32(sdr["AutoId"].ToString());
                    objCH.Client_Code = sdr["Client_Code"].ToString();
                    objCH.Branch_Code = sdr["Branch_Code"].ToString();
                    objCH.Symbol = sdr["Symbol"].ToString();
                    objCH.ISIN_Code = sdr["ISIN_Code"].ToString();
                    objCH.Quantity = sdr["Quantity"].ToString();
                    objCH.BOD_Value = sdr["BOD_Value"].ToString();
                    objCH.HoldingValue = sdr["HoldingValue"].ToString();
                    objPGClHd.Client_Name = sdr["bo_long_name"].ToString();
                    objPGClHd.Client_Code = sdr["Client_Code"].ToString();
                    objPGClHd.HoldingValue = sdr["HoldingValue"].ToString();
                    objCHList.Add(objCH);
                }
                gConnection.Close();
                objPGClHd.ClientHold = objCHList;
                @ViewBag.HoldingValue = objPGClHd.HoldingValue;
                
            }
            catch(Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return PartialView(objPGClHd);
        }


        public PartialViewResult ViewDashboard_Ledger(string UserIdSigned)
        {
            ClientDashLedger objDashLed = new ClientDashLedger();
            UserIdSigned = System.Web.HttpContext.Current.Session["UserId"].ToString();

            try
            {
                gConnection.Open();
                string sqlstr = "Select AutoId,Client_Code,LEDGER_BALANCE,bo_long_name from ClientDashLedger CH, ";
                sqlstr = sqlstr + " Client_Master CM, Client_Login CL where CH.client_Code = CM.bo_client_code ";
                sqlstr = sqlstr + " and CL.FK_client_master_code=CM.client_master_code and username=@Client_Code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@Client_Code", UserIdSigned);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objDashLed.AutoId = Convert.ToInt32(sdr["AutoId"].ToString());
                    objDashLed.CLIENT_CODE = sdr["Client_Code"].ToString();
                    objDashLed.LEDGER_BALANCE = sdr["LEDGER_BALANCE"].ToString();
                    //objDashLed.bo_long_name = sdr["bo_long_name"].ToString();
                }
                gConnection.Close();
                @ViewBag.LedBal = objDashLed.LEDGER_BALANCE;
                @ViewBag.ClientC = objDashLed.CLIENT_CODE;
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return PartialView(objDashLed);
        }

        

        public static string Encryption_Cmots(string strToEncrypt, string strKey)
        {
            try
            {
                TripleDESCryptoServiceProvider objDESCrypto = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();

                byte[] byteHash, byteBuff;
                string strTempKey = strKey;

                byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                objHashMD5 = null;
                objDESCrypto.Key = byteHash;
                objDESCrypto.Mode = CipherMode.ECB; //CBC, CFB

                byteBuff = ASCIIEncoding.ASCII.GetBytes(strToEncrypt);
                return Convert.ToBase64String(objDESCrypto.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
            }
            catch (Exception ex)
            {
                return "Wrong Input. " + ex.Message;
            }
        }
 



        private bool UpdateLastLogin(string id)
        {
            bool result = false;

            try
            {
                gConnection.Open();

                string sqlstr = "update client_login set last_access_date= GETDATE() where username=@username";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@username", id);
                cmd.ExecuteNonQuery();
                gConnection.Close();
                result = true;
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }

            return result;
        }
    }
}