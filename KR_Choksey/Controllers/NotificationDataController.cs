﻿using KR_Choksey.Global;
using KR_Choksey.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KR_Choksey.Controllers
{
    public class NotificationDataController : Controller
    {
        // GET: NotificationData
        public JsonResult NotifData()
        {
            Image icon1 = Image.FromFile(ControllerContext.HttpContext.Server.MapPath("~/Content/images/KRC_white_Fav-Icon_72-X-72_PNG_18-01-2017.png"));
            //icon = "https://www.XXXXXXX.XXX/Content/images/kr-choksey-logo.png",
            NewsClass objNews = new NewsClass();
            objNews = GetNotificationData();
            if (objNews.section_name == "Economy - Reports")
            {
                string _id = Encryption.encrypt(objNews.id.ToString());
                var data = new
                {
                    title = "KRChoksey",
                    body = objNews.text,
                    //icon = "https://www.XXXXXXX.XXX/Content/images/Logo_72X72_18012017.png",
                    //icon = "https://www.XXXXXXX.XXX"+"/Content/images/Logo_72X72_18012017.png",
                    icon = icon1,
                    click_action = "https://www.XXXXXXX.XXX/NotificationDetails/EconomyNews?_id=" + _id
                };
                return Json(data);
            }
            else if (objNews.section_name == "Hot Pursuit")
            {
                string _id = Encryption.encrypt(objNews.id.ToString());
                var data = new
                {
                    title = "KRChoksey",
                    body = objNews.text,
                    //icon = "https://www.XXXXXXX.XXX/Content/images/Logo_72X72_18012017.png",
                    //icon = "https://www.XXXXXXX.XXX" + "/Content/images/Logo_72X72_18012017.png",
                    icon = icon1,
                    click_action = "https://www.XXXXXXX.XXX/NotificationDetails/HotPursuit?_id=" + _id
                };
                return Json(data);
            }
            else if (objNews.section_name == "Market Commentary - Stock Alert")
            {
                string _id = Encryption.encrypt(objNews.id.ToString());
                var data = new
                {
                    title = "KRChoksey",
                    body = objNews.text,
                    //icon = "https://www.XXXXXXX.XXX/Content/images/Logo_72X72_18012017.png",
                    //icon = "https://www.XXXXXXX.XXX" + "/Content/images/Logo_72X72_18012017.png",
                    icon = icon1,
                    click_action = "https://www.XXXXXXX.XXX/NotificationDetails/StockAlertNews?_id=" + _id
                };
                return Json(data);
            }
            else if (objNews.section_name == "Corporate News")
            {
                string _id = Encryption.encrypt(objNews.id.ToString());
                var data = new
                {
                    title = "KRChoksey",
                    body = objNews.text,
                    //icon = "https://www.XXXXXXX.XXX/Content/images/Logo_72X72_18012017.png",
                    //icon = "https://www.XXXXXXX.XXX" + "/Content/images/Logo_72X72_18012017.png",
                    //icon = ControllerContext.HttpContext.Server.MapPath("~/Content/images/Logo_72X72_18012017.png"),
                    icon = icon1,
                    click_action = "https://www.XXXXXXX.XXX/NotificationDetails/CorporateNews?_id=" + _id
                };
                return Json(data);
            }
            else
            {
                
                string _id = Encryption.encrypt(objNews.id.ToString());
                var data = new
                {
                    title = "KRChoksey",
                    body = "New Updates From KRChoksey",
                    //icon = "https://www.XXXXXXX.XXX/Content/images/FAVICON_16X16px_18012017.jpg",
                    //icon = "https://www.XXXXXXX.XXX" + "/Content/images/Logo_72X72_18012017.png",
                    click_action = "https://www.XXXXXXX.XXX"
                };
                return Json(data);
           
            }
            
            
            return Json("");
        }


        public NewsClass GetNotificationData()
        {
            NewsClass objNews = new NewsClass();
            try
            {
                gConnection.Open();
                string sqlstr = "select Isnull(sno,'') as sno, Isnull(heading,'') as heading, ";
                sqlstr = sqlstr + " Isnull(section_name,'') as section_name from NotificationData_Current";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objNews.id = Convert.ToInt32(sdr["sno"].ToString());
                    objNews.text = sdr["heading"].ToString();
                    objNews.section_name = sdr["section_name"].ToString();
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }

            return objNews;
        }




        public NewsClass HotPursuit()
        {
            NewsClass objNews = new NewsClass();
            try
            {
                gConnection.Open();
                string sqlstr = "select sno, heading from CmotsHotPursuit_stockAction where ";
                //sqlstr = sqlstr + " Cast([date] as date) = Cast(Getdate() as date) And ISNULL(notification,'')!= @notification";
                sqlstr = sqlstr + " (date + time) > DATEADD(HOUR, -1, GETDATE()) And ISNULL(notification,'')!= @notification";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@notification", true);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objNews.id = Convert.ToInt32(sdr["sno"].ToString());
                    objNews.text = sdr["heading"].ToString();
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return objNews;
        }


        public NewsClass CorporateNews()
        {
            NewsClass objNews = new NewsClass();
            try
            {
                gConnection.Open();
                string sqlstr = "select sno, heading from CmotsCorporateNews where ";
                sqlstr = sqlstr + " Cast([date] as date) = Cast(Getdate() as date) And ISNULL(notification,'')!= @notification";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@notification", true);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objNews.id = Convert.ToInt32(sdr["sno"].ToString());
                    objNews.text = sdr["heading"].ToString();
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return objNews;
        }


        public NewsClass StockAlert()
        {
            NewsClass objNews = new NewsClass();
            try
            {
                gConnection.Open();
                string sqlstr = "select sno, heading from CmotsStockAlert where ";
                sqlstr = sqlstr + " Cast([date] as date) = Cast(Getdate() as date) And ISNULL(notification,'')!= @notification";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@notification", true);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objNews.id = Convert.ToInt32(sdr["sno"].ToString());
                    objNews.text = sdr["heading"].ToString();
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return objNews;
        }


        public NewsClass EconomyNews()
        {
            NewsClass objNews = new NewsClass();
            try
            {
                gConnection.Open();
                string sqlstr = "select sno, heading from CmotsEconomyNews where ";
                sqlstr = sqlstr + " Cast([date] as date) = Cast(Getdate() as date) And ISNULL(notification,'')!= @notification";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@notification", true);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objNews.id = Convert.ToInt32(sdr["sno"].ToString());
                    objNews.text = sdr["heading"].ToString();
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return objNews;
        }


    }
}