﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Global;
using KR_Choksey.Models;
using KR_Choksey.Global;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text;
using System.Data.SqlClient;
using KR_Choksey.Areas.Admin.Models;
using Newtonsoft.Json;

namespace KR_Choksey.Controllers
{

    public class HomeController : Controller
    {
        public string deviceId = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";


        DropDownSelection objDrop = new DropDownSelection();
        GlobalFunction objGFunc = new GlobalFunction();



        public static string QName = "";
        string QEmail = "";
        string QPhone = "";

        [CustomActionFilter]
        public ActionResult Index()
        {
            //string devId = Hidden1.Value;
            //PushNotification();
            if (System.Web.HttpContext.Current.Session["NotifPopUp"] == null)
            {
                System.Web.HttpContext.Current.Session["NotifPopUp"] = "Yes";
            }
            if (System.Web.HttpContext.Current.Session["QuickContPopUp"] == null)
            {
                System.Web.HttpContext.Current.Session["QuickContPopUp"] = "Yes";
            }
            //@System.Web.HttpContext.Current.Session["QuickContUrl"] = "";
            List<ResearchRptUpload> objRptList = new List<ResearchRptUpload>();
            objRptList = GetLatest3Report();
            System.Web.HttpContext.Current.Session["ReportUrlIndex"] = Request.Url.AbsoluteUri.ToString();
            return View(objRptList);
        }


        private List<ResearchRptUpload> GetLatest3Report()
        {
            List<ResearchRptUpload> objRptList = new List<ResearchRptUpload>();
            try
            {
                gConnection.Open();
                string sqlstr = "select Top 3 ReportName, ReportPath, CreatedOn, OriginalRptName from dbo.ResearchRptUpload Order By CreatedOn Desc";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    ResearchRptUpload objRpt = new ResearchRptUpload();
                    objRpt.ReportName = sdr["ReportName"].ToString();
                    objRpt.ReportPath = sdr["ReportPath"].ToString();
                    objRpt.OriginalRptName = sdr["OriginalRptName"].ToString();
                    objRpt.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());

                    objRpt.ReportName = objRpt.ReportName + "( " + Convert.ToDateTime(sdr["CreatedOn"].ToString()) + " )";

                    objRptList.Add(objRpt);
                }
                gConnection.Close();
            }
            catch(Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }

            return objRptList;
        }


        [CustomActionFilter]
        public ActionResult Notification()
        {            
            return View();
        }




        [CustomActionFilter]
        public ActionResult Network_Partner()
        {
            return View();
        }

        [CustomActionFilter]
        public ActionResult Partners()
        {
            return View();
        }

        [CustomActionFilter]
        public ActionResult Network_Services()
        {
            return View();
        }

        [CustomActionFilter]
        public ActionResult Network_Trust_Area()
        {
            return View();
        }

        [CustomActionFilter]
        public ActionResult About_Us()
        {
            return View();
        }

        [CustomActionFilter]
        public ActionResult Downloads()
        {
            List<IndicativeMarginReport> objIMRList = new List<IndicativeMarginReport>();
            try
            {
                gConnection.Open();
                string sqlstr = "select Top 10 Isnull(ReportName,'') as ReportName from IndicativeMarginReport order by createdOn Desc";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    IndicativeMarginReport objIMR = new IndicativeMarginReport();
                    objIMR.ReportName = sdr["ReportName"].ToString();
                    objIMRList.Add(objIMR);
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return View(objIMRList);
        }

        [CustomActionFilter]
        public ActionResult Contact_Us()
        {
            return View();
        }

        [CustomActionFilter]
        public ActionResult Disclaimer()
        {
            return View();
        }
        [CustomActionFilter]
        public ActionResult Terms_of_Use()
        {
            return View();
        }

        [CustomActionFilter]
        public ActionResult Privacy_Statement()
        {
            return View();
        }

        [CustomActionFilter]
        public ActionResult Network_Partner_Enquiry()
        {
            ViewData["State1"] = objDrop.GetAllStateList("");
            ViewData["Occupation1"] = objDrop.OccupationList();
            ViewData["Qualification1"] = objDrop.QualificationList();
            ViewData["City1"] = objDrop.GetAllCityList();
            return View();
        }

        [HttpPost]
        [CustomActionFilter]
        public ActionResult Network_Partner_Enquiry(NetworkPartnerEnquiry objNPE)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string FromMail = ConfigurationManager.AppSettings["smtpDisplayId"];
                    string ToMail = ConfigurationManager.AppSettings["networkpartnersendingmail"];
                    string Subject = "Network Partner Enquiry";

                    string UserName = ConfigurationManager.AppSettings["smtpUser"];
                    string Password = ConfigurationManager.AppSettings["smtpPass"];
                    string Link = ConfigurationManager.AppSettings["smtpLink"];
                    //string Param = "?_UserName=" + Global.Encryption.encrypt(objCliMa.bo_email) + "&_ClientMasterCode=" + Global.Encryption.encrypt(objCliMa.client_master_code.ToString());
                    //Param = Global.Encryption.encrypt(Link);

                    //Link = Link + Param;
                    string Body = "Name: " + objNPE.Name;
                    Body = Body + Environment.NewLine;
                    Body = Body + "Date Of Birth:" + objNPE.DOB;
                    Body = Body + Environment.NewLine;
                    Body = Body + "Qualification: " + objNPE.Qualification;
                    Body = Body + Environment.NewLine;
                    Body = Body + "Mobile Number: " + objNPE.MobileNumber;
                    Body = Body + Environment.NewLine;
                    Body = Body + "Telephone Number: " + objNPE.TelephoneNumber;
                    Body = Body + Environment.NewLine;
                    Body = Body + "Email Address: " + objNPE.EmailAddress;
                    Body = Body + Environment.NewLine;
                    Body = Body + "Address: " + objNPE.Address;
                    Body = Body + Environment.NewLine;
                    Body = Body + "City: " + objNPE.City;

                    Body = Body + Environment.NewLine;
                    Body = Body + "State: " + objNPE.State;
                    Body = Body + Environment.NewLine;
                    Body = Body + "Pincode: " + objNPE.Pincode;
                    Body = Body + Environment.NewLine;
                    Body = Body + "How Do You Know About Us: " + objNPE.HowYouKnowAboutUs;
                    Body = Body + Environment.NewLine;
                    Body = Body + "Expectations From KRC: " + objNPE.ExpectationsFromKRC;
                    Body = Body + Environment.NewLine;
                    Body = Body + "Experience In Capital Market: " + objNPE.ExperienceInCapitalMarket;
                    Body = Body + Environment.NewLine;
                    Body = Body + "Equity: " + objNPE.Equity;
                    Body = Body + Environment.NewLine;
                    Body = Body + "MF: " + objNPE.MF;
                    Body = Body + Environment.NewLine;
                    Body = Body + "Derivatives: " + objNPE.Derivatives;
                    Body = Body + Environment.NewLine;
                    Body = Body + "Insurance: " + objNPE.Insurance;
                    Body = Body + Environment.NewLine;
                    Body = Body + "IPO: " + objNPE.IPO;
                    Body = Body + Environment.NewLine;
                    Body = Body + "Brokerage House Associated With: " + objNPE.BrokHouseAssWith;
                    Body = Body + Environment.NewLine;
                    Body = Body + "Trading: " + objNPE.Trading;
                    Body = Body + Environment.NewLine;
                    Body = Body + "Delivery: " + objNPE.Delivery;
                    Body = Body + Environment.NewLine;
                    Body = Body + "VSAT: " + objNPE.VSAT;
                    Body = Body + Environment.NewLine;
                    Body = Body + "LeaseLine: " + objNPE.LeaseLine;
                    Body = Body + Environment.NewLine;
                    Body = Body + "Others: " + objNPE.Others;
                    Body = Body + Environment.NewLine;

                    string AttachmentPath = "";
                    bool resu = objGFunc.SendMail(FromMail, ToMail, Subject, Body, UserName, Password, AttachmentPath);

                    if (resu == true)
                    {
                        ModelState.Clear();
                        @ViewBag.Message = "Thanks! we will contact you.";
                        @ViewBag.HideClass = "alert alert-success";
                    }
                    else
                    {
                        @ViewBag.Message = "Sorry!";
                        @ViewBag.HideClass = "alert alert-danger";
                    }

                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            ViewData["State1"] = objDrop.GetAllStateList("");
            ViewData["Occupation1"] = objDrop.OccupationList();
            ViewData["Qualification1"] = objDrop.QualificationList();
            ViewData["City1"] = objDrop.GetAllCityList();
            return View();
        }

        [CustomActionFilter]
        public ActionResult Careers()
        {
            return View();
        }

        [CustomActionFilter]
        public ActionResult Locate_Us()
        {
            ViewData["State"] = StateList();
            return View();
        }

        public List<SelectListItem> StateList()
        {
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            var cities = new List<string>();
            try
            {
                gConnection.Open();
                string sqlstr = "select state from tbl_location group by state";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objSelLst.Add(new SelectListItem
                    {
                        Text = sdr["state"].ToString(),
                        Value = sdr["state"].ToString()
                    });
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return objSelLst;
        }

        public JsonResult GetCities(string state)
        {
            gConnection.Open();
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            string sqlstr = "select city from tbl_location WHERE state='" + state + "' group by city";
            SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
            SqlDataReader sdr = cmd.ExecuteReader();
            while (sdr.Read())
            {
                objSelLst.Add(new SelectListItem
                {
                    Text = sdr["city"].ToString(),
                    Value = sdr["city"].ToString()
                });
            }


            gConnection.Close();
            //cities.Add(objSelLst);

            //Add JsonRequest behavior to allow retrieving states over http get  
            return Json(objSelLst);
        }



        public ActionResult Details()
        {
            return PartialView();
        }

        [HttpPost]
        public PartialViewResult Details(LocateUs objLocateUs, string state, string city)
        {
            gConnection.Open();
            List<LocateUs> LocateUs_List = new List<LocateUs>();
            string sqlstr = "select * from tbl_location WHERE state='" + state + "' and city='" + city + "' ";
            SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
            SqlDataReader sdr = cmd.ExecuteReader();
            while (sdr.Read())
            {
                LocateUs_List.Add(new LocateUs
                {
                    State = sdr["state"].ToString(),
                    City = sdr["city"].ToString(),
                    Contact_Person = sdr["name"].ToString(),
                    Address = sdr["address"].ToString(),
                    PhoneNo = sdr["phone_no"].ToString(),
                    MobileNo = sdr["mobile"].ToString(),
                    Email = sdr["email"].ToString()
                });
            }
            ViewBag.State = state;
            ViewBag.City = city;
            return PartialView(LocateUs_List);
        }

        [CustomActionFilter]
        public ActionResult Individual_Dashboard()
        {
            return View();
        }


        public ActionResult QuickContact()
        {
            System.Web.HttpContext.Current.Session["QuickContPopUp"] = "No";
            System.Web.HttpContext.Current.Session["QuickContPopUp"] = null;
            return PartialView();
            //return View();
        }


        

        [HttpPost]
        //public void QuickContact(KrcQuickContacts objContact)
        public string QuickContact(string Name, string EmailId, string PhoneNumber, string QuickTopic, string PMutualFund,string PInsurance, string PFinancialPlanning, string PIFund, string PIPms, string PIBrokerage, string PILend, string PIHold, string PSubBrokerage)
        {
            string result = "";
            if (System.Web.HttpContext.Current.Session["QuickContPopUp"] != null)
            {
                result = "Success";
                Session["QuickContPopUp"] = null;
            }
            else
            {
                System.Web.HttpContext.Current.Session["QuickContPopUp"] = Name;
                KrcQuickContacts objContact = new KrcQuickContacts();
                string ReturnUrl = Convert.ToString(Session["ReturnUrl"]);

                string EMMutualFund = "";
                string EMInsurance = "";
                string EMFinancialPlanning = "";
                string EMIFund = "";
                string EMIPms = "";
                string EMIBrokerage = "";
                string EMILend = "";
                string EMIHold = "";
                string EMSubBrokerage = "";

                try
                {
                    string Products = "";
                    // Mutual Fund
                    if (PMutualFund == null)
                    {
                        PMutualFund = "";
                    }
                    if (PMutualFund != "")
                    {
                        if (PMutualFund != "false")
                        {
                            Products = Products + PMutualFund;
                            EMMutualFund = "XXXX@XXXX.com";
                        }                        
                    }

                    // Insurance
                    if (PInsurance == null)
                    {
                        PInsurance = "";
                    }
                    if (PInsurance != "")
                    {
                        if (PInsurance != "false")
                        {
                            Products = Products + PInsurance;
                            EMInsurance = "XXXXXXX@XXXX.com";
                        }
                    }

                    // Financial Planning
                    if (PFinancialPlanning == null)
                    {
                        PFinancialPlanning = "";
                    }
                    if (PFinancialPlanning != "")
                    {
                        if (PFinancialPlanning != "false")
                        {
                            Products = Products + PFinancialPlanning;
                            EMFinancialPlanning = "XX@XXXX.com";
                        }
                    }


                    // I Fund
                    if (PIFund == null)
                    {
                        PIFund = "";
                    }
                    if (PIFund != "")
                    {
                        if (PIFund != "false")
                        {
                            Products = Products + PIFund;
                            EMIFund = "XXX@XXXXX.com";
                        }
                    }


                    // I Pms
                    if (PIPms == null)
                    {
                        PIPms = "";
                    }
                    if (PIPms != "")
                    {
                        if (PIPms != "false")
                        {
                            Products = Products + PIPms;
                            EMIPms = "XXXX@XXXXX.com";
                        }
                    }


                    // I Brokerage
                    if (PIBrokerage == null)
                    {
                        PIBrokerage = "";
                    }
                    if (PIBrokerage != "")
                    {
                        if (PIBrokerage != "false")
                        {
                            Products = Products + PIBrokerage;
                            EMIBrokerage = "XXX.XX@XXXXX.com";
                        }
                    }


                    // I Lend
                    if (PILend == null)
                    {
                        PILend = "";
                    }
                    if (PILend != "")
                    {
                        if (PILend != "false")
                        {
                            Products = Products + PILend;
                            EMILend = "XXXX@XXXX.com";
                        }
                    }


                    // I Hold
                    if (PIHold == null)
                    {
                        PIHold = "";
                    }
                    if (PIHold != "")
                    {
                        if (PIHold != "false")
                        {
                            Products = Products + PIHold;
                            EMIHold = "XXX@XXXXX.com";
                        }
                    }


                    // Sub Brokerage
                    if (PSubBrokerage == null)
                    {
                        PSubBrokerage = "";
                    }
                    if (PSubBrokerage != "")
                    {
                        if (PSubBrokerage != "false")
                        {
                            Products = Products + PSubBrokerage;
                            EMSubBrokerage = "XXXX@XXXXX.com";
                        }
                    }



                    objContact.Name = Name;
                    objContact.EmailId = EmailId;
                    objContact.PhoneNumber = PhoneNumber;
                    objContact.QuickTopic = QuickTopic;

                    if (ModelState.IsValid)
                    {
                        bool res = true;
                        foreach (char c in objContact.PhoneNumber)
                        {
                            if (c < '0' || c > '9')
                            {
                                res = false;
                            }
                        }

                        if (res == false || (objContact.PhoneNumber.ToString().Length != 10))
                        {
                            @ViewBag.HideClass = "alert alert-danger";
                            @ViewBag.Message = "Please Enter Valid Mobile Number.";
                            result = "Please Enter Valid Mobile Number.";
                        }
                        else
                        {
                            try
                            {
                                // Insertion Code For QuickContact Table //
                                gConnection.Open();
                                string sqlstr = "insert into KrcQuickContacts(Name,EmailId,PhoneNumber,QuickTopic,Screen,CreatedBy,CreatedOn,Products)";
                                sqlstr = sqlstr + " values (@QuickContactName,@QuickContactEmail,@QuickContactPhone,@QuickContactMessage,@Screen,@CreatedBy,GetDate(),@Products)";
                                //cmd.CommandText = sqlstr;
                                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@QuickContactName", objContact.Name);
                                cmd.Parameters.AddWithValue("@QuickContactEmail", objContact.EmailId);
                                cmd.Parameters.AddWithValue("@QuickContactPhone", objContact.PhoneNumber);
                                cmd.Parameters.AddWithValue("@QuickContactMessage", objContact.QuickTopic);
                                cmd.Parameters.AddWithValue("@Screen", ReturnUrl);
                                cmd.Parameters.AddWithValue("@CreatedBy", "Online");
                                cmd.Parameters.AddWithValue("@Products", Products);
                                cmd.ExecuteNonQuery();

                                gConnection.Close();


                                //Mail for Quick contact
                                string FromMail = ConfigurationManager.AppSettings["smtpDisplayId"];
                                string ToMail = "service@XXXXXXX.XXX";
                                //string ToMail = "souravkuila65@gmail.com";
                                //string Subject = "New contact request from " + objContact.Name;
                                string Subject = "Enquiry received from DIMAG promotions";
                                string Body = "Hi, Team KRChoksey";
                                Body = Body + Environment.NewLine;

                                string UserName = ConfigurationManager.AppSettings["smtpUser"];
                                string Password = ConfigurationManager.AppSettings["smtpPass"];

                                //Body = Body + "New contact request:";
                                Body = Body + Environment.NewLine;
                                Body = Body + Environment.NewLine;
                                Body = Body + "Name: " + objContact.Name;
                                Body = Body + Environment.NewLine;
                                Body = Body + "Phone: " + objContact.PhoneNumber;
                                Body = Body + Environment.NewLine;
                                Body = Body + "Email: " + objContact.EmailId;
                                Body = Body + Environment.NewLine;
                                Body = Body + "Message: " + objContact.QuickTopic;
                                Body = Body + Environment.NewLine;
                                if (Products == "")
                                {
                                    Body = Body + "Products: Not Selected";
                                }
                                else
                                {
                                    Body = Body + "Products: " + Products;
                                }

                                Body = Body + Environment.NewLine;
                                Body = Body + Environment.NewLine;
                                Body = Body + "Thanks";
                                Body = Body + Environment.NewLine;
                                Body = Body + objContact.Name;
                                string AttachmentPath = "";
                                //bool resu = objGFunc.SendMail(FromMail, ToMail, Subject, Body, UserName, Password, AttachmentPath);
                                bool resu = objGFunc.SendMailForQuickContact(FromMail, ToMail, Subject, Body, UserName, Password, AttachmentPath, EMMutualFund, EMInsurance, EMFinancialPlanning, EMIFund, EMIPms, EMIBrokerage, EMILend, EMIHold, EMSubBrokerage);
                                if(resu == true)
                                {
                                    Subject = "Request Received: Enquiry";
                                    Body = "Dear " + objContact.Name + ",";
                                    Body += Environment.NewLine;
                                    Body += Environment.NewLine;
                                    Body += "Thank you for contacting us. Our executive will get back to you within 24 hours.";
                                    Body += Environment.NewLine;
                                    Body += "You can also reach us at service@XXXXXXX.XXX or call us on 022-66965513/5";
                                    Body += Environment.NewLine;
                                    Body += Environment.NewLine;
                                    Body += Environment.NewLine;
                                    Body += "Thanks";
                                    Body += Environment.NewLine;
                                    Body += "Customer Support Team";
                                    bool ack = objGFunc.SendAcknowledgementMail(FromMail, objContact.EmailId, Subject, Body, UserName, Password, AttachmentPath);
                                    if (ack == true)
                                    {
                                        objGFunc.InsertAcknowledgementMailLog(FromMail, objContact.EmailId, "Success", "PopUp");
                                    }
                                    else
                                    {
                                        objGFunc.InsertAcknowledgementMailLog(FromMail, objContact.EmailId, "Not Success", "PopUp");
                                    }
                                }
                                ModelState.Clear();
                                @ViewBag.HideClass = "alert alert-success";
                                @ViewBag.Message = "Thanks for contacting us! Our team shall get back to you very soon.";
                                result = "Success";


                                //System.Web.HttpContext.Current.Session["QuickContUrl"] = Request.Url.AbsoluteUri;
                            }
                            catch (Exception ex)
                            {
                                //transaction.Rollback();
                                gConnection.Close();
                                Global.ErrorHandlerClass.LogError(ex);
                                @ViewBag.HideClass = "alert alert-danger";
                                @ViewBag.Message = "Error!";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Global.ErrorHandlerClass.LogError(ex);
                }
            }
            //return Redirect(ReturnUrl);
            //return PartialView();
            return result;
        }

        

        #region JSONAJAX
        public JsonResult SetBrowserID(string BrowserID)
        {
            if (BrowserID != "")
            {
                //Add cookie
                string AllowCookie = "Yes";
                HttpCookie cookie2 = new HttpCookie("AllowCookie");
                cookie2.Value = AllowCookie;
                cookie2.Expires.AddDays(365);
                HttpContext.Response.Cookies.Add(cookie2);
                // store BrowserId
                string UserId = string.Empty;
                if (System.Web.HttpContext.Current.Session["UserId"] != null)
                {
                    UserId = System.Web.HttpContext.Current.Session["UserId"].ToString();
                    try
                    {
                        gConnection.Open();
                        string sqlstr = "Update Client_Master SET google_id=@GoogleId where bo_email=@UserId";
                        SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                        cmd.Parameters.AddWithValue("@GoogleId", BrowserID);
                        cmd.Parameters.AddWithValue("@UserId", UserId);
                        cmd.ExecuteNonQuery();
                        gConnection.Close();
                    }
                    catch (Exception ex)
                    {
                        Global.ErrorHandlerClass.LogError(ex);
                    }
                }
                else
                {
                    HttpCookie cookie = HttpContext.Request.Cookies.Get("UnSignedUserId");
                    UserId = cookie.Value.ToString();
                    try
                    {
                        gConnection.Open();
                        string sqlstr = "Update UnSignedUserDetails SET GoogleId=@GoogleId where UserId=@UserId";
                        SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                        cmd.Parameters.AddWithValue("@GoogleId", BrowserID);
                        cmd.Parameters.AddWithValue("@UserId", UserId);
                        cmd.ExecuteNonQuery();
                        gConnection.Close();
                    }
                    catch (Exception ex)
                    {
                        Global.ErrorHandlerClass.LogError(ex);
                    }
                }
            }

            return Json("");
        }

        public JsonResult SetNoNotification()
        {
            System.Web.HttpContext.Current.Session.Remove("NotifPopUp");

            System.Web.HttpContext.Current.Session["NotifPopUp"] = "No";
            return Json("");
        }
        #endregion


        public void PushNotification()
        {
            var applicationID = "XXXXXXXXXXXXXXXXXXXX";


            var SENDER_ID = "XXXXXXXXXXXXXXXXXXXXXXX";
            var value = "Hello";
            WebRequest tRequest;
            tRequest = WebRequest.Create("https://android.googleapis.com/gcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

            tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));

            string postData = "collapse_key=score_update&time_to_live=108&delay_while_idle=1&data.message=" + value + "&registration_id=" + deviceId + "";
            Console.WriteLine(postData);
            Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            tRequest.ContentLength = byteArray.Length;

            Stream dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse tResponse = tRequest.GetResponse();

            dataStream = tResponse.GetResponseStream();

            StreamReader tReader = new StreamReader(dataStream);

            String sResponseFromServer = tReader.ReadToEnd();

            //Label1.Text = sResponseFromServer;
            tReader.Close();
            dataStream.Close();
            tResponse.Close();
        }
        public ActionResult SiteMap()
        {
            return View();
        }

        public ActionResult Calculators()
        {
            return View();
        }

        #region EMI Calculators

        public PartialViewResult EMI_Calculator()
        {
            return PartialView();
        }

        [HttpPost]
        public JsonResult EMI_Calculator(string p, string r, string n)
        {
            
            try
            {
                if(p == "")
                {
                    var data = new
                    {
                        emi = 0,
                        ipay = 0,
                        totpay = 0
                    };
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else if(r == "")
                {
                    var data = new
                    {
                        emi = 0,
                        ipay = 0,
                        totpay = 0
                    };
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else if (n == "")
                {
                    var data = new
                    {
                        emi = 0,
                        ipay = 0,
                        totpay = 0
                    };
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    double P = Convert.ToDouble(p);
                    double R = Convert.ToDouble(r);
                    double N = Convert.ToDouble(n);
                    ////decimal E = P * R * Math.Pow(1 + R, N) / (Math.Pow(1 + R, N) - 1);
                    //double rr = (R / 100);
                    //double t = (Math.Pow(1 + rr, N)) - 1;
                    //double t1 = (Math.Pow(1 + rr, N));
                    //double p1 = P * rr;
                    //double t3 = t1 / t;
                    //double E = p1 * t3;
                    //E = Math.Round(E,2);
                    //Console.WriteLine("EMI is {0:F2}\n", E);
                    //double ipay=(E*N)-P;
                    //double totpay = ipay + P;


                    double InterestRate = R;
                    double PaymentPeriods = N;
                    double LoanAmount = P;
                    if (InterestRate > 1)
                    {
                        InterestRate = InterestRate / 100;
                    }
                    double Payment = (LoanAmount * Math.Pow((InterestRate / 12) + 1,
                              (PaymentPeriods)) * InterestRate / 12) / (Math.Pow
                              (InterestRate / 12 + 1, (PaymentPeriods)) - 1);

                    Payment = Math.Round(Payment, 2);
                    double ipay = (Payment * PaymentPeriods) - LoanAmount;
                    ipay = Math.Round(ipay, 2);
                    double totpay = (Payment * PaymentPeriods);
                    totpay = Math.Round(totpay, 2);

                    var data = new
                    {
                        emi = Payment,
                        ipay = ipay,
                        totpay = totpay
                    };
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            //return PartialView();
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public PartialViewResult Web_Search()
        {
            return PartialView();
        }

        #region Search Result

        [CustomActionFilter]
        public ActionResult Search_Result(string Search_Text)
        {

            string result = "";
            if (Search_Text == null)
            {
                Search_Text = "";
            }
            Website_Search objSearch = new Website_Search();
            List<Website_Search> Website_Search_List = new List<Website_Search>();
            try
            {
                if (Search_Text != "")
                {
                    objSearch.SearchTest = Search_Text;
                    gConnection.Open();
                    //string sqlstr = "select * from krc_WebSearch WHERE Page_Content like '%" + Search_Text + "%' ";
                    string sqlstr = "select * from krc_WebSearch WHERE Page_Content like @SearchText ";
                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                    cmd.Parameters.AddWithValue("@SearchText", "%" + Search_Text + "%");
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        Website_Search_List.Add(new Website_Search
                        {
                            SearchTest = Search_Text,
                            Navigation = sdr["Navigation"].ToString(),
                            URL_Link = sdr["Page_Link"].ToString(),
                        });
                    }
                    gConnection.Close();
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            ViewBag.SearchText = Search_Text;
            return View(Website_Search_List);
        }


        [HttpPost]
        [CustomActionFilter]
        public ActionResult Search_Result(Website_Search objweb)
        {
            string Search_Text = objweb.SearchTest;
            if(Search_Text == null)
            {
                Search_Text = "";
            }
            string result = "";
            Website_Search objSearch = new Website_Search();
            List<Website_Search> Website_Search_List = new List<Website_Search>();
            try
            {
                if (Search_Text != "")
                {
                    objSearch.SearchTest = Search_Text;
                    gConnection.Open();
                    string sqlstr = "select * from krc_WebSearch WHERE Page_Content like @SearchText ";
                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                    cmd.Parameters.AddWithValue("@SearchText","%" + Search_Text + "%");
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        Website_Search_List.Add(new Website_Search
                        {
                            SearchTest = Search_Text,
                            Navigation = sdr["Navigation"].ToString(),
                            URL_Link = sdr["Page_Link"].ToString(),
                        });
                    }
                    gConnection.Close();
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            ViewBag.SearchText = Search_Text;
            return View(Website_Search_List);
        }

        #endregion

        public ActionResult Policies()
        {
            return View();
        }

        public ActionResult Demonetisation()
        {
            return PartialView();
        }



        [HttpGet]
        public ActionResult EasyInvest()
        {
            OTPVerification objOtpVer = new OTPVerification();
            @ViewBag.Message = "EnterMobileNo";
            //return PartialView(objOtpVer);
            return View(objOtpVer);
        }


        [HttpPost]
        public ActionResult EasyInvest(FormCollection frm, OTPVerification objOtpVer)
        {
            bool result = false;
            string Submit = frm["Submit"].ToString();
            try
            {
                //string Mno = frm["mno"].ToString().Trim();
                if (Submit == "GET OTP")
                {
                    if (objOtpVer.MobileNo == null)
                    {
                        objOtpVer.MobileNo = "";
                    }
                    if (objOtpVer.MobileNo.Length == 10)
                    {
                        gConnection.Open();
                        string sqlstr = "select bo_mobile from dbo.Client_Master where bo_mobile=@bo_mobile";
                        SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                        cmd.Parameters.AddWithValue("@bo_mobile", objOtpVer.MobileNo);
                        SqlDataReader sdr = cmd.ExecuteReader();
                        while (sdr.Read())
                        {
                            result = true;
                        }
                        gConnection.Close();
                        if (result == true)
                        {
                            var client = new RestSharp.RestClient("http://2factor.in" + objOtpVer.MobileNo + "/AUTOGEN");  // This is Live KRC API
                            var request = new RestSharp.RestRequest(RestSharp.Method.GET);
                            request.AddParameter("undefined", "{}", RestSharp.ParameterType.RequestBody);
                            RestSharp.IRestResponse response = client.Execute(request);
                            string mm = response.Content;

                            Otp tmp = JsonConvert.DeserializeObject<Otp>(mm);

                            if (tmp.Status == "Success")
                            {
                                OTPVerification objOtpVer1 = new OTPVerification();
                                objOtpVer1.MobileNo = objOtpVer.MobileNo;
                                objOtpVer1.MobileNoResponse = tmp.Details;

                                @ViewBag.Message = "EnterOTP";
                                return View(objOtpVer1);
                            }
                            else
                            {
                                @ViewBag.Message = "Enter Registered Mobile No";
                                @ViewBag.HideClass = "alert alert-danger";
                            }
                        }
                        else
                        {
                            @ViewBag.ErrorMessage = "Enter Registered Mobile No";
                            @ViewBag.HideClass = "alert alert-danger";
                            OTPVerification objOtpVer1 = new OTPVerification();
                            @ViewBag.Message = "EnterMobileNo";
                            return View(objOtpVer1);
                        }
                        return PartialView("EasyInvestOtp", objOtpVer);
                       // return View("EasyInvestOtp", objOtpVer);

                        OTPVerification objOtpVerNew = new OTPVerification();
                        objOtpVerNew = objOtpVer;
                        objOtpVerNew.MobileNoResponse = "XXXXXXXX";


                        @ViewBag.Message = "EnterOTP";
                    }
                    else
                    {
                        @ViewBag.ErrorMessage = "Enter Valid Mobile No";
                        @ViewBag.HideClass = "alert alert-danger";

                        OTPVerification objOtpVer1 = new OTPVerification();
                        @ViewBag.Message = "EnterMobileNo";
                        //return PartialView(objOtpVer);
                        return View(objOtpVer1);
                    }
                }
                else if (Submit == "Submit")
                {
                    if (objOtpVer.OTP == null)
                    {
                        objOtpVer.OTP = "";
                    }
                    if (objOtpVer.OTP != "")
                    {
                        var client = new RestSharp.RestClient("http://2factor.in" + objOtpVer.MobileNoResponse + "/" + objOtpVer.OTP); // This isLive KRC API

                        var request = new RestSharp.RestRequest(RestSharp.Method.GET);
                        request.AddParameter("undefined", "{}", RestSharp.ParameterType.RequestBody);
                        RestSharp.IRestResponse response = client.Execute(request);
                        string mm = response.Content;
                        Otp tmp = JsonConvert.DeserializeObject<Otp>(mm);
                        if (tmp.Status == "Success")
                        {
                            if (tmp.Details == "OTP Matched")
                            {
                               // return PartialView("EasyInvestAgree", objOtpVer);
                                @ViewBag.Message = "Agree";
                                OTPVerification objOtpVer1 = new OTPVerification();
                                objOtpVer1.MobileNo = objOtpVer.MobileNo;
                                objOtpVer1.MobileNoResponse = objOtpVer.MobileNoResponse;
                                objOtpVer1.OTP = objOtpVer.OTP;
                                objOtpVer1.OTPResponse = objOtpVer.OTPResponse;
                                return View(objOtpVer1);
                            }
                        }
                        else
                        {
                            @ViewBag.ErrorMessage = "Wrong OTP";
                            @ViewBag.HideClass = "alert alert-danger";
                            OTPVerification objOtpVer1 = new OTPVerification();
                            objOtpVer1.MobileNo = objOtpVer.MobileNo;
                            objOtpVer1.MobileNoResponse = objOtpVer.MobileNoResponse;
                            return View(objOtpVer1);
                        }                        
                    }
                    else
                    {
                        @ViewBag.Message = "EnterOTP";
                        @ViewBag.ErrorMessage = "Enter Valid OTP";
                        @ViewBag.HideClass = "alert alert-danger";

                        OTPVerification objOtpVer1 = new OTPVerification();
                        objOtpVer1.MobileNo = objOtpVer.MobileNo;
                        //return PartialView(objOtpVer);
                        return View(objOtpVer1);
                    }
                }
                else if (Submit == "I agree to signup for the EASY INVEST services and the features offered.")
                {

                    return RedirectToAction("EasyInvestFinal", new { id = Encryption.encrypt(objOtpVer.MobileNo) });
                }
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            //return PartialView();
            return View();
        }

        #region Easy Invest Otp BAckup

        //public ActionResult EasyInvestOtp()
        //{
        //    //return PartialView(objOtpVer);
        //    return View(objOtpVerify);
        //}

        //[HttpPost]
        //public ActionResult EasyInvestOtp(FormCollection frm, OTPVerification objOtpVer)
        //{
        //    try
        //    {
        //        objOtpVer.MobileNo = frm["MobileNo"].ToString();
        //        objOtpVer.MobileNoResponse = frm["MobileNoResponse"].ToString();

        //        ////string dat = frm["Otp"].ToString();
        //        ////var client = new RestSharp.RestClient("http://2factor.in/API/V1/bdd9bcf5-cc16-11e6-afa5-00163ef91450/SMS/VERIFY/" + objOtpVer.MobileNoResponse + "/" + objOtpVer.OTP); // This is Manish Test API
        //        //var client = new RestSharp.RestClient("http://2factor.in/API/V1/a0db6c27-d196-11e6-afa5-00163ef91450/SMS/VERIFY/" + objOtpVer.MobileNoResponse + "/" + objOtpVer.OTP); // This isLive KRC API

        //        //var request = new RestSharp.RestRequest(RestSharp.Method.GET);
        //        //request.AddParameter("undefined", "{}", RestSharp.ParameterType.RequestBody);
        //        //RestSharp.IRestResponse response = client.Execute(request);
        //        //string mm = response.Content;
        //        //Otp tmp = JsonConvert.DeserializeObject<Otp>(mm);
        //        //if (tmp.Status == "Success")
        //        //{
        //        //    if (tmp.Details == "OTP Matched")
        //        //    {
        //        //        return PartialView("EasyInvestAgree", objOtpVer);
        //        //    }
        //        //}
        //        //else
        //        //{
        //        //    @ViewBag.Message = "Wrong OTP";
        //        //    @ViewBag.HideClass = "alert alert-danger";
        //        //}
        //        //return PartialView("EasyInvestAgree", objOtpVer);
        //        return RedirectToAction("EasyInvestAgree", objOtpVer);
        //    }
        //    catch(Exception ex)
        //    {
        //        ErrorHandlerClass.LogError(ex);
        //    }
        //   // return PartialView();
        //    return View();
        //}

        //[HttpGet]
        //public ActionResult EasyInvestAgree(OTPVerification objOtpVer)
        //{
        //    //objOtpVer.MobileNo = Encryption.decrypt(objOtpVer.MobileNo);
        //    //return PartialView(objOtpVer);
        //    return View(objOtpVer);
        //}

        //[HttpPost]
        //public ActionResult EasyInvestAgree(OTPVerification objOtpVer, FormCollection frm)
        //{
        //    //string path = Path.Combine(Server.MapPath("~/App_Data/"), @"wkhtmltopdf\bin\wkhtmltopdf.exe");
        //    ////validate the URL it should be complete URL Like http://www.codingsack.com
        //    //// var validURL = new Uri("https://www.XXXXXXX.XXX/Company/Company_Details?id=34");
        //    //var validURL = "http://localhost:11546/Home/GetData?_id=" + objOtpVer.MobileNo;
        //    ////PDF Bytes from PDF Converter
        //byte[] pdfFileBytes = new PDFConverter().Convert(validURL, path);
        //    ////Return Pdf To client
        //    //return File(pdfFileBytes, "application/pdf");
        //    ////return PartialView();
        //    ////return RedirectToAction("GetData", new { _id=objOtpVer.MobileNo});

        //    EasyInvest objEI = new Models.EasyInvest();
        //    objEI.Phone_No = objOtpVer.MobileNo;
        //    //return PartialView("EasyInvestFinal", objEI);
        //    //return View("EasyInvestFinal", objEI);
        //    return RedirectToAction("EasyInvestFinal", Encryption.encrypt(objEI.Phone_No));
        //}


        #endregion

        [HttpGet]
        public ActionResult EasyInvestFinal(string id)
        {

            EasyInvest objEI = new Models.EasyInvest();
            //string _id = objEI.Phone_No;
            string _id = Encryption.decrypt(id);

            try
            {
                gConnection.Open();
                string sqlstr = "Exec GetEasyInvestData @mobile_no=@MobileNo";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@MobileNo", _id);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objEI.Company_Name = sdr["Compay_Name"].ToString();
                    objEI.Account_No = sdr["Account_No"].ToString();
                    objEI.Bank_Name = sdr["Bank_Name"].ToString();
                    objEI.IFSC = sdr["IFSC"].ToString();
                    objEI.Reference_1 = sdr["Reference1"].ToString();
                    objEI.Phone_No = sdr["Phone_No"].ToString();
                    objEI.Email = sdr["Email"].ToString();
                }
                gConnection.Close();
                ViewBag.Phone_No = objEI.Phone_No;
                ViewBag.EmailId = objEI.Email;
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return View(objEI);
        }


        [HttpPost]
        public ActionResult EasyInvestFinal(EasyInvest objEI, FormCollection frm)
        {
            //string data = frm["ChkAgree"].ToString();
            //if(data == "")
            //{
            //    return RedirectToAction("EasyInvestDone");
            //}
            //return View();
            try
            {
                if (objEI.Phone_No != "")
                {
                    if (objEI.Email == null)
                    {
                        objEI.Email = "";
                    }
                    gConnection.Open();
                    string sqlstr = "insert into EasyInvestEntry(MobileNo,EmailId,SMSText,time,[to],Circle,Operator, CreatedBy,CreatedOn)";
                    sqlstr = sqlstr + " values(@MobileNo,@EmailId,'','','','','', @CreatedBy,GETDATE())";
                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                    cmd.Parameters.AddWithValue("@MobileNo", objEI.Phone_No);
                    cmd.Parameters.AddWithValue("@EmailId", objEI.Email);
                    cmd.Parameters.AddWithValue("@CreatedBy", "");
                    cmd.ExecuteNonQuery();
                    //gConnection.Close();

                    string LongName = "";
                    string EmailId = "";
                    string ClientCode = "";
                    sqlstr = "select bo_client_code, bo_long_name,bo_email from dbo.Client_Master where bo_mobile=@mobile_no";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.AddWithValue("@mobile_no", objEI.Phone_No);
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        ClientCode = sdr["bo_client_code"].ToString();
                        LongName = sdr["bo_long_name"].ToString();                        
                        if(EmailId == "")
                        {
                            EmailId = sdr["bo_email"].ToString();
                        }
                    }
                    gConnection.Close();

                    StreamReader reader = new StreamReader(Server.MapPath("~/Global/index.html"));
                    string readFile = reader.ReadToEnd();
                    string myString = "";
                    myString = readFile;
                    myString = myString.Replace("$$Name$$", LongName);

                    string FromMail = ConfigurationManager.AppSettings["smtpDisplayId"];
                    string Subject = "Easy Invest";
                    string UserName = ConfigurationManager.AppSettings["smtpUser"];
                    string Password = ConfigurationManager.AppSettings["smtpPass"];
                    string Link = ConfigurationManager.AppSettings["smtpLink"];


                    if (EmailId != "")
                    {
                        bool result = objGFunc.SendEasyInvestMail_Client(FromMail, EmailId, Subject, myString, UserName, Password, "");
                        if(result == true)
                        {
                            InsertMailLog(objEI.Phone_No, EmailId, "", "EndUser");
                        }
                        //bool result = objGFunc.SendEasyInvestMail_Client(FromMail, EmailId, Subject, myString, UserName, Password, "");
                    }
                    string body = "";
                    body = "Hello,";
                    body = body + Environment.NewLine;
                    body = body + "Please note that our client - " + LongName + ", with registered mobile number " + objEI.Phone_No + " has expressed interest to subscribe to Easy Invest.";
                    body = body + Environment.NewLine;
                    body = body + "The Client Code in our system is " + ClientCode;
                    body = body + Environment.NewLine;
                    body = body + Environment.NewLine;
                    body = body + "Please request respective RM/NP to get in touch with the client at the earliest for further process.";
                    body = body + Environment.NewLine;
                    body = body + Environment.NewLine;
                    body = body + "Bank Slip PDF is being generated for the client. Will be emailed to you shortly to share with the concerned person.";
                    body = body + Environment.NewLine;
                    body = body + Environment.NewLine;
                    body = body + Environment.NewLine;
                    body = body + "Thank you.";
                    body = body + "XXXXXXXXXXX Bot";
                    bool result1 = objGFunc.SendMail(FromMail, "XXXX@XXXXX.com", Subject, myString, UserName, Password, "");
                    if (result1 == true)
                    {
                        InsertMailLog(objEI.Phone_No, EmailId, "", "KRCTeam");
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return RedirectToAction("EasyInvestDone");
        }

        private bool InsertMailLog(string MobileNo, string EmailId, string CreatedBy, string MailSentTo)
        {
            bool result = false;
            try
            {
                if (MobileNo == null)
                {
                    MobileNo = "";
                }
                gConnection.Open();
                string sqlstr = "Insert Into EasyInvestMail_Log(MobileNo,EmailId,CreatedBy,CreatedOn,MailSentTo)";
                sqlstr = sqlstr + "values(@MobileNo,@EmailId,@CreatedBy,GetDate(),@MailSentTo)";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@MobileNo",MobileNo);
                cmd.Parameters.AddWithValue("@EmailId",EmailId);
                cmd.Parameters.AddWithValue("@CreatedBy",CreatedBy);
                cmd.Parameters.AddWithValue("@MailSentTo", MailSentTo);
                cmd.ExecuteNonQuery();
                gConnection.Close();
                result = true;
            }
            catch(Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return result;
        }


        public ActionResult EasyInvestDone(FormCollection frm)
        {
            return View();
        }

        

        #region Get Data

        public ActionResult GetData(string Company_Name, string Account_No, string Bank_Name, string IFSC, string Reference1, string Phone_No, string Email)
        {
            EasyInvest objEI = new Models.EasyInvest();
            try
            {
                objEI.Company_Name = Company_Name;
                objEI.Account_No = Account_No;
                objEI.Bank_Name = Bank_Name;
                objEI.IFSC = IFSC;
                objEI.Reference_1 = Reference1;
                objEI.Phone_No = Phone_No;
                objEI.Email = Email;


                @ViewBag.EmailId = Email;
                @ViewBag.Phone_No = Phone_No;

            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return PartialView(objEI);
        }

        #endregion

        


        #region Easy Invest SMS Reply

        public void EasyInvSMSReply(string mobile, string SMSText)
        {
            try
            {
                if (mobile == null)
                {
                    mobile = "";
                }
                if (SMSText == null)
                {
                    SMSText = "";
                }

                if (mobile != "")
                {
                    if (SMSText != "")
                    {
                        if (SMSText.ToUpper() == "YES")
                        {
                            gConnection.Open();
                            string sqlstr = "Insert Into EasyInvestEntry(MobileNo,EmailId,SMSText,time,[to],Circle,Operator,createdBy,CreatedOn) ";
                            sqlstr = sqlstr + " values (@MobileNo,'',@SMSText,'','','','','Online',GetDate())";
                            SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                            cmd.Parameters.AddWithValue("@MobileNo", mobile);
                            cmd.Parameters.AddWithValue("@SMSText", SMSText);
                            cmd.ExecuteNonQuery();
                            gConnection.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }

        }

        #endregion



        public ActionResult CreateWealthSignUp()
        {
            return View();
        }

    }
}