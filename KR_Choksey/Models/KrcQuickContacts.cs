﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class KrcQuickContacts
    {
        [Key]
        public decimal AutoId { get; set; }

        
        [MaxLength(110)]
        [Required(ErrorMessage="Name Can't Be Blank!")]
        public string Name { get; set; }

        
        [MaxLength(50)]
        [Required(ErrorMessage="Email Id Can't Be Blank!")]
        [DataType(DataType.EmailAddress)]
        public string EmailId { get; set; }

        
        [MaxLength(15)]
        [Required(ErrorMessage="Phone Number Can't Be Blank!")]
        public string PhoneNumber { get; set; }

        
        [MaxLength(200)]
        public string QuickTopic { get; set; }

        
        [MaxLength(200)]
        public string Screen { get; set; }


        [MaxLength(20)]
        [Required(ErrorMessage = "Aadhar / PAN Number Can't Be Blank!")]
        public string Aadhar_PAN { get; set; }


        [MaxLength(50)]
        public string CreatedBy { get; set; }


        public DateTime CreatedOn { get; set; }

    }



    public class KrcSIPContacts
    {
        [Key]
        public decimal AutoId { get; set; }


        [MaxLength(110)]
        [Required(ErrorMessage = "Name Can't Be Blank!")]
        public string Name { get; set; }


        [MaxLength(50)]
        [Required(ErrorMessage = "Email Id Can't Be Blank!")]
        public string EmailId { get; set; }


        [MaxLength(15)]
        [Required(ErrorMessage = "Phone Number Can't Be Blank!")]
        [DataType(DataType.EmailAddress)]
        public string PhoneNumber { get; set; }


        [MaxLength(200)]
        public string QuickTopic { get; set; }


        [MaxLength(200)]
        public string Screen { get; set; }


        [MaxLength(20)]
        [Required(ErrorMessage = "Aadhar / PAN Number Can't Be Blank!")]
        public string Aadhar_PAN { get; set; }


        [MaxLength(50)]
        public string CreatedBy { get; set; }


        public DateTime CreatedOn { get; set; }

    }

}