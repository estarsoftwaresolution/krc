﻿using KR_Choksey.Global;
using KR_Choksey.Hubs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class GetDataForBSENSEOnline
    {
        readonly string _connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;


        public BSENSEOnlinePrice GetAllMessages()
        {
            //var messages = new BSENSEOnlinePrice();
            //using (var connection = new SqlConnection(_connString))
            //{
            //    connection.Open();
            //    using (var command = new SqlCommand(@"select AutoId,lname,currprice,currtime,prevclose from BSENSEOnlinePrice where AutoId in (Select MAX(AutoId) From BSENSEOnlinePrice", connection))
            //    {
            //        command.Notification = null;

            //        var dependency = new SqlDependency(command);
            //        dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

            //        if (connection.State == ConnectionState.Closed)
            //            connection.Open();

            //        var reader = command.ExecuteReader();

            //        while (reader.Read())
            //        {
            //            messages.Add(item: new BSENSEOnlinePrice { MessageID = (int)reader["MessageID"], Message = (string)reader["Message"], EmptyMessage = reader["EmptyMessage"] != DBNull.Value ? (string)reader["EmptyMessage"] : "", MessageDate = Convert.ToDateTime(reader["Date"]) });
            //        }
            //    }

            //}
            //return messages;

            BSENSEOnlinePrice objData = new BSENSEOnlinePrice();
            try
            {
                gConnection.Open();
                string sqlstr = "select AutoId,lname,currprice,currtime,prevclose from BSENSEOnlinePrice ";
                sqlstr = sqlstr + " where AutoId in (Select MAX(AutoId) From BSENSEOnlinePrice)";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    objData.AutoId = Convert.ToInt32(sdr["AutoId"].ToString());
                    objData.lname = sdr["lname"].ToString();
                    objData.currprice = Convert.ToDecimal(sdr["currprice"].ToString());
                    objData.currtime = Convert.ToDateTime(sdr["currtime"].ToString());
                    objData.prevclose = Convert.ToDecimal(sdr["prevclose"].ToString());
                }
            }
            catch(Exception ex)
            {

            }

            return objData;
        }

        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                MessagesHub.SendMessages();
            }
        }
    }
}