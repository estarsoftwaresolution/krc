﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class NetworkPartnerEnquiry
    {
        [Required(ErrorMessage="Name can't be blank!")]
        [MaxLength(100)]
        public string Name { get; set; }

        
        [DataType(DataType.Date)]
        public DateTime DOB { get; set; }

        [MaxLength(100)]
        public string Qualification { get; set; }


        [Required(ErrorMessage="Mobile Number can't be blank!")]
        [MaxLength(15)]
        public string MobileNumber { get; set; }

        [MaxLength(15)]
        public string TelephoneNumber { get; set; }


        [Required(ErrorMessage="Email Address can't be blank!")]
        [MaxLength(100)]        
        public string EmailAddress { get; set; }


        [MaxLength(200)]
        public string Address { get; set; }


        [MaxLength(75)]
        public string City { get; set; }


        [MaxLength(75)]
        public string State { get; set; }


        [MaxLength(10)]
        public string Pincode { get; set; }


        [MaxLength(200)]
        public string HowYouKnowAboutUs { get; set; }


        [MaxLength(300)]
        public string ExpectationsFromKRC { get; set; }


        [MaxLength(10)]
        public string ExperienceInCapitalMarket { get; set; }


        [MaxLength(100)]
        public string Equity { get; set; }

        [MaxLength(100)]
        public string MF { get; set; }

        [MaxLength(100)]
        public string Derivatives { get; set; }


        [MaxLength(100)]
        public string Insurance { get; set; }

        [MaxLength(100)]
        public string IPO { get; set; }

        [MaxLength(100)]
        public string BrokHouseAssWith { get; set; }


        [MaxLength(100)]
        public string Trading { get; set; }

        
        [MaxLength(100)]
        public string Delivery { get; set;  }


        [MaxLength(100)]
        public string VSAT { get; set; }


        [MaxLength(100)]
        public string LeaseLine { get; set; }


        [MaxLength(100)]
        public string Others { get; set; }

    }
}