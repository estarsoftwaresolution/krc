﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class BSENSEOnlinePrice
    {
        [Key]
        public int AutoId { get; set; }
	
        public string lname { get; set; }
	
        public decimal currprice { get; set; }
	
        public DateTime currtime { get; set; }

        public decimal prevclose { get; set; }
    }
}