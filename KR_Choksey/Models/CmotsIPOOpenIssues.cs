﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KR_Choksey.Models
{
    public class CmotsIPOOpenIssues
    {
        public string co_code { get; set; }
        
        public string lname { get; set; }
        
        public string volyr { get; set; }

        public string volsrno { get; set; }

        public string parvalue { get; set; }

        public string issuetype { get; set; }

        public string ba { get; set; }

        public string nim { get; set; }

        public string noshissued { get; set; }

        public string issueprice { get; set; }

        public string issuepri2 { get; set; }

        public string opendate { get; set; }

        public string closdate { get; set; }

        public string bbopendate { get; set; }

        public string bbclosdate { get; set; }

        public string listprice { get; set; }

        public string min_appln { get; set; }

        public string multiples { get; set; }

        public string issuesize { get; set; }

        public string issue { get; set; }

        public string created_by { get; set; }

        public DateTime created_on { get; set; }

    }
}
