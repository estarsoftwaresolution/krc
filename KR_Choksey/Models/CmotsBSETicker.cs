﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class CmotsBSETicker
    {
        public int AutoId { get; set; }

        [MaxLength(20)]
        public string co_code { get; set; }

        [MaxLength(200)]
        public string co_name { get; set; }

        [MaxLength(30)]
        public string price { get; set; }

        [MaxLength(20)]
        public string price_diff { get; set; }


        public string tr_date { get; set; }

        [MaxLength(30)]
        public string change { get; set; }


        public string created_by { get; set; }

        public DateTime created_on { get; set; }
    }
}