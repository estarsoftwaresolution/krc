﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class Website_Search
    {
        public string SearchTest { get; set; }

        public string Navigation { get; set; }

        public string URL_Link { get; set; }
    }
}