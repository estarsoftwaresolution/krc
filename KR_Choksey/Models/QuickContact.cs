﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class QuickContact
    {
        //[Key]
        //public int QuickContactId { get; set; }
        public string QuickContactName { get; set; }
        public string QuickContactEmail { get; set; }
        public string QuickContactPhone { get; set; }
        public string QuickContactMessage { get; set; }
    }
}