﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class ClientDashLedger
    {
        public int AutoId { get; set; }
	
        public string CLIENT_CODE { get; set; }
	
        public string LEDGER_BALANCE { get; set; }	
        
        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

    }
}