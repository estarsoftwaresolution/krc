﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class CmotsCompanyNews
    {
        [MaxLength(30)]
        public string sr_no { get; set; }

        [MaxLength(30)]
        public string co_code { get; set; }

        
        public string date { get; set; }

        [MaxLength(1000)]
        public string caption { get; set; }

        [MaxLength(20)]
        public string yrc { get; set; }

        public string created_by { get; set; }

        public DateTime created_on { get; set; }

        public string memo { get; set; }
    }


    public class CmotsCompanyNews_Details
    {
        [MaxLength(30)]
        public string sr_no	{ get; set; }

        [MaxLength(30)]
        public string co_code { get; set; }

        
        public string date { get; set; }

        [MaxLength(1000)]
        public string caption { get; set; }

        
        
        public string memo { get; set; }

        public string created_by { get; set; }

        public DateTime created_on { get; set; }

        public string updated_by { get; set; }

        public DateTime updated_on { get; set; }


    }

}