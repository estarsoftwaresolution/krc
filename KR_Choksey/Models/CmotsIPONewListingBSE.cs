﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KR_Choksey.Models
{
    public class CmotsIPONewListingBSE
    {
        public string code { get ; set ; }

        public string co_code { get ; set ; }

        public string co_name { get ; set ; }

        public string listdate { get ; set ; }

        public string listprice { get ; set ; }
        
        public string listvol { get ; set ; }

        public string high { get ; set ; }

        public string low { get ; set ; }

        public string close { get ; set ; }

        public string date { get ; set ; }

        public string volume { get ; set ; }

        public string lasttr_date { get ; set ; }

        public string issuesize { get ; set ; }

        public string offerprice { get ; set ; }

        public string perchange { get ; set ; }

        public string issueprice { get ; set ; }

        public string lname { get ; set ; }

        public string created_by { get ; set ; }

        public DateTime created_on { get ; set ; }

    }
}
