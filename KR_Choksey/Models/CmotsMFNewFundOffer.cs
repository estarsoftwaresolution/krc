﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class CmotsMFNewFundOffer
    {
        [MaxLength(20)]
        public string Mf_SchCode  { get; set; }

        
        [MaxLength(200)]
        public string LName { get; set; }

        [MaxLength(200)]
        public string Sch_Name { get; set; }

        
        public string Launc_Date { get; set; }

        public string CLDATE { get; set; }

        [MaxLength(20)]
        public string MININVT { get; set; }

        [MaxLength(20)]
        public string OFFERPRICE { get; set; }

        [MaxLength(20)]
        public string SCHEME { get; set; }

        [MaxLength(2000)]
        public string Objective { get; set; }

        [MaxLength(20)]
        public string SIZE { get; set; }

        [MaxLength(200)]
        public string VClass { get; set; }

        [MaxLength(20)]
        public string TypeName { get; set; }

        [MaxLength(20)]
        public string VClassCode { get; set; }


        public string created_by { get; set; }

        public DateTime created_on { get; set; }
    }
}