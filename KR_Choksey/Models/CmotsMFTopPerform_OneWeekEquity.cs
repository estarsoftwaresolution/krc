﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class CmotsMFTopPerform_OneWeekEquity
    {
        [MaxLength(20)]
        public string Mf_SchCode { get; set; }

        [MaxLength(20)]
        public string SchClCode { get; set; }

        [MaxLength(20)]
        public string Scheme { get; set; }

        [MaxLength(200)]
        public string TypeName { get; set; }

        [MaxLength(200)]
        public string Sch_Name { get; set; }



        public string NavDate { get; set; }

        [MaxLength(30)]
        public string NavRs { get; set; }

        [MaxLength(30)]
        public string pReturn { get; set; }

        [MaxLength(30)]
        public string oneyear { get; set; }

        [MaxLength(30)]
        public string threeyear { get; set; }

        [MaxLength(30)]
        public string fiveyear { get; set; }

        [MaxLength(30)]
        public string inception { get; set; }

        [MaxLength(30)]
        public string prevnavrs { get; set; }

        [MaxLength(30)]
        public string navchange { get; set; }

        [MaxLength(30)]
        public string navperchange { get; set; }


        public string created_by { get; set; }

        public DateTime created_on { get; set; }
    }
}