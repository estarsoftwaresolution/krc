﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class CmotsDerivativeFuturesTopGainers_Nifty
    {
        [MaxLength(50)]
        public string Prevltp { get; set; }

        [MaxLength(50)]
        public string Ltp { get; set; }

        [MaxLength(50)]
        public string Faodiff { get; set; }

        [MaxLength(50)]
        public string faochange { get; set; }

        [MaxLength(50)]
        public string instname { get; set; }

        [MaxLength(50)]
        public string Symbol { get; set; }

        [MaxLength(50)]
        public string expdate { get; set; }


            [MaxLength(50)]
        public string strikeprice	 { get; set; }

        [MaxLength(50)]
        public string opttype { get; set; }

        
        public string updtime	{ get; set; }

        [MaxLength(50)]
        public string Qty { get; set; }

        [MaxLength(50)]
        public string openinterest { get; set; }

        [MaxLength(50)]
        public string chgopenint { get; set; }

        public string created_by { get; set; }

        public DateTime created_on { get; set; }
    }
}