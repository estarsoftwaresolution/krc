﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class CmotsShareHoldingPattern
    {
        [MaxLength(200)]
        public string type	{ get; set; }

        public string yearmonth  { get; set; }

        public string total_foreign_promoter_and_group_1	 { get; set; }

        public string total_foreign_promoter_and_group_2	 { get; set; }

        public string total_foreign_promoter_and_group_3	 { get; set; }

         public string total_foreign_promoter_and_group_4	 { get; set; }

        
        public string indian_promoter_and_group_1	 { get; set; }

        public string indian_promoter_and_group_2	 { get; set; }

        public string indian_promoter_and_group_3	 { get; set; } 

        public string indian_promoter_and_group_4	 { get; set; }

        public string total_of_promoter_1	 { get; set; }


        public string total_of_promoter_2	 { get; set; }

        public string total_of_promoter_3	 { get; set; } 

        public string total_of_promoter_4	 { get; set; }

        public string non_promoter_institution_1	 { get; set; }

        public string non_promoter_institution_2	 { get; set; }

        public string non_promoter_institution_3	 { get; set; } 

        public string non_promoter_institution_4	 { get; set; }

        public string total_non_promoter_1	 { get; set; }

        public string total_non_promoter_2	 { get; set; }

        public string total_non_promoter_3	 { get; set; } 

        public string total_non_promoter_4	 { get; set; }

        public string total_promoter_and_non_promoter_1	 { get; set; }

        public string total_promoter_and_non_promoter_2	 { get; set; }

        public string total_promoter_and_non_promoter_3	 { get; set; } 

        public string total_promoter_and_non_promoter_4	 { get; set; }

        public string custodiansagainst_depository_receipts_1	 { get; set; }

        public string custodiansagainst_depository_receipts_2	 { get; set; }

        public string custodiansagainst_depository_receipts_3	 { get; set; } 

        public string custodiansagainst_depository_receipts_4	 { get; set; }

        public string grand_total_1	 { get; set; }

        public string grand_total_2	 { get; set; }

        public string grand_total_3	 { get; set; }

        public string grand_total_4 { get; set; }

        public string co_code { get; set; }

        public string created_by { get; set; }

        public Nullable<DateTime> created_on { get; set; }

    }
}