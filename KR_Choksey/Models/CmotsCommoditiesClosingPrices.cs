﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class CmotsCommoditiesClosingPrices
    {
        [MaxLength(100)]
        public string Symbol { get; set; }

        [MaxLength(200)]
        public string CommName { get; set; }

        [MaxLength(50)]
        public string Category { get; set; }

        [MaxLength(50)]
        public string Unit { get; set; }


        public string Trd_Date { get; set; }



        public string Exp_Date { get; set; }

        [MaxLength(50)]
        public string OpenPrice { get; set; }


        [MaxLength(50)]
        public string HPrice { get; set; }


        [MaxLength(50)]
        public string LPrice { get; set; }

        [MaxLength(50)]
        public string ClosePrice { get; set; }

        [MaxLength(50)]
        public string PrevClose { get; set; }


        public string PrevClosedate { get; set; }

        [MaxLength(50)]
        public string Volume { get; set; }

        [MaxLength(50)]
        public string TrdVal { get; set; }

        [MaxLength(50)]
        public string Center { get; set; }

        [MaxLength(50)]
        public string OI { get; set; }

        [MaxLength(50)]
        public string Diff { get; set; }

        [MaxLength(50)]
        public string Change { get; set; }


        public string MaxDate { get; set; }

        public string created_by { get; set; }

        public DateTime created_on { get; set; }
    }
}