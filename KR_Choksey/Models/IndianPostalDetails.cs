﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class IndianPostalDetails
    {

        public decimal AutoId { get; set; }
	
        [MaxLength(100)]
        public string officename { get; set; }
	
        [MaxLength(100)]
        public string pincode { get; set; }
	
        [MaxLength(100)]
        public string officeType { get; set; }
	
        [MaxLength(100)]
        public string DeliveryStatus { get;set; }
	
        [MaxLength(100)]
        public string DivisionName { get; set; }
	
        [MaxLength(100)]
        public string RegionName { get; set; }
	
        [MaxLength(100)]
        public string CircleName { get; set; }
	
        [MaxLength(100)]
        public string Taluk { get; set; }
	
        [MaxLength(100)]
        public string DistrictName { get; set; }
	
        [MaxLength(100)]
        public string StateName { get; set; }

        [MaxLength(100)]
        public string CountryName { get; set; }
    }
}