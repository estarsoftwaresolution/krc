﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class Client_Master
    {
        [Key]
        public decimal client_master_code { get; set; }
	
        
        [MaxLength(10)]
        public string bo_client_code { get; set; }
	
        
        [MaxLength(10)]
        public string bo_branch_code { get; set; }
	
        [MaxLength(10)]
        public string bo_sub_broker_code { get; set; }
	
        [MaxLength(20)]
        public string bo_trader_code { get; set; }
	
        
        [MaxLength(100)]
        public string bo_long_name { get; set; }
	
        
        [MaxLength(21)]
        public string bo_short_name { get; set; }
	
        
        [MaxLength(40)]
        public string bo_addr1 { get; set; }
	
        
        [MaxLength(40)]
        public string bo_addr2 { get; set; }
	
        
        [MaxLength(40)]
        public string bo_addr3 { get; set; }
	
        
        [MaxLength(40)]
        public string bo_city { get; set; }
	
        
        [MaxLength(50)]
        public string bo_state { get; set; }
	
        
        [MaxLength(10)]
        public string bo_zip { get; set; }
	
        
        [MaxLength(40)]
        public string bo_country { get; set; }
	
        
        [MaxLength(50)]
        public string bo_pan { get; set; }
	
        
        [MaxLength(25)]
        public string bo_sebi_regn { get; set; }
	
        
        [MaxLength(15)]
        public string bo_resi_phone1 { get; set; }
	
        
        [MaxLength(15)]
        public string bo_resi_phone2 { get; set; }
	
        
        [MaxLength(15)]
        public string bo_office_phone1 { get; set; }
	
        
        [MaxLength(15)]
        public string bo_office_phone2 { get; set; }
	
        
        [MaxLength(40)]
        [Required(ErrorMessage = "Mobile Number can't be blank!")]
        public string bo_mobile { get; set; }
	
        
        [MaxLength(15)]
        public string bo_fax { get; set; }


        [MaxLength(100)]
        [Display(Name = "EMAIL ADDRESS")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Enter valid email address!")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Required(ErrorMessage = "Email Address can't be blank!")]
        public string bo_email { get; set; }
	
        
        [MaxLength(3)]
        public string bo_client_type { get; set; }
	
        
        [MaxLength(3)]
        public string bo_client_status { get; set; }
	
        
        [MaxLength(10)]
        public string bo_family { get; set; }
	
        
        [MaxLength(20)]
        public string bo_region { get; set; }
	
        
        
        [MaxLength(20)]
        public string bo_area { get; set; }
	
        
        [MaxLength(50)]
        public string bo_p_addr1 { get; set; }
	
        
        [MaxLength(50)]
        public string bo_p_addr2 { get; set; }
	
        
        [MaxLength(50)]
        public string bo_p_addr3 { get; set; }
	
        
        [MaxLength(20)]
        public string bo_p_city { get; set; }
	
        
        [MaxLength(50)]
        public string bo_p_state { get; set; }
	
        
        [MaxLength(15)]
        public string bo_p_country { get; set; }
	
        
        [MaxLength(10)]
        public string bo_p_zip { get; set; }
	
        
        [MaxLength(15)]
        public string bo_p_phone { get; set; }
	
        
        [MaxLength(230)]
        public string bo_multi_email { get; set; }
	
        
        [MaxLength(1)]
        public string bo_gender { get; set; }
	
        
        [DataType(DataType.Date)]
        public DateTime bo_dob { get; set; }
	
        
        [MaxLength(30)]
        public string bo_introducer { get; set; }
	
        
        [MaxLength(30)]
        public string bo_approver { get; set; }
	
        
        [MaxLength(50)]
        public string bo_bank_name { get; set; }
	
        
        [MaxLength(50)]
        public string bo_bank_branch_name { get; set; }
	
        
        [MaxLength(10)]
        public string bo_bank_ac_type { get; set; }
	
        
        [MaxLength(20)]
        public string bo_bank_ac_number { get; set; }
	
        
        [MaxLength(7)]
        public string bo_depository_type { get; set; }
	
        
        [MaxLength(16)]
        public string bo_client_dp_id { get; set; }
	
        
        [MaxLength(1)]
        public string bo_poa { get; set; }
	
        
        [MaxLength(7)]
        public string bo_depository_type2 { get; set; }
	
        
        [MaxLength(16)]
        public string bo_client_dp_id2 { get; set; }
	
        
        [MaxLength(1)]
        public string bo_poa2 { get; set; }
	
        
        [MaxLength(10)]
        public string bo_relationship_mgr { get; set; }
	
        
        [MaxLength(10)]
        public string bo_sbu { get; set; }
	
        
        [MaxLength(25)]
        public string bo_modified_by { get; set; }
	
        
        
        public DateTime bo_modified_on { get; set; }
	
        
        public DateTime bo_active_date { get; set; }
	
        
        public string bo_inactive_date { get; set; }
	

        [MaxLength(10)]
        public string bo_ac_status { get; set; }
	
        
        [MaxLength(50)]
        [Required(ErrorMessage="First Name can't be blank!")]
        public string first_name { get; set; }
	
        
        [MaxLength(50)]
        public string middle_name { get; set; }
	
        
        [MaxLength(50)]
        [Required(ErrorMessage = "Last Name can't be blank!")]
        public string last_name { get; set; }
	
        
        [MaxLength(12)]
        public string aadhaar { get; set; }
	
        
        [MaxLength(50)]
        public string occupation { get; set; }
	
        
        [MaxLength(50)]
        public string income_slab { get; set; }
	
        
        public int net_worth_slab { get; set; }
	

        [MaxLength(50)]
        public string mf_id { get; set; }
	
        
        [MaxLength(10)]
        public string pms_internal_code { get; set; }
	
        
        [MaxLength(50)]
        public string skype_id { get; set; }
	
        
        [MaxLength(50)]
        public string facebook_id { get; set; }
	
        
        [MaxLength(50)]
        public string twitter_id { get; set; }
	
        
        [MaxLength(50)]
        public string instagram_id { get; set; }
	
        
        [MaxLength(50)]
        public string google_id { get; set; }
	
        
        [MaxLength(50)]
        public string snapchat_id { get; set; }
	
        
        [MaxLength(50)]
        public string linkedin_id { get; set; }


        [MaxLength(50)]
        public string apple_id { get; set; }
    }
}