﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class LocateUs
    {

        [MaxLength(100)]
        public string City { get; set; }

        [MaxLength(100)]
        public string State { get; set; }


        public string Contact_Person { get; set; }

        public string Address { get; set; }

        public string PhoneNo { get; set; }

        public string MobileNo { get; set; }

        public string Email { get; set; }

    }
}