﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class OTPVerification
    {
        public string MobileNo { get; set; }

        public string MobileNoResponse { get; set; }

        public string OTP { get; set; }

        public string OTPResponse { get; set; }
    }
}