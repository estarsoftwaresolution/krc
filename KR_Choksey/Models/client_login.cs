﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class client_login
    {
        [Key]
        public int client_login_id { get; set; }
	
        [MaxLength(100)]
        public string username { get; set; }
	
        
        [MaxLength(10)]
        [Required(ErrorMessage="Password can't be blank!")]
        public string password { get; set; }
	
        public DateTime creation_date { get; set; }
	
        
        public DateTime last_access_date { get; set; }
	
        public bool Enable_login { get; set; }
	
        
        public DateTime web_modify_on { get; set; }
	
        [MaxLength(50)]
        public string web_modify_by { get; set; }


        public int FK_client_master_code { get; set; }

        [MaxLength(10)]
        [Required(ErrorMessage="Password can't be blank!")]
        public string password_confirm { get; set; } // For Checking Purpose Only
    }
}