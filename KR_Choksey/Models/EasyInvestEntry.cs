﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class EasyInvestEntry
    {
        [MaxLength(15)]
        public string MobileNo { get; set; }
	
        [MaxLength(100)]
        public string EmailId { get; set; }
	
        
        [MaxLength(50)]
        public string SMSText { get; set; }
	
        
        [MaxLength(100)]
        public string time { get; set; }
	
        
        [MaxLength(50)]
        public string to { get; set; }
	
        
        [MaxLength(200)]
        public string Circle { get; set; }
	
        
        [MaxLength(200)]
        public string Operator { get; set; }
	
        
        [MaxLength(15)]
        public string createdBy { get; set; }



        public DateTime CreatedOn { get; set; }

    }
}