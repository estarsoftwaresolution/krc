﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class CmotsMFNews
    {
        [MaxLength(20)]
        public string Sno { get; set; }

        [MaxLength(200)]
        public string Section_name { get; set; }

        
        public string Date { get; set; }

        [MaxLength(20)]
        public string Time { get; set; }

        [MaxLength(200)]
        public string Heading { get; set; }

        [MaxLength(200)]
        public string Caption { get; set; }

        
        public string Arttext { get; set; }

        [MaxLength(200)]
        public string Co_code { get; set; }

        [MaxLength(20)]
        public string Ind_code { get; set; }

        [MaxLength(20)]
        public string Flag { get; set; }


        public string created_by { get; set; }

        public DateTime created_on { get; set; }

        public string updated_by { get; set; }


    }
}