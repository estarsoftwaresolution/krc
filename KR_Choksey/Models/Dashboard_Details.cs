﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class Dashboard_Details
    {
        public string co_code { get; set; }
        public string CompanyName { get; set; }
        public string CompanySector { get; set; }
        public string NSE_Quote { get; set; }
        public string NSE_Quote_diff { get; set; }
        public string BSE_Quote { get; set; }
        public string BSE_Quote_diff { get; set; }
        public string Com_News { get; set; }
        public string Nws_Details { get; set; }

    }
}