﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class NewsClass
    {
        public int id { get; set; }

        public string text { get; set; }

        public string section_name { get; set; }
    }
}