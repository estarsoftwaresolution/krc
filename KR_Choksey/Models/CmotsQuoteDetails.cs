﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace KR_Choksey.Models
{
    public class CmotsQuoteDetails
    {
        [Key]
        public int AutoId { get; set; }

        [MaxLength(15)]
        public string xchng { get; set; }
                
        public DateTime upd_time { get; set; }

        [MaxLength(50)]
        public string open_price { get; set; }

        [MaxLength(50)]
        public string high_price { get; set; }

        [MaxLength(50)]
        public string low_price { get; set; }

        [MaxLength(50)]
        public string price { get; set; }

        [MaxLength(50)]
        public string bbuy_qty { get; set; }

        [MaxLength(50)]
        public string bbuy_price { get; set; }

        [MaxLength(50)]
        public string bsell_qty { get; set; }

        [MaxLength(50)]
        public string bsell_price { get; set; }

        [MaxLength(50)]
        public string oldprice { get; set; }

        [MaxLength(50)]
        public string pricediff { get; set; }

        [MaxLength(50)]
        public string volume { get; set; }

        [MaxLength(50)]
        public string value { get; set; }

        [MaxLength(50)]
        public string change { get; set; }

        [MaxLength(50)]
        public string trd_qty { get; set; }

        [MaxLength(50)]
        public string hi_52_wk { get; set; }

        [MaxLength(50)]
        public string lo_52_wk { get; set; }

                
        public DateTime h52date { get; set; }

        public DateTime l52date { get; set; }

        [MaxLength(50)]
        public string trd_value { get; set; }
                
        [MaxLength(50)]
        public string co_name { get; set; }
                
        [MaxLength(200)]
        public string complname { get; set; }

        [MaxLength(50)]
        public string symbol { get; set; }

        [MaxLength(50)]
        public string sc_group { get; set; }

        [MaxLength(50)]
        public string created_by { get; set; }

        public DateTime created_on { get; set; }

    }
}