﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class client_product_map
    {
        [Key]
        public int client_product_id { get; set; }
	
        
        
        public bool equity { get; set; }
	
        
        public bool fo { get; set; }
	
        
        public bool currency { get; set; }
	
        
        public bool commodities { get; set; }
	
        
        public bool research_funda { get; set; }
	
        
        public bool research_tech { get; set; }
	
        
        public bool mf { get; set; }
	
        public bool insurance { get; set; }
	
        public bool pms { get; set; }
	
        public bool fd { get; set; }
	
        public bool bonds { get; set; }
	
        public bool margin_fund { get; set; }
	
        public bool las { get; set; }
	
        
        public bool i_hold { get; set; }
	
        
        public bool online_trading { get; set; }
	
        
        public bool w_mgt { get; set; }


        public int FK_client_master_code { get; set; }

    }
}