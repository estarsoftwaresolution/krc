﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class CmotsKeyFinancialRatios
    {
        [MaxLength(20)]
        public string co_code { get; set; }

        [MaxLength(100)]
        public string columnname { get; set; }

        
        public int rid { get; set; }

        //[MaxLength(20)]
        //public string y201512 { get; set; }

        //[MaxLength(20)]
        //public string y201412 { get; set; }

        //[MaxLength(20)]
        //public string y201312 { get; set; }

        //[MaxLength(20)]
        //public string y201212 { get; set; }

        //[MaxLength(20)]
        //public string y201112 { get; set; }

        //[MaxLength(20)]
        //public string y201012 { get; set; }


        [MaxLength(20)]
        public string period1 { get; set; }

        [MaxLength(20)]
        public string period2 { get; set; }

        [MaxLength(20)]
        public string period3 { get; set; }

        [MaxLength(20)]
        public string period4 { get; set; }

        [MaxLength(20)]
        public string period5 { get; set; }

        [MaxLength(20)]
        public string period6 { get; set; }


        public string created_by { get; set; }

        public DateTime created_on { get; set; }

    }
}