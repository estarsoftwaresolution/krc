﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class ClientHold
    {

        public int AutoId { get; set; }
        public string Client_Code { get; set; }
        public string Branch_Code { get; set; }
        public string Symbol { get; set; }
        public string ISIN_Code { get; set; }
        public string Quantity { get; set; }
        public string BOD_Value { get; set; }
        public string HoldingValue { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }


    }

    public class PagedClientHold
    {
        
        public string Client_Code { get; set; }

        public string Client_Name { get; set; }
        
        public List<ClientHold> ClientHold { get; set; }


        public string HoldingValue { get; set; }

    }
}