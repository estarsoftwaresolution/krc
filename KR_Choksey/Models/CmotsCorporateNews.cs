﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class CmotsCorporateNews
    {
        [MaxLength(20)]
        public string sno { get; set; }

        [MaxLength(200)]
        public string section_name { get; set; }


        public string date { get; set; }

        [MaxLength(30)]
        public string time { get; set; }

        [MaxLength(500)]
        public string heading { get; set; }

        [MaxLength(1000)]
        public string caption { get; set; }


        public string arttext { get; set; }

        [MaxLength(20)]
        public string co_code { get; set; }

        [MaxLength(50)]
        public string ind_code { get; set; }

        [MaxLength(10)]
        public string flag { get; set; }


        public string created_by { get; set; }

        public DateTime created_on { get; set; }
        public bool notification { get; set; }

    }
}