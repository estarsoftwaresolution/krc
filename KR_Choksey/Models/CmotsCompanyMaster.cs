﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace KR_Choksey.Models
{
    public class CmotsCompanyMaster
    {

        [Key]
        public int AutoId { get; set; }

        [MaxLength(10)]
        public string co_code { get; set; }

        [MaxLength(10)]
        public string bsecode { get; set; }

        [MaxLength(100)]
        public string nsesymbol { get; set; }

        [MaxLength(200)]
        public string companyname { get; set; }
        
        [MaxLength(100)]
        public string companyshortname { get; set; }
                
        [MaxLength(100)]
        public string categoryname { get; set; }

        [MaxLength(100)]
        public string isin { get; set; }

        [MaxLength(100)]
        public string bsegroup { get; set; }

        [MaxLength(100)]
        public string mcaptype { get; set; }

        [MaxLength(100)]
        public string sectorcode { get; set; }

        [MaxLength(100)]
        public string sectorname { get; set; }

        [MaxLength(50)]
        public string created_by { get; set; }

        public DateTime created_on { get; set; }

    }

    public class CmotsCompanyMasterList
    {
        IEnumerable<CmotsCompanyMaster> CmotsCompanyMaster1 { get; set; }
    }
}