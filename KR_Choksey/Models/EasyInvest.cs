﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{

    public class Otp
    {
        public string Status { get; set; }
        public string Details { get; set; }
    }

    public class EasyInvest
    {
        public string Company_Name { get; set; }

        public string Account_No { get; set; }

        public string Bank_Name { get; set; }

        public string IFSC { get; set; }

        public string Reference_1 { get; set; }

        public string Phone_No { get; set; }

        public string Email { get; set; }

    }
}