﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class NSENetPriceLosersData
    {
        public int AutoId { get; set; }

        //public string stk_exchange { get; set; }
        public string stk_exchng { get; set; }

        public string sc_code { get; set; }

        public string co_code { get; set; }

        public string symbol { get; set; }

        public string isin { get; set; }

        public string co_name { get; set; }

        public string sc_group { get; set; }

        public DateTime upd_time { get; set; }

        public string open_price { get; set; }

        public string high_price { get; set; }

        public string low_price { get; set; }

        public string close_price { get; set; }

        public string bbuy_qty { get; set; }

        public string bbuy_price { get; set; }

        public string bsell_qty { get; set; }

        public string bsell_price { get; set; }

        public string Prevclose { get; set; }

        public string Perchg { get; set; }

        public string Netchg { get; set; }

        public string vol_traded { get; set; }

        public string pervol { get; set; }

        public string val_traded { get; set; }

        public string fiftytwoweekhigh { get; set; }

        public string fiftytwoweeklow { get; set; }

        public string prev_vol_traded { get; set; }

        public string prev_value_traded { get; set; }

        public DateTime prevdate { get; set; }

        public string lname { get; set; }

        public DateTime created_on { get; set; }
    }
}