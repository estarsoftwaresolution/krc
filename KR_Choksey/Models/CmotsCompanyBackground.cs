﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace KR_Choksey.Models
{
    public class CmotsCompanyBackground
    {
        [Key]

        public int AutoId { get; set; }

        [MaxLength(200)]
        public string lname { get; set; }

        [MaxLength(100)]
        public string isin { get; set; }

        [MaxLength(100)]
        public string hse_s_name { get; set; }

        [MaxLength(100)]
        public string inc_dt { get; set; }

        [MaxLength(200)]
        public string regadd1 { get; set; }

        [MaxLength(200)]
        public string regadd2 { get; set; }

        [MaxLength(100)]
        public string regdist { get; set; }

        [MaxLength(100)]
        public string regstate { get; set; }

        [MaxLength(100)]
        public string regpin { get; set; }

        [MaxLength(100)]
        public string tel1 { get; set; }

        [MaxLength(200)]
        public string ind_l_name { get; set; }

        [MaxLength(100)]
        public string tel2 { get; set; }

        [MaxLength(100)]
        public string fax1 { get; set; }

        [MaxLength(100)]
        public string fax2 { get; set; }

        [MaxLength(200)]
        public string auditor { get; set; }

        [MaxLength(20)]
        public string fv { get; set; }

        [MaxLength(20)]
        public string mkt_lot { get; set; }

        [MaxLength(200)]
        public string chairman { get; set; }

        [MaxLength(200)]
        public string co_sec { get; set; }

        [MaxLength(20)]
        public string co_code { get; set; }

        [MaxLength(100)]
        public string email { get; set; }

        [MaxLength(200)]
        public string internet { get; set; }

        [MaxLength(200)]
        public string dir_name { get; set; }

        [MaxLength(200)]
        public string dir_desg { get; set; }



        public string created_by { get; set; }

        public DateTime created_on { get; set; }

        public string updated_by { get; set; }

        public DateTime updated_on { get; set; }

    }
}