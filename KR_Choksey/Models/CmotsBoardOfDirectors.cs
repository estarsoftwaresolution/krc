﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class CmotsBoardOfDirectors
    {
        [MaxLength(20)]
        public string co_code { get; set; }

        [MaxLength(10)]
        public string slno { get; set; }

        [MaxLength(200)]
        public string dir_name { get; set; }

        [MaxLength(200)]
        public string dir_desg { get; set; }

        [MaxLength(20)]
        public string year { get; set; }

        
        public string created_by { get; set; }

        public DateTime created_on { get; set; }

    }
}