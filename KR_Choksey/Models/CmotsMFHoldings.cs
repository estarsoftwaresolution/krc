﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class CmotsMFHoldings
    {

        public int AutoId { get; set; }

        [MaxLength(200)]
        public string sch_name { get; set; }

        [MaxLength(50)]
        public string mf_schcode { get; set; }

        
        public string invdate { get; set; }

        [MaxLength(10)]
        public string mktvalue { get; set; }

        [MaxLength(20)]
        public string co_code { get; set; }

        [MaxLength(200)]
        public string co_name { get; set; }

        [MaxLength(25)]
        public string perc_hold { get; set; }

        [MaxLength(25)]
        public string totnav { get; set; }

        [MaxLength(100)]
        public string sch_type { get; set; }

        public string created_by { get; set; }

        public DateTime created_on { get; set; }

    }
}