﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class CmotsCommoditiesGainers
    {
        public string Symbol { get; set; }
        public string CommName { get; set; }
        public string Category { get; set; }
        public string Unit { get; set; }
        public string Trd_Date { get; set; }
        public string Exp_Date { get; set; }
        public string OpenPrice { get; set; }
        public string HPrice { get; set; }
        public string LPrice { get; set; }
        public string ClosePrice { get; set; }
        public string PrevClose { get; set; }
        public string PrevClosedate { get; set; }
        public string Volume { get; set; }
        public string TrdVal { get; set; }
        public string Center { get; set; }
        public string OI { get; set; }
        public string Diff { get; set; }
        public string Change { get; set; }
        public string MaxDate { get; set; }
        public string Market_LOT { get; set; }
        public string created_by { get; set; }
        public DateTime created_on { get; set; }
    }
}