﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class NSEIndexMovers
    {
        public int AutoId { get; set; }

        public string Co_Code { get; set; }

        public string comp_name { get; set; }

        public DateTime upd_time { get; set; }

        public string open_price { get; set; }

        public string high_price { get; set; }

        public string low_price	 { get; set; }

        public string price	{ get; set; }

        public string pricediff { get; set; }

        public string change { get; set; }
        
        public string Contribution { get; set; }
    }
}