﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class CmotsCompanyChart5Y
    {
        [MaxLength(20)]
        public string co_code { get; set; }


        public string upd_date { get; set; }

        [MaxLength(20)]
        public string price { get; set; }

        [MaxLength(50)]
        public string volume { get; set; }

        [MaxLength(50)]
        public string created_by { get; set; }


        public DateTime created_on { get; set; }

        public string updated_by { get; set; }

        public DateTime updated_on { get; set; }
    }
}