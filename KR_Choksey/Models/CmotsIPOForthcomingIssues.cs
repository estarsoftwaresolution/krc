﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KR_Choksey.Models
{
    public class CmotsIPOForthcomingIssues
    {
        public string co_code { get; set; }

        public string opendate { get; set; }

        public string closdate { get; set; }

        public string parvalue { get; set; }

        public string issueprice { get; set; }

        public string issuepri2 { get; set; }

        public string lname { get; set; }

        public string issuesize { get; set; }

        public string issuetype { get; set; }

        public string issue { get; set; }

        public string daysleft { get; set; }

        public string Created_By { get; set; }

        public DateTime Created_On { get; set; }
    }
}