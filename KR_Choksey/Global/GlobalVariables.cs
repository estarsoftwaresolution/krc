﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace KR_Choksey.Global
{
    public class GlobalVariables
    {
        public static string CompanyName;
        public static string Address1;
        public static string Address2;
        public static string Address3;
        public static string City;
        public static string State;
        public static string Country;
        public static string PinCode;
        public static string ContactNo;
        public static string CompanyLogo;
        public static string CompanyTheme;
        public static string DecorationRate;
        public static string WifiValetParkingRate;

        public static int gRowsPerPage;

        public static string PGUserName;
        public static string PGPassword;
        public static string PGDatabase;
        public static int PGPort;
        public static string PGServer;


        //public static string gSessionId;

        public static bool gLicenseTrail;

        public static int gBanquetLimit;
        public static int gUserLimit;
        public static int gFoodMenuLimit;

        public static string gUserType;


        public static SqlConnection gConn;

        public static Stopwatch stopWatch = new Stopwatch();


    }
}