﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KR_Choksey.Global
{
    public class DropDownSelection
    {
        public static string DropDownSelect = "---Please Select---";


        public List<SelectListItem> GetCategory()
        {
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                gConnection.Open();
                string sqlstr = "select ISNULL(CategoryName,'') As CategoryName from SpaCategoryMaster";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    objSelLst.Add(new SelectListItem
                    {
                        Text = sdr["CategoryName"].ToString(),
                        Value = sdr["CategoryName"].ToString()
                    });
                }
                gConnection.Close();
            }
            catch(Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            return objSelLst;
        }

        public List<SelectListItem> QualificationList()
        {
            List<SelectListItem> objRowsList = new List<SelectListItem>();

            objRowsList.Add(new SelectListItem
            {
                Text = "Under Graduate",
                Value = "Under Graduate"
            });
            objRowsList.Add(new SelectListItem
            {
                Text = "Graduate",
                Value = "Graduate"
            });
            objRowsList.Add(new SelectListItem
            {
                Text = "Post Graduate",
                Value = "Post Graduate"
            });
            objRowsList.Add(new SelectListItem
            {
                Text = "Professional",
                Value = "Professional"
            });
            return objRowsList;
        }


        public List<SelectListItem> OccupationList()
        {
            List<SelectListItem> objRowsList = new List<SelectListItem>();

            objRowsList.Add(new SelectListItem
            {
                Text = "Agriculture",
                Value = "Agriculture"
            });
            objRowsList.Add(new SelectListItem
            {
                Text = "Business",
                Value = "Business"
            });
            objRowsList.Add(new SelectListItem
            {
                Text = "Housewife",
                Value = "Housewife"
            });
            objRowsList.Add(new SelectListItem
            {
                Text = "Not Defined",
                Value = "Not Defined"
            });
            objRowsList.Add(new SelectListItem
            {
                Text = "Private",
                Value = "Private"
            });
            return objRowsList;
        }

        public List<SelectListItem> IncomeSlabList()
        {
            List<SelectListItem> objRowsList = new List<SelectListItem>();

            objRowsList.Add(new SelectListItem
            {
                Text = "0-1 Lac",
                Value = "0-1 Lac"
            });
            objRowsList.Add(new SelectListItem
            {
                Text = "1-5 Lac",
                Value = "1-5 Lac"
            });
            objRowsList.Add(new SelectListItem
            {
                Text = "5-10 Lac",
                Value = "5-10 Lac"
            });
            objRowsList.Add(new SelectListItem
            {
                Text = "10-25 Lac",
                Value = "10-25 Lac"
            });
            objRowsList.Add(new SelectListItem
            {
                Text = "25 Lac - 1Crore",
                Value = "25 Lac - 1Crore"
            });
            objRowsList.Add(new SelectListItem
            {
                Text = "Above 1 Crore",
                Value = "Above 1 Crore"
            });
            return objRowsList;
        }

        public List<SelectListItem> GetAllCityList()
        {
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            try
            {
                gConnection.Open();
                string sqlstr = "Select Distinct Isnull(DistrictName,'') as DistrictName From dbo.IndianPostalDetails order by DistrictName";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objSelLst.Add(new SelectListItem
                    {
                        Text = sdr["DistrictName"].ToString(),
                        Value = sdr["DistrictName"].ToString()
                    });
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            return objSelLst;
        }

        public List<SelectListItem> GetAllStateList(string City)
        {
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            if (City == null)
            {
                City = "";
            }
            try
            {
                gConnection.Open();
                string sqlstr = "Select Distinct Isnull(StateName,'') as StateName From dbo.IndianPostalDetails Where DistrictName Like @DistrictName order by StateName";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@DistrictName", '%'+City+'%');
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objSelLst.Add(new SelectListItem
                    {
                        Text = sdr["StateName"].ToString(),
                        Value = sdr["StateName"].ToString()
                    });
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            return objSelLst;
        }

        public List<SelectListItem> GetAllCountryList(string StateName)
        {
            List<SelectListItem> objSelLst = new List<SelectListItem>();
            if (StateName == null)
            {
                StateName = "";
            }
            try
            {
                gConnection.Open();
                string sqlstr = "Select Distinct Isnull(CountryName,'') as CountryName From dbo.IndianPostalDetails Where StateName Like @StateName order by CountryName";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@StateName", '%' + StateName + '%');
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objSelLst.Add(new SelectListItem
                    {
                        Text = sdr["CountryName"].ToString(),
                        Value = sdr["CountryName"].ToString()
                    });
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            return objSelLst;
        }
      
        
    }
}