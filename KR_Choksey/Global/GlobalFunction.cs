﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Configuration;
using System.Globalization;
using System.Net.NetworkInformation;
using EnCryptDecrypt;
using System.Data.SqlClient;
using KR_Choksey.Global;
using System.Net.Mail;
using KR_Choksey.Models;
using System.Net;


namespace KR_Choksey.Global
{
    public class GlobalFunction
    {

        private WebProxy objProxy1 = null;



        static private readonly byte[] Key = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };
        static private readonly byte[] Iv8Bit = { 1, 2, 3, 4, 5, 6, 7, 8 };

        public string TripleDesEncryption(string datatoEncrypt)
        {


            var bytes = Encoding.Default.GetBytes(datatoEncrypt);
            using (var tripleDes = new TripleDESCryptoServiceProvider())
            {
                using (var ms = new MemoryStream())
                using (var encryptor = tripleDes.CreateEncryptor(Key, Iv8Bit))
                using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                {
                    cs.Write(bytes, 0, bytes.Length);
                    cs.FlushFinalBlock();
                    var cipher = ms.ToArray();
                    return Convert.ToBase64String(cipher);
                }
            }
        }



        


        #region Get Number Of Week

        public int GetIso8601WeekOfYear(DateTime time)
        {
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        #endregion



        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }




        public string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card  
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            } return sMacAddress;
        }

        public bool CheckSessionId(string SessionId, string UserId)
        {
            bool result = false;
            string sqlstr = "select Isnull(user_id,'') as user_id, Isnull(session_id,'') as session_id from SpaUserSessionDetails where user_id=@user_id and session_id=@session_id";

            gConnection.Open();

            SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
            cmd.Parameters.AddWithValue("@user_id", UserId);
            cmd.Parameters.AddWithValue("@session_id", SessionId);
            SqlDataReader sdr = cmd.ExecuteReader();
            while (sdr.Read())
            {
                result = true;
            }
            gConnection.Close();
            return result;
        }




        private decimal GetOpeningBalanceBeforeAParticularDate(DateTime OpDate)
        {
            decimal OB = 0;
            try
            {
                decimal Bill = 0;
                decimal Amt = 0;
                gConnection.Open();
                string sqlstr = "select (Isnull(SUM(NetTotal),0)) As NetTotal From SpaBilling Where PaymentMode='CASH' And CreatedOn<@OpDate";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@OpDate", OpDate);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Bill = Convert.ToDecimal(sdr["NetTotal"].ToString());
                }
                sdr.Close();

                sqlstr = "Select (Isnull(SUM(Amount),0)) As Amount From SpaExpenseDetails Where ExpenseDate<@OpDate";
                SqlCommand cmd1 = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd1.Parameters.AddWithValue("@OpDate", OpDate);
                SqlDataReader sdr1 = cmd1.ExecuteReader();
                while (sdr1.Read())
                {
                    Amt = Convert.ToDecimal(sdr1["Amount"].ToString());
                }
                gConnection.Close();
                OB = Bill - Amt;
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
               
            }
            return OB;
        }


        public bool InsertUserNavigation(string UserId, string MenuName, string IP)
        {
            bool result = false;
            try
            {
                if (UserId == null)
                {
                    UserId = "";
                    UserId = HttpContext.Current.Request.Cookies.AllKeys.Contains("UnSignedUserId").ToString();
                }
                if (UserId != "")
                {
                    gConnection.Open();
                    string sqlstr = "Insert into UserNavigation(UserId,Menu,CreatedOn,CreatedBy, IP)";
                    sqlstr = sqlstr + " values (@UserId,@Menu,GetDate(),@CreatedBy, @IP)";
                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@Menu", MenuName);
                    cmd.Parameters.AddWithValue("@CreatedBy", UserId);
                    cmd.Parameters.AddWithValue("@IP", IP);
                    cmd.ExecuteNonQuery();
                    gConnection.Close();
                    result = true;
                }
                else
                {
                    //string cookie = "There is no cookie!";
                    //if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("UnSignedUserId"))
                    //{
                    //    cookie = this.ControllerContext.HttpContext.Request.Cookies["UnSignedUserId"].Value;
                    //    Session["UserId"] = cookie;
                    //}
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }


        public bool UpdateUserNavigation(string UserId, string MenuName, string IP, string TimeSpent)
        {
            bool result = false;
            try
            {
                if (UserId == null)
                {
                    UserId = "";
                    UserId = HttpContext.Current.Request.Cookies.AllKeys.Contains("UnSignedUserId").ToString();
                }
                if (UserId != "")
                {
                    gConnection.Open();
                    string sqlstr = "update UserNavigation set TimeSpent=@TimeSpent where USerNavId=";
                    sqlstr = sqlstr + " (select Max(USerNavId) from UserNavigation where UserId=@UserId and Menu=@Menu and IP=@IP)";
                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@Menu", MenuName);
                    cmd.Parameters.AddWithValue("@TimeSpent", TimeSpent);
                    cmd.Parameters.AddWithValue("@IP", IP);
                    cmd.ExecuteNonQuery();
                    gConnection.Close();
                    result = true;
                }
                else
                {
                    //string cookie = "There is no cookie!";
                    //if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("UnSignedUserId"))
                    //{
                    //    cookie = this.ControllerContext.HttpContext.Request.Cookies["UnSignedUserId"].Value;
                    //    Session["UserId"] = cookie;
                    //}
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public bool UpdateCompCodeForNavigation(string UserId, string MenuName, string IP, string CoCode)
        {
            bool result = false;
            try
            {
                if (UserId == null)
                {
                    UserId = "";
                    UserId = HttpContext.Current.Request.Cookies.AllKeys.Contains("UnSignedUserId").ToString();
                }
                if (UserId != "")
                {
                    gConnection.Open();
                    string sqlstr = "update UserNavigation set CompanyCodeSearched=@CoCode where USerNavId=";
                    sqlstr = sqlstr + " (select Max(USerNavId) from UserNavigation where UserId=@UserId and Menu=@Menu and IP=@IP)";
                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@Menu", MenuName);
                    cmd.Parameters.AddWithValue("@CoCode", CoCode);
                    cmd.Parameters.AddWithValue("@IP", IP);
                    cmd.ExecuteNonQuery();
                    gConnection.Close();
                    result = true;
                }
                else
                {
                    //string cookie = "There is no cookie!";
                    //if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("UnSignedUserId"))
                    //{
                    //    cookie = this.ControllerContext.HttpContext.Request.Cookies["UnSignedUserId"].Value;
                    //    Session["UserId"] = cookie;
                    //}
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }


        public bool UpdateCompCodeForSignedInNavigation(string UserId, string MenuName, string IP, string CoCode)
        {
            bool result = false;
            try
            {
                if (UserId == null)
                {
                    UserId = "";
                    UserId = HttpContext.Current.Request.Cookies.AllKeys.Contains("OldSignedUserId").ToString();
                }
                if (UserId != "")
                {
                    gConnection.Open();
                    string sqlstr = "update UserNavigation_Signed set CompanyCodeSearched=@CoCode where USerNav_SignedId=";
                    sqlstr = sqlstr + " (select Max(USerNav_SignedId) from UserNavigation_Signed where UserId=@UserId and Menu=@Menu and IP=@IP)";
                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@Menu", MenuName);
                    cmd.Parameters.AddWithValue("@CoCode", CoCode);
                    cmd.Parameters.AddWithValue("@IP", IP);
                    cmd.ExecuteNonQuery();
                    gConnection.Close();
                    result = true;
                }
                else
                {
                    //string cookie = "There is no cookie!";
                    //if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("UnSignedUserId"))
                    //{
                    //    cookie = this.ControllerContext.HttpContext.Request.Cookies["UnSignedUserId"].Value;
                    //    Session["UserId"] = cookie;
                    //}
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }



        public bool InsertUserNavigation_Signed(string UserId, string MenuName, string IP)
        {
            bool result = false;
            try
            {
                if (UserId == null)
                {
                    UserId = "";
                    UserId = HttpContext.Current.Request.Cookies.AllKeys.Contains("UnSignedUserId").ToString();
                }
                if (UserId != "")
                {
                    gConnection.Open();
                    string sqlstr = "Insert into UserNavigation_Signed(UserId,Menu,CreatedOn,CreatedBy, IP)";
                    sqlstr = sqlstr + " values (@UserId,@Menu,GetDate(),@CreatedBy, @IP)";
                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@Menu", MenuName);
                    cmd.Parameters.AddWithValue("@CreatedBy", UserId);
                    cmd.Parameters.AddWithValue("@IP", IP);
                    cmd.ExecuteNonQuery();
                    gConnection.Close();
                    result = true;
                }
                else
                {
                    //string cookie = "There is no cookie!";
                    //if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("UnSignedUserId"))
                    //{
                    //    cookie = this.ControllerContext.HttpContext.Request.Cookies["UnSignedUserId"].Value;
                    //    Session["UserId"] = cookie;
                    //}
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }


        public bool UpdateUserNavigation_Signed(string UserId, string MenuName, string IP, string TimeSpent)
        {
            bool result = false;
            try
            {
                if (UserId == null)
                {
                    UserId = "";
                    UserId = HttpContext.Current.Request.Cookies.AllKeys.Contains("UnSignedUserId").ToString();
                }
                if (UserId != "")
                {
                    gConnection.Open();
                    string sqlstr = "update UserNavigation_Signed set TimeSpent=@TimeSpent where UserNav_SignedId=";
                    sqlstr = sqlstr + " (select Max(USerNav_SignedId) from UserNavigation_Signed where UserId=@UserId and Menu=@Menu and IP=@IP)";
                    SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@Menu", MenuName);
                    cmd.Parameters.AddWithValue("@TimeSpent", TimeSpent);
                    cmd.Parameters.AddWithValue("@IP", IP);
                    cmd.ExecuteNonQuery();
                    gConnection.Close();
                    result = true;
                }
                else
                {
                    //string cookie = "There is no cookie!";
                    //if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("UnSignedUserId"))
                    //{
                    //    cookie = this.ControllerContext.HttpContext.Request.Cookies["UnSignedUserId"].Value;
                    //    Session["UserId"] = cookie;
                    //}
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }



        public string CreateUnSignedUserDetails()
        {
            string UserId = "";
            try
            {
                gConnection.Open();
                string sqlstr = "Insert into UnSignedUserDetails(UserLocation,CreatedBy,CreatedOn)";
                sqlstr = sqlstr + " values(@UserLocation, @CreatedBy, GETDATE())";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@UserLocation", "Kolkata");
                cmd.Parameters.AddWithValue("@CreatedBy", "Auto");
                cmd.ExecuteNonQuery();

                sqlstr = "select MAX(UserId) As UserId From UnSignedUserDetails";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    UserId = sdr["UserId"].ToString();
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {

            }
            return UserId;
        }


        public string SendSMS(string Mobile_Number, string MessageBody)
        {


            string Username = ConfigurationManager.AppSettings["SMSUsername"];
            string password = ConfigurationManager.AppSettings["SMSPassword"];
            string senderid="KRCSEC";
            
            string stringpost = null;            
            stringpost = "Username=" + Username + "&Password=" + password + "&MessageType=txt&Mobile=" + Mobile_Number + "&SenderID=" + senderid + "&Message=" + MessageBody;

            HttpWebRequest objWebRequest = null;
            HttpWebResponse objWebResponse = null;
            StreamWriter objStreamWriter = null;
            StreamReader objStreamReader = null;
            try
            {

                string stringResult = null;

                objWebRequest = (HttpWebRequest)WebRequest.Create("https://www.na.com");
                objWebRequest.Method = "POST";

                if ((objProxy1 != null))
                {
                    objWebRequest.Proxy = objProxy1;
                }

                objWebRequest.ContentType = "application/x-www-form-urlencoded";

                objStreamWriter = new StreamWriter(objWebRequest.GetRequestStream());
                objStreamWriter.Write(stringpost);
                objStreamWriter.Flush();
                objStreamWriter.Close();

                objWebResponse = (HttpWebResponse)objWebRequest.GetResponse();
                objStreamReader = new StreamReader(objWebResponse.GetResponseStream());
                stringResult = objStreamReader.ReadToEnd();

                objStreamReader.Close();
                return stringResult;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {

                if ((objStreamWriter != null))
                {
                    objStreamWriter.Close();
                }
                if ((objStreamReader != null))
                {
                    objStreamReader.Close();
                }
                objWebRequest = null;
                objWebResponse = null;
                objProxy1 = null;
            }
        }

        #region Send Mail 

        public bool SendMail(string FromMail, string ToMail, string Subject, string Body, string UserName, string Password, string AttachmentPath)
        {
            bool result = false;
            try
            {

                MailMessage mail = new MailMessage();
                //put your SMTP address and port here.
                SmtpClient SmtpServer = new SmtpClient("smtp.na.in", 587);
                //Put the email address
                mail.From = new MailAddress(FromMail);
                //Put the email where you want to send.
                mail.To.Add(ToMail);

                mail.Subject = Subject;

                mail.Body = Body;
                

                if (AttachmentPath != "")
                {
                    //Your log file path
                    System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(AttachmentPath);

                    mail.Attachments.Add(attachment);
                }

                //Your username and password!
                SmtpServer.Credentials = new System.Net.NetworkCredential(UserName, Password);
                //Set Smtp Server port
                SmtpServer.Port = 25;
                //SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                //MessageBox.Show("Mail sent! :)");

                result = true;

                return result;
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error On Sending Mail!" + ex.Message, GlobalVariables.gCompanyName, MessageBoxButton.OK, MessageBoxImage.Stop);
                Global.ErrorHandlerClass.LogError(ex);
            }

            return result;
        }


        public bool SendMailForQuickContact(string FromMail, string ToMail, string Subject, string Body, string UserName, string Password, string AttachmentPath, string EMMutualFund, string EMInsurance, string EMFinancialPlanning, string EMIFund, string EMIPms, string EMIBrokerage, string EMILend, string EMIHold, string EMSubBrokerage)
        {
            bool result = false;
            try
            {

                MailMessage mail = new MailMessage();
                //put your SMTP address and port here.
                SmtpClient SmtpServer = new SmtpClient("smtp.na.in", 587);
                //Put the email address
                mail.From = new MailAddress(FromMail);
                //Put the email where you want to send.
                mail.To.Add(ToMail);

                if (EMMutualFund != "")
                {
                    mail.To.Add(EMMutualFund);
                }
                if (EMInsurance != "")
                {
                    mail.To.Add(EMInsurance);
                }
                if (EMFinancialPlanning != "")
                {
                    mail.To.Add(EMFinancialPlanning);
                }
                if (EMIFund != "")
                {
                    mail.To.Add(EMIFund);
                }
                if (EMIPms != "")
                {
                    mail.To.Add(EMIPms);
                }
                if (EMIBrokerage != "")
                {
                    mail.To.Add(EMIBrokerage);
                }
                if (EMILend != "")
                {
                    mail.To.Add(EMILend);
                }
                if (EMIHold != "")
                {
                    mail.To.Add(EMIHold);
                }
                if (EMSubBrokerage != "")
                {
                    mail.To.Add(EMSubBrokerage);
                }

                mail.Subject = Subject;

                mail.Body = Body;


                if (AttachmentPath != "")
                {
                    //Your log file path
                    System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(AttachmentPath);

                    mail.Attachments.Add(attachment);
                }

                //Your username and password!
                SmtpServer.Credentials = new System.Net.NetworkCredential(UserName, Password);
                //Set Smtp Server port
                SmtpServer.Port = 25;
                //SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                //MessageBox.Show("Mail sent! :)");

                result = true;

                return result;
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error On Sending Mail!" + ex.Message, GlobalVariables.gCompanyName, MessageBoxButton.OK, MessageBoxImage.Stop);
                Global.ErrorHandlerClass.LogError(ex);
            }

            return result;
        }


        public bool SendMailForQuickContact_IEquity(string FromMail, string ToMail, string Subject, string Body, string UserName, string Password, string AttachmentPath, string EMMutualFund, string EMInsurance, string EMFinancialPlanning, string EMIFund, string EMIPms, string EMIBrokerage, string EMILend, string EMIHold, string EMSubBrokerage, string EMIequity)
        {
            bool result = false;
            try
            {

                MailMessage mail = new MailMessage();
                //put your SMTP address and port here.
                SmtpClient SmtpServer = new SmtpClient("smtp.na.in", 587);
                //Put the email address
                mail.From = new MailAddress(FromMail);
                //Put the email where you want to send.
                mail.To.Add(ToMail);

                if (EMIequity != "")
                {
                    mail.To.Add(EMIequity);
                }

                if (EMMutualFund != "")
                {
                    mail.To.Add(EMMutualFund);
                }
                if (EMInsurance != "")
                {
                    mail.To.Add(EMInsurance);
                }
                if (EMFinancialPlanning != "")
                {
                    mail.To.Add(EMFinancialPlanning);
                }
                if (EMIFund != "")
                {
                    mail.To.Add(EMIFund);
                }
                if (EMIPms != "")
                {
                    mail.To.Add(EMIPms);
                }
                if (EMIBrokerage != "")
                {
                    mail.To.Add(EMIBrokerage);
                }
                if (EMILend != "")
                {
                    mail.To.Add(EMILend);
                }
                if (EMIHold != "")
                {
                    mail.To.Add(EMIHold);
                }
                if (EMSubBrokerage != "")
                {
                    mail.To.Add(EMSubBrokerage);
                }

                

                mail.Subject = Subject;

                mail.Body = Body;


                if (AttachmentPath != "")
                {
                    //Your log file path
                    System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(AttachmentPath);

                    mail.Attachments.Add(attachment);
                }

                //Your username and password!
                SmtpServer.Credentials = new System.Net.NetworkCredential(UserName, Password);
                //Set Smtp Server port
                SmtpServer.Port = 25;
                //SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                //MessageBox.Show("Mail sent! :)");

                result = true;

                return result;
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error On Sending Mail!" + ex.Message, GlobalVariables.gCompanyName, MessageBoxButton.OK, MessageBoxImage.Stop);
                Global.ErrorHandlerClass.LogError(ex);
            }

            return result;
        }



        public bool SendAcknowledgementMail(string FromMail, string ToMail, string Subject, string Body, string UserName, string Password, string AttachmentPath)
        {
            bool result = false;
            try
            {

                MailMessage mail = new MailMessage();
                //put your SMTP address and port here.
                SmtpClient SmtpServer = new SmtpClient("smtp.na.in", 587);
                //Put the email address
                mail.From = new MailAddress(FromMail);
                //Put the email where you want to send.
                mail.To.Add(ToMail);

                mail.Subject = Subject;

                mail.Body = Body;


                if (AttachmentPath != "")
                {
                    //Your log file path
                    System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(AttachmentPath);

                    mail.Attachments.Add(attachment);
                }

                //Your username and password!
                SmtpServer.Credentials = new System.Net.NetworkCredential(UserName, Password);
                //Set Smtp Server port
                SmtpServer.Port = 25;
                //SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                //MessageBox.Show("Mail sent! :)");

                result = true;

                return result;
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error On Sending Mail!" + ex.Message, GlobalVariables.gCompanyName, MessageBoxButton.OK, MessageBoxImage.Stop);
                Global.ErrorHandlerClass.LogError(ex);
            }

            return result;
        }


        public bool SendAcknowledgementMail_New(string FromMail, string ToMail, string Subject, string Body, string UserName, string Password, string AttachmentPath)
        {
            bool result = false;
            try
            {

                MailMessage mail = new MailMessage();
                //put your SMTP address and port here.
                SmtpClient SmtpServer = new SmtpClient("smtp.na.in", 587);
                //Put the email address
                mail.From = new MailAddress(FromMail);
                //Put the email where you want to send.
                mail.To.Add(ToMail);

                mail.Subject = Subject;

                mail.Body = Body;
                mail.IsBodyHtml = true;


                if (AttachmentPath != "")
                {
                    //Your log file path
                    System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(AttachmentPath);

                    mail.Attachments.Add(attachment);
                }

                //Your username and password!
                SmtpServer.Credentials = new System.Net.NetworkCredential(UserName, Password);
                //Set Smtp Server port
                SmtpServer.Port = 25;
                //SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                //MessageBox.Show("Mail sent! :)");

                result = true;

                return result;
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error On Sending Mail!" + ex.Message, GlobalVariables.gCompanyName, MessageBoxButton.OK, MessageBoxImage.Stop);
                Global.ErrorHandlerClass.LogError(ex);
            }

            return result;
        }



        public void InsertAcknowledgementMailLog(string FromMail, string ToMail, string Success, string SendFromPage)
        {
            try
            {
                gConnection.Open();
                string sqlstr = "Insert Into AcknowledgementMailLog(FromMail,ToMail,Success,SendFromPage,CreatedOn)";
                sqlstr = sqlstr + " values (@FromMail,@ToMail,@Success,@SendFromPage,GetDate())";
                gConnection.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@FromMail", FromMail);
                cmd.Parameters.AddWithValue("@ToMail", ToMail);
                cmd.Parameters.AddWithValue("@Success", Success);
                cmd.Parameters.AddWithValue("@SendFromPage", SendFromPage);
                cmd.ExecuteNonQuery();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
        }



        #endregion

        public Client_Master GetClientMasterFromClientId(string Id)
        {
            Client_Master objCli = new Client_Master();
            try
            {
                gConnection.Open();
                string sqlstr = "select client_master_code,first_name,last_name,bo_email, bo_mobile from Client_Master where client_master_code=@client_master_code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@client_master_code", Id);
                SqlDataReader sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    objCli.client_master_code = Convert.ToInt32(sdr["client_master_code"].ToString());
                    objCli.first_name = sdr["first_name"].ToString();
                    objCli.last_name = sdr["last_name"].ToString();
                    objCli.bo_email = sdr["bo_email"].ToString();
                    objCli.bo_mobile = sdr["bo_mobile"].ToString();

                }
                gConnection.Close();
            }
            catch(Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return objCli;
        }
        
        public IndianPostalDetails GetAllDetailsByPinCode(string PinCode)
        {
            IndianPostalDetails objIPD = new IndianPostalDetails();
            try
            {
                string sqlstr = "select Isnull(officename,'') as officename, Isnull(pincode,'') as pincode, Isnull(officeType,'') As officeType, ";
                sqlstr = sqlstr + " Isnull(DeliveryStatus,'') As DeliveryStatus, Isnull(DivisionName,'') As DivisionName, Isnull(RegionName,'') As RegionName, ";
                sqlstr = sqlstr + " Isnull(CircleName,'') As CircleName, Isnull(Taluk,'') as Taluk, Isnull(DistrictName,'') As DistrictName, ";
                sqlstr = sqlstr + " Isnull(StateName,'') as StateName, Isnull(CountryName,'') As CountryName from IndianPostalDetails Where Isnull(pincode,'')=@pincode";
                gConnection.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@pincode", PinCode);
                SqlDataReader sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    objIPD.officename = sdr["officename"].ToString();
                    objIPD.pincode = sdr["pincode"].ToString();
                    objIPD.officeType = sdr["officeType"].ToString();
                    objIPD.DeliveryStatus = sdr["DeliveryStatus"].ToString();
                    objIPD.DivisionName = sdr["DivisionName"].ToString();
                    objIPD.RegionName = sdr["RegionName"].ToString();
                    objIPD.CircleName = sdr["CircleName"].ToString();
                    objIPD.Taluk = sdr["Taluk"].ToString();
                    objIPD.DistrictName = sdr["DistrictName"].ToString();
                    objIPD.StateName = sdr["StateName"].ToString();
                    objIPD.CountryName = sdr["CountryName"].ToString();
                }

                gConnection.Close();
            }
            catch(Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }
            return objIPD;
        }


        public string MonthName(string mnth)
        {
            string month = string.Empty;
            if (mnth == "01")
            {
                month = "January";
            }
            if (mnth == "02")
            {
                month = "February";
            }
            if (mnth == "03")
            {
                month = "March";
            }
            if (mnth == "04")
            {
                month = "April";
            }
            if (mnth == "05")
            {
                month = "May";
            }
            if (mnth == "06")
            {
                month = "June";
            }
            if (mnth == "07")
            {
                month = "July";
            }
            if (mnth == "08")
            {
                month = "August";
            }
            if (mnth == "09")
            {
                month = "September";
            }
            if (mnth == "10")
            {
                month = "October";
            }
            if (mnth == "11")
            {
                month = "November";
            }
            if (mnth == "12")
            {
                month = "December";
            }
            return month;
        }


        #region Send Easy Invest Mail For Client

        public bool SendEasyInvestMail_Client(string FromMail, string ToMail, string Subject, string Body, string UserName, string Password, string AttachmentPath)
        {
            bool result = false;
            try
            {

                MailMessage mail = new MailMessage();
                //put your SMTP address and port here.
                SmtpClient SmtpServer = new SmtpClient("smtp.na.in", 587);
                //Put the email address
                mail.From = new MailAddress(FromMail);
                //Put the email where you want to send.
                mail.To.Add(ToMail);

                mail.Subject = Subject;

                mail.Body = Body;
                mail.IsBodyHtml = true;

                if (AttachmentPath != "")
                {
                    //Your log file path
                    System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(AttachmentPath);

                    mail.Attachments.Add(attachment);
                }

                //Your username and password!
                SmtpServer.Credentials = new System.Net.NetworkCredential(UserName, Password);
                //Set Smtp Server port
                SmtpServer.Port = 25;
                //SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                //MessageBox.Show("Mail sent! :)");

                result = true;

                return result;
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error On Sending Mail!" + ex.Message, GlobalVariables.gCompanyName, MessageBoxButton.OK, MessageBoxImage.Stop);
                Global.ErrorHandlerClass.LogError(ex);
            }

            return result;
        }

        #endregion

    }




}