﻿using KR_Choksey.Global;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace KR_Choksey.Global
{
    public static class gConnection
    {
        public static SqlConnection Open()
        {
            ConnectionStringSettings connSettings = ConfigurationManager.ConnectionStrings["con1"];
            string connString = connSettings.ConnectionString;

            GlobalVariables.gConn = new SqlConnection(connString);
            //GlobalVariables.gConn = new NpgsqlConnection(GlobalVariables.gConnectionString);
            GlobalVariables.gConn.Open();

            return GlobalVariables.gConn;
        }


        public static SqlConnection Close()
        {
            GlobalVariables.gConn.Close();
            return GlobalVariables.gConn;
        }
    }

    //public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
    //{
    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "*");
    //        base.OnActionExecuting(filterContext);
    //        //var ctx = filterContext.RequestContext.HttpContext;
    //        //var origin = ctx.Request.Headers["http://krchokseyapi.cmlinks.com/Equity/Equity.svc/IntradayChart/NIFTY"];
    //        //var allowOrigin = !string.IsNullOrWhiteSpace(origin) ? origin : "*";
    //        ////ctx.Response.AddHeader("Access-Control-Allow-Origin", allowOrigin);
    //        //ctx.Response.AddHeader("Access-Control-Allow-Origin", "*");
    //        //ctx.Response.AddHeader("Access-Control-Allow-Headers", "*");
    //        //ctx.Response.AddHeader("Access-Control-Allow-Credentials", "true");
    //        //base.OnActionExecuting(filterContext);
    //    }
    //}
}