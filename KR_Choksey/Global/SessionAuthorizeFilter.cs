﻿using KR_Choksey.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Global
{
    public class SessionAuthorizeFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            GlobalFunction objGFunc = new GlobalFunction();
            if (HttpContext.Current.Session.Count == 0)
            {                
                //string redirectTo = "~/SignIn/SignIn";
                //ActionResult result = new RedirectResult(redirectTo);
                //filterContext.Result = result;
            }
            //else if (objGFunc.CheckSessionId(HttpContext.Current.Session.SessionID, HttpContext.Current.Session["UserId"].ToString()) == false)
            //{
            //    string redirectTo = "~/SignIn/SignIn";
            //    ActionResult result = new RedirectResult(redirectTo);
            //    filterContext.Result = result;

            //}
            base.OnActionExecuting(filterContext);
        }        
    }


    public class CustomActionFilter : ActionFilterAttribute, IActionFilter
    {
        GlobalFunction objGFunc = new GlobalFunction();

        public string IdParamName { get; set; }

        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {

            var executionTime1 = GlobalVariables.stopWatch.ElapsedMilliseconds;
            var executionTime = executionTime1 / 1000;
            GlobalVariables.stopWatch.Reset();
            GlobalVariables.stopWatch.Start();
            // TODO: Add your acction filter's tasks here

            // Log Action Filter Call
            string Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            string Action = filterContext.ActionDescriptor.ActionName;
            string IP = filterContext.HttpContext.Request.UserHostAddress;
            DateTime DT = filterContext.HttpContext.Timestamp;




            string OldMenuName = "";
            string OldIP = "";
            string OldUserId = "";
            if (System.Web.HttpContext.Current.Session["OldMenuName"] != null)
            {

                OldMenuName = System.Web.HttpContext.Current.Session["OldMenuName"].ToString();
                OldIP = System.Web.HttpContext.Current.Session["OldIP"].ToString();
                OldUserId = System.Web.HttpContext.Current.Session["OldUserId"].ToString();
            }

            System.Web.HttpContext.Current.Session["OldMenuName"] = Controller + ", " + Action;
            System.Web.HttpContext.Current.Session["OldIP"] = IP;

            // Code For Signed User
            string UserIdSigned = "";
            if (System.Web.HttpContext.Current.Session["UserId"] != null)
            {
                UserIdSigned = System.Web.HttpContext.Current.Session["UserId"].ToString();
                if (System.Web.HttpContext.Current.Session["OldSignedUserId"] != null)
                {
                    OldUserId = System.Web.HttpContext.Current.Session["OldSignedUserId"].ToString();
                }
                System.Web.HttpContext.Current.Session["OldSignedUserId"] = UserIdSigned;
            }


            if (UserIdSigned != "")
            {
                if (objGFunc.InsertUserNavigation_Signed(UserIdSigned, Controller + ", " + Action, IP) == false)
                {
                }
                if (OldMenuName != "")
                {
                    if (OldUserId != "")
                    {
                        if (objGFunc.UpdateUserNavigation_Signed(OldUserId, OldMenuName, OldIP, executionTime.ToString()) == true)
                        {
                        }
                    }
                }
            }
            else
            {
                string UserId = "";
                if (filterContext.HttpContext.Request.Cookies["UnSignedUserId"] != null)
                {
                    UserId = filterContext.HttpContext.Request.Cookies["UnSignedUserId"].Value;
                    System.Web.HttpContext.Current.Session["OldUserId"] = UserId;
                    //HttpContext.Current.Session["NotifyUserId"] = UserId;
                }
                if (UserId == "")
                {
                    UserId = objGFunc.CreateUnSignedUserDetails();
                    HttpCookie cookie1 = new HttpCookie("UnSignedUserId");
                    cookie1.Value = UserId;
                    cookie1.Expires.AddDays(365);
                    filterContext.HttpContext.Response.Cookies.Add(cookie1);
                    System.Web.HttpContext.Current.Session["OldUserId"] = UserId;
                }

                if (OldMenuName != "")
                {
                    if (OldUserId != "")
                    {
                        if (objGFunc.UpdateUserNavigation(OldUserId, OldMenuName, OldIP, executionTime.ToString()) == true)
                        {
                        }
                    }
                }

                if (objGFunc.InsertUserNavigation(UserId, Controller + ", " + Action, IP) == false)
                {
                }
            }

            this.OnActionExecuting(filterContext);

        }


    }


    #region Old Code For CustomActionFilter

    //public class CustomActionFilter : ActionFilterAttribute, IActionFilter
    //{
    //    GlobalFunction objGFunc = new GlobalFunction();

    //    public string IdParamName { get; set; }
                
    //     void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        // TODO: Add your acction filter's tasks here

    //        // Log Action Filter Call
    //        string Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
    //        string Action = filterContext.ActionDescriptor.ActionName;
    //        string IP = filterContext.HttpContext.Request.UserHostAddress;
    //        DateTime DT = filterContext.HttpContext.Timestamp;


    //        // Code For Signed User
    //        string UserIdSigned = "";
    //        if (System.Web.HttpContext.Current.Session["UserId"] != null)
    //         {
    //             UserIdSigned = System.Web.HttpContext.Current.Session["UserId"].ToString();

    //         }

    //        //HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
    //        //if (filterContext.HttpContext.Request.Cookies["SignedUserId"] == null)
    //        //{
    //        //    if (UserIdSigned != "")
    //        //    {
    //        //        //UserIdSigned = filterContext.HttpContext.Request.Cookies["SignedUserId"].Value;
    //        //        HttpCookie cookie1 = new HttpCookie("SignedUserId");
    //        //        cookie1.Value = UserIdSigned;
    //        //        cookie1.Expires.AddDays(365);
    //        //        filterContext.HttpContext.Response.Cookies.Add(cookie1);
    //        //    }
    //        //}
    //        //else if (filterContext.HttpContext.Request.Cookies["SignedUserId"] != null)
    //        //{
    //        //    UserIdSigned = filterContext.HttpContext.Request.Cookies["SignedUserId"].Value;
    //        //    System.Web.HttpContext.Current.Session["UserId"] = UserIdSigned;
    //        //}
            
    //        if (UserIdSigned != "")
    //        {
    //            if (objGFunc.InsertUserNavigation_Signed(UserIdSigned, Controller + ", " + Action, IP) == false)
    //            {
    //            }
    //        }
    //        else
    //        {
    //            string UserId = "";
    //            if (filterContext.HttpContext.Request.Cookies["UnSignedUserId"] != null)
    //            {
    //                UserId = filterContext.HttpContext.Request.Cookies["UnSignedUserId"].Value;
    //                //HttpContext.Current.Session["NotifyUserId"] = UserId;
    //            }
    //            if (UserId == "")
    //            {
    //                UserId = objGFunc.CreateUnSignedUserDetails();
    //                HttpCookie cookie1 = new HttpCookie("UnSignedUserId");
    //                //HttpCookie cookie2 = new HttpCookie("KRCBID");
    //                cookie1.Value = UserId;
    //                cookie1.Expires.AddDays(365);
    //                //cookie2.Value = "";
    //                //cookie2.Expires.AddDays(365);
    //                filterContext.HttpContext.Response.Cookies.Add(cookie1);
    //                //filterContext.HttpContext.Response.Cookies.Add(cookie2);
    //            }
                
    //            if (objGFunc.InsertUserNavigation(UserId, Controller + ", " + Action, IP) == false)
    //            {
    //            }
    //        }

    //        this.OnActionExecuting(filterContext);

    //    }

         
    //}


    #endregion

}