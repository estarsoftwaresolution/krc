'use strict';
console.log('Started', self);

self.addEventListener('install', function (event) {
    self.skipWaiting();
    console.log('Installed', event);
});

self.addEventListener('activate', function (event) {
    console.log('Activated', event);
});


self.addEventListener('push', function (event) {
    event.waitUntil(
       self.registration.pushManager.getSubscription().then(function (subscription) {
           console.log(JSON.stringify(subscription));

           //fetch('Handler.ashx', {
           fetch('https://www.XXXXXXX.XXX/na/na', {
           //fetch('http://localhost:11546/na/na', {
               method: 'POST'
           }).then(function (response) {
               if (response.status != '200') {
                   console.log('Looks like there was a problem. Status Code: ' + response.status);
                   throw new Error();
               }

               return response.json().then(function (data) {

                   console.log(data);

                   var title1 = data.title;
                   var message1 = data.body;
                   var icon1 = data.icon;
                   var click_action1 = data.click_action;

                   self.addEventListener('notificationclick', function (event) {
                       event.notification.close();
                       event.waitUntil(self.clients.openWindow(click_action1));
                   });

                   return self.registration.showNotification(title1, {
                       body: message1,
                       icon: icon1
                   });
               });
           }).catch(function (err) {
               console.error('Unable to retrieve data', err);
           });
       })
     );
});
