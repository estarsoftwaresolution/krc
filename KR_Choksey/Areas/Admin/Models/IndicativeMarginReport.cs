﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Areas.Admin.Models
{
    public class IndicativeMarginReport
    {
        [Key]
        public int AutoId { get; set; }


        [Display(Name = "REPORT NAME")]
        [MaxLength(500)]
        public string ReportName { get; set; }


        [Display(Name = "REPORT PATH")]
        [MaxLength(500)]
        public string ReportPath { get; set; }


        [MaxLength(50)]
        public string CreatedBy { get; set; }


        public DateTime CreatedOn { get; set; }

    }
}