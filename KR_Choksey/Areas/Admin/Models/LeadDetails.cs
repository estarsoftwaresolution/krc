﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KR_Choksey.Areas.Admin.Models
{
    public class LeadDetails
    {
        public string Name { get; set; }
        public string EmailId { get; set; }
        public string PhoneNumber { get; set; }
        public string QuickTopic { get; set; }
        public string Screen { get; set; }
        public DateTime CreatedOn { get; set; }

        public string Products { get; set; }

    }
}