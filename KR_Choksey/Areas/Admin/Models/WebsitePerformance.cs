﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KR_Choksey.Areas.Admin.Models
{
    public class WebsitePerformance
    {
        public string PageName { get; set; }

        public string SignedUser { get; set; }

        public string SignedTimeSpent { get; set; }

        public string UnSignedUser { get; set; }

        public string UnSignedTimeSpent { get; set; }
    }
}