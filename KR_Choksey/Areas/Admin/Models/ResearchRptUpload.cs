﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KR_Choksey.Areas.Admin.Models
{
    public class ResearchRptUpload
    {
        [Key]
        public int AutoId { get; set; }

        
        [MaxLength(200)]
        [Required(ErrorMessage="Report Name Can't be blank!")]
        public string ReportName { get; set; }

        
        [MaxLength(500)]
        public string ReportPath { get; set; }

        
        [MaxLength(50)]
        public string PMS_Research { get; set; }

        
        [MaxLength(50)]
        public string ReportType { get; set; }

        
        [MaxLength(300)]
        public string FacebookLink { get; set; }

        
        [MaxLength(300)]
        public string TwitterLink { get; set; }

        
        [MaxLength(15)]
        public string CreatedBy { get; set; }


        public DateTime CreatedOn { get; set; }


        [MaxLength(500)]
        public string OriginalRptName { get; set; }

    }
}