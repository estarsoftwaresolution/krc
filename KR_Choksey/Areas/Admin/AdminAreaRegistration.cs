﻿using System.Web.Mvc;

namespace KR_Choksey.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
            //context.MapRoute(
            //    "Login",
            //    "Admin/Dashboard/Dashboard",
            //    new { controller = "AdminAccount", action = "Admin_Login", id = UrlParameter.Optional }
            //);
        }
    }
}