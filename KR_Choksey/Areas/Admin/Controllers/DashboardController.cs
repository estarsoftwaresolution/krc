﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Global;
using KR_Choksey.Areas.Admin.Models;
using KR_Choksey.Global;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text;
using System.Data.SqlClient;
using KR_Choksey.Models;

namespace KR_Choksey.Areas.Admin.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Admin/Dashboard
        public ActionResult Dashboard()
        {
            if (System.Web.HttpContext.Current.Session["AdminUserId"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Admin_Login", "AdminAccount");
            }
            return View();
        }

        public PartialViewResult WebsitePerformence()
        {
            List<WebsitePerformance> WebsitePerformance_List = new List<WebsitePerformance>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "select b.menu AS MENU, a.SignedCount,(a.SignedTimeSpent/a.SignedCount) AS SignedTimeSpent,b.UnSignedCount,(b.UnSignedTimeSpent/b.UnSignedCount) AS UnSignedTimeSpent from VW_UserNavigation_Signed a right outer join VW_UserNavigation b on a.Menu=b.Menu";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    WebsitePerformance Performance_List = new WebsitePerformance();
                    Performance_List.PageName = sdr["MENU"].ToString();
                    Performance_List.SignedUser = sdr["SignedCount"].ToString();
                    Performance_List.SignedTimeSpent = sdr["SignedTimeSpent"].ToString();
                    Performance_List.UnSignedUser = sdr["UnSignedCount"].ToString();
                    Performance_List.UnSignedTimeSpent = sdr["UnSignedTimeSpent"].ToString();
                    WebsitePerformance_List.Add(Performance_List);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }

            return PartialView(WebsitePerformance_List);
        }

        public PartialViewResult ContactDetails()
        {
            List<LeadDetails> LeadsContactDetails = new List<LeadDetails>();
            gConnection.Open();
            SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
            cmd.Connection = GlobalVariables.gConn;
            try
            {
                string sqlstr = "";
                sqlstr = "SELECT AutoId,Name,EmailId,PhoneNumber,QuickTopic,Screen,CreatedBy,CreatedOn, ";
                sqlstr = sqlstr + " ISNULL(Products,'') as Products from KrcQuickContacts order by CreatedOn DESC";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    LeadDetails Leads_List = new LeadDetails();
                    Leads_List.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    Leads_List.Name = sdr["Name"].ToString();
                    Leads_List.EmailId = sdr["EmailId"].ToString();
                    Leads_List.PhoneNumber = sdr["PhoneNumber"].ToString();
                    Leads_List.Screen = sdr["Screen"].ToString();
                    Leads_List.QuickTopic = sdr["QuickTopic"].ToString();
                    Leads_List.Products = sdr["Products"].ToString();
                    LeadsContactDetails.Add(Leads_List);
                }
                sdr.Close();
                gConnection.Close();
            }
            catch (Exception ex)
            {
                gConnection.Close();
                Global.ErrorHandlerClass.LogError(ex);
            }

            return PartialView(LeadsContactDetails);
        }

        public PartialViewResult RegisteredClients()
        {
            return PartialView();
        }

        [HttpPost]
        public JsonResult RegisteredClients(string id)
        {
            Client_Master objCliMa = new Client_Master();
            client_login objCliLog = new client_login();
            try
            {
                string sqlstr = "select client_master_code,bo_client_code,bo_branch_code,bo_sub_broker_code,bo_trader_code,bo_long_name,";
                sqlstr = sqlstr + " bo_short_name,bo_addr1,bo_addr2,bo_addr3,bo_city,bo_state,bo_zip,bo_country,bo_pan,bo_sebi_regn,";
                sqlstr = sqlstr + " bo_resi_phone1,bo_resi_phone2,bo_office_phone1,bo_office_phone2,bo_mobile,bo_fax,bo_email,bo_client_type,";
                sqlstr = sqlstr + " bo_client_status,bo_family,bo_region,bo_area,bo_gender,bo_dob,bo_introducer,bo_approver,bo_bank_name,";
                sqlstr = sqlstr + " bo_bank_branch_name,bo_bank_ac_type,bo_bank_ac_number,bo_depository_type,bo_client_dp_id,";
                sqlstr = sqlstr + " bo_poa,bo_depository_type2,bo_client_dp_id2,bo_poa2,bo_relationship_mgr,bo_sbu,bo_modified_by,bo_modified_on,";
                sqlstr = sqlstr + " Isnull(bo_active_date,'01/01/1900') as bo_active_date,bo_inactive_date,bo_ac_status,first_name,middle_name,last_name,aadhaar,occupation,";
                sqlstr = sqlstr + " income_slab,net_worth_slab,mf_id,pms_internal_code,skype_id,facebook_id,twitter_id,";
                sqlstr = sqlstr + " instagram_id,google_id,snapchat_id,linkedin_id,apple_id ";
                sqlstr = sqlstr + " from [Client_Master] where client_master_code = @client_master_code";
                gConnection.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@client_master_code", id);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objCliMa.client_master_code = Convert.ToInt32(sdr["client_master_code"].ToString());
                    objCliMa.bo_long_name = sdr["bo_long_name"].ToString();
                    objCliMa.bo_addr1 = sdr["bo_addr1"].ToString();
                    objCliMa.bo_addr2 = sdr["bo_addr2"].ToString();
                    objCliMa.bo_addr3 = sdr["bo_addr3"].ToString();
                    objCliMa.bo_city = sdr["bo_city"].ToString();
                    objCliMa.bo_state = sdr["bo_state"].ToString();
                    objCliMa.bo_country = sdr["bo_country"].ToString();
                    objCliMa.bo_zip = sdr["bo_zip"].ToString();
                    objCliMa.bo_mobile = sdr["bo_mobile"].ToString();
                    objCliMa.bo_email = sdr["bo_email"].ToString();
                    objCliMa.bo_pan = sdr["bo_pan"].ToString();
                    objCliMa.income_slab = sdr["income_slab"].ToString();
                    objCliMa.net_worth_slab = Convert.ToInt32(sdr["net_worth_slab"].ToString());
                    objCliMa.occupation = sdr["occupation"].ToString();
                    objCliMa.bo_active_date = Convert.ToDateTime(sdr["bo_active_date"].ToString());
                    objCliMa.mf_id = sdr["mf_id"].ToString();
                    objCliMa.aadhaar = sdr["aadhaar"].ToString();
                    objCliMa.skype_id = sdr["skype_id"].ToString();
                    objCliMa.facebook_id = sdr["facebook_id"].ToString();
                    objCliMa.twitter_id = sdr["twitter_id"].ToString();
                    objCliMa.instagram_id = sdr["instagram_id"].ToString();
                    objCliMa.google_id = sdr["google_id"].ToString();
                    objCliMa.snapchat_id = sdr["snapchat_id"].ToString();
                    objCliMa.linkedin_id = sdr["linkedin_id"].ToString();
                    objCliMa.apple_id = sdr["apple_id"].ToString();
                    objCliMa.bo_modified_by = sdr["bo_modified_by"].ToString();
                    objCliMa.bo_modified_on = Convert.ToDateTime(sdr["bo_modified_on"].ToString());
                }
                sdr.Close();

                //GEt User NAme From 
                sqlstr = "select username,last_access_date,enable_login from dbo.client_login where FK_client_master_code=@FK_client_master_code";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@FK_client_master_code", id);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objCliLog.username = sdr["username"].ToString();
                    objCliLog.last_access_date = Convert.ToDateTime(sdr["last_access_date"].ToString());
                    objCliLog.Enable_login = Convert.ToBoolean(sdr["enable_login"].ToString());
                }
                sdr.Close();


                // Get Product Mapping Data
                string Product = "";
                sqlstr = "select Isnull(equity,'') as equity,Isnull(fo,'') as fo, Isnull(currency,'') as currency, Isnull(commodities,'') as commodities,";
                sqlstr = sqlstr + " Isnull(research_funda,'') as research_funda, Isnull(research_tech,'') as research_tech, Isnull(mf,'') as mf,";
                sqlstr = sqlstr + " Isnull(insurance,'') as insurance, Isnull(pms,'') as pms, Isnull(fd,'') as fd, Isnull(bonds,'') as bonds,";
                sqlstr = sqlstr + " Isnull(margin_fund,'') as margin_fund,Isnull(las,'') as las, Isnull(i_hold,'') as i_hold, ";
                sqlstr = sqlstr + " Isnull(online_trading,'') as online_trading, Isnull(w_mgt,'') as w_mgt, FK_client_master_code from dbo.client_product_map";
                sqlstr = sqlstr + " where FK_client_master_code = @FK_client_master_code";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@FK_client_master_code", id);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    if (Convert.ToBoolean(sdr["equity"].ToString()) == true)
                    {
                        Product = Product + "Equity ";
                    }
                    if (Convert.ToBoolean(sdr["fo"].ToString()) == true)
                    {
                        Product = Product + "fo ";
                    }
                    if (Convert.ToBoolean(sdr["currency"].ToString()) == true)
                    {
                        Product = Product + "currency ";
                    }
                    if (Convert.ToBoolean(sdr["commodities"].ToString()) == true)
                    {
                        Product = Product + "commodities ";
                    }
                    if (Convert.ToBoolean(sdr["research_funda"].ToString()) == true)
                    {
                        Product = Product + "research_funda ";
                    }
                    if (Convert.ToBoolean(sdr["research_tech"].ToString()) == true)
                    {
                        Product = Product + "research_tech ";
                    }
                    if (Convert.ToBoolean(sdr["mf"].ToString()) == true)
                    {
                        Product = Product + "mf ";
                    }
                    if (Convert.ToBoolean(sdr["insurance"].ToString()) == true)
                    {
                        Product = Product + "insurance ";
                    }
                    if (Convert.ToBoolean(sdr["pms"].ToString()) == true)
                    {
                        Product = Product + "pms ";
                    }
                    if (Convert.ToBoolean(sdr["fd"].ToString()) == true)
                    {
                        Product = Product + "fd ";
                    }
                    if (Convert.ToBoolean(sdr["bonds"].ToString()) == true)
                    {
                        Product = Product + "bonds ";
                    }
                    if (Convert.ToBoolean(sdr["margin_fund"].ToString()) == true)
                    {
                        Product = Product + "margin_fund ";
                    }
                    if (Convert.ToBoolean(sdr["las"].ToString()) == true)
                    {
                        Product = Product + "las ";
                    }
                    if (Convert.ToBoolean(sdr["i_hold"].ToString()) == true)
                    {
                        Product = Product + "i_hold ";
                    }
                    if (Convert.ToBoolean(sdr["online_trading"].ToString()) == true)
                    {
                        Product = Product + "online_trading ";
                    }
                    if (Convert.ToBoolean(sdr["w_mgt"].ToString()) == true)
                    {
                        Product = Product + "w_mgt ";
                    }
                    if (Product == "")
                    {
                        Product = "No Product Subscribed";
                    }
                }

                gConnection.Close();

                var data = new
                {
                    client_master_code = objCliMa.client_master_code,
                    bo_long_name = objCliMa.bo_long_name,
                    bo_addr1 = objCliMa.bo_addr1,
                    bo_addr2 = objCliMa.bo_addr2,
                    bo_addr3 = objCliMa.bo_addr3,
                    bo_city = objCliMa.bo_city,
                    bo_state = objCliMa.bo_state,
                    bo_country = objCliMa.bo_country,
                    bo_zip = objCliMa.bo_zip,
                    bo_mobile = objCliMa.bo_mobile,
                    bo_email = objCliMa.bo_email,
                    bo_pan = objCliMa.bo_pan,
                    income_slab = objCliMa.income_slab,
                    net_worth_slab = objCliMa.net_worth_slab,
                    occupation = objCliMa.occupation,
                    bo_active_date = objCliMa.bo_active_date,
                    mf_id = objCliMa.mf_id,
                    aadhaar = objCliMa.aadhaar,
                    pms_internal_code = objCliMa.pms_internal_code,
                    skype_id = objCliMa.skype_id,
                    facebook_id = objCliMa.facebook_id,
                    twitter_id = objCliMa.twitter_id,
                    instagram_id = objCliMa.instagram_id,
                    google_id = objCliMa.google_id,
                    snapchat_id = objCliMa.snapchat_id,
                    linkedin_id = objCliMa.linkedin_id,
                    apple_id = objCliMa.apple_id,
                    modified_on = objCliMa.bo_modified_on,
                    modified_by = objCliMa.bo_modified_by,

                    username = objCliLog.username,
                    enable_login = objCliLog.Enable_login,
                    product = Product,
                    last_access_date = objCliLog.last_access_date
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }

            return Json(null, JsonRequestBehavior.AllowGet);
        }



        #region Indicative Margin Report Upload


        public ActionResult IndicativeMarginReportUpload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult IndicativeMarginReportUpload(HttpPostedFileBase filename1)
        {
            try
            {
                IndicativeMarginReport objIMR = new IndicativeMarginReport();
                //var filename1 = frm["filename1"].ToString();

                if (filename1 != null && filename1.ContentLength > 0)
                {

                    var allowedExtensions = new[] 
                    {  
                        ".pdf", ".doc", ".docx"  
                    };
                    objIMR.ReportName = filename1.ToString(); //getting complete url  
                    var fileName = Path.GetFileName(filename1.FileName); //getting only file name(ex-ganesh.jpg)  
                    var ext = Path.GetExtension(filename1.FileName); //getting the extension(ex-.jpg)  
                    if (allowedExtensions.Contains(ext)) //check what type of extension  
                    {
                        string name = Path.GetFileNameWithoutExtension(fileName); //getting file name without extension  
                        string myfile = objIMR.ReportName + ext; //appending the name with id  
                        // store the file inside ~/project folder(Img)  
                        var path = Path.Combine(Server.MapPath("~/IndicativeMarginReport"), fileName);

                        //obj.tbl_details.Add(tbl);
                        //obj.SaveChanges();
                        //filename1.SaveAs(path);
                        var path1 = "../../IndicativeMarginReport/" + fileName;
                        objIMR.ReportPath = path1;
                        filename1.SaveAs(path);
                        objIMR.ReportName = fileName;
                        objIMR.ReportPath = path;
                        if (IndicativeMarginReportSave(objIMR) == true)
                        {
                            @ViewBag.Message = "File Uploaded Successfully!";
                            @ViewBag.HideClass = "alert alert-success";
                        }
                    }
                    else
                    {
                        @ViewBag.Message = "Please choose only Pdf Doc file";
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return RedirectToAction("Dashboard");
            //return View();
            //return View("Dashboard");
        }


        private bool IndicativeMarginReportSave(IndicativeMarginReport objIMR)
        {
            bool result = false;
            try
            {
                gConnection.Open();
                string sqlstr = "insert into IndicativeMarginReport(ReportName,ReportPath,CreatedBy,CreatedOn)";
                sqlstr = sqlstr + " values(@ReportName,@ReportPath,@CreatedBy,GETDATE())";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@ReportName", objIMR.ReportName);
                cmd.Parameters.AddWithValue("@ReportPath", objIMR.ReportPath);
                cmd.Parameters.AddWithValue("@CreatedBy", Session["AdminUserId"].ToString());
                cmd.ExecuteNonQuery();
                gConnection.Close();
                result = true;
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return result;
        }

        #endregion



        #region Research Report Upload

        public ActionResult ResearchReportUpload()
        {
            return View();
        }


        [HttpPost]
        public ActionResult ResearchReportUpload(HttpPostedFileBase upload, FormCollection frm)
        {
            ResearchRptUpload objRRU = new ResearchRptUpload();
            try
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    objRRU.FacebookLink = frm["FBMessage"].ToString();
                    objRRU.TwitterLink = frm["TWMessage"].ToString();
                    objRRU.ReportName = frm["ReportName"].ToString();
                    objRRU.PMS_Research = frm["rating"].ToString();
                    objRRU.ReportType = frm["Selecthour"].ToString();

                    if (objRRU.ReportName == "")
                    {

                    }
                    else
                    {
                        var allowedExtensions = new[] 
                    {  
                        ".pdf", ".doc", ".docx"  
                    };
                        objRRU.OriginalRptName = upload.ToString(); //getting complete url  
                        var fileName = Path.GetFileName(upload.FileName); //getting only file name(ex-ganesh.jpg)  
                        var ext = Path.GetExtension(upload.FileName); //getting the extension(ex-.jpg)  
                        if (allowedExtensions.Contains(ext)) //check what type of extension  
                        {
                            string name = Path.GetFileNameWithoutExtension(fileName); //getting file name without extension  
                            string myfile = objRRU.ReportName + ext; //appending the name with id  
                            // store the file inside ~/project folder(Img)  
                            var path = Path.Combine(Server.MapPath("~/ResearchReport"), fileName);

                            //obj.tbl_details.Add(tbl);
                            //obj.SaveChanges();
                            //filename1.SaveAs(path);
                            var path1 = "../../ResearchReport/" + fileName;
                            objRRU.ReportPath = path1;
                            upload.SaveAs(path);
                            objRRU.OriginalRptName = fileName;
                            objRRU.ReportPath = path;
                            if (ResearchReportSave(objRRU) == true)
                            {
                                @ViewBag.Message = "File Uploaded Successfully!";
                                @ViewBag.HideClass = "alert alert-success";
                            }
                        }
                        else
                        {
                            @ViewBag.Message = "Please choose only Pdf file";
                            @ViewBag.HideClass = "alert alert-danger";

                        }
                    }
                }


            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            //return View();
            return RedirectToAction("Dashboard");
        }


        private bool ResearchReportSave(ResearchRptUpload objRRU)
        {
            bool result = false;
            try
            {
                gConnection.Open();
                string sqlstr = "Insert Into ResearchRptUpload(ReportName,ReportPath,PMS_Research,";
                sqlstr = sqlstr + " ReportType,FacebookLink,TwitterLink,CreatedBy,CreatedOn,OriginalRptName)";
                sqlstr = sqlstr + " values(@ReportName,@ReportPath,@PMS_Research,";
                sqlstr = sqlstr + " @ReportType,@FacebookLink,@TwitterLink,@CreatedBy,GetDate(),@OriginalRptName)";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@ReportName", objRRU.ReportName);
                cmd.Parameters.AddWithValue("@ReportPath", objRRU.ReportPath);
                cmd.Parameters.AddWithValue("@PMS_Research", objRRU.PMS_Research);
                cmd.Parameters.AddWithValue("@FacebookLink", objRRU.FacebookLink);
                cmd.Parameters.AddWithValue("@TwitterLink", objRRU.TwitterLink);
                cmd.Parameters.AddWithValue("@ReportType", objRRU.ReportType);
                cmd.Parameters.AddWithValue("@OriginalRptName", objRRU.OriginalRptName);
                cmd.Parameters.AddWithValue("@CreatedBy", Session["AdminUserId"].ToString());
                cmd.ExecuteNonQuery();
                gConnection.Close();
                result = true;
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return result;
        }
                

        public ActionResult ViewAllResearchReport()
        {
            List<ResearchRptUpload> objRptList = new List<ResearchRptUpload>();
            try
            {
                string sqlstr = "select AutoId, Isnull(ReportName,'') as ReportName, Isnull(PMS_Research,'') as PMS_Research, ";
                sqlstr = sqlstr + " Isnull(ReportType,'') as ReportType from dbo.ResearchRptUpload order by AutoId";
                gConnection.Open();
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    ResearchRptUpload objRpt = new ResearchRptUpload();
                    objRpt.AutoId = Convert.ToInt32(sdr["AutoId"].ToString());
                    objRpt.ReportName = sdr["ReportName"].ToString();
                    objRpt.PMS_Research = sdr["PMS_Research"].ToString();
                    objRpt.ReportType = sdr["ReportType"].ToString();
                    objRptList.Add(objRpt);
                }
                gConnection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return View(objRptList);
        }


        public ActionResult DeleteResearchReport(string id)
        {
            try
            {
                gConnection.Open();
                SqlCommand cmd = GlobalVariables.gConn.CreateCommand();
                SqlTransaction transaction;
                transaction = GlobalVariables.gConn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = GlobalVariables.gConn;
                try
                {
                    string sqlstr = "Insert Into ResearchRptUpload_Log(AutoId,ReportName,ReportPath,PMS_Research,ReportType,";
                    sqlstr = sqlstr + " FacebookLink,TwitterLink,CreatedBy,CreatedOn,OriginalRptName)";
                    sqlstr = sqlstr + " select AutoId,ReportName,ReportPath,PMS_Research,ReportType,";
                    sqlstr = sqlstr + " FacebookLink,TwitterLink,CreatedBy,GetDate(),OriginalRptName from ResearchRptUpload";
                    sqlstr = sqlstr + "  where AutoId=@AutoId";
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@AutoId", id);
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["AdminUserId"].ToString());
                    cmd.ExecuteNonQuery();


                    sqlstr = "Delete From ResearchRptUpload Where AutoId=@AutoId";
                    cmd.CommandText = sqlstr;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@AutoId", id);
                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    gConnection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    gConnection.Close();
                    Global.ErrorHandlerClass.LogError(ex);
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Error!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }
            return RedirectToAction("Dashboard");
        }



        #endregion





        #region Get Client Name

        [HttpPost]
        public JsonResult GetClientName(string Prefix)
        {
            //Note : you can bind same list from database 
            List<SelectListItem> objRowsList = new List<SelectListItem>();

            try
            {
                gConnection.Open();
                string sqlstr = "select top 10 Isnull(bo_long_name,'') as bo_long_name, client_master_Code";
                sqlstr = sqlstr + " from dbo.Client_Master where Isnull(bo_long_name,'') Like @bo_long_name";
                sqlstr = sqlstr + " or Isnull(bo_client_code,'') like @bo_client_code";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@bo_long_name", "%" + Prefix + "%");
                cmd.Parameters.AddWithValue("@bo_client_code", "%" + Prefix + "%");
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objRowsList.Add(new SelectListItem
                    {
                        Text = sdr["bo_long_name"].ToString(),
                        Value = sdr["client_master_Code"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
                @ViewBag.HideClass = "alert alert-danger";
                @ViewBag.Message = "Error!";
            }
            var Data = objRowsList;
            return Json(Data, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}