﻿using KR_Choksey.Global;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KR_Choksey.Areas.Admin.Controllers
{
    public class AdminAccountController : Controller
    {
        // GET: Admin/AdminAccount
        public ActionResult Admin_Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Admin_Login(string UserName, string Password)
        {
            //string ReturnUrl = "http://XXXXXXX.XXX/Account/User_Dashboard";
            string ReturnUrl = "http://localhost:11546/Admin/Dashboard/Dashboard";

            if(UserName == null)
            {
                UserName = "";
            }
            if(Password==null)
            {
                Password = "";
            }

            
                if (UserName != "" & Password != "")
                {
                    try
                    {
                        
                        string sqlstr = "select l.username from admin_login l ";
                        sqlstr = sqlstr + " where  ";
                        sqlstr = sqlstr + " l.username = @username and l.password=@password";
                        gConnection.Open();
                        SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                        //cmd.Parameters.AddWithValue("@bo_email", '%' + UserName + '%');
                        //cmd.Parameters.AddWithValue("@bo_email", UserName);
                        //cmd.Parameters.AddWithValue("@password", Password);

                        // Add the input parameter and set its properties.
                        SqlParameter parameter = new SqlParameter();
                        parameter.ParameterName = "@username";
                        parameter.SqlDbType = SqlDbType.VarChar;
                        parameter.Direction = ParameterDirection.Input;
                        parameter.Value = UserName;
                        // Add the parameter to the Parameters collection. 
                        cmd.Parameters.Add(parameter);


                        // Add the input parameter and set its properties.
                        SqlParameter parameter1 = new SqlParameter();
                        parameter1.ParameterName = "@password";
                        parameter1.SqlDbType = SqlDbType.VarChar;
                        parameter1.Direction = ParameterDirection.Input;
                        parameter1.Value = Password;

                        // Add the parameter to the Parameters collection. 
                        cmd.Parameters.Add(parameter1);


                        //cmd.CommandTimeout = 120;
                        SqlDataReader sdr = cmd.ExecuteReader();
                        while (sdr.Read())
                        {

                            System.Web.HttpContext.Current.Session["AdminUserId"] = UserName;

                            bool res = UpdateLastLogin(UserName);
                            return RedirectToAction("Dashboard", "Dashboard");
                            //return Redirect(ReturnUrl);
                        }
                        gConnection.Close();

                        //return RedirectResult(ReturnUrl);
                    }
                    catch (Exception ex)
                    {
                        ErrorHandlerClass.LogError(ex);
                        //return "";
                    }
                }
                else
                {
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Enter Valid Credentials";
                    //ErrorHandlerClass.LogError(ex);
                    //return "";
                }
                //return Redirect(ReturnUrl);
                //return PartialView();
                //return ReturnUrl;

                if (System.Web.HttpContext.Current.Session["AdminUserId"] == null)
                {
                    @ViewBag.HideClass = "alert alert-danger";
                    @ViewBag.Message = "Enter Valid Credentials";
                }
            return View();
            
        }


        private bool UpdateLastLogin(string id)
        {
            bool result = false;

            try
            {
                gConnection.Open();

                string sqlstr = "update admin_login set last_access_date= GETDATE() where username=@username";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@username", id);
                cmd.ExecuteNonQuery();
                gConnection.Close();
                result = true;
            }
            catch (Exception ex)
            {
                ErrorHandlerClass.LogError(ex);
            }

            return result;
        }



        public ActionResult AdminLog_Out()
        {
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            return RedirectToAction("Admin_Login", "AdminAccount");
        }

    }
}