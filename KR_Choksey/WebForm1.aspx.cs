﻿using Global;
using KR_Choksey.Global;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KR_Choksey
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public string deviceId = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";

        [CustomActionFilter]
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        
        protected void Button1_Click(object sender, EventArgs e)
        {
            string devId = Hidden1.Value;
            devId = devId.Substring(40, devId.Length - 40);

            try
            {
                gConnection.Open();
                string sqlstr = "insert into UserNotify(GoogleId,created_on)values(@GoogleId,GetDate())";
                SqlCommand cmd = new SqlCommand(sqlstr, GlobalVariables.gConn);
                cmd.Parameters.AddWithValue("@GoogleId", devId);
                cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                Global.ErrorHandlerClass.LogError(ex);
            }

            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);

        }
    }
}