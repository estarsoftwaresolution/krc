﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KR_Choksey.Startup))]
namespace KR_Choksey
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
