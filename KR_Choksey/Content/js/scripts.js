$(document).ready(function() {

    $("#first").rlAccordion();

    $("#second").rlAccordion('single', {
        childNumOptions: false
    });

    $("#thirth").rlAccordion('mix', {
        childNum: 4
    });

    $("#menu-items label").click(function(){
      // If this isn't already active
      if (!$(this).prev().hasClass("active")) {
        // Remove the class from anything that is active
        $("#menu-items .bg-piece.active").removeClass("active");
        // And make this active
        $(this).prev().addClass("active");
      }
    });

});
