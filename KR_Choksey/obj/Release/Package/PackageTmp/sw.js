'use strict';
console.log('Started', self);

self.addEventListener('install', function(event) {
    self.skipWaiting();  
  console.log('Installed', event);
});

self.addEventListener('activate', function(event) {
  console.log('Activated', event);
});

//self.addEventListener('push', function(event) {
//  console.log('Push message received', event);
//  // TODO
//});

self.addEventListener('push', function (event) {
    console.log('Push message', event);
    var url = 'https://www.krchoksey.com:83';
    var title = 'KRChoksey';

    event.waitUntil(
      self.registration.showNotification(title, {
          'body': 'Hey, There is an update! Please visit us.',
          'icon': '~/content/images/icon.png'
      }));
});